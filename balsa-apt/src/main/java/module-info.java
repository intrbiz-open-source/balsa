module com.intrbiz.balsa.apt {
    
    requires org.slf4j;
    
    requires transitive com.intrbiz.balsa.core;
    
    /* Based on JAR name */
    requires doxia.core;
    requires doxia.sink.api;
    requires doxia.logging.api;
    requires doxia.module.apt;
    
    exports com.intrbiz.balsa.apt;
    
    provides com.intrbiz.balsa.BalsaPlugin with
            com.intrbiz.balsa.apt.BalsaAPT;
    
}
