package com.intrbiz.balsa.apt;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaPlugin;
import com.intrbiz.balsa.engine.view.BalsaViewSource;
import com.intrbiz.balsa.engine.view.source.FileViewSource;

/**
 * Balsa plugin to register APT support
 */
public class BalsaAPT implements BalsaPlugin
{
    @Override
    public void setup(BalsaApplication application)
    {
        // register view loaders
        application.getViewEngine().addSource(new FileViewSource(BalsaViewSource.Formats.APT, BalsaViewSource.Charsets.UTF8, "apt"));
        application.getViewEngine().addParser(BalsaViewSource.Formats.APT, new APTBalsaViewParser());
    }
}
