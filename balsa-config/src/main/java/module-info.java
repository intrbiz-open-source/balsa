module com.intrbiz.balsa.config {
    
    requires transitive java.xml;
    requires transitive java.xml.bind;
    requires com.intrbiz.balsa.util;
    
    exports com.intrbiz.configuration;
    exports com.intrbiz.configuration.metadata;
    
}