module com.intrbiz.balsa.util.compiler {
    
    requires java.compiler;
    requires org.slf4j;
    
    exports com.intrbiz.util.compiler;
    exports com.intrbiz.util.compiler.model;
    exports com.intrbiz.util.compiler.util;
    
}