package com.intrbiz.balsa.markdown.library;

import java.io.IOException;
import java.util.List;

import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.renderer.Renderer;
import com.intrbiz.express.ExpressException;

public class MarkdownRenderer extends Renderer
{
    private static final Logger logger = LoggerFactory.getLogger(MarkdownRenderer.class);
    
    private final Parser parser;
    
    private final HtmlRenderer renderer;
    
    public MarkdownRenderer()
    {
        super();
        // create our markdown parser
        this.parser = Parser.builder()
                            .extensions(List.of(TablesExtension.create()))
                            .build();
        // create our renderer
        this.renderer = HtmlRenderer.builder()
                            .escapeHtml(true)
                            .sanitizeUrls(true)
                            .percentEncodeUrls(true)
                            .softbreak("<br/>")
                            .build();
    }
    
    public void encodeChildren(Component component, BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException
    {
        // get the content to render
        Object content = component.getAttributeValue(MarkdownComponent.VALUE, context);
        if (content != null)
        {
            try
            {
                // parse the context
                Node document = this.parser.parse(String.valueOf(content));
                // render to HTML
                this.renderer.render(document, to);
            }
            catch (Exception e)
            {
                logger.error("Failed to render Markdown content", e);
            }
        }
    }
}
