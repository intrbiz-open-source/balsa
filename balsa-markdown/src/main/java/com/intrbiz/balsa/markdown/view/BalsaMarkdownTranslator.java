package com.intrbiz.balsa.markdown.view;

import java.io.StringReader;

import org.commonmark.ext.gfm.tables.TableBlock;
import org.commonmark.ext.gfm.tables.TableCell;
import org.commonmark.ext.gfm.tables.TableHead;
import org.commonmark.ext.gfm.tables.TableRow;
import org.commonmark.node.AbstractVisitor;
import org.commonmark.node.BlockQuote;
import org.commonmark.node.BulletList;
import org.commonmark.node.Code;
import org.commonmark.node.CustomBlock;
import org.commonmark.node.CustomNode;
import org.commonmark.node.Document;
import org.commonmark.node.Emphasis;
import org.commonmark.node.FencedCodeBlock;
import org.commonmark.node.HardLineBreak;
import org.commonmark.node.Heading;
import org.commonmark.node.HtmlBlock;
import org.commonmark.node.HtmlInline;
import org.commonmark.node.Image;
import org.commonmark.node.IndentedCodeBlock;
import org.commonmark.node.Link;
import org.commonmark.node.LinkReferenceDefinition;
import org.commonmark.node.ListItem;
import org.commonmark.node.OrderedList;
import org.commonmark.node.Paragraph;
import org.commonmark.node.SoftLineBreak;
import org.commonmark.node.StrongEmphasis;
import org.commonmark.node.Text;
import org.commonmark.node.ThematicBreak;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.express.SlugFunction;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.component.View;
import com.intrbiz.balsa.view.core.generic.GenericComponent;
import com.intrbiz.balsa.view.core.generic.GenericRenderer;
import com.intrbiz.balsa.view.core.html.CodeComponent;
import com.intrbiz.balsa.view.core.html.CodeRenderer;
import com.intrbiz.balsa.view.core.html.PreComponent;
import com.intrbiz.balsa.view.core.html.PreRenderer;
import com.intrbiz.balsa.view.core.template.FragmentComponent;
import com.intrbiz.balsa.view.core.template.FragmentRenderer;
import com.intrbiz.balsa.view.parser.Parser;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.operator.literal.BooleanLiteral;
import com.intrbiz.express.operator.literal.StringLiteral;
import com.intrbiz.express.value.ValueExpression;

/**
 * Translate the Commonmark-Java AST tree into Balsa View components
 */
public class BalsaMarkdownTranslator extends AbstractVisitor
{
    private static final Logger logger = LoggerFactory.getLogger(BalsaMarkdownTranslator.class);
    
    private boolean hideTitle;
    
    private View view;
    
    private Component current;
    
    private Component title;
    
    private boolean tight = false;
    
    public BalsaMarkdownTranslator(boolean hideTitle, View view)
    {
        super();
        this.hideTitle = hideTitle;
        this.view = view;
    }
    
    protected Component fragmentComponent()
    {
        return new FragmentComponent().withName("fragment").withRenderer(new FragmentRenderer());
    }
    
    protected Component genericComponent(String name)
    {
        return new GenericComponent().withName(name).withRenderer(new GenericRenderer());
    }
    
    protected CodeComponent codeComponent()
    {
        CodeComponent code= new CodeComponent();
        code.setName("code");
        code.setRenderer(new CodeRenderer());
        return code;
    }
    
    protected PreComponent preComponent()
    {
        PreComponent component = new PreComponent();
        component.setName("pre");
        component.setRenderer(new PreRenderer());
        return component;
    }
    
    protected Component peek()
    {
        return this.current;
    }
    
    protected void push(Component component)
    {
        if (this.current != null)
            this.current.addChild(component);
        this.current = component;
    }
    
    protected void pushText(String text)
    {
        if (this.current != null)
            this.current.addText(text);
    }
    
    protected void pushAttr(String name, ValueExpression value)
    {
        if (this.current != null)
            this.current.addAttribute(name, value);
    }
    
    protected void pushAttr(String name, String value)
    {
        this.pushAttr(name, new ValueExpression(new StringLiteral(value, false)));
    }
    
    protected Component pop()
    {
        Component component = this.current;
        this.current = component.getParent();
        return component;
    }
    
    //

    @Override
    public void visit(BlockQuote blockQuote)
    {
        this.push(this.genericComponent("blockquote"));
        super.visit(blockQuote);
        this.pop();
    }

    @Override
    public void visit(BulletList bulletList)
    {
        boolean oldTight = this.tight;
        this.push(this.genericComponent("ul"));
        this.tight = bulletList.isTight();
        super.visit(bulletList);
        this.pop();
        this.tight = oldTight;
    }

    @Override
    public void visit(Code code)
    {
        this.push(this.codeComponent());
        this.pushText(code.getLiteral());
        super.visit(code);
        this.pop();
    }

    @Override
    public void visit(Document document)
    {
        // start the document
        this.push(this.fragmentComponent());
        // descend to children
        super.visit(document);
        // end the document
        this.view.setRoot(this.pop());
        // pull our key metadata
        if (this.view.getMetadata().containsAttribute("title"))
        {
            this.view.getRoot().addAttribute("title", new ValueExpression(new StringLiteral((String) this.view.getMetadata().getAttribute("title"), false)));
        }
        else if (this.title != null)
        {
            String titleText = this.getText(this.title, BalsaContext.Balsa().getExpressContext());
            this.view.getRoot().addAttribute("title", new ValueExpression(new StringLiteral(titleText, false)));
            this.view.getMetadata().setAttribute("title", titleText);
        }
    }
    
    protected String getText(Component comp, ExpressContext context)
    {
        StringBuilder sb = new StringBuilder();
        if (comp.getText() != null)
        {
            sb.append(comp.getText().get(context, null));
        }
        for (Component child : comp.getChildren())
        {
            if (child.getText() != null)
            {
                sb.append(child.getText().get(context, null));
            }
            if (child instanceof com.intrbiz.balsa.view.component.TextNode)
            {
                com.intrbiz.balsa.view.component.TextNode node = (com.intrbiz.balsa.view.component.TextNode) child;
                sb.append(node.getText().get(context, null));
            }
        }
        return sb.toString();
    }

    @Override
    public void visit(Emphasis emphasis)
    {
        this.push(this.genericComponent("em"));
        super.visit(emphasis);
        this.pop();
    }

    @Override
    public void visit(FencedCodeBlock fencedCodeBlock)
    {
        this.push(this.preComponent());
        this.push(this.codeComponent());
        if (fencedCodeBlock.getInfo() != null)
        {
            this.pushAttr("class", "language-" + fencedCodeBlock.getInfo());
            this.pushAttr("data-language", fencedCodeBlock.getInfo());
        }
        this.pushText(fencedCodeBlock.getLiteral());
        super.visit(fencedCodeBlock);
        this.pop();
        this.pop();
    }

    @Override
    public void visit(HardLineBreak hardLineBreak)
    {
        this.push(this.genericComponent("br"));
        super.visit(hardLineBreak);
        this.pop();
    }

    @Override
    public void visit(Heading heading)
    {
        // add anchor
        this.push(this.genericComponent("a"));
        this.pushAttr("name", SlugFunction.generateSlug(TextExtractor.extractText(heading)));
        this.pop();
        // start heading
        this.push(this.genericComponent("h" + heading.getLevel()));
        // capture the first heading
        if (this.title == null)
        {
            this.title = this.peek();
            // optionally hide this heading
            if (this.hideTitle)
                this.title.addAttribute(Component.RENDERED, new ValueExpression(new BooleanLiteral(false)));
        }
        // process children
        super.visit(heading);
        // end heading
        this.pop();
    }

    @Override
    public void visit(ThematicBreak thematicBreak)
    {
        this.push(this.genericComponent("hr"));
        super.visit(thematicBreak);
        this.pop();
    }

    @Override
    public void visit(HtmlInline htmlInline)
    {
        // parse the HTML
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.append("<fragment xmlns=\"com.intrbiz.balsa\">");
            sb.append(htmlInline.getLiteral());
            sb.append("</fragment>");
            Component comp = Parser.parse(BalsaContext.Balsa(), new StringReader(sb.toString()), "text/html");
            // TODO: should we only copy the root child components
            this.peek().addChild(comp);
        }
        catch (Exception e)
        {
            logger.warn("Failed to parse inline HTML:\r\n" + htmlInline.getLiteral(), e);
        }
        super.visit(htmlInline);
    }

    @Override
    public void visit(HtmlBlock htmlBlock)
    {
        // parse the HTML
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.append("<fragment xmlns=\"com.intrbiz.balsa\">");
            sb.append(htmlBlock.getLiteral());
            sb.append("</fragment>");
            Component comp = Parser.parse(BalsaContext.Balsa(), new StringReader(sb.toString()), "text/html");
            // TODO: should we only copy the root child components
            this.peek().addChild(comp);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.warn("Failed to parse inline HTML block:\r\n" + htmlBlock.getLiteral(), e);
        }
        super.visit(htmlBlock);
    }
    
    

    @Override
    public void visit(Image image)
    {
        // add the image
        Component img = this.genericComponent("img");
        // extract an image style fragment from the image
        String src = image.getDestination();
        boolean autolink = src.contains("autolink");
        int fragmentIndex = src.indexOf('#');
        if (fragmentIndex > 0)
        {
            // extract the fragment
            String fragment = src.substring(fragmentIndex + 1);
            // remove the fragment
            src = src.substring(0, fragmentIndex);
            // process the fragment
            for (String attr : fragment.split(";"))
            {
                // TODO: should we URLDecode?
                int sep = attr.indexOf('=');
                if (sep > 0)
                {
                    img.addAttribute(attr.substring(0, sep), new ValueExpression(new StringLiteral(attr.substring(sep + 1).replace('+', ' '), false)));
                }
                else
                {
                    img.addAttribute("class", new ValueExpression(new StringLiteral(attr.replace('+', ' '), false)));
                }
            }
        }
        // image source
        img.addAttribute("src", new ValueExpression(new StringLiteral(src, false)));
        // extract the ALT text
        String alt = TextExtractor.extractText(image);
        img.addAttribute("alt", new ValueExpression(new StringLiteral(alt, false)));
        img.addAttribute("title", new ValueExpression(new StringLiteral(alt, false)));
        // link in the image
        if (autolink)
        {
            this.push(this.genericComponent("a"));
            this.pushAttr("href", src);
            this.pushAttr("target", "_blank");
            this.push(img);
            this.pop();
            this.pop();
        }
        else
        {
            this.push(img);
            this.pop();
        }
    }

    @Override
    public void visit(IndentedCodeBlock indentedCodeBlock)
    {
        this.push(this.preComponent());
        this.push(this.codeComponent());
        this.pushText(indentedCodeBlock.getLiteral());
        super.visit(indentedCodeBlock);
        this.pop();
        this.pop();
    }

    @Override
    public void visit(Link link)
    {
        this.push(this.genericComponent("a"));
        this.pushAttr("href", link.getDestination());
        if (link.getTitle() != null) this.pushAttr("title", link.getTitle());
        super.visit(link);
        this.pop();
    }

    @Override
    public void visit(ListItem listItem)
    {
        this.push(this.genericComponent("li"));
        super.visit(listItem);
        this.pop();
    }

    @Override
    public void visit(OrderedList orderedList)
    {
        boolean oldTight = this.tight;
        this.push(this.genericComponent("ol"));
        this.tight = orderedList.isTight();
        super.visit(orderedList);
        this.pop();
        this.tight = oldTight;
    }

    @Override
    public void visit(Paragraph paragraph)
    {
        if (this.tight)
        {
            super.visit(paragraph);
        }
        else
        {
            this.push(this.genericComponent("p"));
            super.visit(paragraph);
            this.pop();
        }
    }

    @Override
    public void visit(SoftLineBreak softLineBreak)
    {
        this.pushText(" ");
        super.visit(softLineBreak);
    }

    @Override
    public void visit(StrongEmphasis strongEmphasis)
    {
        this.push(this.genericComponent("strong"));
        super.visit(strongEmphasis);
        this.pop();
    }

    @Override
    public void visit(Text text)
    {
        // add the text to the current node
        this.pushText(text.getLiteral());
        // descend
        super.visit(text);
    }

    @Override
    public void visit(LinkReferenceDefinition linkReferenceDefinition)
    {
        // TODO: link references not currently supported
        super.visit(linkReferenceDefinition);
    }

    @Override
    public void visit(CustomBlock customBlock)
    {
        if (customBlock instanceof TableBlock)
            this.visit((TableBlock) customBlock);
        else
            super.visit(customBlock);
    }

    @Override
    public void visit(CustomNode customNode)
    {
        if (customNode instanceof TableHead)
            this.visit((TableHead) customNode);
        else if (customNode instanceof TableRow)
            this.visit((TableRow) customNode);
        else if (customNode instanceof TableCell)
            this.visit((TableCell) customNode);
        else
            super.visit(customNode);
    }
    
    public void visit(TableBlock tableBlock)
    {
        this.push(this.genericComponent("table"));
        this.visitChildren(tableBlock);
        this.pop();
    }
    
    public void visit(TableHead tableHead)
    {
        this.visitChildren(tableHead);
    }

    public void visit(TableRow tableRow)
    {
        this.push(this.genericComponent("tr"));
        this.visitChildren(tableRow);
        this.pop();
    }

    public void visit(TableCell tableCell)
    {
        this.push(this.genericComponent(tableCell.isHeader() ? "th" : "td"));
        if (tableCell.getAlignment() != null) this.pushAttr("class", "cell-align-" + tableCell.getAlignment().toString().toLowerCase());
        this.visitChildren(tableCell);
        this.pop();
    }
}
