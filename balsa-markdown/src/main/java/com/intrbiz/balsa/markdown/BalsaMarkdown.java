package com.intrbiz.balsa.markdown;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaPlugin;
import com.intrbiz.balsa.engine.view.BalsaViewSource;
import com.intrbiz.balsa.engine.view.source.FileViewSource;
import com.intrbiz.balsa.markdown.view.MarkdownViewParser;

/**
 * Balsa plugin to register markdown support
 */
public class BalsaMarkdown implements BalsaPlugin
{   
    public static final String PROPERTY_PARSE_METADATA = "balsa.markdown.metadata";
    
    public static final String PROPERTY_PARSE_TITLE = "balsa.markdown.title";
    
    public BalsaMarkdown()
    {
        super();
    }
    
    public void setup(BalsaApplication application)
    {
        boolean parseMetadata = BalsaApplication.getBooleanProperty(PROPERTY_PARSE_METADATA, true);
        boolean hideTitle     = ! BalsaApplication.getBooleanProperty(PROPERTY_PARSE_TITLE, false);
        // add view loaders
        application.getViewEngine().addSource(new FileViewSource(BalsaViewSource.Formats.MARKDOWN, BalsaViewSource.Charsets.UTF8, "md"));
        application.getViewEngine().addParser(BalsaViewSource.Formats.MARKDOWN, new MarkdownViewParser(parseMetadata, hideTitle));
    }
}
