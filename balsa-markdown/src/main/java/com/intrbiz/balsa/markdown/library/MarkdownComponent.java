package com.intrbiz.balsa.markdown.library;

import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.express.value.ValueExpression;

public class MarkdownComponent extends Component
{
    public static final String VALUE = "value";
    
    public MarkdownComponent()
    {
        super();
    }
    
    public ValueExpression getValue()
    {
        return this.getAttribute(VALUE);
    }
    
    public void setValue(ValueExpression value)
    {
        this.addAttribute(VALUE, value);
    }
    
}
