package com.intrbiz.balsa.markdown.library;

import com.intrbiz.balsa.view.library.impl.DefaultComponentLibrary;

public class MarkdownComponentLibrary extends DefaultComponentLibrary
{
    
    public MarkdownComponentLibrary()
    {
        super("com.intrbiz.balsa.markdown", "LGPL V3", "Markdown rendering support");
        // register components
        this.addComponent("render-markdown", name -> new MarkdownComponent());
    }
}
