package com.intrbiz.balsa.markdown.express;

import com.intrbiz.express.ExpressExtensionRegistry;

public class MarkdownExtensionRegistry extends ExpressExtensionRegistry
{
    public MarkdownExtensionRegistry()
    {
        super();
        /* Functions */
        this.addFunction("render_markdown", RenderMarkdownFunction::new);
    }
}
