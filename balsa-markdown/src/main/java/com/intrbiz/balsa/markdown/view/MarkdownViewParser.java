package com.intrbiz.balsa.markdown.view;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.impl.view.metadata.ViewMetadataParser;
import com.intrbiz.balsa.engine.view.BalsaView;
import com.intrbiz.balsa.engine.view.BalsaViewParser;
import com.intrbiz.balsa.engine.view.BalsaViewSource.Resource;
import com.intrbiz.balsa.view.component.View;

public class MarkdownViewParser implements BalsaViewParser
{
    private boolean parseMetadata = true;
    
    private boolean hideTitle = true;
    
    private Parser parser;
    
    public MarkdownViewParser(boolean parseMetadata, boolean hideTitle)
    {
        super();
        this.parseMetadata = parseMetadata;
        this.hideTitle = hideTitle;
        this.parser = this.buildDefaultParser();
    }
    
    protected Parser buildDefaultParser()
    {
        return Parser.builder().extensions(Arrays.asList(TablesExtension.create())).build();
    }

    public MarkdownViewParser()
    {
        this(true, true);
    }

    public boolean isParseMetadata()
    {
        return parseMetadata;
    }

    public MarkdownViewParser setParseMetadata(boolean parseMetadata)
    {
        this.parseMetadata = parseMetadata;
        return this;
    }

    public boolean isHideTitle()
    {
        return this.hideTitle;
    }

    public MarkdownViewParser setHideTitle(boolean hideTitle)
    {
        this.hideTitle = hideTitle;
        return this;
    }
    
    public MarkdownViewParser configureParser(Consumer<Parser.Builder> configure)
    {
        Parser.Builder builder = Parser.builder();
        builder.extensions(List.of(TablesExtension.create()));
        if (configure != null) configure.accept(builder);
        this.parser = builder.build();
        return this;
    }

    @Override
    public BalsaView parse(Resource resource, BalsaContext context, String renderFormat) throws BalsaException
    {
        try
        {
            View view = this.createView(resource, context);
            // extract metadata
            String viewText = resource.slurp();
            if (this.parseMetadata) viewText = ViewMetadataParser.extractMetadata(viewText, view.getMetadata());
            // parse the markdown
            Node document = parser.parse(viewText);
            // translate to our view model
            BalsaMarkdownTranslator translator = new BalsaMarkdownTranslator(this.hideTitle, view);
            document.accept(translator);
            // return the view
            return view;
        }
        catch (IOException e)
        {
            throw new BalsaException("Failed to parse view: " + resource.getName(), e);
        }
    }

    protected View createView(Resource resource, BalsaContext context)
    {
        return new View(resource.getPath());
    }
}
