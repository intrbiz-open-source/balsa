package com.intrbiz.balsa.markdown.view;

import org.commonmark.node.AbstractVisitor;
import org.commonmark.node.Node;
import org.commonmark.node.Text;

public class TextExtractor extends AbstractVisitor
{
    private StringBuilder text = new StringBuilder();
    
    public TextExtractor()
    {
        super();
    }
    
    @Override
    public void visit(Text text)
    {
        this.text.append(text.getLiteral());
        super.visit(text);
    }

    @Override
    public String toString()
    {
        return this.text.toString();
    }
    
    public static String extractText(Node node)
    {
        TextExtractor text = new TextExtractor();
        node.accept(text);
        return text.toString();
    }
}
