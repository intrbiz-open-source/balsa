package com.intrbiz.balsa.markdown.library;

import com.intrbiz.balsa.view.library.impl.DefaultRenderLibrary;

public class MarkdownRenderLibrary extends DefaultRenderLibrary
{
    public MarkdownRenderLibrary()
    {
        super("text/html", "com.intrbiz.balsa.markdown", "LGPL V3", "Markdown rendering support");
        // register renderer
        this.addRenderer(MarkdownComponent.class, comp -> new MarkdownRenderer());
    }
}
