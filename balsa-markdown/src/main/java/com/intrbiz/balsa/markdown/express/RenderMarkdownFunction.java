package com.intrbiz.balsa.markdown.express;

import java.util.List;

import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.template.io.TemplateWriter;

public class RenderMarkdownFunction extends Function
{
    private static final Logger logger = LoggerFactory.getLogger(RenderMarkdownFunction.class);
    
    private final Parser parser;
    
    private final HtmlRenderer renderer;
    
    public RenderMarkdownFunction()
    {
        super("render_markdown");
        // create our markdown parser
        this.parser = Parser.builder()
                            .extensions(List.of(TablesExtension.create()))
                            .build();
        // create our renderer
        this.renderer = HtmlRenderer.builder()
                            .escapeHtml(true)
                            .sanitizeUrls(true)
                            .percentEncodeUrls(true)
                            .softbreak("<br/>")
                            .build();
    }

    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        TemplateWriter writer = context.getWriter();
        if (writer != null)
        {
            // get the content to render
            String content = this.getStringParameterValue(0, context, source, "");
            // parse the context
            try
            {
                Node document = this.parser.parse(content);
                // render to HTML
                this.renderer.render(document, writer.asPrefilteredWriter());
            }
            catch (Exception e)
            {
                logger.error("Failed to render Markdown content", e);
            }
        }
        return null;
    }
    
    @Override
    public boolean isIdempotent()
    {
        return false;
    }
}
