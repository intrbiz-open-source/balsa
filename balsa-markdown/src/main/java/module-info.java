module com.intrbiz.balsa.markdown {
    
    requires org.slf4j;
    
    requires org.commonmark;
    requires org.commonmark.ext.gfm.tables;
    
    requires transitive com.intrbiz.balsa.core;
    
    exports com.intrbiz.balsa.markdown;
    
    provides com.intrbiz.balsa.BalsaPlugin with
            com.intrbiz.balsa.markdown.BalsaMarkdown;
}
