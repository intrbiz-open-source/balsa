package com.intrbiz.data.db.compiler.metadata;

public enum LockMode {
    FOR_UPDATE,
    FOR_NO_KEY_UPDATE,
    FOR_SHARE,
    FOR_KEY_SHARE,
    NONE
}
