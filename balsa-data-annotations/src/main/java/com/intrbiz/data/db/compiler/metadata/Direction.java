package com.intrbiz.data.db.compiler.metadata;

public enum Direction {
    ASC,
    DESC
}
