package com.intrbiz.data.db.compiler.metadata;

public enum PartitionMode {
    NONE,
    RANGE,
    HASH,
    LIST
}
