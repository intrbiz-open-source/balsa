package com.intrbiz.data.db.compiler.metadata;

public enum Nulls {
    LAST,
    FIRST
}
