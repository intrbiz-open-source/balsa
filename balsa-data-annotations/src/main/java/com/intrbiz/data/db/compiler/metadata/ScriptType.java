package com.intrbiz.data.db.compiler.metadata;

public enum ScriptType {
    INSTALL,
    UPGRADE,
    BOTH,
    INSTALL_LAST,
    UPGRADE_LAST,
    BOTH_LAST,
}
