package com.intrbiz.data.db.compiler.metadata;

public enum Deferable {
    DEFERRABLE,
    NOT_DEFERRABLE,
    INITIALLY_IMMEDIATE,
    INITIALLY_DEFERRED
}
