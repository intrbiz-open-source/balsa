package com.intrbiz.balsa.hazelcast.engine.session;

import java.security.Principal;
import java.util.Map;

import com.hazelcast.map.IMap;
import com.intrbiz.balsa.engine.security.AuthenticationResponse;
import com.intrbiz.balsa.engine.security.AuthenticationState;
import com.intrbiz.balsa.engine.security.challenge.AuthenticationChallenge;
import com.intrbiz.balsa.engine.security.info.AuthenticationInfo;
import com.intrbiz.balsa.engine.session.BalsaSession;

/**
 * Proxy all attribute lookups directly into the session attribute distributed map
 */
public final class HazelcastSession implements BalsaSession, AuthenticationState
{
    private final String sessionId;
    
    private final IMap<String, Object> attributes;

    public HazelcastSession(String id, IMap<String, Object> attributes)
    {
        super();
        this.sessionId = id;
        this.attributes = attributes;
    }

    @Override
    public String id()
    {
        return this.sessionId;
    }

    @Override
    public AuthenticationState authenticationState()
    {
        return this;
    }

    @Override
    public long authenticationStartedAt()
    {
        Long startedAt = (Long) this.attributes.get(this.authenticationStartedAtId());
        return startedAt == null ? -1L : startedAt.longValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Principal> T authenticatingPrincipal()
    {
        return (T) this.attributes.get(this.authenticatingPrincipalId());
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, AuthenticationChallenge> challenges()
    {
        return (Map<String, AuthenticationChallenge>) this.attributes.get(this.authenticationChallengesId());
    }

    @Override
    public AuthenticationInfo info()
    {
        return (AuthenticationInfo) this.attributes.get(this.authenticationInfoId());
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Principal> T currentPrincipal()
    {
        return (T) this.attributes.get(this.principalId());
    }

    @Override
    public AuthenticationResponse update(AuthenticationResponse response)
    {
        if (response == null) throw new IllegalArgumentException("Response cannot be null");
        IMap<String, Object> attrs = this.attributes;
        if (response.isComplete())
        {
            attrs.remove(this.authenticationChallengesId());
            attrs.remove(this.authenticatingPrincipalId());
            attrs.remove(this.authenticationStartedAtId());
            attrs.put(this.authenticationInfoId(), HazelcastAuthenticationInfo.wrap(response.getInfo()));
            attrs.put(this.principalId(), response.getPrincipal());
        }
        else
        {
            attrs.remove(this.principalId());
            attrs.put(this.authenticationInfoId(), HazelcastAuthenticationInfo.wrap(response.getInfo()));
            attrs.put(this.authenticationChallengesId(), response.getChallenges());
            attrs.put(this.authenticatingPrincipalId(), response.getPrincipal());
            attrs.put(this.authenticationStartedAtId(), Long.valueOf(System.currentTimeMillis()));
        }
        return response;
    }

    @Override
    public AuthenticationState reset()
    {
        IMap<String, Object> attrs = this.attributes;
        attrs.remove(this.principalId());
        attrs.remove(this.authenticatingPrincipalId());
        attrs.remove(this.authenticationStartedAtId());
        attrs.remove(this.authenticationInfoId());
        attrs.remove(this.authenticationChallengesId());
        return this;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getVar(String name)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        return (T) this.attributes.get(this.varId(name));
    }

    @Override
    public <T> T putVar(String name, T object)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        if (object == null)
        {
            this.attributes.remove(this.varId(name));
        }
        else
        {
            this.attributes.put(this.varId(name), object);
        }
        return object;
    }

    @Override
    public void removeVar(String name)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        this.attributes.remove(this.varId(name));
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getModel(String name)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        return (T) this.attributes.get(this.modelId(name));
    }

    @Override
    public <T> T putModel(String name, T model)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        if (model == null)
        {
            this.attributes.remove(this.modelId(name));
        }
        else
        {
            this.attributes.put(this.modelId(name), model);
        }
        return model;
    }

    @Override
    public void removeModel(String name)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        this.attributes.remove(this.modelId(name));
    }

    @Override
    public void deactivate()
    {
    }

    // util

    protected String varId(String name)
    {
        return this.id() + ".var." + name;
    }

    protected String modelId(String name)
    {
        return this.id() + ".model." + name;
    }
    
    protected String principalId()
    {
        return this.id() + ".principal";
    }
    
    protected String authenticatingPrincipalId()
    {
        return this.id() + ".authentication.principal";
    }
    
    protected String authenticationStartedAtId()
    {
        return this.id() + ".authentication.started.at";
    }
    
    protected String authenticationInfoId()
    {
        return this.id() + ".authentication.info";
    }
    
    protected String authenticationChallengesId()
    {
        return this.id() + ".authentication.challenges";
    }
}
