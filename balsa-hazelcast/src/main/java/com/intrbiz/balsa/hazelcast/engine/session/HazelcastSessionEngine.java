package com.intrbiz.balsa.hazelcast.engine.session;

import java.security.Principal;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionConfig;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.InMemoryFormat;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizePolicy;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.impl.session.AbstractSessionEngine;
import com.intrbiz.balsa.hazelcast.util.DefaultHazelcastFactory;

public class HazelcastSessionEngine extends AbstractSessionEngine
{    
    public static final String BALSA_SESSION_MAP_NAME = "balsa.session";
    
    private final Function<String, HazelcastInstance> hazelcastInstanceSupplier;
    
    private HazelcastInstance hazelcastInstance;

    private IMap<String, Object> sessionAttributeMap;    

    public HazelcastSessionEngine(Function<String, HazelcastInstance> hazelcastInstanceSupplier)
    {
        super();
        this.hazelcastInstanceSupplier = hazelcastInstanceSupplier;
    }
    
    public HazelcastSessionEngine(HazelcastInstance hazelcastInstance)
    {
        this((instanceName) -> hazelcastInstance);
    }
    
    public HazelcastSessionEngine()
    {
        this(new DefaultHazelcastFactory());
    }

    @Override
    public String getEngineName()
    {
        return "Hazelcast-Balsa-Session-Engine";
    }
    
    private void applyHazelcastConfiguration(Config hazelcastConfig)
    {
        // inject our configuration
        // setup the session map, this actually just store session attributes and the HazelcastSession is simply a proxy to it
        MapConfig sessionMapConfig = hazelcastConfig.getMapConfig(BALSA_SESSION_MAP_NAME);
        // configure eviction of session attributes after not being accessed for the session
        sessionMapConfig.setMaxIdleSeconds((int) TimeUnit.MINUTES.toSeconds(this.getSessionLifetime()));
        sessionMapConfig.setEvictionConfig(
            new EvictionConfig()
                .setEvictionPolicy(EvictionPolicy.LRU)
                .setSize(10)
                .setMaxSizePolicy(MaxSizePolicy.USED_HEAP_PERCENTAGE)
        );
        // default to storing objects, as with sticky balancing requests tend to the same server
        sessionMapConfig.setInMemoryFormat(InMemoryFormat.OBJECT);
        hazelcastConfig.addMapConfig(sessionMapConfig);
    }

    @Override
    public void start() throws BalsaException
    {
        super.start();
        try
        {
            // Get our hazelcast instance
            this.hazelcastInstance = this.hazelcastInstanceSupplier.apply(this.application.getInstanceName());
            // Inject our HZ config
            this.applyHazelcastConfiguration(this.hazelcastInstance.getConfig());
            // create the session attribute map
            this.sessionAttributeMap   = this.hazelcastInstance.getMap(BALSA_SESSION_MAP_NAME);
        }
        catch (Exception e)
        {
            throw new BalsaException("Failed to start Hazelcast Session Engine", e);
        }
    }
    
    @Override
    public HazelcastSession getSession(String sessionId)
    {
        // create the session proxy
        return new HazelcastSession(sessionId, this.sessionAttributeMap);
    }
    
    @Override
    public <T extends Principal> void revokeAllSessionsOfPrincipal(T principal)
    {
        // TODO: we need to run a distributed delete
    }

    @Override
    public void stop()
    {
        super.stop();
    }
}
