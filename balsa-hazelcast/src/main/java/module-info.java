module com.intrbiz.balsa.hazelcast {
    
    requires com.intrbiz.balsa.core;
    
    requires transitive com.hazelcast.core;
    
    exports com.intrbiz.balsa.hazelcast.engine.session;
    exports com.intrbiz.balsa.hazelcast.util;
    
}
