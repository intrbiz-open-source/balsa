package com.intrbiz.validation.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.intrbiz.validation.converter.Converter;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface UseConverter {
    Class<? extends Converter<?>> value();
}
