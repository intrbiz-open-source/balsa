package com.intrbiz.validation.validator.validators;

import java.lang.annotation.Annotation;

import com.intrbiz.validation.metadata.IsaBoolean;
import com.intrbiz.validation.validator.Validator;

public class BooleanValidator extends Validator<Boolean>
{
    public BooleanValidator()
    {
        super(Boolean.class);
    }

    @Override
    public void configure(Annotation data, Annotation[] additional)
    {
        if (data instanceof IsaBoolean)
        {
            IsaBoolean iv = (IsaBoolean) data;
            this.setMandatory(iv.mandatory());
            this.setCoalesce(iv.coalesce());
            this.setDefaultValue(iv.defaultValue());
        }
    }
}
