package com.intrbiz.validation.validator.validators;

import java.lang.annotation.Annotation;
import java.util.Date;

import com.intrbiz.validation.metadata.IsaDate;
import com.intrbiz.validation.validator.Validator;

public class DateValidator extends Validator<Date>
{   
    public DateValidator()
    {
        super(Date.class);
    }    
    
    @Override
    public void configure(Annotation data, Annotation[] additional)
    {
        if (data instanceof IsaDate)
        {
            IsaDate v = (IsaDate) data;
            this.setMandatory(v.mandatory());
        }
    }
}
