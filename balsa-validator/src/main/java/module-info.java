module com.intrbiz.balsa.validator {
    
    requires com.intrbiz.balsa.util;
    
    exports com.intrbiz.validation.metadata;
    exports com.intrbiz.validation.validator;
    exports com.intrbiz.validation.converter;
    
    /* export the specific validators and converters too */
    exports com.intrbiz.validation.validator.validators;
    exports com.intrbiz.validation.converter.converters;
}