#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import com.intrbiz.balsa.BalsaApplication;

/**
 * Your Balsa web app
 */
public class App extends BalsaApplication
{
    public App()
    {
        super();
    }
    
    @Override
    protected void setupEngines() throws Exception
    {
        // Setup any application services and customised runtime engines
        // For example: Database pools, security engine
    }
    
    @Override
    protected void setupRouters() throws Exception
    {
        // Setup the application routers
        router(new AppRouter());
    }
    
    @Override
    protected void setupModels() throws Exception
    {
        // Register any form model object factories
    }
    
    @Override
    protected void setupFunctions() throws Exception
    {
        // Register any custom Express functions
    }
    
    public static void main(String[] args) throws Exception
    {
        try
        {
            App app = new App();
            app.start();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
