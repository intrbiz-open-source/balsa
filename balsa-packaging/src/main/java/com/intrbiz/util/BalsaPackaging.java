package com.intrbiz.util;

/**
 * Package an application
 * 
 * @goal balsa-package
 * @phase package
 * @requiresProject
 * @requiresDependencyResolution runtime
 */
public class BalsaPackaging extends IntrbizPackaging
{

    public BalsaPackaging()
    {
        super();
    }

}
