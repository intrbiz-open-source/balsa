package com.intrbiz.express.tests;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.value.ValueExpression;

public class TestDecorators
{
    private ExpressContext context;
    
    @Before
    public void setupContext()
    {
        this.context = ExpressContext.create();
        this.context.exportEntity("instant", Instant.ofEpochSecond(1668651745), null);
        this.context.exportEntity("localdatetime", Instant.ofEpochSecond(1668651745).atOffset(ZoneOffset.UTC).toLocalDateTime(), null);
        this.context.withCurrentTimeZone(ZoneId.of("UTC"));
        this.context.withCurrentLocale(Locale.ENGLISH);
    }
    
    @Test
    public void testDoubleNumberFormat()
    {
        ValueExpression ve = new ValueExpression(this.context, "#{(4.2).format('#.00')}");
        Object o = ve.get(this.context, null);
        assertThat(o, is(instanceOf(String.class)));
        assertThat((String) o, is(equalTo("4.20")));
    }
    
    @Test
    public void testFloatNumberFormat()
    {
        ValueExpression ve = new ValueExpression(this.context, "#{(4.2F).format('#.00')}");
        Object o = ve.get(this.context, null);
        assertThat(o, is(instanceOf(String.class)));
        assertThat((String) o, is(equalTo("4.20")));
    }
    
    @Test
    public void testIntNumberFormat()
    {
        ValueExpression ve = new ValueExpression(this.context, "#{(4).format('#.00')}");
        Object o = ve.get(this.context, null);
        assertThat(o, is(instanceOf(String.class)));
        assertThat((String) o, is(equalTo("4.00")));
    }
    
    @Test
    public void testLongNumberFormat()
    {
        ValueExpression ve = new ValueExpression(this.context, "#{(4L).format('#.00')}");
        Object o = ve.get(this.context, null);
        assertThat(o, is(instanceOf(String.class)));
        assertThat((String) o, is(equalTo("4.00")));
    }
    
    @Test
    public void testChunkList()
    {
        ValueExpression ve = new ValueExpression(this.context, "#{[1, 2, 3, 4, 5].chunk(2)}");
        Object o = ve.get(this.context, null);
        assertThat(o, is(instanceOf(List.class)));
        assertThat((List<?>) o, is(equalTo(Arrays.asList( Arrays.asList(1, 2), Arrays.asList(3, 4), Arrays.asList(5) ))));
    }
    
    @Test
    public void testFormatLocalDateTimeDefault()
    {
        ValueExpression ve = new ValueExpression(this.context, "#{localdatetime.dateformat()}");
        Object o = ve.get(this.context, null);
        assertThat(o, is(instanceOf(String.class)));
        assertThat((String) o, is(equalTo("2022-11-17")));
    }
    
    @Test
    public void testFormatLocalDateTimePattern()
    {
        ValueExpression ve = new ValueExpression(this.context, "#{localdatetime.dateformat('dd-MM-yyyy')}");
        Object o = ve.get(this.context, null);
        assertThat(o, is(instanceOf(String.class)));
        assertThat((String) o, is(equalTo("17-11-2022")));
    }
    
    @Test
    public void testFormatInstantDefault()
    {
        ValueExpression ve = new ValueExpression(this.context, "#{instant.dateformat()}");
        Object o = ve.get(this.context, null);
        assertThat(o, is(instanceOf(String.class)));
        assertThat((String) o, is(equalTo("2022-11-17Z")));
    }
    
    @Test
    public void testFormatInstantPattern()
    {
        ValueExpression ve = new ValueExpression(this.context, "#{instant.dateformat('dd-MM-yyyy')}");
        Object o = ve.get(this.context, null);
        assertThat(o, is(instanceOf(String.class)));
        assertThat((String) o, is(equalTo("17-11-2022")));
    }
}
