package com.intrbiz.express.script;

import static org.junit.Assert.*;

import org.junit.Test;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;

public class TestScriptSecurity
{

    @Test
    public void testPreventExit()
    {
        System.class.getMethods()[0].setAccessible(true);
        //
        ExpressScriptEngineFactory factory = new RestrictedExpressScriptEngineFactory();
        try
        {
            ExpressScriptEngine script = factory.parseUnwrapped(
                "java('java.lang.System').exit(1);"
            );
            script.execute(ExpressContext.create(), null);
            fail("Exit didn't happen, yet a AccessControlException was not raised");
        }
        catch (Exception e)
        {
            assertTrue("Caught access exception when trying to call exit()", true);
        }
    }
    
    
    @Test
    public void testIterationLimit()
    {
        ExpressScriptEngineFactory factory = new RestrictedExpressScriptEngineFactory();
        try
        {
            ExpressScriptEngine script = factory.parseUnwrapped(
                "for (i = 0; i < 100000; i++)" +
                "{" +
                "}"
            );
            //
            ExpressContext context = script.createContext();
            //
            script.execute(context, null);
            fail("Script was not aborted due to iteration limit");
        }
        catch (Exception e)
        {
            assertTrue("Script was aborted due to iteration limit", e instanceof ExpressException);
        }
    }
    
    @Test
    public void testOpLimit()
    {
        ExpressScriptEngineFactory factory = new RestrictedExpressScriptEngineFactory();
        try
        {
            ExpressScriptEngine script = factory.parseUnwrapped(
                "function test()" +
                "{" +
                "  test();" +
                "}"
            );
            //
            ExpressContext context = script.createContext();
            //
            script.execute(context, null);
            fail("Script was not aborted due to operation limit");
        }
        catch (Exception e)
        {
            assertTrue("Script was aborted due to operation limit", e instanceof ExpressException);
        }
    }
}
