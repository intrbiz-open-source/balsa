package com.intrbiz.express;

import com.intrbiz.express.operator.Decorator;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.security.Hidden;

/**
 * A library of express extensions
 */
public interface ExpressExtensionLibrary extends Hidden
{    
    boolean containsFunction(String name);
    
    Function loadFunction(String name);
    
    boolean containsDecorator(String name, Class<?> entityType);
    
    Decorator loadDecorator(String name, Class<?> entityType);
    
    default String toString(String indent)
    {
        return this.toString();
    }
}
