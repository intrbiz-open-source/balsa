package com.intrbiz.express.functions.text;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.operator.Operator;

public class URLDecodeFunction extends Function
{
	public URLDecodeFunction()
	{
		super("urldecode");
	}

	@Override
	public Object get(ExpressContext context, Object source) throws ExpressException
	{
	    context.checkOp();
		Operator op = this.getParameters().get(0);
		Object val = op.get(context, source);
		if (val != null)
		{
			String str = String.valueOf(val);
			return URLDecoder.decode(str, StandardCharsets.UTF_8);
		}
		return null;
	}

	@Override
	public void set(ExpressContext context, Object value, Object source) throws ExpressException
	{
	}

	@Override
    public boolean isIdempotent()
    {
        return true;
    }
}
