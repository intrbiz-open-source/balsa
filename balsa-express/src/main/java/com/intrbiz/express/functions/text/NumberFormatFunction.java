package com.intrbiz.express.functions.text;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.operator.Operator;

public class NumberFormatFunction extends Function
{
    private static Logger logger = LoggerFactory.getLogger(NumberFormatFunction.class);

    public NumberFormatFunction()
    {
        super("decimalformat");
    }

    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        context.checkOp();
        // pattern
        Operator arg1 = this.getParameters().get(0);
        // value
        Operator arg2 = this.getParameters().get(1);
        // format
        try
        {
            DecimalFormat df = new DecimalFormat((String) arg1.get(context, source));
            Object val = arg2.get(context, source);
            if (val instanceof Number || val instanceof AtomicLong || val instanceof AtomicInteger || val instanceof BigInteger || val instanceof BigDecimal)
            {
                return df.format(val);
            }
        }
        catch (Exception e)
        {
            logger.warn("Failed to format decimal", e);
        }
        return "";
    }

    @Override
    public boolean isIdempotent()
    {
        return true;
    }
}
