package com.intrbiz.express.functions.text;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Decorator;

public class DateFormatDecorator extends Decorator
{
    private transient DateTimeFormatter formatCache;
    
    public DateFormatDecorator()
    {
        super("format");
    }
    
    protected DateTimeFormatter getDateTimeFormat(ExpressContext context, Object source)
    {
        DateTimeFormatter format = this.formatCache;
        if (format == null)
        {
            String pattern = this.getStringParameterValue(0, context, source, null);
            format = (pattern == null) ? DateTimeFormatter.ISO_DATE : DateTimeFormatter.ofPattern(pattern);
            if (this.isParamterConstant(0)) this.formatCache = format;
        }
        return format
                .withZone(context.getCurrentTimeZone())
                .withLocale(context.getCurrentLocale());
    }
    
    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        context.checkOp();
        Object val = this.getEntity().get(context, source);
        DateTimeFormatter format = this.getDateTimeFormat(context, source);
        if (val instanceof TemporalAccessor)
        {
            return format.format((TemporalAccessor) val);
        }
        else if (val instanceof Date)
        {
            return format.format(Instant.ofEpochMilli(((Date) val).getTime()));
        }
        else if (val instanceof Calendar)
        {
            return format.format(Instant.ofEpochMilli(((Calendar) val).getTimeInMillis()));
        }
        else if (val instanceof Long)
        {
            return format.format(Instant.ofEpochMilli(((Long) val)));
        }
        return "";
    }

    @Override
    public boolean isIdempotent()
    {
        return false;
    }
}
