package com.intrbiz.express.functions.collection;

import java.util.ArrayList;
import java.util.List;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Decorator;

public class ChunkListDecorator extends Decorator
{
    public ChunkListDecorator()
    {
        super("chunk");
    }

    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        context.checkOp();
        // get the value to process
        List<?> value = (List<?>) this.getEntity().get(context, source);
        // get the chunk size
        int size = this.getIntParameterValue(0, context, source, 3);
        // chunk the list
        List<List<Object>> ret = new ArrayList<>(value.size() / size);
        List<Object> chunk = new ArrayList<>(size);
        for (Object e : value)
        {
            chunk.add(e);
            // is the chunk full?
            if (chunk.size() == size)
            {
                ret.add(chunk);
                chunk = new ArrayList<>(size);
            }
        }
        if (! chunk.isEmpty()) ret.add(chunk);
        return ret;
    }
    
    @Override
    public boolean isIdempotent()
    {
        return true;
    }
}
