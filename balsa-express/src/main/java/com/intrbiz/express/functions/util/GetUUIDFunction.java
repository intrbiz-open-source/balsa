package com.intrbiz.express.functions.util;

import java.util.UUID;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;

public class GetUUIDFunction extends Function
{
    public GetUUIDFunction()
    {
        super("getuuid");
    }

    @Override
    public Object get(ExpressContext context,Object source) throws ExpressException
    {
        context.checkOp();
        
    	return UUID.randomUUID();
    }

    @Override
    public void set(ExpressContext context, Object value,Object source) throws ExpressException
    {
    }
    
    @Override
    public boolean isIdempotent()
    {
        return false;
    }
}
