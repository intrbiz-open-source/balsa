package com.intrbiz.express.functions.text;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.operator.Operator;

public class URLEncodeFunction extends Function
{
	public URLEncodeFunction()
	{
		super("urlencode");
	}

	@Override
	public Object get(ExpressContext context, Object source) throws ExpressException
	{
	    context.checkOp();
		Operator op = this.getParameters().get(0);
		Object val = op.get(context, source);
		if (val != null)
		{
			String str = String.valueOf(val);
			return URLEncoder.encode(str, StandardCharsets.UTF_8);
		}
		return null;
	}

	@Override
	public void set(ExpressContext context, Object value, Object source) throws ExpressException
	{
	}

	@Override
    public boolean isIdempotent()
    {
        return true;
    }
}
