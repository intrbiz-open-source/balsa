package com.intrbiz.express.functions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.intrbiz.express.ExpressExtensionRegistry;
import com.intrbiz.express.functions.collection.ChunkListDecorator;
import com.intrbiz.express.functions.collection.JoinFunction;
import com.intrbiz.express.functions.text.DateFormatDecorator;
import com.intrbiz.express.functions.text.DateFormatFunction;
import com.intrbiz.express.functions.text.LcFunction;
import com.intrbiz.express.functions.text.NumberFormatDecorator;
import com.intrbiz.express.functions.text.NumberFormatFunction;
import com.intrbiz.express.functions.text.URLDecodeFunction;
import com.intrbiz.express.functions.text.URLEncodeFunction;
import com.intrbiz.express.functions.text.UcFunction;
import com.intrbiz.express.functions.text.UcfirstFunction;
import com.intrbiz.express.functions.util.GetUUIDFunction;

public class ExpressBuiltinFunctionRegistry extends ExpressExtensionRegistry
{
    public ExpressBuiltinFunctionRegistry()
    {
        super();
        // text case utils
        this.addFunction("uc", UcFunction.class);
        this.addFunction("lc", LcFunction.class);
        this.addFunction("ucfirst", UcfirstFunction.class);
        // text encoding
        this.addFunction("urlencode", URLEncodeFunction.class);
        this.addFunction("urldecode", URLDecodeFunction.class);
        // format a number
        this.addFunction("decimalformat", NumberFormatFunction.class);
        this.addDecorator("format", Number.class, NumberFormatDecorator.class);
        this.addDecorator("format", AtomicLong.class, NumberFormatDecorator.class);
        this.addDecorator("format", AtomicInteger.class, NumberFormatDecorator.class);
        this.addDecorator("format", BigInteger.class, NumberFormatDecorator.class);
        this.addDecorator("format", BigDecimal.class, NumberFormatDecorator.class);
        // format a date
        this.addFunction("dateformat", DateFormatFunction.class);
        this.addDecorator("dateformat", TemporalAccessor.class, DateFormatDecorator.class);
        this.addDecorator("dateformat", Date.class, DateFormatDecorator.class);
        this.addDecorator("dateformat", Calendar.class, DateFormatDecorator.class);
        this.addDecorator("dateformat", Long.class, DateFormatDecorator.class);
        // join a collection
        this.addFunction("join", JoinFunction.class);
        // split a list into a set of chunks
        this.addDecorator("chunk", List.class, ChunkListDecorator.class);
        // util
        this.addFunction("getuuid", GetUUIDFunction.class);
    }
}
