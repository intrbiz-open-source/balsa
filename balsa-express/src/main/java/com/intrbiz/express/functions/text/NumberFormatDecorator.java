package com.intrbiz.express.functions.text;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Decorator;

public class NumberFormatDecorator extends Decorator
{
    private transient DecimalFormat formatCache;
    
    public NumberFormatDecorator()
    {
        super("format");
    }
    
    protected DecimalFormat getDecimalFormat(ExpressContext context, Object source)
    {
        // use the cache if we can
        if (this.formatCache != null) return this.formatCache;
        // build the format
        DecimalFormat format = new DecimalFormat(this.getStringParameterValue(0, context, source, "#.00"));
        if (this.isParamterConstant(0)) this.formatCache = format;
        return format;
    }
    
    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        context.checkOp();
        Object val = this.getEntity().get(context, source);
        if (val instanceof Number || val instanceof AtomicLong || val instanceof AtomicInteger || val instanceof BigInteger || val instanceof BigDecimal)
            return this.getDecimalFormat(context, source).format(val);
        return "";
    }

    @Override
    public boolean isIdempotent()
    {
        return true;
    }
}
