package com.intrbiz.express.functions.collection;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;

public class JoinFunction extends Function
{
    public JoinFunction()
    {
        super("join");
    }

    @Override
    public boolean isIdempotent()
    {
        return true;
    }

    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        context.checkOp();
        String separator = (String) this.getParameter(0).get(context, source);
        Object value = this.getParameter(1).get(context, source);
        if (value instanceof Collection)
        {
            return ((Collection<?>) value).stream()
                    .filter(Objects::nonNull)
                    .map(String::valueOf)
                    .collect(Collectors.joining(separator));
        }
        else if (value instanceof Object[])
        {
            return Arrays.stream((Object[]) value)
                    .filter(Objects::nonNull)
                    .map(String::valueOf)
                    .collect(Collectors.joining(separator));
        }
        else if (value instanceof Iterable)
        {
            StreamSupport.stream(((Iterable<?>) value).spliterator(), false)
                .filter(Objects::nonNull)
                .map(String::valueOf)
                .collect(Collectors.joining(separator));
        }
        return "";
    }
}
