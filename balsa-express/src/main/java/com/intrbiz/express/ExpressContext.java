package com.intrbiz.express;

import java.io.Writer;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import com.intrbiz.express.action.ActionHandler;
import com.intrbiz.express.operator.Decorator;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.security.Hidden;
import com.intrbiz.express.stack.ELStatementFrame;
import com.intrbiz.express.template.filter.ContentFilter;
import com.intrbiz.express.template.filter.ContentFilterRegistry;
import com.intrbiz.express.template.io.TemplateWriter;
import com.intrbiz.express.template.loader.TemplateLoader;
import com.intrbiz.express.value.ValueExpression;

/**
 * The execution context used when parsing and evaluating expressions
 */
public interface ExpressContext extends Hidden
{
    // customisable stuff
    
    Function getCustomFunction(String name);
    
    Decorator getCustomDecorator(String name, Class<?> entityType);
    
    ExpressExtensionRegistry getExpressExtensionRegistry();
    
    // entities

    Object getEntity(String name, Object source);

    void setEntity(String name, Object value, Object source);
    
    void exportEntity(String name, Object value, Object source);
    
    void declareEntity(String name, Object value, Object source);
    
    // actions
    
    ActionHandler getAction(String name, Object source);

    // Stack

    void enterFrame(boolean function, boolean lookupParent);
    
    default void enterFunctionFrame()
    {
        this.enterFrame(true, false);
    }
    
    default void enterInlineFunctionFrame()
    {
        this.enterFrame(true, true);
    }
    
    default void enterFrame()
    {
        this.enterFrame(false, true);
    }
    
    default void enterFrame(Map<String, ValueExpression> arguments, Object source)
    {
        // enter a normal frame
        this.enterFrame(false, true);
        // declare the bindings
        for (Entry<String, ValueExpression> binding : arguments.entrySet())
        {
            this.declareEntity(binding.getKey(), binding.getValue(), source);
        }
    }
    
    default void enterCachingFrame(Map<String, ValueExpression> arguments, Object source)
    {
        // enter frame
        this.enterFrame(false, true);
        // ensure that we pre-eval expressions
        for (Entry<String, ValueExpression> binding : arguments.entrySet())
        {
            this.declareEntity(binding.getKey(), binding.getValue().get(this, source), source);
        }
    }
    
    default void enterFunctionFrame(Map<String, ValueExpression> arguments, Object source)
    {
        // ensure that we pre-eval expressions
        Map<String, Object> bindings = new HashMap<>();
        for (Entry<String, ValueExpression> binding : arguments.entrySet())
        {
            bindings.put(binding.getKey(), binding.getValue().get(this, source));
        }
        // enter function frame
        this.enterFrame(true, false);
        // declare the bindings
        for (Entry<String, Object> binding : bindings.entrySet())
        {
            this.declareEntity(binding.getKey(), binding.getValue(), source);
        }
    }

    void exitFrame();

    ELStatementFrame getFrame();
    
    // security
    
    void checkIteration();
    
    void checkOp();
    
    boolean checkJavaAccess(String className);
    
    boolean allowSetAccessible();
    
    boolean suppressMethodExceptions();
    
    // config
    
    boolean isCaching();
    
    void setCaching(boolean caching);
    
    // template stuff
    
    ContentFilterRegistry getContentFilterRegistry();
    
    TemplateLoader getTemplateLoader();
    
    TemplateWriter getWriter();
    
    void setupWriter(Writer to, ContentFilter defaultFilter);
    
    void clearWriter();
    
    // contextual stuff
    
    Locale getCurrentLocale();
    
    ExpressContext withCurrentLocale(Locale locale);
    
    ZoneId getCurrentTimeZone();
    
    ExpressContext withCurrentTimeZone(ZoneId zone);
    
    // factory methods
    
    /**
     * Get an Express context builder to easily create configured contexts
     */
    public static ExpressContextBuilder builder()
    {
        return new ExpressContextBuilder();
    }
    
    /**
     * Create a default Express context
     */
    public static ExpressContext create()
    {
        return builder().build();
    }
    
    /**
     * Create a default Express context
     */
    public static ExpressContext create(ExpressEntityResolver resolver)
    {
        return builder().build(resolver);
    }
}
