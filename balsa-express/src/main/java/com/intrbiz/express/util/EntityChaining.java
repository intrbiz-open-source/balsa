package com.intrbiz.express.util;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.dynamic.EntityProxy;
import com.intrbiz.express.operator.Operator;
import com.intrbiz.express.value.ValueExpression;

public class EntityChaining
{
    public static Object resolve(ExpressContext context, Object source, Object toReolve) throws ExpressException
    {
        if (toReolve instanceof ValueExpression)
        {
            return ((ValueExpression) toReolve).get(context, source);
        }
        else if (toReolve instanceof Operator)
        {
            return ((Operator) toReolve).get(context, source);
        }
        else if (toReolve instanceof EntityProxy)
        {
            return ((EntityProxy) toReolve).getEntity(context, source);
        }
        return toReolve;
    }
}
