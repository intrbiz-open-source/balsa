package com.intrbiz.express.util;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.ExpressExtensionRegistry;
import com.intrbiz.express.value.ValueScript;

public class ScriptedFunctionScanner
{
    private static final Logger logger = LoggerFactory.getLogger(ScriptedFunctionScanner.class);
    
    public static void scanScriptedFunctions(ExpressExtensionRegistry registry, File directory)
    {
        if (directory.exists() && directory.isDirectory())
        {
            for (File child : directory.listFiles())
            {
                if (child.exists() && child.isDirectory())
                {
                    scanScriptedFunctions(registry, child);
                }
                else if (child.exists() && child.isFile())
                {
                    loadScriptedFunctions(registry, child);
                }
            }
        }
    }
    
    public static void loadScriptedFunctions(ExpressExtensionRegistry registry, File file)
    {
        if (file.exists() && file.isFile() && (file.getName().endsWith(".express") || file.getName().endsWith(".es")))
        {
            try
            {
                // parsing the script will load any functions into the registry
                ValueScript.readUnwrapped(
                        ExpressContext.builder().withExtensions(registry).build(), 
                        file
                );
            }
            catch (ExpressException e)
            {
                logger.warn("Failed to load express script from: " + file.getAbsolutePath(), e);
            }
        }
    }
}
