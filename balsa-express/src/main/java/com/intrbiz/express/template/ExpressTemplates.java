package com.intrbiz.express.template;

import java.io.File;

import com.intrbiz.express.template.loader.TemplateLoader;
import com.intrbiz.express.template.loader.impl.ClassPathTemplateSource;
import com.intrbiz.express.template.loader.impl.FileTemplateSource;

/**
 * Helper class for using Express for templating
 *
 */
public class ExpressTemplates
{
    
    public static TemplateLoader fromClasspath(String basePath)
    {
        return new TemplateLoader()
                .withSource(new ClassPathTemplateSource(basePath));
    }
    
    public static TemplateLoader fromClasspath()
    {
        return new TemplateLoader()
                .withSource(new ClassPathTemplateSource());
    }
    
    public static TemplateLoader fromFilesystem(File basePath)
    {
        return new TemplateLoader()
                .withSource(new FileTemplateSource(basePath));
    }
    
}
