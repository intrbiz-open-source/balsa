package com.intrbiz.express.template;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Objects;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.template.filter.ContentFilter;
import com.intrbiz.express.value.ValueScript;

public class ExpressTemplate
{
    private final String name;

    private final ContentFilter defaultFilter;

    private final ValueScript script;

    public ExpressTemplate(String name, ContentFilter defaultFilter, ValueScript script)
    {
        super();
        this.name = Objects.requireNonNull(name);
        this.defaultFilter = Objects.requireNonNull(defaultFilter);
        this.script = Objects.requireNonNull(script);
    }

    public String getName()
    {
        return name;
    }

    public ContentFilter getDefaultFilter()
    {
        return defaultFilter;
    }

    public ValueScript getScript()
    {
        return script;
    }
    
    public String toString()
    {
        return "Express Template " + this.name + ", filter=" + this.defaultFilter.getContentType() + "\n" + this.script;
    }
    
    /**
     * Encode this template to a String using the given context
     * @param context
     * @param source
     * @return
     * @throws ExpressException
     */
    public String encodeToString(ExpressContext context, Object source) throws ExpressException
    {
        StringWriter to = new StringWriter();
        this.encode(context, source, to);
        return to.toString();
    }

    /**
     * Encode this template to the given writer using the given context
     * @param context
     * @param source
     * @param to
     * @throws ExpressException
     */
    public void encode(ExpressContext context, Object source, Writer to) throws ExpressException
    {
        try
        {
            // setup the writer
            context.setupWriter(to, this.getDefaultFilter());
            // process the script
            this.script.execute(context, source);
            // flush
            context.getWriter().flush();
        }
        finally
        {
            // reset the writer
            context.clearWriter();
        }
    }
    
    public void include(ExpressContext context, Object source) throws ExpressException
    {
        try
        {
            // push our content filter
            context.getWriter().pushContentFilter(this.getDefaultFilter());
            // process the script
            this.script.execute(context, source);
        }
        finally
        {
            // pop our content filter
            context.getWriter().popContentFilter();
        }
    }
}
