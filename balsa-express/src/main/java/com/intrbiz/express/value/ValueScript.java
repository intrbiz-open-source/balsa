package com.intrbiz.express.value;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.parser.ELParser;
import com.intrbiz.express.parser.ELParserConstants;
import com.intrbiz.express.parser.ELParserTokenManager;
import com.intrbiz.express.parser.ParseException;
import com.intrbiz.express.parser.SimpleCharStream;
import com.intrbiz.express.parser.TokenMgrError;
import com.intrbiz.express.statement.Statement;

public class ValueScript implements Serializable
{
    private static final long serialVersionUID = 1L;

    private Statement statement;

    public ValueScript()
    {
        super();
    }
    
    public ValueScript(Statement statement)
    {
        super();
        this.statement = statement;
    }

    public ValueScript(ExpressContext context, String script) throws ExpressException
    {
        this();
        ELParser parser = new ELParser(new StringReader(script));
        try
        {
            this.statement = parser.readFullStatements(context);
        }
        catch (TokenMgrError e)
        {
            throw new ExpressException("Error parsing script: [" + script + "]", e);
        }
        catch (ExpressException ee)
        {
            throw new ExpressException("Error parsing script: [" + script + "]", ee);
        }
        catch (ParseException pe)
        {
            throw new ExpressException("Error parsing script: [" + script + "]", pe);
        }
    }
    
    public ValueScript(ExpressContext context, Reader input) throws ExpressException
    {
        this();
        ELParser parser = new ELParser(input);
        try
        {
            this.statement = parser.readFullStatements(context);
        }
        catch (TokenMgrError e)
        {
            throw new ExpressException("Error parsing script", e);
        }
        catch (ExpressException ee)
        {
            throw new ExpressException("Error parsing script", ee);
        }
        catch (ParseException pe)
        {
            throw new ExpressException("Error parsing script", pe);
        }
    }
    
    public ValueScript(ExpressContext context, InputStream input) throws ExpressException
    {
        this();
        ELParser parser = new ELParser(input);
        try
        {
            this.statement = parser.readFullStatements(context);
        }
        catch (TokenMgrError e)
        {
            throw new ExpressException("Error parsing script", e);
        }
        catch (ExpressException ee)
        {
            throw new ExpressException("Error parsing script", ee);
        }
        catch (ParseException pe)
        {
            throw new ExpressException("Error parsing script", pe);
        }
    }
    
    /**
     * Execute this script with return data, in an isolated function stack frame
     * @param context the context in which to run this script
     * @param source the source object invoking this script or null
     * @return the script return data
     * @throws ExpressException
     */
    public ScriptReturn executeWithState(ExpressContext context, Object source) throws ExpressException
    {
        return this.executeWithState(false, context, source);
    }
    
    /**
     * Execute this script with return data, in an isolated function stack frame with access to previously declared entities
     * @param context the context in which to run this script
     * @param source the source object invoking this script or null
     * @return the script return data
     * @throws ExpressException
     */
    public ScriptReturn executeInlineWithState(ExpressContext context, Object source) throws ExpressException
    {
        return this.executeWithState(true, context, source);
    }

    private ScriptReturn executeWithState(boolean lookupParent, ExpressContext context, Object source) throws ExpressException
    {
        // enter the root frame
        context.enterFrame(true, lookupParent);
        try
        {
            // execute the script
            statement.execute(context, source);
            // process the results
            return new ScriptReturn(context.getFrame().isReturn(), context.getFrame().getReturnValue());
        }
        finally
        {
            context.exitFrame();
        }
    }
    
    /**
     * Execute this script and return any result, in an isolated function stack frame
     * @param context the context in which to run this script
     * @param source the source object invoking this script or null
     * @return any result from the script
     * @throws ExpressException
     */
    public Object execute(ExpressContext context, Object source) throws ExpressException
    {
        return this.execute(false, context, source);
    }
    
    /**
     * Execute this script and return any result, in an isolated function stack frame with access to previously declared entities
     * @param context the context in which to run this script
     * @param source the source object invoking this script or null
     * @return any result from the script
     * @throws ExpressException
     */
    public Object executeInline(ExpressContext context, Object source) throws ExpressException
    {
        return this.execute(true, context, source);
    }

    private Object execute(boolean lookupParent, ExpressContext context, Object source) throws ExpressException
    {
        // enter the root frame
        context.enterFrame(true, lookupParent);
        try
        {
            // execute the script
            statement.execute(context, source);
            // process the results
            if (context.getFrame().isReturn()) return context.getFrame().getReturnValue();
        }
        finally
        {
            context.exitFrame();
        }
        return null;
    }

    public Statement getStatement()
    {
        return this.statement;
    }

    @Override
    public String toString()
    {
        return String.valueOf(this.statement);
    }
    
    /**
     * Read a express script which is not surrounded by '<#' and '#>' wrappings.
     * @param context
     * @param script
     * @return
     * @throws ExpressException
     */
    public static ValueScript readUnwrapped(ExpressContext context, Reader script) throws ExpressException
    {
        ELParser parser = new ELParser(new ELParserTokenManager(new SimpleCharStream(script), ELParserConstants.IN_SCRIPT));
        try
        {
            return new ValueScript(parser.readStatements(context));
        }
        catch (TokenMgrError e)
        {
            throw new ExpressException("Error parsing script", e);
        }
        catch (ExpressException ee)
        {
            throw new ExpressException("Error parsing script", ee);
        }
        catch (ParseException pe)
        {
            throw new ExpressException("Error parsing script", pe);
        }
    }
    
    public static ValueScript readUnwrapped(ExpressContext context, File script) throws ExpressException
    {
        try (FileReader reader = new FileReader(script))
        {
            return readUnwrapped(context, reader);
        }
        catch (FileNotFoundException e)
        {
            throw new ExpressException("Error parsing script", e);
        }
        catch (IOException e)
        {
            throw new ExpressException("Error parsing script", e);
        }
    }
    
    /**
     * Read a express script which is not surrounded by '<#' and '#>' wrappings.
     * @param context
     * @param script
     * @return
     * @throws ExpressException
     */
    public static ValueScript readUnwrapped(ExpressContext context, String script) throws ExpressException
    {
        ELParser parser = new ELParser(new ELParserTokenManager(new SimpleCharStream(new StringReader(script)), ELParserConstants.IN_SCRIPT));
        try
        {
            return new ValueScript(parser.readStatements(context));
        }
        catch (TokenMgrError e)
        {
            throw new ExpressException("Error parsing script: [" + script + "]", e);
        }
        catch (ExpressException ee)
        {
            throw new ExpressException("Error parsing script: [" + script + "]", ee);
        }
        catch (ParseException pe)
        {
            throw new ExpressException("Error parsing script: [" + script + "]", pe);
        }
    }
}
