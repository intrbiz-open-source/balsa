package com.intrbiz.express.action;

import static com.intrbiz.util.Util.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.intrbiz.express.ExpressException;
import com.intrbiz.util.metadata.Action;

public final class MethodActionHandler implements ActionHandler
{
    private final Object on;

    private final String action;

    private final Method method;

    private final List<ActionArgument> arguments;

    public MethodActionHandler(Object on, String action, Method method)
    {
        super();
        this.on = on;
        this.action = action;
        this.method = method;
        this.arguments = this.parseArguments(method);
    }

    private List<ActionArgument> parseArguments(Method m)
    {
        List<ActionArgument> args = new LinkedList<ActionArgument>();
        if (m.getParameterTypes().length > 0)
        {
            Parameter[] params = m.getParameters();
            Class<?>[]  types  = m.getParameterTypes();
            for (int i = 0; i < types.length; i++)
            {
                args.add(new ActionArgument(types[i], (params[i].getName() == null) ? "arg" + i : params[i].getName()));
            }
        }
        return Collections.unmodifiableList(args);
    }

    @Override
    public List<ActionArgument> getArguments()
    {
        return this.arguments;
    }

    @Override
    public String getName()
    {
        return this.action;
    }

    @Override
    public Object act(Object[] arguments) throws Exception
    {
        try
        {
            return this.method.invoke(this.on, arguments);
        }
        catch (InvocationTargetException e)
        {
            // try to unwrap the exception
            Throwable t = e.getCause();
            if (t instanceof Exception) throw (Exception) e;
            if (t instanceof RuntimeException) throw (RuntimeException) t;
            if (t instanceof Error)
                throw (Error) t;
            else
                throw e;
        }
        catch (IllegalArgumentException e)
        {
            throw new ExpressException("Failed to execute action: " + this.action, e);
        }
        catch (IllegalAccessException e)
        {
            throw new ExpressException("Failed to execute action: " + this.action, e);
        }
    }

    public static List<ActionHandler> findActionHandlers(Object on)
    {
        List<ActionHandler> actions = new LinkedList<ActionHandler>();
        if (on != null)
        {
            for (Method method : on.getClass().getDeclaredMethods())
            {
                if (Modifier.isPublic(method.getModifiers()))
                {
                    Action action = method.getAnnotation(Action.class);
                    if (action != null)
                    {
                        String name = action.value();
                        if (isEmpty(name)) name = method.getName();
                        actions.add(new MethodActionHandler(on, name, method));
                    }
                }
            }
        }
        return actions;
    }
    
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("@");
        sb.append(this.getName());
        sb.append("(");
        sb.append(this.getArguments().stream().map(i -> i.getName()).collect(Collectors.joining(", ")));
        sb.append(")");
        return sb.toString();
    }
}
