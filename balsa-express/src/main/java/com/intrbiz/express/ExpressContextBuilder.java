package com.intrbiz.express;

import java.time.ZoneId;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Supplier;

import com.intrbiz.express.access.JavaAccessController;
import com.intrbiz.express.security.Limiter;
import com.intrbiz.express.template.filter.ContentFilterRegistry;
import com.intrbiz.express.template.loader.TemplateLoader;

public class ExpressContextBuilder
{
    private ExpressExtensionRegistry extensions = ExpressExtensionRegistry.getDefaultRegistry();

    private ExpressEntityResolver resolver = ExpressEntityResolver.EMPTY;

    private Supplier<Limiter> iterationLimiter = Limiter::defaultIterationLimit;

    private Supplier<Limiter> opLimiter = Limiter::defaultOpLimit;

    private boolean caching = true;

    private TemplateLoader templateLoader = null;

    private ContentFilterRegistry contentFilterRegistry = ContentFilterRegistry.getDefault();

    private Locale currentLocale = Locale.getDefault();

    private ZoneId currentTimeZone = ZoneId.systemDefault();

    private boolean allowSetAccessible = true;

    private boolean suppressMethodExceptions = true;

    private JavaAccessController javaAccessController = JavaAccessController.defaultRules();

    ExpressContextBuilder()
    {
        super();
    }
    
    ExpressContextBuilder(ExpressExtensionRegistry extensions, ExpressEntityResolver resolver, Supplier<Limiter> iterationLimiter, Supplier<Limiter> opLimiter, boolean caching, TemplateLoader templateLoader, ContentFilterRegistry contentFilterRegistry, Locale currentLocale, ZoneId currentTimeZone, boolean allowSetAccessible, boolean suppressMethodExceptions, JavaAccessController javaAccessController)
    {
        super();
        this.extensions = extensions;
        this.resolver = resolver;
        this.iterationLimiter = iterationLimiter;
        this.opLimiter = opLimiter;
        this.caching = caching;
        this.templateLoader = templateLoader;
        this.contentFilterRegistry = contentFilterRegistry;
        this.currentLocale = currentLocale;
        this.currentTimeZone = currentTimeZone;
        this.allowSetAccessible = allowSetAccessible;
        this.suppressMethodExceptions = suppressMethodExceptions;
        this.javaAccessController = javaAccessController;
    }



    public ExpressContextBuilder withExtensions(ExpressExtensionRegistry extensions)
    {
        this.extensions = (extensions != null) ? extensions : ExpressExtensionRegistry.getDefaultRegistry();
        return this;
    }

    public ExpressContextBuilder withResolver(ExpressEntityResolver resolver)
    {
        this.resolver = (resolver != null) ? resolver : ExpressEntityResolver.EMPTY;
        return this;
    }

    public ExpressContextBuilder withIterationLimiter(Supplier<Limiter> iterationLimiter)
    {
        this.iterationLimiter = (iterationLimiter != null) ? iterationLimiter : Limiter::defaultIterationLimit;
        return this;
    }
    
    public ExpressContextBuilder withIterationLimi(final int limit)
    {
        this.iterationLimiter = () -> Limiter.of(limit);
        return this;
    }

    public ExpressContextBuilder withOpLimiter(Supplier<Limiter> opLimiter)
    {
        this.opLimiter = (opLimiter != null) ? opLimiter : Limiter::defaultOpLimit;
        return this;
    }
    
    public ExpressContextBuilder withOpLimit(final int limit)
    {
        this.opLimiter = () -> Limiter.of(limit);
        return this;
    }

    public ExpressContextBuilder withCaching(boolean caching)
    {
        this.caching = caching;
        return this;
    }

    public ExpressContextBuilder withTemplateLoader(TemplateLoader templateLoader)
    {
        this.templateLoader = templateLoader;
        return this;
    }

    public ExpressContextBuilder withContentFilterRegistry(ContentFilterRegistry contentFilterRegistry)
    {
        this.contentFilterRegistry = (contentFilterRegistry != null) ? contentFilterRegistry : ContentFilterRegistry.getDefault();
        return this;
    }

    public ExpressContextBuilder withCurrentLocale(Locale currentLocale)
    {
        this.currentLocale = currentLocale;
        return this;
    }

    public ExpressContextBuilder withCurrentTimeZone(ZoneId currentTimeZone)
    {
        this.currentTimeZone = currentTimeZone;
        return this;
    }

    public ExpressContextBuilder withAllowSetAccessible(boolean allowSetAccessible)
    {
        this.allowSetAccessible = allowSetAccessible;
        return this;
    }

    public ExpressContextBuilder withSuppressMethodExceptions(boolean suppressMethodExceptions)
    {
        this.suppressMethodExceptions = suppressMethodExceptions;
        return this;
    }
    
    public ExpressContextBuilder withJavaAccessController(JavaAccessController javaAccessController)
    {
        this.javaAccessController = javaAccessController;
        return this;
    }

    /**
     * Allow permissive access to Java classes
     */
    public ExpressContextBuilder withPermissiveJavaAccess()
    {
        this.allowSetAccessible = true;
        this.suppressMethodExceptions = true;
        this.javaAccessController = JavaAccessController.permissiveRules();
        return this;
    }
    
    /**
     * Allow restricted access to Java classes
     * @return
     */
    public ExpressContextBuilder withSafeJavaAccess()
    {
        this.allowSetAccessible = false;
        this.suppressMethodExceptions = false;
        this.javaAccessController = JavaAccessController.safeRules();
        return this;
    }
    
    /**
     * Allow strict access to Java classes with the given controller
     * @param javaAccessController the Java access controller to use
     */
    public ExpressContextBuilder withStrictJavaAccess(JavaAccessController javaAccessController)
    {
        this.allowSetAccessible = false;
        this.suppressMethodExceptions = false;
        this.javaAccessController = Objects.requireNonNull(javaAccessController);
        return this;
    }
    
    /**
     * Enable audit logging of Java access checks
     */
    public ExpressContextBuilder withAuditJavaAccess()
    {
        this.javaAccessController = JavaAccessController.auditing(
                JavaAccessController.orDefaultRules(this.javaAccessController)
        );
        return this;
    }
    
    public ExpressContextBuilder copy()
    {
        return new ExpressContextBuilder(
                this.extensions,
                this.resolver,
                this.iterationLimiter,
                this.opLimiter,
                this.caching,
                this.templateLoader,
                this.contentFilterRegistry,
                this.currentLocale,
                this.currentTimeZone,
                this.allowSetAccessible,
                this.suppressMethodExceptions,
                this.javaAccessController
        );
    }

    public ExpressContext build()
    {
        return new DefaultContext(
                this.extensions, 
                this.resolver, 
                this.iterationLimiter.get(), 
                this.opLimiter.get(), 
                this.caching, 
                this.allowSetAccessible, 
                this.suppressMethodExceptions, 
                this.javaAccessController,
                this.currentLocale, 
                this.currentTimeZone, 
                this.templateLoader, 
                this.contentFilterRegistry
        );
    }
    
    public ExpressContext build(ExpressEntityResolver resolver)
    {
        Objects.requireNonNull(resolver);
        return new DefaultContext(
                this.extensions, 
                (this.resolver != null && this.resolver != ExpressEntityResolver.EMPTY) ? 
                        ExpressEntityResolver.of(resolver, this.resolver) : 
                        resolver, 
                this.iterationLimiter.get(), 
                this.opLimiter.get(), 
                this.caching, 
                this.allowSetAccessible, 
                this.suppressMethodExceptions, 
                this.javaAccessController,
                this.currentLocale, 
                this.currentTimeZone, 
                this.templateLoader, 
                this.contentFilterRegistry
        );
    }
}
