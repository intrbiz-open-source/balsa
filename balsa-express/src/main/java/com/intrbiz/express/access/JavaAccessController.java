package com.intrbiz.express.access;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.express.security.Hidden;

/**
 * Control access to Java classes and packages from Express expression, scripts and templates. 
 *
 */
@FunctionalInterface
public interface JavaAccessController extends Hidden
{
    /**
     * Check if access to the given class name, or package.
     * 
     * @param javaName the full class name or a package wildcard
     * @return null if no decision, true if allowed, false if denied
     */
    Boolean checkJavaAccess(String javaName);
    
    // factory methods
    
    /**
     * Create an access controller from the map of class / package rules
     * @param rules a map of class name / package wildcard to allow/deny decision
     */
    public static JavaAccessController of(final Map<String, Boolean> rules)
    {
        Objects.requireNonNull(rules);
        return rules::get;
    }
    
    /**
     * Create an access controller from the two given access controllers 
     * with decision priority given to the child.
     * 
     * @param child the child controller
     * @param parent the parent controller
     * @return the composite access controller
     */
    public static JavaAccessController of(final JavaAccessController child, final JavaAccessController parent)
    {
        Objects.requireNonNull(parent);
        Objects.requireNonNull(child);
        return (String name) -> {
            Boolean result = child.checkJavaAccess(name);
            if (result == null)
                result = parent.checkJavaAccess(name);
            return result;
        };
    }
    
    /**
     * Create an access controller from the given access controllers 
     * with the decision priority going to the first controller 
     * which provides a result (from index 0 -> n).
     * 
     * @param controllers the collection of controllers
     * @return the composite access controller
     */
    public static JavaAccessController of(Collection<JavaAccessController> controllers)
    {
        final JavaAccessController[] controllerArray = Objects.requireNonNull(controllers).stream()
                                                            .filter(Objects::nonNull)
                                                            .toArray(l -> new JavaAccessController[l]);
        return (String name) -> {
            Boolean result = null;
            for (int i = 0; i < controllerArray.length; i++)
            {
                result = controllerArray[i].checkJavaAccess(name);
                if (result != null)
                    break;
            }
            return result;
        };
    }
    
    /**
     * Create an access controller from the given access controllers 
     * with the decision priority going to the first controller 
     * which provides a result (from index 0 -> n).
     * 
     * @param controllers the collection of controllers
     * @return the composite access controller
     */
    public static JavaAccessController of(JavaAccessController... controllers)
    {
        return of(Arrays.asList(Objects.requireNonNull(controllers)));
    }
    
    /**
     * Build a JavaAccessController by configuring simple rules
     */
    public static JavaAccessControllerBuilder builder()
    {
        return new JavaAccessControllerBuilder();
    }
    
    /**
     * The default JavaAccessController which 
     * denies access to well known unsafe Java classes, 
     * but allows access by default.
     */
    public static JavaAccessController defaultRules()
    {
        return BuiltInJavaAccessRules.DEFAULT_RULES;
    }
    
    /**
     * Return the default rules if the given controller is null
     * @param controller the optional controller
     * @return a controller
     */
    public static JavaAccessController orDefaultRules(JavaAccessController controller)
    {
        return (controller != null) ? controller : defaultRules();
    }
    
    /**
     * The safe JavaAccessController which
     * denies access to well known unsafe Java classes, 
     * and denies access by default.
     */
    public static JavaAccessController safeRules()
    {
        return BuiltInJavaAccessRules.SAFE_RULES;
    }
    
    /**
     * The permissive JavaAccessController which
     * denies access to a limited set of unsafe Java classes,
     * and allow wide access by default.
     */
    public static JavaAccessController permissiveRules()
    {
        return BuiltInJavaAccessRules.PERMISSIVE_RULES;
    }
    
    /**
     * A JavaAccessController which allows no access
     */
    public static JavaAccessController denyAll()
    {
        return (name) -> false;
    }
    
    /**
     * A JavaAccessController which allows all access
     */
    public static JavaAccessController allowAll()
    {
        return (name) -> true;
    }
    
    public static JavaAccessController auditing(final JavaAccessController controller)
    {
        Objects.requireNonNull(controller);
        final Logger logger = LoggerFactory.getLogger(JavaAccessController.class);
        return (name) -> {
            Boolean result = controller.checkJavaAccess(name);
            logger.debug("Checking Java access: {} -> {}", name, result);
            return result;
        };
    }
}
