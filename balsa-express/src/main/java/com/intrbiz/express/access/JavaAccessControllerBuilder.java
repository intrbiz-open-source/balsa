package com.intrbiz.express.access;

import java.util.Map;
import java.util.TreeMap;

public class JavaAccessControllerBuilder
{   
    private final Map<String, Boolean> rules = new TreeMap<>();
    
    JavaAccessControllerBuilder()
    {
        super();
    }
    
    public JavaAccessControllerBuilder allRule(boolean decision)
    {
        this.rules.put("*", decision);
        return this;
    }
    
    public JavaAccessControllerBuilder denyAll()
    {
        this.allRule(false);
        return this;
    }
    
    public JavaAccessControllerBuilder allowAll()
    {
        this.allRule(true);
        return this;
    }
    
    public JavaAccessControllerBuilder rule(String nameExpression, boolean decision)
    {
        this.rules.put(nameExpression, decision);
        return this;
    }
    
    public JavaAccessControllerBuilder classRule(Class<?> cls, boolean decision)
    {
        this.rules.put(cls.getCanonicalName(), decision);
        return this;
    }
    
    public JavaAccessControllerBuilder denyClasses(Class<?>... classes)
    {
        for (Class<?> cls : classes)
        {
            this.classRule(cls, false);
        }
        return this;
    }
    
    public JavaAccessControllerBuilder allowClasses(Class<?>... classes)
    {
        for (Class<?> cls : classes)
        {
            this.classRule(cls, true);
        }
        return this;
    }
    
    public JavaAccessControllerBuilder packageRule(String pkg, boolean decision)
    {
        this.rules.put(pkg + "*", decision);
        return this;
    }
    
    public JavaAccessControllerBuilder denyPackages(String... packages)
    {
        for (String pkg : packages)
        {
            this.packageRule(pkg, false);
        }
        return this;
    }
    
    public JavaAccessControllerBuilder allowPackages(String... packages)
    {
        for (String pkg : packages)
        {
            this.packageRule(pkg, true);
        }
        return this;
    }

    public JavaAccessController build()
    {
        // create the controller from a clone of these rules
        return JavaAccessController.of(new TreeMap<>(this.rules));
    }
}
