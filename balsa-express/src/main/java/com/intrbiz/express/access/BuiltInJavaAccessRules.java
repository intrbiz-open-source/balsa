package com.intrbiz.express.access;

class BuiltInJavaAccessRules
{    
    private static final Class<?>[] UNSAFE_CLASSES = {
            System.class,
            Thread.class,
            Runtime.class,
            Process.class,
            ProcessBuilder.class,
            Class.class,
            ClassLoader.class,
            ThreadLocal.class,
    };
    
    private static final String[] UNSAFE_PACKAGES = {
            "java.lang.invoke",
            "java.lang.management",
            "java.lang.ref",
            "java.lang.reflect",
            "java.net",
            "java.io",
            "java.nio",
            "java.sql",
            "java.applet",
            "java.awt",
            "java.beans",
            "java.rmi",
            "com.intrbiz.express"
    };
    
    static final JavaAccessController DEFAULT_RULES = new JavaAccessControllerBuilder()
            .allowAll()
            .denyClasses(UNSAFE_CLASSES)
            .denyPackages(UNSAFE_PACKAGES)
            .build();
    
    static final JavaAccessController SAFE_RULES = new JavaAccessControllerBuilder()
            .denyAll()
            .denyClasses(UNSAFE_CLASSES)
            .denyPackages(UNSAFE_PACKAGES)
            .build();
    
    static final JavaAccessController PERMISSIVE_RULES = new JavaAccessControllerBuilder()
            .allowAll()
            .denyClasses(UNSAFE_CLASSES)
            .build();
}
