package com.intrbiz.express.statement.loop;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Operator;
import com.intrbiz.express.stack.ELStatementFrame;
import com.intrbiz.express.statement.ControlStatement;
import com.intrbiz.express.statement.StatementBlock;

public class ForEachStatement extends ControlStatement
{
    private String entityName;

    private Operator collection;

    private StatementBlock block;

    public ForEachStatement(String entityName, Operator collection, StatementBlock block)
    {
        super("foreach");
        this.setEntityName(entityName);
        this.setCollection(collection);
        this.setBlock(block);
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void execute(ExpressContext ctx, Object source) throws ExpressException
    {
        // get the collection to process
        Iterable collection = this.resolveIterable(ctx, source);
        // loop
        StatementBlock block = this.getBlock();
        ELStatementFrame frame = ctx.getFrame();
        for (Object entry : collection)
        {
            ctx.checkIteration();
            // set the entity
            if (entry instanceof Entry)
            {
                ctx.setEntity(this.getEntityName() + "_key", ((Entry) entry).getKey(), source);
                ctx.setEntity(this.getEntityName() + "_value", ((Entry) entry).getValue(), source);
            }
            else
            {
                ctx.setEntity(this.getEntityName(), entry, source);
            }
            // execute the block
            block.execute(ctx, source);
            // check the context state
            if (frame.isHalt())
            {
                if (frame.isBreak())
                {
                    frame.doReset();
                    break;
                }
                else if (frame.isContinue())
                {
                    frame.doReset();
                }
                else if (frame.isReturn())
                {
                    return;
                }
            }
        }
    }
    
    @SuppressWarnings("rawtypes")
    protected Iterable resolveIterable(ExpressContext ctx, Object source) throws ExpressException
    {
        Object collection = this.getCollection().get(ctx, source);
        if (collection instanceof Iterable)
        {
            return (Iterable) collection;
        }
        else if (collection instanceof Map)
        {
            return ((Map) collection).entrySet();
        }
        else if (collection instanceof Stream)
        {
            return () -> ((Stream) collection).iterator();
        }
        else if (collection != null)
        {
            throw new ExpressException("Cannot execute foreach on: " + collection.getClass());
        }
        return Collections.emptyList();
    }

    public String getEntityName()
    {
        return entityName;
    }

    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    public Operator getCollection()
    {
        return collection;
    }

    public void setCollection(Operator collection)
    {
        this.collection = collection;
    }

    public StatementBlock getBlock()
    {
        return block;
    }

    public void setBlock(StatementBlock block)
    {
        this.block = block;
    }

    public String toString(String p)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(p).append("for (").append(this.getEntityName()).append(" in ").append(this.getCollection().toString()).append(") {\r\n");
        sb.append(this.block.toString(p + "  "));
        sb.append(p).append("}\r\n");
        return sb.toString();
    }
}
