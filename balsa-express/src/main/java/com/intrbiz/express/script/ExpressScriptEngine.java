package com.intrbiz.express.script;

import java.util.Objects;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressEntityResolver;
import com.intrbiz.express.ExpressExtensionRegistry;
import com.intrbiz.express.access.JavaAccessController;
import com.intrbiz.express.value.ScriptReturn;
import com.intrbiz.express.value.ValueScript;

public final class ExpressScriptEngine
{    
    private final JavaAccessController javaAccessController;
    
    private final ExpressExtensionRegistry registry;
    
    private final ValueScript script;
    
    public ExpressScriptEngine(ExpressExtensionRegistry registry, JavaAccessController javaAccessController, ValueScript script)
    {
        super();
        this.registry = (registry != null) ? registry : ExpressExtensionRegistry.getDefaultRegistry();
        this.javaAccessController = (javaAccessController != null) ? javaAccessController : JavaAccessController.defaultRules();
        this.script = Objects.requireNonNull(script);
    }
    
    public ExpressExtensionRegistry getExpressExtensionRegistry()
    {
        return this.registry;
    }
    
    public ExpressContext createContext(ExpressEntityResolver bindings)
    {
        return ExpressContext.builder()
            .withExtensions(this.registry)
            .withResolver(bindings)
            .withJavaAccessController(this.javaAccessController)
            .build();
    }
    
    public ExpressContext createContext()
    {
        return this.createContext(null);
    }
    
    public Object execute(ExpressContext context, Object source)
    {
        return this.script.execute(context, source);
    }
    
    public ScriptReturn executeWithState(ExpressContext context, Object source)
    {
        return this.script.executeWithState(context, source);
    }
}
