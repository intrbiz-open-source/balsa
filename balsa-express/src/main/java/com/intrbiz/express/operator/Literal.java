package com.intrbiz.express.operator;

import java.util.Collection;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.literal.BooleanLiteral;
import com.intrbiz.express.operator.literal.DoubleLiteral;
import com.intrbiz.express.operator.literal.FloatLiteral;
import com.intrbiz.express.operator.literal.IntLiteral;
import com.intrbiz.express.operator.literal.LongLiteral;
import com.intrbiz.express.operator.literal.NullLiteral;
import com.intrbiz.express.operator.literal.StaticListLiteral;
import com.intrbiz.express.operator.literal.StringLiteral;


public abstract class Literal extends Operator
{

	protected Literal(String name)
	{
		super(name);
	}
	
	public abstract Object getValue();
	
	public String toString()
	{
		return String.valueOf(this.getValue());
	}

	@Override
	public Object get(ExpressContext context, Object source) throws ExpressException
	{
		return this.getValue();
	}

	@Override
	public void set(ExpressContext context, Object value, Object source) throws ExpressException
	{
	}
	
	@Override
	public boolean isConstant()
	{
	    return true;
	}
	
	// TODO: utils
	
	public static final Literal of(Object value)
	{
	    if (value instanceof String)
	        return new StringLiteral((String) value, false);
       if (value instanceof Integer)
	            return new IntLiteral((Integer) value);
	    if (value instanceof Long)
	        return new LongLiteral((Long) value);
       if (value instanceof Float)
            return new FloatLiteral((Float) value);
       if (value instanceof Double)
           return new DoubleLiteral((Double) value);
       if (value instanceof Boolean)
           return new BooleanLiteral((Boolean) value);
       if (value instanceof Collection)
           return new StaticListLiteral((Collection<?>) value);
       if (value instanceof Iterable)
           return new StaticListLiteral((Iterable<?>) value);
	    return new NullLiteral();
	}
}
