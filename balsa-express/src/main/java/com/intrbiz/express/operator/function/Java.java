package com.intrbiz.express.operator.function;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;

public class Java extends Function
{

    public Java()
    {
        super("java");
    }

    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        context.checkOp();
        
        try
        {
            if (this.getParameters().size() == 1)
            {
                String className = (String) this.getParameters().get(0).get(context, source);
                if (! context.checkJavaAccess(className))
                    throw new ExpressException("Cannot load Java class: " + className);
                return Class.forName(className);
            }
        }
        catch (Exception e)
        {
            throw new ExpressException("Failed to load Java class", e);
        }
        return null;
    }

    @Override
    public void set(ExpressContext context, Object value,Object source) throws ExpressException
    {
    }
    
    @Override
    public boolean isIdempotent()
    {
        return true;
    }
}
