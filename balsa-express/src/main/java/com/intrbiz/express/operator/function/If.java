package com.intrbiz.express.operator.function;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.operator.Operator;

public class If extends Function
{

    public If()
    {
        super("if");
    }

    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        context.checkOp();
        // fetch the parameters 
        Operator ev = this.getParameter(0);
        Operator tr = this.getParameter(1);
        Operator fl = this.getParameter(2);
        // evaluate the if condition
        Object evo = (ev == null) ? false : ev.get(context,source);
        if (evo instanceof Boolean ? ((Boolean)evo).booleanValue() : evo != null)
        {
            return (tr == null) ? null : tr.get(context,source);
        }
        else
        {
            return (fl == null) ? null : fl.get(context,source);
        }
    }
    
    @Override
    public boolean isIdempotent()
    {
        return true;
    }
}
