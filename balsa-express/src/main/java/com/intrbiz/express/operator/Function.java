package com.intrbiz.express.operator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.validation.converter.Converter;
import com.intrbiz.validation.validator.Validator;

/**
 * A user extendable function within the EL language.
 * 
 * To create a function, create a sub class of this class and 
 * register it with the context.  Its functionality is then 
 * accessible from the EL.
 *
 */
public abstract class Function extends Operator
{

    private List<Operator> parameters = new ArrayList<Operator>();

    private Map<String, Operator> namedParameters = new TreeMap<String, Operator>();

    protected Function(String name)
    {
        super(name);
    }

    /**
     * Get the functions parameters list
     * @return the parameters
     */
    public List<Operator> getParameters()
    {
        return parameters;
    }

    /**
     * Set the functions parameters list
     * @param parameters
     *            the parameters to set
     */
    public void setParameters(List<Operator> parameters)
    {
        this.parameters = parameters;
    }

    /**
     * Get the functions named parameters
     * @return
     * returns Map<String,Operator>
     */
    public Map<String, Operator> getNamedParameters()
    {
        return namedParameters;
    }

    /**
     * Set the functions named parameters
     * @param namedParameters
     * returns void
     */
    public void setNamedParameters(Map<String, Operator> namedParameters)
    {
        this.namedParameters = namedParameters;
    }
    
    /**
     * Get a standard parameter by its index.
     * The index is not affect by named parameters.
     * @param idx
     * @return
     * returns Operator
     */
    public Operator getParameter(int idx)
    {
        return (idx < this.parameters.size()) ? this.parameters.get(idx) : null;
    }
    
    public Object getParameterValue(int idx, ExpressContext context, Object source) throws ExpressException
    {
        Operator op = this.getParameter(idx);
        return (op != null) ? op.get(context, source) : null;
    }
    
    public String getStringParameterValue(int idx, ExpressContext context, Object source, String defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(idx, context, source);
        return (val == null) ? defaultValue : String.valueOf(val);
    }
    
    public Integer getIntParameterValue(int idx, ExpressContext context, Object source, int defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(idx, context, source);
        return (val instanceof Number) ? ((Number) val).intValue() : defaultValue;
    }
    
    public Long getLongParameterValue(int idx, ExpressContext context, Object source, long defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(idx, context, source);
        return (val instanceof Number) ? ((Number) val).longValue() : defaultValue;
    }
    
    public Float getFloatParameterValue(int idx, ExpressContext context, Object source, float defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(idx, context, source);
        return (val instanceof Number) ? ((Number) val).floatValue() : defaultValue;
    }
    
    public Double getDoubleParameterValue(int idx, ExpressContext context, Object source, double defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(idx, context, source);
        return (val instanceof Number) ? ((Number) val).doubleValue() : defaultValue;
    }
    
    public Boolean getBooleanParameterValue(int idx, ExpressContext context, Object source, boolean defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(idx, context, source);
        return (val instanceof Boolean) ? ((Boolean) val).booleanValue() : defaultValue;
    }
    
    @SuppressWarnings("unchecked")
    public <T> Collection<T> getCollectionParameterValue(int idx, ExpressContext context, Object source, Collection<T> defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(idx, context, source);
        return (val instanceof Collection) ? (Collection<T>) val : defaultValue;
    }
    
    public boolean isParamterConstant(int idx)
    {
        Operator op = this.getParameter(idx);
        return op != null && op.isConstant();
    }
    
    /**
     * Get a named parameter
     * @param name
     * @return
     * returns Operator
     */
    public Operator getParameter(String name)
    {
        return this.namedParameters.get(name);
    }
    
    public Object getParameterValue(String name, ExpressContext context, Object source) throws ExpressException
    {
        Operator op = this.getParameter(name);
        return (op != null) ? op.get(context, source) : null;
    }
    
    public String getStringParameterValue(String name, ExpressContext context, Object source, String defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(name, context, source);
        return (val == null) ? defaultValue : String.valueOf(val);
    }
    
    public Integer getIntParameterValue(String name, ExpressContext context, Object source, int defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(name, context, source);
        return (val instanceof Number) ? ((Number) val).intValue() : defaultValue;
    }
    
    public Long getLongParameterValue(String name, ExpressContext context, Object source, long defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(name, context, source);
        return (val instanceof Number) ? ((Number) val).longValue() : defaultValue;
    }
    
    public Float getFloatParameterValue(String name, ExpressContext context, Object source, float defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(name, context, source);
        return (val instanceof Number) ? ((Number) val).floatValue() : defaultValue;
    }
    
    public Double getDoubleParameterValue(String name, ExpressContext context, Object source, double defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(name, context, source);
        return (val instanceof Number) ? ((Number) val).doubleValue() : defaultValue;
    }
    
    public Boolean getBooleanParameterValue(String name, ExpressContext context, Object source, boolean defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(name, context, source);
        return (val instanceof Boolean) ? ((Boolean) val).booleanValue() : defaultValue;
    }
    
    @SuppressWarnings("unchecked")
    public <T> Collection<T> getCollectionParameterValue(String name, ExpressContext context, Object source, Collection<T> defaultValue) throws ExpressException
    {
        Object val = this.getParameterValue(name, context, source);
        return (val instanceof Collection) ? (Collection<T>) val : defaultValue;
    }
    
    public boolean isParamterConstant(String name)
    {
        Operator op = this.getParameter(name);
        return op != null && op.isConstant();
    }

    @Override
    public String toString()
    {
        StringBuffer ret = new StringBuffer();
        ret.append(this.getName());
        ret.append("(");
        boolean npa = false;
        for (Operator op : this.getParameters())
        {
            if (npa) ret.append(", ");
            ret.append(op.toString());
            npa = true;
        }
        for (Entry<String,Operator> nop : this.getNamedParameters().entrySet())
        {
            if (npa) ret.append(", ");
            ret.append(nop.getKey()).append(" = ").append(nop.getValue());
            npa = true;
        }
        ret.append(")");
        return ret.toString();
    }

    @Override
    public Converter<?> getConverter(ExpressContext context, Object source) throws ExpressException
    {
        throw new ExpressException("Cannot get converter,  expression contains a function");
    }

    @Override
    public Validator<?> getValidator(ExpressContext context, Object source) throws ExpressException
    {
        throw new ExpressException("Cannot get validator,  expression contains a function");
    }
    
    @Override
    public void set(ExpressContext context, Object value, Object source) throws ExpressException
    {
        throw new ExpressException("Cannot set value, expression contains a function");
    }

    @Override
    public boolean isConstant()
    {
        if (! this.isIdempotent()) return false;
        for (Operator op : this.getParameters())
        {
            if (! op.isConstant())
                return false;
        }
        for (Operator op : this.getNamedParameters().values())
        {
            if (! op.isConstant())
                return false;
        }
        return true;
    }
    
    /**
     * If given the same arguments does this function return the same result.
     * @return true if the result of this function is the same given the same arguments everytime it is invoked.
     */
    public boolean isIdempotent()
    {
        return false;
    }
}
