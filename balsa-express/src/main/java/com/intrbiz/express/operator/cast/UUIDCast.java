package com.intrbiz.express.operator.cast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.operator.Operator;

public class UUIDCast extends Function
{
    
    public UUIDCast()
    {
        super("uuid");
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Object get(ExpressContext context,Object source) throws ExpressException
    {
        context.checkOp();
        // generate a random uuid if called as uuid()
        if (this.getParameters().isEmpty())
        {
            return UUID.randomUUID();
        }
        // cast to uuid
        Operator op = this.getParameters().get(0) ;
        Object val = op.get(context,source);
        // case
        if (val instanceof String)
        {
            return UUID.fromString((String) val);
        }
        else if (val instanceof Iterable)
        {
            List<UUID> ret = new ArrayList<>();
            for (Object e : (Iterable) val)
            {
                if (e instanceof String)
                {
                    ret.add(UUID.fromString((String) e));
                }
            }
            return ret;
        }
        return null;
    }

    @Override
    public void set(ExpressContext context, Object value,Object source) throws ExpressException
    {
    }
    
    @Override
    public boolean isIdempotent()
    {
        return true;
    }
}
