package com.intrbiz.express.operator.function;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.operator.Operator;

public class IsNull extends Function
{

    public IsNull()
    {
        super("isnull");
    }

    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        context.checkOp();
        
        Operator ev = this.getParameters().get(0);
        Object evo = ev.get(context,source);
        return evo == null;
    }
    
    @Override
    public boolean isIdempotent()
    {
        return true;
    }
}
