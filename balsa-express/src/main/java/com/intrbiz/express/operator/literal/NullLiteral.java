package com.intrbiz.express.operator.literal;

import com.intrbiz.express.operator.Literal;

public class NullLiteral extends Literal
{
	
	public NullLiteral()
	{
		super("NullLiteral");
	}

	public Object getValue()
	{
		return null;
	}
}
