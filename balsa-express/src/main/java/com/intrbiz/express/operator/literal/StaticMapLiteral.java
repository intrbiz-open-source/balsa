package com.intrbiz.express.operator.literal;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.intrbiz.express.operator.Literal;

public class StaticMapLiteral extends Literal
{	
    protected LinkedHashMap<String, Object> elements = new LinkedHashMap<>();
    
	public StaticMapLiteral()
	{
	    super("{}");
	}
	
    @Override
    public Object getValue()
    {
        return this.elements;
    }

	public LinkedHashMap<String, Object> getElements()
    {
        return this.elements;
    }
	
	public void putElement(String name, Literal element)
	{
	    this.elements.put(name, element.getValue());
	}
	
	public void putElement(String name, Object element)
    {
        this.elements.put(name, element);
    }

    public void setElements(LinkedHashMap<String, Object> elements)
    {
        this.elements = elements;
    }

    @Override
    public String toString()
    {
        StringBuffer ret = new StringBuffer();
        ret.append("{");
        boolean npa = false;
        for (Entry<String, Object> element : this.elements.entrySet())
        {
            if (npa) ret.append(", ");
            ret.append(StringLiteral.format(element.getKey()));
            ret.append(": ");
            ret.append(Literal.of(element.getValue()).toString());
            npa = true;
        }
        ret.append("}");
        return ret.toString();
    }

}
