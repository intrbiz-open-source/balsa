package com.intrbiz.express.operator.literal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.intrbiz.express.operator.Literal;

/**
 * A specialised form of List literal where the elements are static literals
 */
public class StaticListLiteral extends Literal
{	
    protected List<Object> elements = new ArrayList<>();
    
	public StaticListLiteral()
	{
	    super("[]");
	}
	
	public StaticListLiteral(Collection<?> values)
    {
        this();
        if (values != null)
            this.elements.addAll(values);
    }
	
	public StaticListLiteral(Iterable<?> values)
    {
        this();
        for (Object value : values)
        {
            this.elements.add(value);
        }
    }

	@Override
    public Object getValue()
    {
        return this.elements;
    }

    public List<Object> getElements()
    {
        return this.elements;
    }
	
	public void addElement(Literal element)
	{
	    this.elements.add(element.getValue());
	}
	
	public void addElement(Object element)
    {
        this.elements.add(element);
    }

    public void setElements(List<Object> elements)
    {
        this.elements = elements;
    }

    @Override
    public String toString()
    {
        StringBuffer ret = new StringBuffer();
        ret.append("[");
        boolean npa = false;
        for (Object e : this.elements)
        {
            if (npa) ret.append(", ");
            // format the static value using our built in literal types
            ret.append(Literal.of(e).toString());
            npa = true;
        }
        ret.append("]");
        return ret.toString();
    }
}
