package com.intrbiz.express.operator.entity;

import java.util.Objects;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Operator;

public class Var extends Operator
{
    private final String varName;
    
    private final boolean export;

	public Var(String value, boolean export)
	{
		super("var");
		this.varName = Objects.requireNonNull(value);
		this.export = export;
	}
	
	public String getVarName()
	{
	    return this.varName;
	}

	@Override
	public Object get(ExpressContext context, Object source) throws ExpressException
	{
        if (this.export)
            context.exportEntity(this.varName, context.getEntity(varName, source), source);
	    return null;
	}

	@Override
	public void set(ExpressContext context, Object value, Object source) throws ExpressException
	{
	    if (this.export)
	        context.exportEntity(this.varName, value, source);
	    else
	        context.declareEntity(this.varName, value, source);
	}

    @Override
    public String toString()
    {
        return (this.export ? "export " : "var ") + this.varName;
    }
}
