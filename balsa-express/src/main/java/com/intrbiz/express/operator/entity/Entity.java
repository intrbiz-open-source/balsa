package com.intrbiz.express.operator.entity;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Literal;
import com.intrbiz.express.operator.Operator;
import com.intrbiz.express.util.EntityChaining;
import com.intrbiz.express.value.ValueExpression;
import com.intrbiz.validation.converter.Converter;
import com.intrbiz.validation.validator.Validator;

public class Entity extends Literal
{
	private String value;

	public Entity(String name)
	{
		super("EntityName");
		this.value = name;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
	
	protected Object tryBuiltInEntity(String name, Object source)
	{
	    switch (name)
	    {
	        case "_":           return source;
	        case "Math":        return Math.class;
	        case "Arrays":      return Arrays.class;
	        case "Collections": return Collections.class;
	        case "Collectors":  return Collectors.class;
	        case "Stream":      return Stream.class;
	    }
	    return null;
	}
	
	protected Object getEntity(ExpressContext context, Object source)
	{
	    String name = this.getValue();
	    // Try built ins
	    Object obj = this.tryBuiltInEntity(name, source);
	    // Go to the context
	    if (obj == null)
	    {
    	    obj = context.getEntity(name, source);
            // Allow for entites to be proxied
            if (obj instanceof ExpressContext)
            {
                return ((ExpressContext) obj).getEntity(name, source);
            }
            // Allow for expression chaining
            return EntityChaining.resolve(context, source, obj);
	    }
        return obj;
	}

	@Override
	public Object get(ExpressContext context, Object source) throws ExpressException
	{
	    context.checkOp();
	    return this.getEntity(context, source);
	}

	@Override
	public void set(ExpressContext context, Object value, Object source) throws ExpressException
	{
	    context.checkOp();
		Object obj = this.getEntity(context, source);
		// Allow for chaining of expressions
		if (obj instanceof ValueExpression)
		{
			((ValueExpression) obj).set(context, EntityChaining.resolve(context, source, value), source);
		}
		else if (obj instanceof Operator)
		{
			((Operator) obj).set(context, EntityChaining.resolve(context, source, value), source);
		}
		else
		{
		    // create the entity within the context
		    context.setEntity(this.getValue(), EntityChaining.resolve(context, source, value), source);
		}
	}

	@Override
	public Converter<?> getConverter(ExpressContext context, Object source) throws ExpressException
	{
	    Object obj = this.getEntity(context, source);
		// Allow for chaining of expressions
		if (obj instanceof ValueExpression)
		{
			return ((ValueExpression) obj).getConverter(context, source);
		}
		else if (obj instanceof Operator)
		{
			return ((Operator) obj).getConverter(context, source);
		}
		return null;
	}

	@Override
	public Validator<?> getValidator(ExpressContext context, Object source) throws ExpressException
	{
	    Object obj = this.getEntity(context, source);
		// Allow for chaining of expressions
		if (obj instanceof ValueExpression)
		{
			return ((ValueExpression) obj).getValidator(context, source);
		}
		else if (obj instanceof Operator)
		{
			return ((Operator) obj).getValidator(context, source);
		}
		return null;		
	}
	
	@Override
	public boolean isConstant()
	{
	    return false;
	}
}
