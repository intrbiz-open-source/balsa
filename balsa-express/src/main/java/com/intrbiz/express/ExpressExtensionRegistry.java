package com.intrbiz.express;

import java.util.Collection;
import java.util.Collections;
import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.intrbiz.express.operator.Decorator;
import com.intrbiz.express.operator.Function;

/**
 * A registry of Express extensions
 */
public class ExpressExtensionRegistry implements ExpressExtensionLibrary
{
    private static final ExpressExtensionRegistry DEFAULT = new ExpressExtensionRegistry()
                                                                    .scanExtensionLibraries();
    
    public static final ExpressExtensionRegistry getDefaultRegistry()
    {
        return DEFAULT;
    }
    
    protected final ConcurrentMap<String, FunctionFactory> functions = new ConcurrentHashMap<>();
    
    protected final ConcurrentMap<DecoratorName, DecoratorFactory> decorators = new ConcurrentHashMap<>();
    
    protected final ConcurrentMap<Class<?>, ExpressExtensionLibrary> subRegistries = new ConcurrentHashMap<>();
    
    public ExpressExtensionRegistry()
    {
        super();
    }
    
    public ExpressExtensionRegistry(ExpressExtensionRegistry... subRegistries)
    {
        this();
        for (ExpressExtensionRegistry reg : subRegistries)
        {
            this.addSubRegistry(reg);
        }
    }
    
    /**
     * Scan the class path for {@link ExpressExtensionLibrary} libraries using {@link ServiceLoader}
     * @return this extension registry with all discovered libraries registered
     */
    public ExpressExtensionRegistry scanExtensionLibraries()
    {
        for (ExpressExtensionLibrary library : ServiceLoader.load(ExpressExtensionLibrary.class))
        {
            this.addSubRegistry(library);
        }
        return this;
    }
    
    public Collection<ExpressExtensionLibrary> getSubRegistries()
    {
        return Collections.unmodifiableCollection(this.subRegistries.values());
    }
    
    public ExpressExtensionRegistry addSubRegistry(ExpressExtensionLibrary registry)
    {
        this.subRegistries.put(registry.getClass(), registry);
        return this;
    }
    
    public ExpressExtensionRegistry addFunction(String name, final Class<? extends Function> functionClass)
    {
        return this.addFunction(name, new FunctionFactory() {
            @Override
            public Function loadFunction()
            {
                try
                {
                    return functionClass.newInstance();
                }
                catch (InstantiationException | IllegalAccessException e)
                {
                }
                return null;
            }
        });
    }
    
    public ExpressExtensionRegistry addFunction(final Function immutableFunction)
    {
        return this.addFunction(immutableFunction.getName(), new FunctionFactory() {
            @Override
            public Function loadFunction()
            {
                return immutableFunction;
            }
        });
    }
    
    public ExpressExtensionRegistry addFunction(String name, FunctionFactory factory)
    {
        this.functions.put(name.toLowerCase(), factory);
        return this;
    }
    
    @Override
    public boolean containsFunction(String name)
    {
        String lName = name.toLowerCase();
        if (this.functions.containsKey(lName)) return true;
        for (ExpressExtensionLibrary reg : this.subRegistries.values())
        {
            if (reg.containsFunction(lName)) return true;
        }
        return false;
    }
    
    @Override
    public Function loadFunction(String name)
    {
        String lName = name.toLowerCase();
        FunctionFactory ff = this.functions.get(lName);
        if (ff != null) return ff.loadFunction();
        // try the subs
        for (ExpressExtensionLibrary sub : this.subRegistries.values())
        {
            Function f = sub.loadFunction(lName);
            if (f != null) return f;
        }
        return null;
    }
    
    public ExpressExtensionRegistry addDecorator(String name, Class<?> entityType, final Class<? extends Decorator> decoratorClass)
    {
        return this.addDecorator(name, entityType, new DecoratorFactory(){
            @Override
            public Decorator loadDecorator()
            {
                try
                {
                    return decoratorClass.newInstance();
                }
                catch (InstantiationException | IllegalAccessException e)
                {
                }
                return null;
            }            
        });
    }
    
    public ExpressExtensionRegistry addDecorator(String name, Class<?> entityType, DecoratorFactory factory)
    {
        this.decorators.put(new DecoratorName(name, entityType), factory);
        return this;
    }
    
    @Override
    public boolean containsDecorator(String name, Class<?> entityType)
    {
        if (this.decorators.containsKey(new DecoratorName(name, entityType))) return true;
        for (ExpressExtensionLibrary sub : this.subRegistries.values())
        {
            if (sub.containsDecorator(name, entityType)) return true;
        }
        return false;
    }
    
    @Override
    public Decorator loadDecorator(String name, Class<?> entityType)
    {
        DecoratorFactory df = this.decorators.get(new DecoratorName(name, entityType));
        if (df != null) return df.loadDecorator();
        // subs
        for (ExpressExtensionLibrary sub : this.subRegistries.values())
        {
            Decorator d = sub.loadDecorator(name, entityType);
            if (d != null) return d;
        }
        return null;
    }
    
    public static interface FunctionFactory
    {
        Function loadFunction();
    }
    
    public static interface DecoratorFactory
    {
        Decorator loadDecorator();
    }
    
    public static final class DecoratorName
    {
        public final String name;
        public final Class<?> entityType;
        
        public DecoratorName(String name, Class<?> entityType)
        {
            super();
            this.name = name.toLowerCase();
            this.entityType = entityType;
        }

        @Override
        public int hashCode()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            DecoratorName other = (DecoratorName) obj;
            if (entityType == null)
            {
                if (other.entityType != null) return false;
            }
            else if (entityType != other.entityType) return false;
            if (name == null)
            {
                if (other.name != null) return false;
            }
            else if (!name.equals(other.name)) return false;
            return true;
        }
        
        public String toString()
        {
            return this.entityType + ":" + this.name;
        }
    }
    
    @Override
    public String toString(String indent)
    {
        StringBuilder sb = new StringBuilder();
        //
        sb.append(indent).append("ExpressExtensionRegistry ").append(this.getClass().getSimpleName()).append(" {\r\n");
        sb.append(indent).append("  Functions: [\r\n");
        for (String name : this.functions.keySet())
        {
            sb.append(indent).append("    ").append(name).append(",\r\n");
        }
        sb.append(indent).append("  ]\r\n");
        sb.append(indent).append("  Decorators: [\r\n");
        for (DecoratorName name : this.decorators.keySet())
        {
            sb.append(indent).append("    ").append(name).append(",\r\n");
        }
        sb.append(indent).append("  ]\r\n");
        sb.append(indent).append("  SubRegistries: [\r\n");
        for (ExpressExtensionLibrary sub : this.subRegistries.values())
        {
            sb.append( sub.toString(indent + "    ") ).append(",\r\n");
        }
        sb.append(indent).append("  ]\r\n");
        sb.append(indent).append("}");
        //
        return sb.toString();
    }
    
    @Override
    public String toString()
    {
        return this.toString("");
    }
}
