package com.intrbiz.express;

import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;

import com.intrbiz.express.action.ActionHandler;

/**
 * Resolve entities for Express
 */
public interface ExpressEntityResolver
{
    default Object getEntity(String name, Object source)
    {
        return null;
    }
    
    default void setEntity(String name, Object in, Object source, boolean export)
    {
    }
    
    default ActionHandler getAction(String name, Object source)
    {
        return null;
    }
    
    // factory methods
    
    /**
     * An entity resolver which doesn't look up anything
     */
    public static ExpressEntityResolver EMPTY = new ExpressEntityResolver() {};
    
    public static ExpressEntityResolver empty()
    {
        return EMPTY;
    }
    
    
    /**
     * A simple entity resolver which returns the expression source always
     */
    public static ExpressEntityResolver SOURCE = new ExpressEntityResolver() {
        
        public Object getEntity(String name, Object source)
        {
            return source;
        }
        
    };
    
    public static ExpressEntityResolver source()
    {
        return SOURCE;
    }
    
    public static ExpressEntityResolver of(final BiFunction<String, Object, Object> entityResolver)
    {
        return new ExpressEntityResolver() {
            
            public Object getEntity(String name, Object source)
            {
                return entityResolver.apply(name, source);
            }
        };
    }
    
    public static ExpressEntityResolver of(final Map<String, Object> entities)
    {
        return new ExpressEntityResolver() {
            
            public Object getEntity(String name, Object source)
            {
                return entities.get(name);
            }
        };
    }
    
    public static ExpressEntityResolver of(final String entityName, final Object value)
    {
        return new ExpressEntityResolver() {
            
            public Object getEntity(String name, Object source)
            {
                return (entityName.equals(name)) ? value : null;
            }
        };
    }
    
    public static ExpressEntityResolver of(final ExpressEntityResolver child, final ExpressEntityResolver parent)
    {
        Objects.requireNonNull(child);
        Objects.requireNonNull(parent);
        return new ExpressEntityResolver() {
            public Object getEntity(String name, Object source)
            {
                Object o = child.getEntity(name, source);
                if (o == null)
                    o = parent.getEntity(name, source);
                return o;
            }
            
            public ActionHandler getAction(String name, Object source)
            {
                ActionHandler a = child.getAction(name, source);
                if (a == null)
                    a = parent.getAction(name, source);
                return a;
            }
        };
    }
}
