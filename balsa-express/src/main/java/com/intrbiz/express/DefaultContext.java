package com.intrbiz.express;

import java.io.Writer;
import java.time.ZoneId;
import java.util.Locale;

import com.intrbiz.express.access.JavaAccessController;
import com.intrbiz.express.action.ActionHandler;
import com.intrbiz.express.operator.Decorator;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.security.Limiter;
import com.intrbiz.express.stack.ELStatementFrame;
import com.intrbiz.express.template.filter.ContentFilter;
import com.intrbiz.express.template.filter.ContentFilterRegistry;
import com.intrbiz.express.template.io.TemplateWriter;
import com.intrbiz.express.template.loader.TemplateLoader;

public class DefaultContext implements ExpressContext
{
    // context config
    
    private final ExpressExtensionRegistry extensions;

    private final ExpressEntityResolver resolver;
    
    private final Limiter iterationLimiter;
    
    private final Limiter opLimiter;
    
    private final boolean allowSetAccessible;
    
    private final boolean suppressMethodExceptions;
    
    private final JavaAccessController javaAccessController;
    
    private final TemplateLoader templateLoader;
    
    private final ContentFilterRegistry contentFilterRegistry;
    
    // state
    
    private boolean caching;
    
    private Locale currentLocale;
    
    private ZoneId currentTimeZone;
    
    private final ELStatementFrame root = new ELStatementFrame();

    private ELStatementFrame frame = root;
    
    private TemplateWriter writer;

    DefaultContext(ExpressExtensionRegistry extensions, ExpressEntityResolver resolver, Limiter iterationLimiter, Limiter opLimiter, boolean caching, boolean allowSetAccessible, boolean suppressMethodExceptions, JavaAccessController javaAccessController, Locale currentLocale, ZoneId currentTimeZone, TemplateLoader templateLoader, ContentFilterRegistry contentFilterRegistry)
    {
        super();
        this.extensions = extensions;
        this.resolver = resolver;
        this.iterationLimiter = iterationLimiter;
        this.opLimiter = opLimiter;
        this.caching = caching;
        this.allowSetAccessible = allowSetAccessible;
        this.suppressMethodExceptions = suppressMethodExceptions;
        this.javaAccessController = javaAccessController;
        this.currentLocale = currentLocale;
        this.currentTimeZone = currentTimeZone;
        this.templateLoader = templateLoader;
        this.contentFilterRegistry = contentFilterRegistry;
    }

    @Override
    public Object getEntity(String name, Object source)
    {
        if (this.frame != null && this.frame.containsEntity(name))
        {
            return this.frame.getEntity(name);
        }
        else if (this.root.containsEntity(name)) { return this.root.getEntity(name); }
        return this.getEntityInner(name, source);
    }

    protected Object getEntityInner(String name, Object source)
    {
        if (this.resolver == null) return null;
        return this.resolver.getEntity(name, source);
    }

    @Override
    public ActionHandler getAction(String name, Object source)
    {
        if (this.resolver == null) return null;
        return this.resolver.getAction(name, source);
    }

    @Override
    public void setEntity(String name, Object value, Object source)
    {
        if (this.resolver != null)
            this.resolver.setEntity(name, value, source, false);
        // set the entity
        if (this.frame != null)
            this.frame.setEntity(name, value);
        else
            this.root.setEntity(name, value);
    }

    @Override
    public void declareEntity(String name, Object value, Object source)
    {        
        if (this.frame != null)
            this.frame.declareEntity(name, value);
        else
            this.root.declareEntity(name, value);        
    }

    @Override
    public void exportEntity(String name, Object value, Object source)
    {
        if (this.resolver != null)
            this.resolver.setEntity(name, value, source, true);
        // Export the entity to the root of the stack
        this.root.setEntity(name, value);
    }

    // default implementations to make like simpler

    @Override
    public Function getCustomFunction(String name)
    {
        if (this.extensions == null) return null;
        return this.extensions.loadFunction(name);
    }

    @Override
    public Decorator getCustomDecorator(String name, Class<?> entityType)
    {
        if (entityType == null) return null;
        if (this.extensions == null) return null;
        // can we find a decorator for the given class
        Decorator decorator = this.extensions.loadDecorator(name, entityType);
        if (decorator != null) return decorator;
        // check interfaces
        for (Class<?> iface : entityType.getInterfaces())
        {
            decorator = this.getCustomDecorator(name, iface);
            if (decorator != null) return decorator;    
        }
        // super class
        decorator = this.getCustomDecorator(name, entityType.getSuperclass());
        return decorator;
    }
    
    @Override
    public ExpressExtensionRegistry getExpressExtensionRegistry()
    {
        return this.extensions;
    }

    // Stack Control

    @Override
    public void enterFrame(boolean function, boolean lookupParent)
    {
        this.frame = new ELStatementFrame(this.frame, function, lookupParent);
    }

    @Override
    public void exitFrame()
    {
        if (this.frame != null)
        {
            if (this.frame.getParent() == null) throw new IllegalStateException("You cannot remove the root stack frame!");
            this.frame = this.frame.getParent();
        }
    }

    @Override
    public ELStatementFrame getFrame()
    {
        return this.frame;
    }

    @Override
    public void checkIteration()
    {
        this.iterationLimiter.check();
    }

    @Override
    public void checkOp()
    {
        this.opLimiter.check();
    }

    @Override
    public boolean allowSetAccessible()
    {
        return this.allowSetAccessible;
    }

    @Override
    public boolean suppressMethodExceptions()
    {
        return this.suppressMethodExceptions;
    }
    
    @Override
    public boolean checkJavaAccess(String className)
    {
        if (this.javaAccessController == null)
            return true;
        // check the java class trying to be accessed
        if (className != null)
        {
            Boolean result = this.javaAccessController.checkJavaAccess(className);
            if (result != null)
                return result;
            // check package wildcards
            int index = className.lastIndexOf('.');
            while (index > 0)
            {
                result = this.javaAccessController.checkJavaAccess(className.substring(0, index) + ".*");
                if (result != null)
                    return result;
                index = className.lastIndexOf('.', index - 1);
            }
            // check global wildcard
            result = this.javaAccessController.checkJavaAccess("*");
            if (result != null)
                return result;
        }
        return false;
    }

    @Override
    public boolean isCaching()
    {
        return this.caching;
    }

    @Override
    public void setCaching(boolean caching)
    {
        this.caching = caching;
    }
    
    @Override
    public TemplateLoader getTemplateLoader()
    {
        return this.templateLoader;
    }

    @Override
    public ContentFilterRegistry getContentFilterRegistry()
    {
        return this.contentFilterRegistry;
    }
    
    @Override
    public TemplateWriter getWriter()
    {
        return this.writer;
    }
    
    @Override
    public void setupWriter(Writer to, ContentFilter defaultFilter)
    {
        this.writer = new TemplateWriter(this, to, defaultFilter);
    }
    
    @Override
    public void clearWriter()
    {
        this.writer = null;
    }
    
    @Override
    public Locale getCurrentLocale()
    {
        return this.currentLocale;
    }
    
    @Override
    public DefaultContext withCurrentLocale(Locale locale)
    {
        this.currentLocale = locale;
        return this;
    }
    
    @Override
    public ZoneId getCurrentTimeZone()
    {
        return this.currentTimeZone;
    }
    
    @Override
    public DefaultContext withCurrentTimeZone(ZoneId zone)
    {
        this.currentTimeZone = zone;
        return this;
    }
}
