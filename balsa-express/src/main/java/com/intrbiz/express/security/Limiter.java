package com.intrbiz.express.security;

import com.intrbiz.express.ExpressException;

public final class Limiter implements Hidden
{
    private static final int DEFAULT_ITERATION_LIMIT = 10_000;
    
    private static final int DEFAULT_OP_LIMIT = 250_000;
    
    public static final Limiter defaultIterationLimit()
    {
        return new Limiter(DEFAULT_ITERATION_LIMIT);
    }
    
    public static final Limiter defaultOpLimit()
    {
        return new Limiter(DEFAULT_OP_LIMIT);
    }
    
    public static final Limiter of(int limit)
    {
        return new Limiter(limit);
    }
    
    private final int limit;
    
    private int count;
    
    public Limiter(int limit)
    {
        super();
        this.limit = limit;
        this.count = 0;
    }
    
    public void reset()
    {
        this.count = limit;
    }
    
    public void check()
    {
        this.count++;
        if (this.count > this.limit)
            throw new ExpressException("Script op limit reached");
    }
}
