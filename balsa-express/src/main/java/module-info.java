module com.intrbiz.balsa.express {
    
    requires java.scripting;
    requires java.logging;
    
    requires org.slf4j;
    requires com.intrbiz.balsa.util;
    requires com.intrbiz.balsa.validator;
    
    exports com.intrbiz.express;
    exports com.intrbiz.express.access;
    exports com.intrbiz.express.action;
    exports com.intrbiz.express.dynamic;
    exports com.intrbiz.express.functions;
    exports com.intrbiz.express.functions.collection;
    exports com.intrbiz.express.functions.script;
    exports com.intrbiz.express.functions.text;
    exports com.intrbiz.express.functions.util;
    exports com.intrbiz.express.operator;
    exports com.intrbiz.express.operator.bit;
    exports com.intrbiz.express.operator.cast;
    exports com.intrbiz.express.operator.collections;
    exports com.intrbiz.express.operator.comparison;
    exports com.intrbiz.express.operator.entity;
    exports com.intrbiz.express.operator.function;
    exports com.intrbiz.express.operator.lambda;
    exports com.intrbiz.express.operator.literal;
    exports com.intrbiz.express.operator.logical;
    exports com.intrbiz.express.operator.math;
    exports com.intrbiz.express.operator.text;
    exports com.intrbiz.express.security;
    exports com.intrbiz.express.stack;
    exports com.intrbiz.express.statement;
    exports com.intrbiz.express.statement.branch;
    exports com.intrbiz.express.statement.control;
    exports com.intrbiz.express.statement.function;
    exports com.intrbiz.express.statement.loop;
    exports com.intrbiz.express.statement.text;
    exports com.intrbiz.express.template;
    exports com.intrbiz.express.template.loader;
    exports com.intrbiz.express.template.io;
    exports com.intrbiz.express.template.filter;
    exports com.intrbiz.express.util;
    exports com.intrbiz.express.value;
    
    uses com.intrbiz.express.ExpressExtensionLibrary;
    
    provides com.intrbiz.express.ExpressExtensionLibrary with
            com.intrbiz.express.functions.ExpressBuiltinFunctionRegistry;
    
}