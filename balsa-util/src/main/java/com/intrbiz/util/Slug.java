package com.intrbiz.util;

/**
 * Generate a URL slug from the given input
 */
public final class Slug
{
    private Slug()
    {
    }
    
    /**
     * Generate a safe URL name from the given input
     * @param name the name to process
     * @param slug the slug to append to
     */
    public final static void generateSlug(String name, StringBuilder slug, boolean allowSlash)
    {
        if (name == null || name.length() == 0) return;
        boolean lastCharValid = false;
        name = name.trim();
        for (int i = 0; i < name.length(); i++)
        {
            char c = Character.toLowerCase(name.charAt(i));
            if (('a' <= c && c <= 'z') || ('0' <= c && c <= '9'))
            {
                lastCharValid = true;
                slug.append(c);
            }
            else if (c == '/')
            {
                if (allowSlash) slug.append('/');
                else if (lastCharValid) slug.append("-");
            }
            else if (c == ' ' || c == '-' || c == '_' || c == '\\' || c == '.' || c == '~' || c == '+' || c == '=' || c == '&' || c == '|' || c == '>' || c == '<')
            {
                if (lastCharValid) slug.append("-");
            }
            else
            {
                lastCharValid = false;
            }
        }
    }
    
    public final static void generateSlug(String name, StringBuilder slug)
    {
        generateSlug(name, slug, false);
    }
    
    public final static String generateSlug(String name)
    {
        StringBuilder slug = new StringBuilder();
        generateSlug(name, slug, false);
        return slug.toString();
    }
    
    public final static String generateSlug(String name, boolean allowSlash)
    {
        StringBuilder slug = new StringBuilder();
        generateSlug(name, slug, allowSlash);
        return slug.toString();
    }
}
