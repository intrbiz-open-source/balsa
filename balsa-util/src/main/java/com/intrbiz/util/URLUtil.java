package com.intrbiz.util;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Map.Entry;

public final class URLUtil
{
    private URLUtil()
    {
    }
    
    public final static String buildURL(String baseUrl, Map<?, ?> query, String fragment)
    {
        // construct the URL
        StringBuilder url = new StringBuilder();
        // the base href
        if (! Util.isEmpty(baseUrl)) url.append(baseUrl);
        // query string
        if (query != null && ! query.isEmpty())
        {
            if (baseUrl != null && baseUrl.contains("?")) url.append("&");
            else url.append("?");
            boolean ns = false;
            for (Entry<?, ?> param : query.entrySet())
            {
                if (ns) url.append("&");
                try
                {
                    url.append(URLEncoder.encode(param.getKey().toString(), StandardCharsets.UTF_8))
                        .append("=")
                        .append((param.getValue() == null) ? "" : URLEncoder.encode(param.getValue().toString(), StandardCharsets.UTF_8));
                }
                catch (NullPointerException e)
                {
                }
                ns = true;
            }
        }
        // fragement
        if (! Util.isEmpty(fragment)) url.append("#").append(fragment);
        // build
        return url.toString();
    }
    
    public final static String buildURL(String baseUrl, Map<?, ?> query)
    {
        return buildURL(baseUrl, query, null);
    }
}
