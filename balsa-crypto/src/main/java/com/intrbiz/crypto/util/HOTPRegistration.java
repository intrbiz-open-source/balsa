package com.intrbiz.crypto.util;

import com.intrbiz.crypto.util.HOTP.HOTPSecret;
import com.intrbiz.crypto.util.HOTP.HOTPState;

public interface HOTPRegistration
{
    /**
     * The secret for this HOTP registration
     */
    HOTPSecret getHOTPSecret();
    
    /**
     * The current HTOP state
     */
    <T extends HOTPState> T getHOTPState();
}
