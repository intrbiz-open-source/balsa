package com.intrbiz.balsa.test.route;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.engine.impl.route.Route;
import com.intrbiz.balsa.engine.impl.route.exec.ExecBuilder;
import com.intrbiz.balsa.test.route.model.RestrictedRouter;
import com.intrbiz.balsa.test.route.model.TestRouter;

public class ExecBuilderTests
{
    public static void main(String[] args) throws Exception
    {
        BalsaApplication app = new BalsaApplication() {
            @Override
            protected void setupRouters() throws Exception
            {
            }
        };
        //
        RestrictedRouter rr = new RestrictedRouter();
        //
        for (Route r : Route.fromRouter("/", app, rr))
        {
            System.out.println("Route: " + r);
            System.out.println("Handler:\r\n" + ExecBuilder.build(r).writeClass());
            System.out.println(ExecBuilder.build(r).executor());
        }
        //        
        TestRouter tr = new TestRouter();
        for (Route r : Route.fromRouter("/", app, tr))
        {
            System.out.println("Route: " + r);
            System.out.println("Handler:\r\n" + ExecBuilder.build(r).writeClass());
            System.out.println(ExecBuilder.build(r).executor());
        }
    }
}
