package com.intrbiz.balsa.test.route.model;

import java.util.Date;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.engine.route.Router;
import com.intrbiz.balsa.metadata.Any;
import com.intrbiz.balsa.metadata.Cookie;
import com.intrbiz.balsa.metadata.Get;
import com.intrbiz.balsa.metadata.Header;
import com.intrbiz.balsa.metadata.Param;
import com.intrbiz.balsa.metadata.Post;
import com.intrbiz.balsa.metadata.Prefix;
import com.intrbiz.balsa.metadata.XML;
import com.intrbiz.validation.metadata.AsDate;
import com.intrbiz.validation.metadata.IsaDate;
import com.intrbiz.validation.metadata.IsaInt;
import com.intrbiz.validation.metadata.IsaUUID;

@Prefix("/")
public class TestRouter extends Router<BalsaApplication>
{
    @Get("/test/:param")
    public void test(String param)
    {
    }

    @Get("/test1")
    public void test1(@Param("a") String a, @Header("b") String b, @Cookie("c") String c)
    {
    }

    @Any("/test2")
    @XML
    public XMLObj test2(@Param("a") String param)
    {
        return new XMLObj();
    }

    @Post("/test3")
    @XML
    public XMLObj test3(@Param("a") String param, @XML XMLObj in)
    {
        return in;
    }
    
    @Get("/test4/:param")
    public void test4(@Param("param") String param)
    {
    }
    
    @Get("/test/asuuid/:param")
    public void testAsUUID(@Param("param") @IsaUUID(mandatory = true) UUID param)
    {
    }
    
    @Get("/test/asint/:param")
    public void testAsInt(@IsaInt int param)
    {
    }
    
    @Get("/test/asint/:param")
    public void testAsDate(@AsDate("yyyy-MM-dd") @IsaDate Date param)
    {
    }

    @XmlType(name = "test")
    @XmlRootElement(name = "test")
    public static class XMLObj
    {
        private String stat = "OK";

        @XmlElement(name = "stat")
        public String getStat()
        {
            return stat;
        }

        public void setStat(String stat)
        {
            this.stat = stat;
        }
    }
}
