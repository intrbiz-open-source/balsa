module com.intrbiz.balsa.core {
    
    requires transitive java.xml;
    requires transitive java.xml.bind;
    
    requires org.slf4j;
    requires ch.qos.reload4j;
    
    requires io.netty.all;
    
    requires transitive com.fasterxml.jackson.core;
    requires transitive com.fasterxml.jackson.annotation;
    requires transitive com.fasterxml.jackson.databind;
    requires transitive com.fasterxml.jackson.dataformat.yaml;
    requires transitive com.fasterxml.jackson.datatype.jdk8;
    requires transitive com.fasterxml.jackson.datatype.jsr310;
    requires transitive com.fasterxml.jackson.module.paramnames;
    requires transitive org.yaml.snakeyaml;
    
    requires org.apache.commons.codec;
    
    /* Currently based on JAR name */
    requires transitive commons.fileupload;
    requires commons.io;
    requires servlet.api;
    
    requires transitive com.intrbiz.balsa.config;
    requires transitive com.intrbiz.balsa.validator;
    requires transitive com.intrbiz.balsa.express;
    requires transitive com.intrbiz.balsa.scgi;
    requires transitive com.intrbiz.balsa.metrics;
    requires transitive com.intrbiz.balsa.crypto;
    requires transitive com.intrbiz.balsa.util;
    requires com.intrbiz.balsa.util.compiler;
    
    exports com.intrbiz.balsa;
    exports com.intrbiz.balsa.action;
    exports com.intrbiz.balsa.bean;
    exports com.intrbiz.balsa.component;
    exports com.intrbiz.balsa.engine;
    exports com.intrbiz.balsa.engine.publicresource;
    exports com.intrbiz.balsa.engine.route;
    exports com.intrbiz.balsa.engine.security;
    exports com.intrbiz.balsa.engine.security.challenge;
    exports com.intrbiz.balsa.engine.security.credentials;
    exports com.intrbiz.balsa.engine.security.info;
    exports com.intrbiz.balsa.engine.security.method;
    exports com.intrbiz.balsa.engine.session;
    exports com.intrbiz.balsa.engine.task;
    exports com.intrbiz.balsa.engine.view;
    exports com.intrbiz.balsa.engine.view.source;
    exports com.intrbiz.balsa.error;
    exports com.intrbiz.balsa.error.http;
    exports com.intrbiz.balsa.error.security;
    exports com.intrbiz.balsa.error.view;
    exports com.intrbiz.balsa.listener;
    exports com.intrbiz.balsa.metadata;
    exports com.intrbiz.balsa.metadata.doc;
    exports com.intrbiz.balsa.view.component;
    exports com.intrbiz.balsa.view.renderer;
    exports com.intrbiz.balsa.view.parser;
    exports com.intrbiz.balsa.view.library;
    exports com.intrbiz.balsa.view.library.registry;
    exports com.intrbiz.balsa.view.library.impl;
    exports com.intrbiz.balsa.view.core.library;
    
    /* Internals exported for specific plugins */
    exports com.intrbiz.balsa.view.core to com.intrbiz.balsa.markdown, com.intrbiz.balsa.apt;
    exports com.intrbiz.balsa.view.core.data to com.intrbiz.balsa.markdown, com.intrbiz.balsa.apt;
    exports com.intrbiz.balsa.view.core.generic to com.intrbiz.balsa.markdown, com.intrbiz.balsa.apt;
    exports com.intrbiz.balsa.view.core.html to com.intrbiz.balsa.markdown, com.intrbiz.balsa.apt;
    exports com.intrbiz.balsa.view.core.security to com.intrbiz.balsa.markdown, com.intrbiz.balsa.apt;
    exports com.intrbiz.balsa.view.core.template to com.intrbiz.balsa.markdown, com.intrbiz.balsa.apt;
    exports com.intrbiz.balsa.engine.impl.view.metadata to com.intrbiz.balsa.markdown, com.intrbiz.balsa.apt;
    exports com.intrbiz.balsa.express to com.intrbiz.balsa.markdown, com.intrbiz.balsa.apt;
    
    exports com.intrbiz.balsa.engine.impl.route.exec to com.intrbiz.balsa.data;
    exports com.intrbiz.balsa.engine.impl.route.exec.argument to com.intrbiz.balsa.data;
    exports com.intrbiz.balsa.engine.impl.route.exec.wrapper to com.intrbiz.balsa.data;
    
    exports com.intrbiz.balsa.engine.impl.session to com.intrbiz.balsa.hazelcast;
    
    uses com.intrbiz.balsa.BalsaPlugin;
    uses com.intrbiz.balsa.view.library.RenderLibrary;
    uses com.intrbiz.balsa.view.library.ComponentLibrary;

    provides com.intrbiz.express.ExpressExtensionLibrary with
            com.intrbiz.balsa.express.BalsaBuiltinFunctionRegistry;
    
    provides com.intrbiz.balsa.view.library.RenderLibrary with
            com.intrbiz.balsa.view.core.library.HTMLRenderLibrary;
    
    provides com.intrbiz.balsa.view.library.ComponentLibrary with
            com.intrbiz.balsa.view.core.library.CoreComponentLibrary;
}
