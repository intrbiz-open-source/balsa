package com.intrbiz.balsa.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.intrbiz.balsa.engine.impl.route.exec.argument.JSONArgument;
import com.intrbiz.balsa.engine.impl.route.exec.response.JSONResponse;
import com.intrbiz.balsa.scgi.http.HTTP.HTTPStatus;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.PARAMETER })
@IsResponse(JSONResponse.class)
@IsArgument(JSONArgument.class)
public @interface JSON
{
    Class<?>[] value() default {};
    
    /**
     * The HTTP response status
     */
    HTTPStatus status() default HTTPStatus.OK;
    
    /**
     * If the method returns null, set the HTTP response status to 404 not found
     */
    boolean notFoundIfNull() default false;
    
    /**
     * Optional view to configure the writer with, for selecting different properties, see: {link @JsonView}
     */
    Class<?>[] view() default {};
}
