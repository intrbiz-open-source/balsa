package com.intrbiz.balsa.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.intrbiz.balsa.engine.impl.route.Route.InternalRouteBuilder;

/**
 * Define that this method can handle internal request of the given pattern, and is not directly exposed externally
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@IsRoute(InternalRouteBuilder.class)
public @interface Internal {
    String value();
    boolean regex() default false;
    String[] as() default {};
}
