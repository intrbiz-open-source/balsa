package com.intrbiz.balsa.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.intrbiz.balsa.engine.impl.route.exec.argument.YAMLArgument;
import com.intrbiz.balsa.engine.impl.route.exec.response.YAMLResponse;
import com.intrbiz.balsa.scgi.http.HTTP.HTTPStatus;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.PARAMETER })
@IsResponse(YAMLResponse.class)
@IsArgument(YAMLArgument.class)
public @interface YAML
{
    Class<?>[] value() default {};
    
    /**
     * The HTTP response status
     * @return
     */
    HTTPStatus status() default HTTPStatus.OK;
    
    /**
     * If the method returns null, set the HTTP response satus to 404 not found
     * @return
     */
    boolean notFoundIfNull() default false;
    
    /**
     * Optional view to configure the writer with, for selecting different properties, see: {link @JsonView}
     */
    Class<?>[] view() default {};
}
