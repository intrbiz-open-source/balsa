package com.intrbiz.balsa.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.intrbiz.balsa.component.BalsaService;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.PARAMETER })
public @interface InjectService {
    
    String value() default BalsaService.DEFAULT_SERVICE_NAME;
    
}
