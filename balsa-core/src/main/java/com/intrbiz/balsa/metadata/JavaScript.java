package com.intrbiz.balsa.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.intrbiz.balsa.engine.impl.route.exec.response.TextResponse;
import com.intrbiz.balsa.scgi.http.HTTP.HTTPStatus;

/**
 * Write the String return value of the route to the response
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.PARAMETER})
@IsResponse(TextResponse.class)
public @interface JavaScript {
    /**
     * The HTTP response status
     * @return
     */
    HTTPStatus status() default HTTPStatus.OK;
    
    /**
     * A view to encode the response object with, using the default templates of the router
     */
    String[] encode() default {};
    
    /**
     * A view to encode the response object with
     */
    String[] encodeOnly() default {};
    
    /**
     * Set the method return value as a context variable ({@code var()}) of the given name
     */
    String set() default "";
}
