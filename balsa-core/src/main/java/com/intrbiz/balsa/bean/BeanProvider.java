package com.intrbiz.balsa.bean;

/**
 * A bean manager
 */
public interface BeanProvider<E>
{
    /**
     * Get the name of the bean
     * @return the bean name
     */
    String getName();
    
    /**
     * Is this bean session scoped
     * @return true if the bean should be session scoped
     */
    boolean isSession();
    
    /**
     * The bean name
     * @return
     * returns String
     */
    Class<E> getBeanClass();
    
    /**
     * Create a bean
     * @return
     * returns E
     */
    E create();
    
}
