package com.intrbiz.balsa.bean.impl;

import java.util.function.Supplier;

public class SupplierBeanProvider<E> extends BaseBeanProvider<E>
{
    protected final Supplier<E> supplier;
    
    public SupplierBeanProvider(Class<E> clazz, Supplier<E> supplier)
    {
        super(clazz);
        this.supplier = supplier;
    }
    
    public E create()
    {
        return this.supplier.get();
    }
}
