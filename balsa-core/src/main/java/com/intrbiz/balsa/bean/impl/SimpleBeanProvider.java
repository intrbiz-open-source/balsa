package com.intrbiz.balsa.bean.impl;

public class SimpleBeanProvider<E> extends BaseBeanProvider<E>
{
    public SimpleBeanProvider(Class<E> clazz)
    {
        super(clazz);
    }
    
    public E create()
    {
        try
        {
            return this.clazz.newInstance();
        }
        catch (InstantiationException e)
        {
        }
        catch (IllegalAccessException e)
        {
        }
        return null;
    }
}
