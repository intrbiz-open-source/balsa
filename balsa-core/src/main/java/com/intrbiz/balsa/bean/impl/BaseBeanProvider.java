package com.intrbiz.balsa.bean.impl;

import java.util.Objects;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.bean.BeanProvider;
import com.intrbiz.balsa.metadata.Model;

public abstract class BaseBeanProvider<E> implements BeanProvider<E>
{
    protected final Class<E> clazz;
    
    protected final String name;
    
    protected final boolean session;

    public BaseBeanProvider(Class<E> clazz)
    {
        this.clazz = Objects.requireNonNull(clazz);
        Model model = clazz.getAnnotation(Model.class);
        if (model == null) throw new BalsaException("Cannot create provider for the bean " + clazz.getName() + " it is not annotated with @Model");
        this.name = model.value();
        this.session = model.session();
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public boolean isSession()
    {
        return this.session;
    }

    @Override
    public Class<E> getBeanClass()
    {
        return clazz;
    }    
}
