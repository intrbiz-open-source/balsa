package com.intrbiz.balsa.component;

import com.intrbiz.balsa.BalsaApplication;

public interface BalsaService extends BalsaComponent<BalsaApplication>
{
    public static final String DEFAULT_SERVICE_NAME = "default";
    
    default void serviceName(String name)
    {
    }
}
