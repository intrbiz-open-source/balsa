package com.intrbiz.balsa.component;

import com.intrbiz.balsa.BalsaApplication;

public interface BalsaEngine extends BalsaComponent<BalsaApplication>
{
    /**
     * Get the name of this engine
     * @return
     */
    default String getEngineName()
    {
        return this.getClass().getSimpleName();
    }
    
}
