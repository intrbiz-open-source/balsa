package com.intrbiz.balsa.component;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaException;

/**
 * A generic component of a Balsa application
 */
public interface BalsaComponent<A extends BalsaApplication>
{
    /**
     * Setup this component, invoked when it is registered with the application
     * @param application the {@link BalsaApplication} this component is now part of
     * @throws BalsaException
     */
    default void setup(A application) throws BalsaException
    {
    }
    
    /**
     * Make this component ready to handle application requests
     * @throws BalsaException
     */
    @Deprecated
    default void start() throws BalsaException
    {
    }
    
    /**
     * Make this component ready to handle application requests
     * @throws BalsaException
     */
    default void start(A application) throws BalsaException
    {
        this.start();
    }

    /**
     * The application has stopped
     */
    default void stop()
    {
    }
    
}
