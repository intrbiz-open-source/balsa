package com.intrbiz.balsa.view.library.impl;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.library.ComponentLibrary;
import com.intrbiz.balsa.view.parser.ParserContext;

public class DefaultComponentLibrary extends BaseNamedLibrary implements ComponentLibrary
{
    private final Map<String, Function<String, Component>> components = new TreeMap<>();

    private Function<String, Component> genericSupplier = null;

    public DefaultComponentLibrary(String name, String license, String description)
    {
        super(name, license, description);
    }

    public DefaultComponentLibrary(String name)
    {
        super(name);
    }

    protected void addComponent(String name, Function<String, Component> supplier)
    {
        this.components.put(Objects.requireNonNull(name), Objects.requireNonNull(supplier));
    }

    protected Function<String, Component> getGenericSupplier()
    {
        return this.genericSupplier;
    }

    protected void setGenericSupplier(Function<String, Component> genericSupplier)
    {
        this.genericSupplier = genericSupplier;
    }

    @Override
    public Component load(String name, ParserContext context) throws BalsaException
    {
        // get the component factory by name
        Function<String, Component> supplier = this.components.get(name);
        if (supplier != null) return supplier.apply(name);
        // use the generic factory if we have one
        return (this.genericSupplier != null) ? this.genericSupplier.apply(name) : null;
    }

    @Override
    public Set<String> componentNames()
    {
        return Collections.unmodifiableSet(this.components.keySet());
    }

}
