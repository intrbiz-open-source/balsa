package com.intrbiz.balsa.view.parser;

import java.io.IOException;
import java.io.StringReader;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.renderer.Renderer;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.value.ValueExpression;

/**
 * A SAX handler which parses XML into Balsa component tree
 */
public class BalsaSAXHandler extends DefaultHandler
{
    private static final Logger logger = LoggerFactory.getLogger(BalsaSAXHandler.class);
    
    private final ParserContext parserContext;
    
    // parse state
    
    private final Stack<Component> stack = new Stack<Component>();
    
    private final Stack<StringBuilder> textStack = new Stack<StringBuilder>();
    
    private Component root;

    public BalsaSAXHandler(ParserContext parserContext)
    {
        this.parserContext = parserContext;
    }
    
    public Component getRoot()
    {
        return this.root;
    }

    @Override
    public void startPrefixMapping(String prefix, String uri) throws SAXException
    {
        try
        {
            // check the library exists
            this.parserContext.getComponents().hasLibrary(uri.trim());
        }
        catch (BalsaException je)
        {
            throw new SAXException("Error loading component library: " + prefix + "=" + uri, je);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException
    {
        if (length > 0)
        {
            Component parent = this.stack.peek();
            if (parent != null)
            {
                if (parent.coalesceText())
                {
                    StringBuilder sb = this.textStack.peek();
                    if (sb != null) sb.append(ch, start, length);
                }
                else
                {
                    String text = new String(ch, start, length).trim();
                    if (text.length() > 0)
                    {
                        parent.addText(this.parseText(text));
                    }
                }
            }
        }
    }
    
    private ValueExpression parseText(String text) throws SAXException
    {
        try
        {
            return new ValueExpression(this.parserContext.getBalsaContext().getExpressContext(), text);
        }
        catch (ExpressException e)
        {
            throw new SAXException("Failed to parse text EL expression", e);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        if (this.stack.isEmpty())
        {
            throw new SAXException("Could not close the element \"" + localName + "\".  It looks like the document is not well formed!");
        }
        else
        {
            Component comp = this.stack.pop();
            // coalesced text
            if (comp.coalesceText())
            {
                StringBuilder compText = this.textStack.pop();
                if (compText != null)
                {
                    String text = comp.preformattedText() ? compText.toString() : compText.toString().trim();
                    if (text.length() > 0)
                    {
                        comp.setText(this.parseText(text));
                    }
                }
            }
            // validate closing
            if (comp.getName() == null || ! comp.getName().equalsIgnoreCase(localName))
            {
                throw new SAXException("Could not close the element \"" + localName + "\".  It looks like the document is not well formed!");
            }
            // merge
            if (this.stack.isEmpty())
            {
                this.stack.push(comp);
            }
            else
            {
                this.stack.peek().addChild(comp);
            }
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        try
        {
            Component comp = this.parserContext.getComponents().loadComponent(uri, localName, this.parserContext);
            // set the component name
            comp.setName(localName);
            // load the renderer
            if (comp.getRenderer() == null)
            {
                Renderer renderer = this.parserContext.getRenders().loadRenderer(this.parserContext.getRenderFormat(), comp, this.parserContext);
                comp.setRenderer(renderer);
            }
            // set the attributes
            for (int i = 0; i < attributes.getLength(); i++)
            {
                String name = attributes.getLocalName(i);
                String value = attributes.getValue(i);
                //
                ValueExpression valExp = new ValueExpression(this.parserContext.getBalsaContext().getExpressContext(), value);
                comp.getAttributes().put(name, valExp);
            }
            this.stack.push(comp);
            if (comp.coalesceText()) this.textStack.push(new StringBuilder());
        }
        catch (Exception je)
        {
            throw new SAXException("Could not load component or renderer: " + uri + ":" + localName + "?format=" + this.parserContext.getRenderFormat(), je);
        }
    }

    @Override
    public void processingInstruction(String target, String data) throws SAXException
    {
        // define process instructions
        if ("RenderLibrary".equalsIgnoreCase(target))
        {
            logger.debug("Ignoring RenderLibrary process instruction: {}", data);
        }
    }

    @Override
    public void endDocument() throws SAXException
    {
        // pop the root
        this.root = this.stack.pop();
    }

    @Override
    public void startDocument() throws SAXException
    {
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws IOException, SAXException
    {
        if (BalsaDTD.Balsa_DTD_URI.equalsIgnoreCase(systemId))
            return new InputSource(new StringReader(BalsaDTD.Balsa_DTD));
        return super.resolveEntity(publicId, systemId);
    }
}
