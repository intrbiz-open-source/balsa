package com.intrbiz.balsa.view.parser;

import java.io.IOException;
import java.io.Reader;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.view.component.Component;

public final class Parser
{
    public static Component parse(BalsaContext context, Reader reader, String renderFormat) throws BalsaException
    {
        return parse(new ParserContext(context, renderFormat), reader);
    }
    
    public static Component parse(ParserContext parserContext, Reader reader) throws BalsaException
    {
        try
        {
            BalsaSAXHandler handler = new BalsaSAXHandler(parserContext);
            // use sax to parse the xml document
            XMLReader xr = XMLReaderFactory.createXMLReader();
            xr.setContentHandler(handler);
            xr.setErrorHandler(handler);
            xr.setEntityResolver(handler);
            xr.parse(new InputSource(reader));
            // get the view root
            Component root = handler.getRoot();
            root.load(parserContext.getBalsaContext());
            return root;
        }
        catch (SAXException e)
        {
            throw new BalsaException("Failed to parse view XML", e);
        }
        catch (IOException e)
        {
            throw new BalsaException("Failed to parse view", e);
        }
    }
}
