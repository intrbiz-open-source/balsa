package com.intrbiz.balsa.view.library;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.parser.ParserContext;
import com.intrbiz.balsa.view.renderer.Renderer;

public interface RenderLibrary extends NamedLibrary
{   
    String getRenderFormat();
    
    default boolean matchesRenderFormat(String renderFormat)
    {
        return this.getRenderFormat().equalsIgnoreCase(renderFormat);
    }
    
    Renderer loadRenderer(Component component, ParserContext context) throws BalsaException;
    
    Renderer loadGenericRenderer(Component component, ParserContext context) throws BalsaException;
    
}
