package com.intrbiz.balsa.view.core.html;

import java.util.Collections;
import java.util.Map;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.express.value.ValueExpression;
import com.intrbiz.util.URLUtil;

public class AComponent extends Component
{
    public AComponent()
    {
        super();
    }
    
    public String getFragment(BalsaContext context)
    {
        return this.getAttributeValue("fragment", context);
    }
    
    @SuppressWarnings("unchecked")
    public Map<Object, Object> getQueryParameters(BalsaContext context)
    {
        ValueExpression attr = this.getAttribute("query");
        if (attr == null) return Collections.emptyMap();
        Object val = attr.get(context.getExpressContext(), this);
        return (val instanceof Map) ? (Map<Object, Object>) val : Collections.emptyMap();
    }
    
    public String getHref(BalsaContext context)
    {
        return this.getAttributeValue("href", context);
    }
    
    public String buildHref(BalsaContext context)
    {
        String href = this.getHref(context);
        Map<Object, Object> query = this.getQueryParameters(context);
        String fragment = this.getFragment(context);
        // build the url
        return URLUtil.buildURL(href, query, fragment);
    }
}
