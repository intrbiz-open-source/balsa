package com.intrbiz.balsa.view.core.html;

import java.util.Collections;
import java.util.Map;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.express.value.ValueExpression;
import com.intrbiz.util.URLUtil;

public class FormComponent extends Component
{
    public FormComponent()
    {
        super();
    }
    
    public String getAccessToken()
    {
        ValueExpression ve = this.getAttribute("access-token");
        if (ve != null)
        {
            Object val = ve.get(BalsaContext.Balsa().getExpressContext(), this);
            return val == null ? null : val.toString();
        }
        return "access-token";
    }
    
    public String getFragment(BalsaContext context)
    {
        return this.getAttributeValue("fragment", context);
    }
    
    @SuppressWarnings("unchecked")
    public Map<Object, Object> getQueryParameters(BalsaContext context)
    {
        ValueExpression attr = this.getAttribute("query");
        if (attr == null) return Collections.emptyMap();
        Object val = attr.get(context.getExpressContext(), this);
        return (val instanceof Map) ? (Map<Object, Object>) val : Collections.emptyMap();
    }
    
    public String getPath(BalsaContext context)
    {
        return this.getAttributeValue("path", context);
    }
    
    public String buildPath(BalsaContext context)
    {
        String href = this.getPath(context);
        Map<Object, Object> query = this.getQueryParameters(context);
        String fragment = this.getFragment(context);
        // build the url
        return URLUtil.buildURL(href, query, fragment);
    }
}
