package com.intrbiz.balsa.view.library.registry;

import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.library.ComponentLibrary;
import com.intrbiz.balsa.view.parser.ParserContext;

public class ComponentLibraryRegister
{
    private static final Logger logger = LoggerFactory.getLogger(ComponentLibraryRegister.class);
    
    private static final ComponentLibraryRegister DEFAULT = new ComponentLibraryRegister().scan();
    
    public static final ComponentLibraryRegister getDefault()
    {
        return DEFAULT;
    }
    
    private Map<String, ComponentLibrary> libraries = new TreeMap<String, ComponentLibrary>();

    public ComponentLibraryRegister()
    {
        super();
    }

    protected ComponentLibraryRegister scan()
    {
        // automatically discover libraries via service loader
        for (ComponentLibrary lib : ServiceLoader.load(ComponentLibrary.class))
        {
            logger.info("Auto-discovered component library {} => {}", lib.getName(), lib.getClass().getCanonicalName());
            this.registerLibrary(lib);
        }
        return this;
    }
    
    public ComponentLibraryRegister registerLibrary(ComponentLibrary library)
    {
        this.libraries.put(Objects.requireNonNull(library.getName()), library);
        return this;
    }

    public Component loadComponent(String library, String name, ParserContext context) throws BalsaException
    {
        ComponentLibrary lib = this.libraries.get(library);
        if (lib == null) throw new BalsaException("Cannot find library: " + library);
        Component comp = lib.load(name, context);
        if (comp != null)
            return comp;
        throw new BalsaException("Cannot find component: " + library + ":" + name);
    }
    
    public boolean hasLibrary(String library)
    {
        return this.libraries.containsKey(library);
    }
}
