package com.intrbiz.balsa.view.core.template;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.express.value.ValueExpression;

/**
 * An instance of a composite component
 */
public final class CompositeComponent extends Component
{    
    protected final Component definition;
    
    public CompositeComponent(Component definition)
    {
        super();
        this.definition = Objects.requireNonNull(definition);
    }
    
    /**
     * Get the composite component definition
     * @return
     */
    public Component getDefinition()
    {
        return this.definition;
    }
    
    /**
     * The attributes to bind
     */
    public Map<String, ValueExpression> getDataBindings()
    {
        Map<String, ValueExpression> bindings = new HashMap<String, ValueExpression>();
        for (Entry<String, ValueExpression> attribute : this.getAttributes().entrySet())
        {
            String key = attribute.getKey();
            if (!(key.startsWith("xmlns") || RENDERED.equals(key)))
            {
                bindings.put(key, attribute.getValue());
            }
        }
        return bindings;
    }
}
