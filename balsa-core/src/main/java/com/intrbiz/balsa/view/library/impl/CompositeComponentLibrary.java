package com.intrbiz.balsa.view.library.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.core.library.CoreComponentLibrary;
import com.intrbiz.balsa.view.core.template.CompositeComponent;
import com.intrbiz.balsa.view.core.template.CompositeRenderer;
import com.intrbiz.balsa.view.core.template.FragmentComponent;
import com.intrbiz.balsa.view.library.ComponentLibrary;
import com.intrbiz.balsa.view.parser.Parser;
import com.intrbiz.balsa.view.parser.ParserContext;

/**
 * A component library which loads {@link CompositeComponent)s by parsing files in the library directory.
 */
public class CompositeComponentLibrary implements ComponentLibrary
{
    private static final Logger logger = LoggerFactory.getLogger(CompositeComponentLibrary.class);
    
    public final File base;
    
    /**
     * @param base the component library base directory
     */
    public CompositeComponentLibrary(File base)
    {
        super();
        this.base = base;
    }

    @Override
    public String getName()
    {
        return this.base.getName();
    }

    @Override
    public String getLicense()
    {
        return null;
    }

    @Override
    public String getDescription()
    {
        return null;
    }

    @Override
    public Component load(String name, ParserContext context) throws BalsaException
    {
        Component component = this.loadDefinition(name, context);
        // create the composite instance proxy
        if (component != null)
            return new CompositeComponent(component).withRenderer(new CompositeRenderer());
        // to work around XMLNS issues delegate to core library for components we didn't find
        return context.getComponents().loadComponent(CoreComponentLibrary.NAME, name, context);
    }
    
    public Component loadDefinition(String name, ParserContext context) throws BalsaException
    {
        // check the component cache
        String key = this.getName() + ":" + name;
        Component component = context.loadCachedComponent(key);
        // load from source
        if (component == null)
        {
            File componentFile = new File(this.base, name + ".xml");
            if (componentFile.exists() && componentFile.isFile())
            {
                try (FileReader reader = new FileReader(componentFile))
                {
                    // create component and cache it to support self-referencing composite components
                    FragmentComponent composite = new FragmentComponent();
                    context.storeCachedComponent(key, composite);
                    // parse and copy
                    component = Parser.parse(context, reader);
                    logger.debug("Loaded composite component {} from {}", name, componentFile.getAbsolutePath());
                    // copy over the parsed component into the container
                    composite.copyFrom(component);
                }
                catch (FileNotFoundException e)
                {
                    // purge on parse error
                    context.clearCachedComponent(key);
                    return null;
                }
                catch (IOException e)
                {
                    // purge on parse error
                    context.clearCachedComponent(key);
                    throw new BalsaException("Failed to parse component: " + name, e);
                }
            }
        }
        return component;
    }

    @Override
    public Set<String> componentNames()
    {
        Set<String> names = new HashSet<>();
        for (File child : this.base.listFiles())
        {
            if (child.exists() && child.isFile())
            {
                String name = child.getName();
                if (name.length() > 4 && name.endsWith(".xml"))
                {
                    names.add(name.substring(0, name.length() - 4));
                }
            }
        }
        return names;
    }
}
