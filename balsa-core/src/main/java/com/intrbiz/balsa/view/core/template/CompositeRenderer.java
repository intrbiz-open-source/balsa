package com.intrbiz.balsa.view.core.template;

import java.io.IOException;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.renderer.Renderer;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;

public class CompositeRenderer extends Renderer
{    
    @Override
    public void encodeChildren(Component component, BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException
    {
        CompositeComponent composite = (CompositeComponent) component;
        // fetch the composite component definition
        Component definition = composite.getDefinition();
        // push this composite component onto the stack
        context.pushCompositeComponent(component);
        // encode the definition
        try
        {
            ExpressContext elctx = context.getExpressContext();
            // enter an express frame in function mode to fully isolate variables
            elctx.enterFunctionFrame(composite.getDataBindings(), composite);
            try
            {
                // encode our children
                for (Component child : definition.getChildren())
                {
                    child.encode(context, to);
                }
            }
            finally
            {
                elctx.exitFrame();
            }
        }
        finally
        {
            // pop the composite component from the stack
            if (context.popCompositeComponent() != component)
                throw new BalsaException("Composite component stack corrupted");
        }
    }

    @Override
    public void decodeChildren(Component component, BalsaContext context) throws BalsaException, ExpressException
    {
        CompositeComponent composite = (CompositeComponent) component;
        // fetch the composite component definition
        Component definition = composite.getDefinition();
        // push this composite component onto the stack
        context.pushCompositeComponent(component);
        // encode the definition
        try
        {
            // enter an express frame to isolate variables
            ExpressContext elctx = context.getExpressContext();
            // enter an express frame in function mode to fully isolate variables
            elctx.enterFunctionFrame(composite.getDataBindings(), composite);
            try
            {
                // encode our children
                for (Component child : definition.getChildren())
                {
                    child.decode(context);
                }
            }
            finally
            {
                elctx.exitFrame();
            }
        }
        finally
        {
            // pop the composite component from the stack
            if (context.popCompositeComponent() != component)
                throw new BalsaException("Composite component stack corrupted");
        }
    }
}
