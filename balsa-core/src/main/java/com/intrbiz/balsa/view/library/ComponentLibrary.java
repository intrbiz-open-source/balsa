package com.intrbiz.balsa.view.library;

import java.util.Set;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.parser.ParserContext;

public interface ComponentLibrary extends NamedLibrary
{
    public Component load(String name, ParserContext context) throws BalsaException;
    
    public Set<String> componentNames();

}
