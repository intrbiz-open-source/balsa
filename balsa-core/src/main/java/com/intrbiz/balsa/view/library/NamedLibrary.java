package com.intrbiz.balsa.view.library;

public interface NamedLibrary
{
    String getName();
    
    String getLicense();
    
    String getDescription();
}
