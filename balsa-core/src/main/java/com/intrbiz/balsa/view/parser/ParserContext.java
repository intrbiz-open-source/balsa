package com.intrbiz.balsa.view.parser;

import java.util.HashMap;
import java.util.Map;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.library.registry.ComponentLibraryRegister;
import com.intrbiz.balsa.view.library.registry.RenderLibraryRegister;
import com.intrbiz.util.Util;

/**
 * Context for the life time of a view parse
 */
public class ParserContext
{
    private final BalsaContext balsaContext;
    
    private final ComponentLibraryRegister components;
    
    private final RenderLibraryRegister renders;

    private final Map<String, Component> componentCache = new HashMap<>();
    
    private final String renderFormat;

    public ParserContext(BalsaContext balsaContext, ComponentLibraryRegister components, RenderLibraryRegister renders, String renderFormat)
    {
        super();
        this.balsaContext = balsaContext;
        this.components = components;
        this.renders = renders;
        this.renderFormat = renderFormat;
    }
    
    public ParserContext(BalsaContext balsaContext, String renderFormat)
    {
        this(balsaContext, ComponentLibraryRegister.getDefault(), RenderLibraryRegister.getDefault(), renderFormat);
    }

    public BalsaContext getBalsaContext()
    {
        return this.balsaContext;
    }
    
    public String getRenderFormat()
    {
        return Util.coalesceEmpty(this.renderFormat, this.balsaContext.getRenderFormat(), "text/html");
    }

    public ComponentLibraryRegister getComponents()
    {
        return this.components;
    }

    public RenderLibraryRegister getRenders()
    {
        return this.renders;
    }

    public void storeCachedComponent(String key, Component value)
    {
        this.componentCache.put(key, value);
    }
    
    public void clearCachedComponent(String key)
    {
        this.componentCache.remove(key);
    }

    public Component loadCachedComponent(String key)
    {
        return this.componentCache.get(key);
    }
}
