package com.intrbiz.balsa.view.library.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.library.RenderLibrary;
import com.intrbiz.balsa.view.parser.ParserContext;
import com.intrbiz.balsa.view.renderer.Renderer;

public class DefaultRenderLibrary extends BaseNamedLibrary implements RenderLibrary
{
    private final String renderFormat;
    
    private final Map<Class<? extends Component>, Function<Component, Renderer>> renderers = new HashMap<>();
    
    private Function<Component, Renderer> genericRendererSupplier;

    public DefaultRenderLibrary(String renderFormat, String name, String license, String description)
    {
        super(name, license, description);
        this.renderFormat = Objects.requireNonNull(renderFormat);
    }

    public DefaultRenderLibrary(String renderFormat, String name)
    {
        this(renderFormat, name, null, null);
    }
    
    @Override
    public String getRenderFormat()
    {
        return this.renderFormat;
    }

    protected void addRenderer(Class<? extends Component> componentType, Function<Component, Renderer> supplier)
    {
        this.renderers.put(Objects.requireNonNull(componentType), Objects.requireNonNull(supplier));
    }

    protected Function<Component, Renderer> getGenericRendererSupplier()
    {
        return this.genericRendererSupplier;
    }

    protected void setGenericRendererSupplier(Function<Component, Renderer> genericRendererSupplier)
    {
        this.genericRendererSupplier = genericRendererSupplier;
    }

    @Override
    public Renderer loadRenderer(Component component, ParserContext context) throws BalsaException
    {
        Function<Component, Renderer> supplier = this.renderers.get(component.getClass());
        if (supplier != null)
            return supplier.apply(component);
        return null;
    }
    
    @Override
    public Renderer loadGenericRenderer(Component component, ParserContext context) throws BalsaException
    {
        return (this.genericRendererSupplier != null) ? this.genericRendererSupplier.apply(component) : null;
    }

}
