package com.intrbiz.balsa.view.core.template;

import java.io.IOException;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.renderer.Renderer;
import com.intrbiz.express.ExpressException;

public class ChildrenRenderer extends Renderer
{
    @Override
    public void decodeChildren(Component component, BalsaContext context) throws BalsaException
    {
        // decode the children of the current composite component
        Component instance = context.peekCompositeComponent();
        if (instance != null)
        {
            for (Component child : instance.getChildren())
            {
                child.decode(context);
            }
        }
    }

    @Override
    public void encodeChildren(Component component, BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException
    {
        // encode the children of the current composite component
        Component instance = context.peekCompositeComponent();
        if (instance != null)
        {
            // encode child text if it exists
            if (instance.getText() != null)
            {
                Object theText = instance.getText().get(context.getExpressContext(), instance);
                if (theText != null)
                    to.putEncPadLn(theText.toString());
            }
            // encode any child components
            for (Component child : instance.getChildren())
            {
                child.encode(context, to);
            }
        }
    }
}
