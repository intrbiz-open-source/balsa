package com.intrbiz.balsa.view.core.template;

public class TryIncludeComponent extends IncludeComponent
{
    public TryIncludeComponent()
    {
        super();
    }
    
    public boolean isSuppressErrors()
    {
        return true;
    }
    
}
