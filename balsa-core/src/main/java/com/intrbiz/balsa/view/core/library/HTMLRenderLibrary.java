package com.intrbiz.balsa.view.core.library;

import com.intrbiz.balsa.view.core.data.DataSetComponent;
import com.intrbiz.balsa.view.core.data.DataSetRenderer;
import com.intrbiz.balsa.view.core.generic.GenericComponent;
import com.intrbiz.balsa.view.core.generic.GenericRenderer;
import com.intrbiz.balsa.view.core.html.AComponent;
import com.intrbiz.balsa.view.core.html.ARenderer;
import com.intrbiz.balsa.view.core.html.CodeComponent;
import com.intrbiz.balsa.view.core.html.CodeRenderer;
import com.intrbiz.balsa.view.core.html.FormComponent;
import com.intrbiz.balsa.view.core.html.FormRenderer;
import com.intrbiz.balsa.view.core.html.HTMLComponent;
import com.intrbiz.balsa.view.core.html.HTMLRenderer;
import com.intrbiz.balsa.view.core.html.InputComponent;
import com.intrbiz.balsa.view.core.html.InputRenderer;
import com.intrbiz.balsa.view.core.html.OptionComponent;
import com.intrbiz.balsa.view.core.html.OptionRenderer;
import com.intrbiz.balsa.view.core.html.PreComponent;
import com.intrbiz.balsa.view.core.html.PreRenderer;
import com.intrbiz.balsa.view.core.html.ScriptComponent;
import com.intrbiz.balsa.view.core.html.ScriptRenderer;
import com.intrbiz.balsa.view.core.html.StyleComponent;
import com.intrbiz.balsa.view.core.html.StyleRenderer;
import com.intrbiz.balsa.view.core.html.TextAreaComponent;
import com.intrbiz.balsa.view.core.html.TextAreaRenderer;
import com.intrbiz.balsa.view.core.security.AccessTokenComponent;
import com.intrbiz.balsa.view.core.security.AccessTokenForURLComponent;
import com.intrbiz.balsa.view.core.security.AccessTokenForURLRenderer;
import com.intrbiz.balsa.view.core.security.AccessTokenRenderer;
import com.intrbiz.balsa.view.core.template.ChildrenComponent;
import com.intrbiz.balsa.view.core.template.ChildrenRenderer;
import com.intrbiz.balsa.view.core.template.CompositeComponent;
import com.intrbiz.balsa.view.core.template.ContainerComponent;
import com.intrbiz.balsa.view.core.template.ContainerRenderer;
import com.intrbiz.balsa.view.core.template.ContentComponent;
import com.intrbiz.balsa.view.core.template.ContentRenderer;
import com.intrbiz.balsa.view.core.template.FragmentComponent;
import com.intrbiz.balsa.view.core.template.FragmentRenderer;
import com.intrbiz.balsa.view.core.template.IncludeComponent;
import com.intrbiz.balsa.view.core.template.IncludeRenderer;
import com.intrbiz.balsa.view.core.template.TryIncludeComponent;
import com.intrbiz.balsa.view.library.impl.DefaultRenderLibrary;

public class HTMLRenderLibrary extends DefaultRenderLibrary
{
    public HTMLRenderLibrary()
    {
        super("text/html", "com.intrbiz.balsa.html", "LGPL V3", "HTML5 render library for Balsa");
        // register renderer
        // templating renderers
        this.addRenderer(ContentComponent.class, comp -> new ContentRenderer());
        this.addRenderer(IncludeComponent.class, comp -> new IncludeRenderer());
        this.addRenderer(TryIncludeComponent.class, comp -> new IncludeRenderer());
        this.addRenderer(FragmentComponent.class, comp -> new FragmentRenderer());
        this.addRenderer(ContainerComponent.class, comp -> new ContainerRenderer());
        this.addRenderer(CompositeComponent.class, comp -> new ContainerRenderer());
        this.addRenderer(ChildrenComponent.class, comp -> new ChildrenRenderer());
        // special html renderers
        this.addRenderer(HTMLComponent.class, comp -> new HTMLRenderer());
        this.addRenderer(AComponent.class, comp -> new ARenderer());
        this.addRenderer(StyleComponent.class, comp -> new StyleRenderer());
        this.addRenderer(ScriptComponent.class, comp -> new ScriptRenderer());
        this.addRenderer(PreComponent.class, comp -> new PreRenderer());
        this.addRenderer(CodeComponent.class, comp -> new CodeRenderer());
        this.addRenderer(TextAreaComponent.class, comp -> new TextAreaRenderer());
        this.addRenderer(OptionComponent.class, comp -> new OptionRenderer());
        this.addRenderer(InputComponent.class, comp -> new InputRenderer());
        this.addRenderer(FormComponent.class, comp -> new FormRenderer());
        // data looping renderers
        this.addRenderer(DataSetComponent.class, comp -> new DataSetRenderer());
        // security renderer
        this.addRenderer(AccessTokenComponent.class, comp -> new AccessTokenRenderer());
        this.addRenderer(AccessTokenForURLComponent.class, comp -> new AccessTokenForURLRenderer());
        // generic renderer
        this.addRenderer(GenericComponent.class, comp -> new GenericRenderer());
    }
}
