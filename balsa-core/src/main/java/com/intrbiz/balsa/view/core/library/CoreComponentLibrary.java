package com.intrbiz.balsa.view.core.library;

import com.intrbiz.balsa.view.core.data.DataSetComponent;
import com.intrbiz.balsa.view.core.generic.GenericComponent;
import com.intrbiz.balsa.view.core.html.AComponent;
import com.intrbiz.balsa.view.core.html.CodeComponent;
import com.intrbiz.balsa.view.core.html.FormComponent;
import com.intrbiz.balsa.view.core.html.HTMLComponent;
import com.intrbiz.balsa.view.core.html.InputComponent;
import com.intrbiz.balsa.view.core.html.OptionComponent;
import com.intrbiz.balsa.view.core.html.PreComponent;
import com.intrbiz.balsa.view.core.html.ScriptComponent;
import com.intrbiz.balsa.view.core.html.StyleComponent;
import com.intrbiz.balsa.view.core.html.TextAreaComponent;
import com.intrbiz.balsa.view.core.security.AccessTokenComponent;
import com.intrbiz.balsa.view.core.security.AccessTokenForURLComponent;
import com.intrbiz.balsa.view.core.template.ChildrenComponent;
import com.intrbiz.balsa.view.core.template.ContainerComponent;
import com.intrbiz.balsa.view.core.template.ContentComponent;
import com.intrbiz.balsa.view.core.template.FragmentComponent;
import com.intrbiz.balsa.view.core.template.IncludeComponent;
import com.intrbiz.balsa.view.core.template.TryIncludeComponent;
import com.intrbiz.balsa.view.library.impl.DefaultComponentLibrary;

public class CoreComponentLibrary extends DefaultComponentLibrary
{
    public static final String NAME = "com.intrbiz.balsa";
    
    public CoreComponentLibrary()
    {
        super(NAME, "LGPL V3", "Core components of Balsa view engine");
        // register components
        // templating specific functions
        this.addComponent("content"    , name -> new ContentComponent());
        this.addComponent("include"    , name -> new IncludeComponent());
        this.addComponent("try-include", name -> new TryIncludeComponent());
        this.addComponent("fragment"   , name -> new FragmentComponent());
        this.addComponent("container"  , name -> new ContainerComponent());
        this.addComponent("component"  , name -> new FragmentComponent());
        this.addComponent("children"   , name -> new ChildrenComponent());
        // special html components
        this.addComponent("html"    , name -> new HTMLComponent());
        this.addComponent("a"       , name -> new AComponent());
        this.addComponent("style"   , name -> new StyleComponent());
        this.addComponent("script"  , name -> new ScriptComponent());
        this.addComponent("pre"     , name -> new PreComponent());
        this.addComponent("code"    , name -> new CodeComponent());
        this.addComponent("textarea", name -> new TextAreaComponent());
        this.addComponent("option"  , name -> new OptionComponent());
        this.addComponent("input"   , name -> new InputComponent());
        this.addComponent("form"    , name -> new FormComponent());
        // data looping components
        this.addComponent("data-set", name -> new DataSetComponent());
        // security components
        this.addComponent("access-token"        , name -> new AccessTokenComponent());
        this.addComponent("access-token-for-url", name -> new AccessTokenForURLComponent());
        // generic component
        this.setGenericSupplier(name -> new GenericComponent(name));
    }
}
