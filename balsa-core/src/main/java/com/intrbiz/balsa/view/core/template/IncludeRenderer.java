package com.intrbiz.balsa.view.core.template;

import java.io.IOException;
import java.util.List;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.renderer.Renderer;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;

public class IncludeRenderer extends Renderer
{   
    @Override
    public void decodeChildren(Component component, BalsaContext context) throws BalsaException
    {
        IncludeComponent inc = (IncludeComponent) component;
        List<String> views = inc.getViews(context);
        if (! views.isEmpty())
        {
            ExpressContext elctx = context.getExpressContext();
            // enter an express frame to isolate variables
            elctx.enterFrame(inc.getDataBindings(), inc);
            try
            {
                // encode the include
                try
                {
                    context.decodeInclude(views.toArray(new String[views.size()]));
                }
                catch (RuntimeException e)
                {
                    if (! inc.isSuppressErrors())
                        throw e;
                }
            }
            finally
            {
                elctx.exitFrame();
            }
        }
    }

    @Override
    public void encodeChildren(Component component, BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException
    {
        IncludeComponent inc = (IncludeComponent) component;
        List<String> views = inc.getViews(context);
        if (! views.isEmpty())
        {
            ExpressContext elctx = context.getExpressContext();
            // enter an express frame to isolate variables
            elctx.enterFrame(inc.getDataBindings(), inc);
            try
            {
                // encode the include
                try
                {
                    context.encodeInclude(to, views.toArray(new String[views.size()]));
                }
                catch (RuntimeException e)
                {
                    if (! inc.isSuppressErrors())
                        throw e;
                }
            }
            finally
            {
                elctx.exitFrame();
            }
        }
    }
}
