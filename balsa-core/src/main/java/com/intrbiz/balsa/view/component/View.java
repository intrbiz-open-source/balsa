package com.intrbiz.balsa.view.component;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.Objects;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.view.BalsaView;
import com.intrbiz.balsa.engine.view.ViewMetadata;
import com.intrbiz.balsa.engine.view.ViewPath;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.value.ValueExpression;

public class View extends BalsaView
{
    private Component root;

    public View(ViewPath path)
    {
        super(path);
    }
    
    public View(ViewPath path, Component root)
    {
        this(path);
        this.root = Objects.requireNonNull(root);
    }

    public Component getRoot()
    {
        return root;
    }

    public void setRoot(Component root)
    {
        this.root = root;
    }

    @Override
    public void decode(BalsaContext context) throws BalsaException, ExpressException
    {
        this.root.decode(context);
    }

    @Override
    public void encode(BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException
    {
        this.root.encode(context, to);
    }
    
    /**
     * Extract any metadata declared in this view as either attributes on the root element or as meta elements
     */
    public void extractMetadata()
    {
        ViewMetadata metadata = this.getMetadata();
        ExpressContext ctx = BalsaContext.Balsa().getExpressContext();
        // extract metadata from the root component
        for (Entry<String, ValueExpression> attribute : this.root.getAttributes().entrySet())
        {
            // only evaluate constant metadata values
            if ((! attribute.getKey().startsWith("data")) && attribute.getValue().isConstant())
            {            
                Object value = attribute.getValue().get(ctx, this.root);
                if (value != null)
                    metadata.setAttributeIfAbsent(attribute.getKey(), String.valueOf(value));
            }   
        }
        // visit the component tree looking for meta components
        root.visit(c -> "meta".equalsIgnoreCase(c.getName()), c -> {
            ValueExpression name    = c.getAttribute("name");
            ValueExpression content = c.getAttribute("content");
            if (name != null && name.isConstant() && content != null && content.isConstant())
            {
                Object attrName  = name.get(ctx, c);
                Object attrValue = content.get(ctx, c);
                if (attrName != null && attrValue != null)
                    metadata.setAttributeIfAbsent(String.valueOf(attrName), String.valueOf(attrValue));
            }
        });
    }
    
    /**
     * Evaluate the view title attribute if defined
     */
    @Override
    public String getTitle()
    {
        if (this.root == null) return null;
        ValueExpression titleExp = this.root.getAttribute("title");
        if (titleExp == null) return null;
        Object title = titleExp.get(BalsaContext.Balsa().getExpressContext(), this.root);
        if (title == null) return null;
        return String.valueOf(title);
    }
    
    /**
     * Lookup a view attribute
     */
    @Override
    public ValueExpression attribute(String name)
    {
        if (this.root == null) return null;
        return this.root.getAttribute(name);
    }
}
