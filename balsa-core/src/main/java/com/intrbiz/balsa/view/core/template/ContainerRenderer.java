package com.intrbiz.balsa.view.core.template;

import java.io.IOException;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.renderer.Renderer;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;

public class ContainerRenderer extends Renderer
{
    @Override
    public void encodeChildren(Component component, BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException
    {
        ContainerComponent inc = (ContainerComponent) component;
        ExpressContext elctx = context.getExpressContext();
        // enter an express frame to isolate variables, which will eval the bindings
        elctx.enterCachingFrame(inc.getDataBindings(), inc);
        try
        {
            // encode our children
            for (Component child : component.getChildren())
            {
                child.encode(context, to);
            }
        }
        finally
        {
            elctx.exitFrame();
        }
    }
    
    @Override
    public void decodeChildren(Component component, BalsaContext context) throws BalsaException, ExpressException
    {
        ContainerComponent inc = (ContainerComponent) component;
        ExpressContext elctx = context.getExpressContext();
        // enter an express frame to isolate variables, which will eval the bindings
        elctx.enterCachingFrame(inc.getDataBindings(), inc);
        try
        {
            // decode our children
            for (Component child : component.getChildren())
            {
                child.decode(context);
            }
        }
        finally
        {
            elctx.exitFrame();
        }
    }
}
