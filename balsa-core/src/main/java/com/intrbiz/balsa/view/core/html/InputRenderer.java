package com.intrbiz.balsa.view.core.html;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.listener.fileupload.FileParameter;
import com.intrbiz.balsa.scgi.parameter.Parameter;
import com.intrbiz.balsa.scgi.parameter.StringParameter;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.core.generic.GenericRenderer;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.value.ValueExpression;
import com.intrbiz.validation.converter.ConversionException;
import com.intrbiz.validation.converter.Converter;
import com.intrbiz.validation.validator.ValidationException;
import com.intrbiz.validation.validator.Validator;

public class InputRenderer extends GenericRenderer
{
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void decodeStart(Component component, BalsaContext context) throws BalsaException, ExpressException
    {
        ValueExpression value = component.getAttribute("value");
        if (value != null)
        {
            // eval the parameter name
            String inputName = component.getAttributeValue("name", context);
            if (inputName != null)
            {
                // do we have a parameter
                Parameter parameter = context.request().getParameter(inputName);
                if (parameter instanceof StringParameter)
                {
                    Object inputValue = parameter.getStringValue();
                    // convert?
                    Converter conv = value.getConverter(context.getExpressContext(), component);
                    if (conv != null) 
                    {
                        try
                        {
                            inputValue = conv.parseValue(parameter.getStringValue());
                        }
                        catch (ConversionException e)
                        {
                            context.addConversionError(inputName, e);
                        }
                    }
                    // validate?
                    Validator valid = value.getValidator(context.getExpressContext(), component);
                    if (valid != null)
                    {
                        try
                        {
                            valid.validate(inputValue);
                        }
                        catch (ValidationException e)
                        {
                            context.addValidationError(inputName, e);
                        }
                    }
                    // set
                    value.set(context.getExpressContext(), inputValue, component);
                }
                else if (parameter instanceof FileParameter)
                {
                    value.set(context.getExpressContext(), ((FileParameter) parameter).getFile(), component);
                }
            }
        }
    }
    
    
}
