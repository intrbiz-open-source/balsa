package com.intrbiz.balsa.view.library.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.library.RenderLibrary;
import com.intrbiz.balsa.view.parser.ParserContext;
import com.intrbiz.balsa.view.renderer.Renderer;

public class RenderLibraryRegister
{
    private static final Logger logger = LoggerFactory.getLogger(RenderLibraryRegister.class);
    
    private static final RenderLibraryRegister DEFAULT = new RenderLibraryRegister().scan();
    
    public static final RenderLibraryRegister getDefault()
    {
        return DEFAULT;
    }
    
    // TODO: we could store a list per format
    private List<RenderLibrary> libraries = new ArrayList<>();

    public RenderLibraryRegister()
    {
        super();
    }

    protected RenderLibraryRegister scan()
    {
        // automatically discover libraries via service loader
        for (RenderLibrary lib : ServiceLoader.load(RenderLibrary.class))
        {
            logger.info("Auto-discovered component library {} ({}) => {}", lib.getRenderFormat(), lib.getName(), lib.getClass().getCanonicalName());
            this.registerLibrary(lib);
        }
        return this;
    }
    
    public RenderLibraryRegister registerLibrary(RenderLibrary library)
    {
        this.libraries.add(Objects.requireNonNull(library));
        return this;
    }

    public Renderer loadRenderer(String renderFormat, Component comp, ParserContext context) throws BalsaException
    {
        // try specific renders
        for (RenderLibrary lib : this.libraries)
        {
            if (lib.matchesRenderFormat(renderFormat))
            {
                Renderer r = lib.loadRenderer(comp, context);
                if (r != null) 
                    return r;
            }
        }
        // try generic renders
        for (RenderLibrary lib : this.libraries)
        {
            if (lib.matchesRenderFormat(renderFormat))
            {
                Renderer r = lib.loadGenericRenderer(comp, context);
                if (r != null) 
                    return r;
            }
        }
        throw new BalsaException("Cannot find renderer: " + comp.getClass().getSimpleName() + "?format=" + renderFormat);
    }

}
