package com.intrbiz.balsa.view.library.impl;

import java.util.Objects;

import com.intrbiz.balsa.view.library.NamedLibrary;

public abstract class BaseNamedLibrary implements NamedLibrary
{
    private final String name;
    
    private final String license;
    
    private final String description;

    public BaseNamedLibrary(String name, String license, String description)
    {
        super();
        this.name = Objects.requireNonNull(name);
        this.license = license;
        this.description = description;
    }
    
    public BaseNamedLibrary(String name)
    {
        this(name, null, null);
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public String getLicense()
    {
        return this.license;
    }

    @Override
    public String getDescription()
    {
        return this.description;
    }
    
}
