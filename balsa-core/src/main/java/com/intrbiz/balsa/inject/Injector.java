package com.intrbiz.balsa.inject;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Objects;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.metadata.InjectService;
import com.intrbiz.util.Util;

/**
 * Very simple service dependency injection
 */
public final class Injector
{
    private Injector()
    {
    }
    
    private static final InjectService findInjectService(Annotation[] annos)
    {
        for (Annotation anno : annos)
        {
            if (anno instanceof InjectService)
                return (InjectService) anno;
        }
        return null;
    }
    
    private static final boolean canInjectMethod(Method method)
    {
        return (! Modifier.isAbstract(method.getModifiers())) && 
                Modifier.isPublic(method.getModifiers()) && 
                method.getParameterCount() > 0 && 
                method.getAnnotation(InjectService.class) != null;
    }
    
    private static final void injectMethod(BalsaApplication application, Object into, Class<?> type, Method method) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        InjectService methodInject = method.getAnnotation(InjectService.class);
        Class<?>[] parameterTypes = method.getParameterTypes();
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        // resolve the services for each parameter
        Object[] services = new Object[parameterTypes.length];
        for (int i = 0; i < services.length; i++)
        {
            Class<?> serviceType = parameterTypes[i];
            String serviceName = Util.coalesce(findInjectService(parameterAnnotations[i]), methodInject).value();
            services[i] = Objects.requireNonNull(application.service(serviceType, serviceName), "Could not resolve service: " + serviceType.getSimpleName() + "::" + serviceName);
        }        
        // invoke the setter
        method.invoke(into, services);
    }
    
    private static void injectServices(BalsaApplication application, Object into, Class<?> type) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        // go up the class train
        if (type.getSuperclass() != null)
            injectServices(application, into, type.getSuperclass());
        // scan all methods
        for (Method method : type.getDeclaredMethods())
        {            
            // inject into a method
            if (canInjectMethod(method))
                injectMethod(application, into, type, method);
        }
    }
    
    public static final void injectServices(BalsaApplication application, Object into) throws BalsaException
    {
        try
        {
            injectServices(application, into, into.getClass());
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
        {
            throw new BalsaException("Could not inject services into " + into.getClass().getSimpleName(), e);
        }
    }
}
