package com.intrbiz.balsa;

/**
 * Some functionality which can be mixed into a Balsa Application
 */
public interface BalsaPlugin
{
    /**
     * Setup the given Balsa application
     * @param app the Balsa application to setup
     */
    void setup(BalsaApplication app) throws Exception;
}
