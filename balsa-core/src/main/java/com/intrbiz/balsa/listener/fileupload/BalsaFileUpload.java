package com.intrbiz.balsa.listener.fileupload;

import java.util.List;
import java.util.Locale;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;

import com.intrbiz.balsa.listener.BalsaRequest;

public class BalsaFileUpload extends FileUpload
{
    private static final String POST = "POST";

    private static final String MULTIPART = "multipart/";

    public static final boolean isMultipartContent(BalsaRequest request)
    {
        if (! POST.equalsIgnoreCase(request.getRequestMethod())) return false;
        String contentType = request.getContentType();
        if (contentType == null) return false;
        if (contentType.toLowerCase(Locale.ENGLISH).startsWith(MULTIPART)) return true;
        return false;
    }

    public BalsaFileUpload(FileItemFactory fileItemFactory)
    {
        super(fileItemFactory);
    }

    public List<FileItem> parseRequest(BalsaRequest request) throws FileUploadException
    {
        return super.parseRequest(new BalsaRequestContext(request));
    }
}
