package com.intrbiz.balsa.listener.http;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.listener.BalsaProcessor;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.FullHttpRequest;

public class BalsaHTTPHandler extends ChannelInboundHandlerAdapter
{
    private final BalsaApplication app;
    
    private final BalsaProcessor proc;
    
    private static Logger logger = LoggerFactory.getLogger(BalsaHTTPHandler.class);
    
    private final JsonFactory jsonFactory;
    
    private final YAMLFactory yamlFactory;
    
    private final XMLOutputFactory xmlOutFactory;
    
    private final XMLInputFactory xmlInFactory;

    public BalsaHTTPHandler(BalsaApplication app, BalsaProcessor proc, JsonFactory jsonFactory, YAMLFactory yamlFactory, XMLOutputFactory xmlOutFactory, XMLInputFactory xmlInFactory)
    {
        this.app = app;
        this.proc = proc;
        this.jsonFactory = jsonFactory;
        this.yamlFactory = yamlFactory;
        this.xmlOutFactory = xmlOutFactory;
        this.xmlInFactory = xmlInFactory;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception
    {
        if (msg instanceof FullHttpRequest)
        {
            FullHttpRequest req = (FullHttpRequest) msg;
            // parse the request
            logger.trace("HTTP Request: " + req.method() + " " + req.uri());
            BalsaHTTPRequest  breq = new BalsaHTTPRequest(ctx, req, this.jsonFactory, this.xmlInFactory, this.yamlFactory);
            BalsaHTTPResponse bres = new BalsaHTTPResponse(this.jsonFactory, this.xmlOutFactory, this.yamlFactory);
            BalsaContext bctx = new BalsaContext(this.app, breq, bres);
            // process
            try
            {
                // activate and bind
                bctx.activate();
                bctx.bind();
                try
                {
                    this.proc.process(bctx);
                    bres.sendResponse(ctx);
                }
                finally
                {
                    // unbind and deactivate
                    bctx.unbind();
                    bctx.deactivate();
                }
            }
            catch (Error e)
            {
                throw e;
            }
            catch (Throwable t)
            {
                logger.debug("Error handling HTTP request", t);
            }
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx)
    {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
    {
        cause.printStackTrace();
        ctx.close();
    }
}
