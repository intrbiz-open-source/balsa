package com.intrbiz.balsa.listener.fileupload;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.fileupload.RequestContext;

import com.intrbiz.balsa.listener.BalsaRequest;
import com.intrbiz.balsa.scgi.http.HTTP;

public class BalsaRequestContext implements RequestContext
{
    private final BalsaRequest request;
    
    public BalsaRequestContext(BalsaRequest request)
    {
        super();
        this.request = request;
    }

    @Override
    public String getCharacterEncoding()
    {
        // TODO
        return HTTP.Charsets.SCGI.name();
    }

    @Override
    public String getContentType()
    {
        return this.request.getContentType();
    }

    @Override
    public int getContentLength()
    {
        return this.request.getContentLength();
    }

    @Override
    public InputStream getInputStream() throws IOException
    {
        return this.request.getInput();
    }
}
