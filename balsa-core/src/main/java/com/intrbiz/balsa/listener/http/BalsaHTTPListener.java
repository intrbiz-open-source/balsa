package com.intrbiz.balsa.listener.http;

import java.net.InetSocketAddress;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.listener.BalsaListener;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;

public class BalsaHTTPListener extends BalsaListener
{
    public static final int DEFAULT_PORT = 8080;
    
    public static final int MAX_REQUEST_BODY = 50 * 1024 * 1024; /* 50 MiB */
    
    private static Logger logger = LoggerFactory.getLogger(BalsaHTTPListener.class);

    private EventLoopGroup acceptGroup;

    private EventLoopGroup ioGroup;

    private ServerBootstrap server;

    private Channel serverChannel;
    
    private JsonFactory jsonFactory = new JsonFactory(new ObjectMapper());
    
    private YAMLFactory yamlFactory = new YAMLFactory().disable(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID).enable(YAMLGenerator.Feature.MINIMIZE_QUOTES);
    
    private XMLOutputFactory xmlOutFactory = XMLOutputFactory.newFactory();
    
    private XMLInputFactory xmlInFactory = XMLInputFactory.newFactory();

    public BalsaHTTPListener()
    {
        super(DEFAULT_PORT);
    }

    public BalsaHTTPListener(int port, int poolSize)
    {
        super(port, poolSize);
    }

    public BalsaHTTPListener(int port)
    {
        super(port);
    }

    @Override
    public String getEngineName()
    {
        return "Balsa-HTTP-Listener";
    }
    
    public String getListenerType()
    {
        return "http";
    }

    @Override
    public void start() throws BalsaException
    {
        try
        {
            logger.info("Listening for HTTP requests on port " + this.getPort());
            //
            this.acceptGroup = new NioEventLoopGroup(1);
            this.ioGroup = new NioEventLoopGroup(this.getPoolSize());
            //
            this.server = new ServerBootstrap();
            this.server.group(this.acceptGroup, this.ioGroup);
            this.server.channel(NioServerSocketChannel.class);
            this.server.childHandler(new ChannelInitializer<SocketChannel>()
            {
                @Override
                public void initChannel(SocketChannel ch) throws Exception
                {
                    ChannelPipeline p = ch.pipeline();
                    p.addLast("decoder", new HttpRequestDecoder());
                    p.addLast("encoder", new HttpResponseEncoder());
                    p.addLast("aggregator", new HttpObjectAggregator(MAX_REQUEST_BODY));
                    p.addLast("handler", new BalsaHTTPHandler(
                            BalsaHTTPListener.this.application, 
                            BalsaHTTPListener.this.getProcessor(),
                            BalsaHTTPListener.this.jsonFactory,
                            BalsaHTTPListener.this.yamlFactory,
                            BalsaHTTPListener.this.xmlOutFactory,
                            BalsaHTTPListener.this.xmlInFactory
                    ));
                }
            });
            this.server.localAddress(new InetSocketAddress(this.getPort()));
            this.serverChannel = this.server.bind().sync().channel();
            //
            logger.info("Accepting HTTP requests on port " + this.getPort());
        }
        catch (Exception e)
        {
            throw new BalsaException("Failed to start HTTP listener", e);
        }
    }

    @Override
    public void shutdown()
    {
        this.stop();
        logger.info("Shutting down workers");
        try
        {
            this.acceptGroup.shutdownGracefully().sync();
            this.ioGroup.shutdownGracefully().sync();
        }
        catch (Exception e)
        {
        }
    }

    @Override
    public void stop()
    {
        logger.info("Stopping listener");
        try
        {
            this.serverChannel.close().sync();
        }
        catch (Exception e)
        {
        }
    }
}
