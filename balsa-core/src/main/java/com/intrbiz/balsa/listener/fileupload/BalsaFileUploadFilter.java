package com.intrbiz.balsa.listener.fileupload;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.listener.BalsaFilter;
import com.intrbiz.balsa.listener.BalsaRequest;
import com.intrbiz.balsa.scgi.parameter.StringParameter;
import com.intrbiz.balsa.scgi.util.QueryStringParser;

public class BalsaFileUploadFilter implements BalsaFilter
{
    private BalsaFileUpload fileUpload;
    
    public BalsaFileUploadFilter(FileItemFactory fileItemFactory)
    {
        super();
        this.fileUpload = new BalsaFileUpload(fileItemFactory);
    }

    @Override
    public void filter(BalsaContext context, BalsaFilterChain next) throws Throwable
    {
        BalsaRequest request = context.request();
        if (BalsaFileUpload.isMultipartContent(request))
        {
            for (FileItem item : this.fileUpload.parseRequest(request))
            {
                if (item.isFormField())
                {
                    request.mergeParameter(
                        QueryStringParser.parseParameter(item.getFieldName(), item.getString(), StringParameter::new)
                    );
                }
                else
                {
                    request.mergeParameter(
                        QueryStringParser.parseParameter(item.getFieldName(), item, FileParameter::new)
                    );
                }
            }
        }
        next.filter(context);
    }
}
