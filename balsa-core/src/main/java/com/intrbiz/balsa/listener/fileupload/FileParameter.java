package com.intrbiz.balsa.listener.fileupload;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.scgi.parameter.AbstractParameter;
import com.intrbiz.balsa.scgi.parameter.Parameter;
import com.intrbiz.util.Util;

public final class FileParameter extends AbstractParameter
{
    private final FileItem file;
    
    public FileParameter(String name, int index, FileItem file)
    {
        super(name, index);
        this.file = file;
    }

    @Override
    public Object getValue()
    {
        return this.file.isFormField() ? this.file.getString() : this.getFile();
    }
    
    public UploadedFile getFile()
    {
        return new UploadedFile() {
            public long getSize()
            {
                return file.getSize();
            }
            
            public InputStream getInputStream() throws IOException
            {
                return file.getInputStream();
            }

            public String getContentType()
            {
                return file.getContentType();
            }
            
            public String getName()
            {
                return file.getName();
            }
            
            public byte[] get()
            {
                return file.get();
            }
            
            public void delete()
            {
                file.delete();
            }
        };
    }

    @Override
    public String getStringValue()
    {
        return this.file.isFormField() ? this.file.getString() : null;
    }

    @Override
    public List<Parameter> getListValue()
    {
        return Collections.singletonList(this);
    }

    @Override
    public int getLength()
    {
        return 0;
    }

    @Override
    public byte[] getBytesValue()
    {
        return this.file.get();
    }

    @Override
    public InputStream getInputStream()
    {
        try
        {
            return this.file.getInputStream();
        }
        catch (IOException e)
        {
            throw new BalsaException("Failed to read uploaded file", e);
        }
    }

    @Override
    public void close()
    {
        this.file.delete();
    }
    
    @Override
    public boolean isEmpty()
    {
        return this.file.isFormField() ?
                Util.isEmpty(this.file.getString()) :
                this.file.getSize() == 0;
    }
}
