package com.intrbiz.balsa.listener.fileupload;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

public interface UploadedFile
{
    long getSize();
    
    InputStream getInputStream() throws IOException;

    String getContentType();
    
    String getName();
    
    default void write(OutputStream to) throws IOException
    {
        IOUtils.copyLarge(getInputStream(), to);
    }
    
    byte[] get();
    
    void delete();
}
