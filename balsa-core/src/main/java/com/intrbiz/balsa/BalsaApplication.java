package com.intrbiz.balsa;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Supplier;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.intrbiz.balsa.bean.BeanProvider;
import com.intrbiz.balsa.bean.impl.SimpleBeanProvider;
import com.intrbiz.balsa.bean.impl.SupplierBeanProvider;
import com.intrbiz.balsa.component.BalsaComponent;
import com.intrbiz.balsa.component.BalsaService;
import com.intrbiz.balsa.engine.PublicResourceEngine;
import com.intrbiz.balsa.engine.RouteEngine;
import com.intrbiz.balsa.engine.SecurityEngine;
import com.intrbiz.balsa.engine.SessionEngine;
import com.intrbiz.balsa.engine.TaskEngine;
import com.intrbiz.balsa.engine.ViewEngine;
import com.intrbiz.balsa.engine.impl.publicresource.PublicResourceEngineImpl;
import com.intrbiz.balsa.engine.impl.route.RouteEngineImpl;
import com.intrbiz.balsa.engine.impl.security.SecurityEngineImpl;
import com.intrbiz.balsa.engine.impl.session.SimpleSessionEngine;
import com.intrbiz.balsa.engine.impl.task.TaskEngineImpl;
import com.intrbiz.balsa.engine.impl.view.BalsaViewEngineImpl;
import com.intrbiz.balsa.engine.route.Router;
import com.intrbiz.balsa.engine.security.method.AuthenticationMethod;
import com.intrbiz.balsa.engine.view.source.FileViewSource;
import com.intrbiz.balsa.inject.Injector;
import com.intrbiz.balsa.listener.BalsaFilter;
import com.intrbiz.balsa.listener.BalsaListener;
import com.intrbiz.balsa.listener.BalsaProcessor;
import com.intrbiz.balsa.listener.fileupload.BalsaFileUploadFilter;
import com.intrbiz.balsa.listener.filter.PublicResourceFilter;
import com.intrbiz.balsa.listener.filter.SessionFilter;
import com.intrbiz.balsa.listener.http.BalsaHTTPListener;
import com.intrbiz.balsa.listener.processor.FilterProcessor;
import com.intrbiz.balsa.listener.processor.RouteProcessor;
import com.intrbiz.balsa.listener.scgi.BalsaSCGIListener;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressContextBuilder;
import com.intrbiz.express.ExpressExtensionRegistry;
import com.intrbiz.express.action.ActionHandler;
import com.intrbiz.express.action.MethodActionHandler;
import com.intrbiz.express.operator.Decorator;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.util.ScriptedFunctionScanner;

/**
 * A balsa Application
 */
public abstract class BalsaApplication
{
    
    private static final Logger logger = LoggerFactory.getLogger(BalsaApplication.class);
    
    /**
     * The environment this application is running in
     */
    private String env = getApplicationEnv();
    
    /**
     * The name of this application instance
     */
    private String instanceName = getApplicationInstanceName(this.getClass());
    
    /**
     * Optional app config
     */
    private Object config;

    /**
     * Application scoped variables
     */
    private final ConcurrentMap<String, Object> vars = new ConcurrentHashMap<String, Object>();
    
    /**
     * Bean providers
     */
    private final ConcurrentMap<String, BeanProvider<?>> models = new ConcurrentHashMap<String, BeanProvider<?>>();
    
    /**
     * The Actions
     */
    private final ConcurrentMap<String, ActionHandler> actions = new ConcurrentHashMap<String, ActionHandler>();

    /**
     * The listener
     */
    private final ConcurrentMap<String, BalsaListener> listeners = new ConcurrentHashMap<String, BalsaListener>();
    
    /**
     * The services
     */
    private final ConcurrentMap<Class<?>, ConcurrentMap<String, Object>> services = new ConcurrentHashMap<>();
    
    /**
     * Balsa plugins which were found and registered
     */
    private final Map<Class<? extends BalsaPlugin>, BalsaPlugin> plugins = new ConcurrentHashMap<>();

    /**
     * The routing engine
     */
    private RouteEngine routingEngine;

    /**
     * The session engine
     */
    private SessionEngine sessionEngine;

    /**
     * The view engine
     */
    private ViewEngine viewEngine;
    
    /**
     * The security engine
     */
    private SecurityEngine securityEngine;
    
    /**
     * The task engine for executing long running jobs
     */
    private TaskEngine taskEngine;
    
    /**
     * The public resource Engine
     */
    private PublicResourceEngine publicResourceEngine;
    
    /**
     * Where to load views from
     */
    private final List<File> viewPath = new CopyOnWriteArrayList<File>();

    /**
     * Where to load components from
     */
    private final List<File> componentPath = new CopyOnWriteArrayList<File>();
    
    /**
     * Where to load functions from
     */
    private final List<File> functionsPath = new CopyOnWriteArrayList<File>();
    
    /**
     * Filters for this application
     */
    private final List<BalsaFilter> filters = new CopyOnWriteArrayList<BalsaFilter>();

    /**
     * File uplaod item factory
     */
    private FileItemFactory fileItemFactory;

    /**
     * The ExpressExtensionRegistry where Balsa stores its extensions
     */
    private final ExpressExtensionRegistry expressExtensions = new ExpressExtensionRegistry()
                                                               .addSubRegistry(ExpressExtensionRegistry.getDefaultRegistry());
    
    private final ExpressContextBuilder expressContextBuilder = ExpressContext.builder()
                                                                .withExtensions(this.expressExtensions)
                                                                .withPermissiveJavaAccess();
    
    private String[] templates = new String[0];
    
    private Thread shutdownHook;

    public BalsaApplication()
    {
        super();
    }
    
    @SuppressWarnings("unchecked")
    public <T> T config()
    {
        return (T) this.config;
    }
    
    public void config(Object config)
    {
        this.config = config;
    }

    /**
     * Get the listener for this application
     * 
     * @return returns BalsaListener
     */
    public Collection<BalsaListener> getListeners()
    {
        return this.listeners.values();
    }
    
    /**
     * Find the (first) listener of the given type
     * @param <T>
     * @param type the listener type
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T extends BalsaListener> T listener(Class<T> type)
    {
        for (BalsaListener listener : this.listeners.values())
        {
            if (type.isInstance(listener))
                return (T) listener;
        }
        return null;
    }

    /**
     * Setup the listener which will be used to process requests from the web server
     * 
     * @param listener
     *            The listener to use
     */
    public void listener(BalsaListener listener)
    {
        if (listener != null)
        {
            listener.setup(this);
            this.listeners.put(listener.getListenerType(), listener);
        }
    }
    
    /**
     * Register an application service and any actions is exposes too
     * @param <T> the service type
     * @param serviceType the service type
     * @param name the service name
     * @param service the service
     * @return the service
     */
    public <S, T extends S> T service(Class<S> serviceType, String name, T service)
    {
        Objects.requireNonNull(serviceType, "The service type must be given");
        Objects.requireNonNull(service, "Cannot register a null service");
        if (! serviceType.isAssignableFrom(service.getClass())) throw new IllegalArgumentException("The service must be an instance of " + serviceType);
        // setup the service if it implements BalsaService
        if (service instanceof BalsaService)
        {
            ((BalsaService) service).setup(this);
            ((BalsaService) service).serviceName(name);
        }
        // register the service
        this.services.computeIfAbsent(serviceType, (k) -> new ConcurrentHashMap<>())
            .put(name, service);
        // register the actions of the service
        this.action(service);
        // return the service which was registered
        return service;
    }
    
    /**
     * Register an application service and any actions is exposes too
     * @param <T> the service type
     * @param name the service name
     * @param service the service
     * @return the service
     */
    @SuppressWarnings("unchecked")
    public <T> T service(String name, T service)
    {
        Objects.requireNonNull(service, "Cannot register a null service");
        return service((Class<T>) service.getClass(), name, service);
    }
    
    /**
     * Register the default instance of a service
     * @param <T> the service type
     * @param serviceType the service type
     * @param service the service
     * @return the service
     */
    public <S, T extends S> T service(Class<S> serviceType, T service)
    {
        return this.service(serviceType, BalsaService.DEFAULT_SERVICE_NAME, service);
    }
    
    /**
     * Register the default instance of a service
     * @param <T> the service type
     * @param service the service
     * @return the service
     */
    @SuppressWarnings("unchecked")
    public <T> T service(T service)
    {
        Objects.requireNonNull(service, "Cannot register a null service");
        return this.service((Class<T>) service.getClass(), BalsaService.DEFAULT_SERVICE_NAME, service);
    }
    
    /**
     * Get the named instance of a service
     * @param <T> the service type
     * @param type the service type
     * @return the service or null if none is defined
     */
    @SuppressWarnings("unchecked")
    public <T> T service(Class<T> type, String name)
    {
        ConcurrentMap<String, Object> namedServices = this.services.get(type);
        return (namedServices != null) ? (T) namedServices.get(name) : null;
    }
    
    /**
     * Get the default instance of a service
     * @param <T> the service type
     * @param type the service type
     * @return the service or null if none is defined
     */
    public <T> T service(Class<T> type)
    {
        return this.service(type, BalsaService.DEFAULT_SERVICE_NAME);
    }
    
    /**
     * Get the named instance of a service with the given type simple name.
     * 
     * Note: This is intended to be used by Express expressions rather than from code.
     * 
     * @param <T>
     * @param type the class simple name
     * @param name the service registered name
     * @return the service or null if none can be found
     */
    @SuppressWarnings("unchecked")
    public <T> T service(String type, String name)
    {
        if (type != null && name != null)
        {
            for (Entry<Class<?>, ConcurrentMap<String, Object>> serviceSet : this.services.entrySet())
            {
                if (type.equals(serviceSet.getKey().getSimpleName()))
                {
                    return (T) serviceSet.getValue().get(name);
                }
            }
        }
        return null;
    }
    
    /**
     * Get the default instance of a service with the given type simple name.
     * 
     * Note: This is intended to be used by Express expressions rather than from code.
     * 
     * @param <T>
     * @param type the class simple name
     * @return the service or null if none can be found
     */
    public <T> T service(String type)
    {
        return this.service(type, BalsaService.DEFAULT_SERVICE_NAME);
    }
    
    /**
     * Get the registered service types
     * @return the {@link Set} of service types
     */
    public Set<Class<?>> serviceTypes()
    {
        return Collections.unmodifiableSet(this.services.keySet());
    }
    
    /**
     * Get the names of the named service for a given type
     * @param type the service type
     * @return the {@link Set} of service names
     */
    public Set<String> serviceNames(Class<?> type)
    {
        ConcurrentMap<String, Object> namedServices = this.services.get(type);
        return (namedServices != null) ? Collections.unmodifiableSet(namedServices.keySet()) : Collections.emptySet();
    }
    
    /**
     * Set the routing engine used by this application
     * @param engine
     */
    public void routingEngine(RouteEngine engine)
    {
        this.routingEngine = engine;
        if (this.routingEngine != null)
            this.routingEngine.setup(this);
    }

    /**
     * Get the routing engine used by this application
     * @return returns the routing engine in use
     */
    public RouteEngine getRoutingEngine()
    {
        return routingEngine;
    }

    /**
     * Get the routers in the routing engine
     * 
     * @return returns List<Router>
     */
    public List<Router<?>> getRouters()
    {
        return this.getRoutingEngine().getRouters();
    }

    /**
     * Add a router to this application
     * 
     * @param router
     *            returns void
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void router(Router<?> router) throws Exception
    {
        if (router != null)
        {
            ((Router) router).setup(this);
            this.getRoutingEngine().router(router);
        }
    }

    /**
     * Get the session engine
     * 
     * @return returns SessionEngine
     */
    public SessionEngine getSessionEngine()
    {
        return this.sessionEngine;
    }

    /**
     * Set the session engine
     * 
     * @param engine
     *            returns void
     */
    public void sessionEngine(SessionEngine engine)
    {
        this.sessionEngine = engine;
        if (this.sessionEngine != null)
            this.sessionEngine.setup(this);
    }

    /**
     * Get the view engine
     * 
     * @return returns ViewEngine
     */
    public ViewEngine getViewEngine()
    {
        return this.viewEngine;
    }

    /**
     * Set the view engine
     * 
     * @param engine
     *            returns void
     */
    public void viewEngine(ViewEngine engine)
    {
        this.viewEngine = engine;
        if (this.viewEngine != null)
            this.viewEngine.setup(this);
    }
    
    /**
     * The view paths
     */
    public List<File> getViewPath()
    {
        return Collections.unmodifiableList(this.viewPath);
    }
    
    /**
     * Add a view path
     */
    public void viewPath(File path)
    {
        this.viewPath.add(path);
    }
    
    /**
     * The component paths
     */
    public List<File> getComponentPath()
    {
        return Collections.unmodifiableList(this.componentPath);
    }
    
    /**
     * Add a scripted functions path
     */
    public void functionsPath(File path)
    {
        this.functionsPath.add(path);
    }
    
    /**
     * The scripted functions paths
     */
    public List<File> getFunctionsPath()
    {
        return Collections.unmodifiableList(this.functionsPath);
    }
    
    /**
     * Add a component path
     */
    public void componentPath(File path)
    {
        this.componentPath.add(path);
    }
    
    /**
     * The application templates
     */
    public String[] templates()
    {
        return this.templates;
    }
    
    /**
     * The application templates
     */
    public String[] getTemplates()
    {
        return this.templates;
    }
    
    /**
     * Add an application template
     */
    public void template(String template)
    {
        String[] s = new String[this.templates.length + 1];
        System.arraycopy(this.templates, 0, s, 0, this.templates.length);
        s[s.length] = template;
        this.templates = s;
    }
    
    /**
     * Get the engine responsible for handling authentication and authorisation
     */
    public SecurityEngine getSecurityEngine()
    {
        return this.securityEngine;
    }
    
    /**
     * Set the engine responsible for handling authentication and authorisation
     */
    public void securityEngine(SecurityEngine securityEngine)
    {
        this.securityEngine = securityEngine;
        if (this.securityEngine != null)
            this.securityEngine.setup(this);
    }
    
    /**
     * Get the named authentication method
     */
    public <T extends AuthenticationMethod<?>> T getAuthenticationMethod(String name)
    {
        return this.getSecurityEngine().getAuthenticationMethod(name);
    }
    
    public void authenticationMethod(AuthenticationMethod<?> authenticationMethod)
    {
        if (this.securityEngine == null)
            throw new IllegalStateException("Cannot register authentication method as no security engine is currently setup");
        this.securityEngine.registerAuthenticationMethod(authenticationMethod);
    }
    
    /**
     * Get the engine responsible for executing long running jobs in the background
     */
    public TaskEngine getTaskEngine()
    {
        return this.taskEngine;
    }
    
    /**
     * Set the engine responsible for executing long running jobs in the background
     */
    public void taskEngine(TaskEngine taskEngine)
    {
        this.taskEngine = taskEngine;
        if (this.taskEngine != null)
            this.taskEngine.setup(this);
    }
    
    /**
     * Get the engine responsible for handling all public resource URLs
     */
    public PublicResourceEngine getPublicResourceEngine()
    {
        return this.publicResourceEngine;
    }
    
    /**
     * Set the engine responsible for handling all public resource URLs
     */
    public void publicResourceEngine(PublicResourceEngine publicResourceEngine)
    {
        this.publicResourceEngine = publicResourceEngine;
        if (this.publicResourceEngine != null)
            this.publicResourceEngine.setup(this);
    }

    /**
     * Get the filters this application is using
     * 
     * @return the List<BalsaFilter> of filters
     */
    public List<BalsaFilter> filters()
    {
        return this.filters;
    }

    /**
     * Add a filter to this application
     * 
     * @param filter
     *            the BalsaFilter to add
     */
    public void filter(BalsaFilter filter)
    {
        this.filters.add(filter);
    }

    /**
     * Remove all filters from this application
     */
    public void clearFilters()
    {
        this.filters.clear();
    }
    
    /**
     * Get the application file upload factory
     */
    public FileItemFactory fileItemFactory() {
        return this.fileItemFactory;
    }
    
    /**
     * Set the application file upload factory
     */
    public void fileItemFactory(FileItemFactory factory) {
        this.fileItemFactory = factory;
    }

    /*
     * Beans
     */

    /**
     * Register a model
     * 
     * @param model
     *            returns void
     */
    public <E> BeanProvider<E> model(Class<E> bean) throws BalsaException
    {
        return this.model(new SimpleBeanProvider<E>(bean));
    }
    
    /**
     * Register a model with the given supplier
     * @param <E> the model type
     * @param bean the model class
     * @param supplier the supplier which will create an instance of the model
     * @return the registered bean provider
     * @throws BalsaException
     */
    public <E> BeanProvider<E> model(Class<E> bean, Supplier<E> supplier) throws BalsaException
    {
        return this.model(new SupplierBeanProvider<E>(bean, supplier));
    }

    /**
     * Register a model provider
     * 
     * @param provider
     *            returns void
     */
    public <E> BeanProvider<E> model(BeanProvider<E> provider)
    {
        if (provider != null)
        {
            String name = provider.getName();
            if (!this.models.containsKey(name))
            {
                this.models.put(name, provider);
                logger.info("Registered model bean " + name + " with " + (provider.isSession() ? "session" : "request") + " scope and with provider " + provider.getClass().getSimpleName() + " and implementation " + provider.getBeanClass());
            }
        }
        return provider;
    }

    /**
     * Create a registered model object
     * @param name the model name
     * @return returns the model
     */
    @SuppressWarnings("unchecked")
    public <E> E model(String name)
    {
        BeanProvider<E> provider = (BeanProvider<E>) this.models.get(name);
        if (provider == null) throw new BalsaException("The model '" + name + "' is not registed");
        return provider.create();
    }
    
    /**
     * Is the given model name session scoped
     * @param name the model name
     * @return true if the model is session scoped
     */
    public boolean isSessionModel(String name)
    {
        BeanProvider<?> provider = this.models.get(name);
        return provider != null ? provider.isSession() : false;
    }
    
    /**
     * Is the given name a registered model object
     * @param name the bean / variable name
     * @return true if the model name is registered
     */
    public boolean isModel(String name)
    {
        return this.models.containsKey(name);
    }
    
    /**
     * Get an application scoped variable
     * @param <E> the expected variable type
     * @param name the variable name
     * @return the variable value
     */
    @SuppressWarnings("unchecked")
    public <E> E var(String name)
    {
        return (E) this.vars.get(name);
    }
    
    /**
     * Set an application scoped variable
     * @param <E> the variable type
     * @param name the variable name
     * @param value the variable value
     * @return the given variable value
     */
    public <E> E var(String name, E value)
    {
        if (value == null) this.vars.remove(name);
        else this.vars.put(name, value);
        return value;
    }

    /**
     * Register the given actions
     * 
     * returns void
     */
    public void action(Object action)
    {
        // scan the given class for actions
        for (ActionHandler handler : MethodActionHandler.findActionHandlers(action))
        {
            this.action(handler);
        }
    }

    /**
     * Register the given action handler
     * 
     * @param action
     *            returns void
     */
    public void action(ActionHandler action)
    {
        if (action != null)
        {
            if (logger.isTraceEnabled()) logger.trace("Registered action handler " + action.getName() + " => " + action);
            this.actions.put(action.getName(), action);
        }
    }

    /**
     * Get the action handler for the given name
     * 
     * @param name
     *            the action name
     * @return returns ActionHandler
     */
    public ActionHandler action(String name)
    {
        return this.actions.get(name);
    }
    
    /**
     * Get the Express extensions for this Balsa application
     * @return
     */
    public ExpressExtensionRegistry expressExtensions()
    {
        return this.expressExtensions;
    }
    
    public ExpressExtensionRegistry immutableFunction(Function immutableFunction)
    {
        return expressExtensions.addFunction(immutableFunction);
    }

    public ExpressExtensionRegistry function(String name, Class<? extends Function> functionClass)
    {
        return expressExtensions.addFunction(name, functionClass);
    }

    public ExpressExtensionRegistry decorator(String name, Class<?> entityType, Class<? extends Decorator> decoratorClass)
    {
        return expressExtensions.addDecorator(name, entityType, decoratorClass);
    }
    
    public ExpressContextBuilder getExpressContextBuilder()
    {
        return this.expressContextBuilder;
    }
    
    public Collection<BalsaPlugin> getPlugins()
    {
        return Collections.unmodifiableCollection(this.plugins.values());
    }
    
    /**
     * Register a {@link BalsaPlugin}, by setting up this application using it and tracking the loaded plugin.
     * @param plugin the Balsa plugin to register with this application 
     * @throws Exception
     */
    public void registerPlugin(BalsaPlugin plugin) throws Exception
    {
        if (plugin != null)
        {
            if (this.plugins.putIfAbsent(plugin.getClass(), plugin) == null)
            {
                // setup this application with the plugin
                logger.info("Registering Balsa plugin {}", plugin.getClass().getSimpleName());
                plugin.setup(this);
            }
        }
    }
    
    public String getEnv()
    {
        return this.env;
    }
    
    public boolean isDevEnv()
    {
        return "dev".equals(this.getEnv());
    }
    
    public boolean isTestEnv()
    {
        return "test".equals(this.getEnv());
    }
    
    public boolean isProdEnv()
    {
        return "prod".equals(this.getEnv());
    }

    /*
     * Life cycle
     */

    /**
     * See:
     * - loadConfig();
     * - setupListeners()
     * - setupEngines()
     * - setupServices()
     * - setupActions()
     * - setupModels()
     * - setupRouters()
     * - setupFunctions()
     * @throws Exception
     */
    protected void setup() throws Exception
    {
        // load any config vars
        this.loadConfig();
        // setup application listeners
        this.setupListeners();
        // setup application engines
        this.setupEngines();
        // setup the application services
        this.setupServices();
        // setup the application actions
        this.setupActions();
        // setup the application model objects
        this.setupModels();
        // setup the application routers
        this.setupRouters();
        // setup the application functions
        this.setupFunctions();
    }
    
    /**
     * Load any application configuration
     * @throws Exception
     */
    protected void loadConfig() throws Exception
    {
    }
    
    /**
     * Setup the application listeners
     * @throws Exception
     */
    protected void setupListeners() throws Exception
    {
    }
    
    /**
     * Setup the application services
     * @throws Exception
     */
    protected void setupServices() throws Exception
    {
    }
    
    /**
     * Setup the application engines
     * @throws Exception
     */
    protected void setupEngines() throws Exception
    {
    }
    
    /**
     * Setup the application Express functions
     * @throws Exception
     */
    protected void setupFunctions() throws Exception
    {
    }
    
    /**
     * Setup the application model objects
     * @throws Exception
     */
    protected void setupModels() throws Exception
    {
    }
    
    /**
     * Setup the application actions
     * @throws Exception
     */
    protected void setupActions() throws Exception
    {
    }
    
    /**
     * Setup the application routers
     * @throws Exception
     */
    protected abstract void setupRouters() throws Exception;
    
    /**
     * Scan and register any {@link BalsaPlugin}s which can be located
     * @throws Exception
     */
    protected void setupPlugins() throws Exception
    {
        // scan for all Balsa plugins and register them
        for (BalsaPlugin plugin : ServiceLoader.load(BalsaPlugin.class))
        {
            this.registerPlugin(plugin);
        }
    }

    /**
     * Start the Balsa application
     * 
     * @throws Exception
     *             returns void
     */
    public final void start() throws Exception
    {
        // configure logging
        this.configureLogging();
        // defaults
        this.setupDefaultEngines();
        this.setupDefaultListeners();
        // plugins
        this.setupPlugins();
        // Setup the app
        this.setup();
        // Check we have stuff
        if (this.getListeners().isEmpty()) throw new BalsaException("The Balsa application must have a listener");
        if (this.getViewEngine() == null) throw new BalsaException("The Balsa application must have a view engine");
        // configure the listeners
        for (BalsaListener listener : this.getListeners())
        {
            listener.setPort(this.getListenerPort(listener.getListenerType(), listener.getDefaultPort()));
            listener.setPoolSize(this.getListenerThreadCount(listener.getListenerType(), BalsaListener.DEFAULT_POOL_SIZE));
        }
        // configure the session engine
        if (this.getSessionEngine() != null)
        {
            this.getSessionEngine().setSessionLifetime(this.getSessionLifetime(SessionEngine.DEFAULT_SESSION_LIFETIME));
        }
        // setup view engine
        this.viewPath(this.computeViewPath());
        this.viewPath(this.computeContentPath());
        this.componentPath(this.computeComponentPath());
        this.functionsPath(this.computeFunctionsPath());
        if (this.isDevEnv())
        {
            this.getViewEngine().cacheOff();
        }
        for (File functionsPath : this.getFunctionsPath())
        {
            logger.debug("Scanning {} for scripted functions", functionsPath.getAbsolutePath());
            ScriptedFunctionScanner.scanScriptedFunctions(this.expressExtensions(), functionsPath);
        }
        logger.debug("Express extensions: {}", this.expressExtensions);
        if (this.isDevEnv())
        {
            logger.debug("Enabling Express Java access logging");
            this.getExpressContextBuilder().withAuditJavaAccess();
        }
        // Construct the processor chain
        BalsaProcessor proc = this.constructProcessingChain();
        logger.debug("Balsa processing chain: {}", proc);
        for (BalsaListener listener : this.getListeners())
        {
            listener.setProcessor(proc);
        }
        // Application specific pre-start
        this.startApplicationEarly();
        // Start the application services
        this.startServices();
        // Start the security engine
        Injector.injectServices(this, this.getSecurityEngine());
        this.getSecurityEngine().start(this);
        // Start the task engine
        if (this.getTaskEngine() != null)
        {
            Injector.injectServices(this, this.getTaskEngine());
            this.getTaskEngine().start(this);
        }
        // Start the session engine
        if (this.getSessionEngine() != null)
        {
            Injector.injectServices(this, this.getSessionEngine());
            this.getSessionEngine().start(this);
        }
        // Start the view engine
        Injector.injectServices(this, this.getViewEngine());
        this.getViewEngine().start(this);
        // Application specific start up
        this.startApplication();
        // start the routers
        Injector.injectServices(this, this.getRoutingEngine());
        this.getRoutingEngine().start(this);
        // Start the listeners
        for (BalsaListener listener : this.getListeners())
        {
            Injector.injectServices(this, listener);
            logger.info("Starting listener: " + listener.getEngineName() + " on port " + listener.getPort());
            listener.start();
        }
        // Application specific post-start
        this.startApplicationLate();
        // finally add a shutdown hook to invoke stop
        synchronized (this)
        {
            if (this.shutdownHook == null)
            {
                this.shutdownHook = new Thread(this::stop);
                Runtime.getRuntime().addShutdownHook(this.shutdownHook);
            }
        }
    }
    
    
    /**
     * Start up application servers
     * @throws Exception
     */
    @SuppressWarnings({"rawtypes", "unchecked" })
    protected void startServices() throws Exception
    {
        // dep injection
        for (ConcurrentMap<String, Object> named : this.services.values())
        {
            for (Object service : named.values())
            {
                Injector.injectServices(this, service);
            }
        }
        // start and lifecycled services
        for (ConcurrentMap<String, Object> named : this.services.values())
        {
            for (Object service : named.values())
            {
                if (service instanceof BalsaComponent)
                    ((BalsaComponent) service).start(this);
            }
        }
    }

    protected BalsaProcessor constructProcessingChain()
    {
        return this.constructHeadFilterChain(this.constructFilterChain(this.filters(), this.constructTailFilterChain(new RouteProcessor(this.getRoutingEngine()))));
    }

    protected BalsaProcessor constructHeadFilterChain(BalsaProcessor processor)
    {
        return new FilterProcessor(new SessionFilter(), processor);
    }

    protected BalsaProcessor constructTailFilterChain(BalsaProcessor processor)
    {
        return processor;
    }

    protected BalsaProcessor constructFilterChain(List<BalsaFilter> filters, BalsaProcessor processor)
    {
        ListIterator<BalsaFilter> i = filters.listIterator(filters.size());
        while (i.hasPrevious())
        {
            processor = new FilterProcessor(i.previous(), processor);
        }
        return processor;
    }
    
    /**
     * Configure the logging framework in use, by default Reload4J is used.
     * 
     * If you wish to use a different framework, override this method and exclude Reload4J.
     * 
     */
    protected void configureLogging()
    {
        String logging = getProperty("balsa.logging", "console");
        if ("console".equals(logging))
        {
            // configure logging to terminal
            BasicConfigurator.configure();
            org.apache.log4j.Logger.getRootLogger().setLevel(Level.toLevel(getProperty("balsa.logging.level", this.isProdEnv() ? "info" : "debug").toUpperCase()));
        }
        else
        {
            // configure from file
            PropertyConfigurator.configure(new File(logging).getAbsolutePath());
        }
    }
    
    protected void setupDefaultEngines() throws Exception
    {
        /* Default Engines */
        this.routingEngine(new RouteEngineImpl());
        this.sessionEngine(new SimpleSessionEngine());
        this.viewEngine(new BalsaViewEngineImpl());
        this.securityEngine(new SecurityEngineImpl());
        this.publicResourceEngine(new PublicResourceEngineImpl());
        this.taskEngine(new TaskEngineImpl());
        // setup the file item factory
        String uploadDirProp = getProperty("balsa.file.upload.path", null);
        File uploadDir = uploadDirProp == null ? null : new File(uploadDirProp);
        if (uploadDir != null) uploadDir.mkdirs();
        this.fileItemFactory(new DiskFileItemFactory(1 * 1024 * 1024, uploadDir));
    }
    
    protected void setupDefaultListeners() throws Exception
    {
        /* Default Listeners */
        this.listener(
                new BalsaSCGIListener()
                .withReusePort(Boolean.parseBoolean(getProperty("balsa.reuseport", "false")))
        );
        // dev env by defauly add HTTP listener
        if (Boolean.parseBoolean(getProperty("balsa.http", this.isDevEnv() ? "true" : "false")))
        {
            // regidter the http listener
            this.listener(
                    new BalsaHTTPListener()
            );
            // resource filter
            filter(
                new PublicResourceFilter(computePublicResourcePath())
                    .setMaxAge(Integer.valueOf(getProperty("balsa.public.maxage", "3600")))
            );
        }
        // setup the file upload filter
        if (this.fileItemFactory() != null)
        {
            this.filter(new BalsaFileUploadFilter(this.fileItemFactory()));
        }
    }
    
    /**
     * Factory method used by route handlers to create a JSON ObjectMapper
     * @return an {@link ObjectMapper} configured for use with JSON formatted data
     */
    public ObjectMapper createJSONMapper()
    {
        return new ObjectMapper()
                .setSerializationInclusion(Include.NON_NULL)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false)
                .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());
    }
    
    /**
     * Factory method used by route handlers to create a YAML ObjectMapper
     * @return an {@link ObjectMapper} configured for use with YAML formatted data
     */
    public ObjectMapper createYAMLMapper()
    {
        YAMLFactory yaml = new YAMLFactory()
                .disable(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID)
                .enable(YAMLGenerator.Feature.MINIMIZE_QUOTES);
        return new ObjectMapper(yaml)
                .setSerializationInclusion(Include.NON_NULL)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false)
                .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());
    }
    
    /**
     * Any application specific startup actions, before any Balsa engines are started
     * @throws Exception
     */
    protected void startApplicationEarly() throws Exception
    {
    }
    
    /**
     * Any application specific startup actions, before the listeners are started
     * @throws Exception
     */
    protected void startApplication() throws Exception
    {
    }
    
    /**
     * Any application specific startup actions, after the listeners are started
     * @throws Exception
     */
    protected void startApplicationLate() throws Exception
    {
    }

    /**
     * Shutdown the Balsa application
     */
    protected void shutdown()
    {
    }

    /**
     * Stop the Balsa application
     */
    @SuppressWarnings("rawtypes")
    public final void stop()
    {
        // stop listeners
        for (BalsaListener listener : this.getListeners())
        {
            listener.stop();
        }
        // stop the view engine
        this.getViewEngine().stop();
        // stop the session engine
        if (this.getSessionEngine() != null) this.getSessionEngine().stop();
        // stop and lifecycled services
        for (ConcurrentMap<String, Object> named : this.services.values())
        {
            for (Object service : named.values())
            {
                if (service instanceof BalsaComponent)
                    ((BalsaComponent) service).stop();
            }
        }
        // shutdown
        this.shutdown();
        // finally remove the shutdown hook
        synchronized (this)
        {
            if (this.shutdownHook != null)
            {
                Runtime.getRuntime().removeShutdownHook(this.shutdownHook);
            }
        }
    }
    
    public String getInstanceName()
    {
        return this.instanceName;
    }
    
    public static String getApplicationInstanceName(Class<? extends BalsaApplication> applicationClass)
    {
        return getProperty("balsa.instance.name", applicationClass.getSimpleName().toLowerCase() + "." + getApplicationEnv());
    }
    
    public static String getApplicationEnv()
    {
        return getProperty("balsa.env", "dev");
    }
    
    protected int getListenerPort(String listenerType, int defaultPort)
    {
        return getIntProperty("balsa." + listenerType + ".port", defaultPort);
    }
    
    protected int getListenerThreadCount(String listenerType, int defaultThreadCount)
    {
        return getIntProperty("balsa." + listenerType + ".workers", getIntProperty("balsa.workers", defaultThreadCount));
    }
    
    protected int getSessionLifetime(int defaultSessionLifetime)
    {
        return getIntProperty("balsa.session-lifetime", defaultSessionLifetime);
    }
    
    protected String getViewPath(String defaultPath)
    {
        return getProperty("balsa.view.path", defaultPath);
    }
    
    protected String getContentPath(String defaultPath)
    {
        return getProperty("balsa.content.path", defaultPath);
    }
    
    protected String getComponentPath(String defaultPath)
    {
        return getProperty("balsa.component.path", defaultPath);
    }
    
    protected String getFunctionsPath(String defaultPath)
    {
        return getProperty("balsa.functions.path", defaultPath);
    }
    
    protected File computeViewPath()
    {
        if (this.isDevEnv()) return new File(getViewPath(FileViewSource.DEV_VIEW_PATH));
        else if (this.isTestEnv()) return new File(getViewPath(FileViewSource.TEST_VIEW_PATH));
        return new File(getViewPath(FileViewSource.PROD_VIEW_PATH));
    }
    
    protected File computeContentPath()
    {
        if (this.isDevEnv()) return new File(getContentPath(FileViewSource.DEV_CONTENT_PATH));
        else if (this.isTestEnv()) return new File(getContentPath(FileViewSource.TEST_CONTENT_PATH));
        return new File(getContentPath(FileViewSource.PROD_CONTENT_PATH));
    }
    
    protected File computeComponentPath()
    {
        if (this.isDevEnv()) return new File(getComponentPath(FileViewSource.DEV_COMPONENT_PATH));
        else if (this.isTestEnv()) return new File(getComponentPath(FileViewSource.TEST_COMPONENT_PATH));
        return new File(getComponentPath(FileViewSource.PROD_COMPONENT_PATH));
    }
    
    protected File computeFunctionsPath()
    {
        if (this.isDevEnv()) return new File(getFunctionsPath(FileViewSource.DEV_FUNCTIONS_PATH));
        else if (this.isTestEnv()) return new File(getFunctionsPath(FileViewSource.TEST_FUNCTIONS_PATH));
        return new File(getFunctionsPath(FileViewSource.PROD_FUNCTIONS_PATH));
    }
    
    protected File computePublicResourcePath()
    {
        return new File(getProperty("balsa.public.path", this.isDevEnv() ? PublicResourceFilter.DEV_PUBLIC_PATH : PublicResourceFilter.PUBLIC_PUBLIC_PATH));
    }
    
    /**
     * Get a configuration property from either system properties or env vars
     * @param propertyName
     * @param defaultValue
     * @return the property value or the default value
     */
    public static String getProperty(String propertyName, String defaultValue)
    {
        String value = System.getProperty(propertyName);
        if (value != null) return value;
        value = System.getenv(propertyName.toUpperCase().replace('.', '_'));
        if (value != null) return value;
        return defaultValue;
    }
    
    public static boolean getBooleanProperty(String propertyName, boolean defaultValue)
    {
        String val = getProperty(propertyName, null);
        return val == null ? defaultValue : ("true".equalsIgnoreCase(val) || "yes".equalsIgnoreCase(val));
    }
    
    public static int getIntProperty(String propertyName, int defaultValue)
    {
        String val = getProperty(propertyName, null);
        return val == null ? defaultValue : Integer.parseInt(val);
    }
}
