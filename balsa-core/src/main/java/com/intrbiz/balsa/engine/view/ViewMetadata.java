package com.intrbiz.balsa.engine.view;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.dynamic.DynamicEntity;

public final class ViewMetadata implements DynamicEntity
{
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ISO_LOCAL_DATE.withZone(ZoneId.systemDefault());
    
    private static final Logger logger = LoggerFactory.getLogger(ViewMetadata.class);
    
    private final Map<String, Object> metadata = new TreeMap<String, Object>();
    
    private transient ZonedDateTime date = null;
    
    public ViewMetadata()
    {
        super();
    }
    
    public void setAllAttributes(Map<String, Object> data)
    {
        if (data != null)
        {
            for (Entry<String, Object> entry : data.entrySet())
            {
                this.metadata.put(entry.getKey().toLowerCase(), entry.getValue());
            }
        }
    }
    
    public Object getAttribute(String name)
    {
        return this.metadata.get(name.toLowerCase());
    }
    
    @SuppressWarnings("unchecked")
    public <T> T getAttribute(String name, Class<T> type)
    {
        Object a = this.metadata.get(name.toLowerCase());
        return type.isInstance(a) ? (T) a : null;
    }
    
    public Object getAttribute(String name, Object defaultValue)
    {
        Object value = this.getAttribute(name);
        return (value == null) ? defaultValue : value;
    }
    
    public <T> T getAttribute(String name, T defaultValue, Class<T> type)
    {
        T a = this.getAttribute(name, type);
        return (a == null) ? defaultValue : a;
    }
    
    public void setAttribute(String name, Object value)
    {
        if (value != null)
            this.metadata.put(name.toLowerCase(), value);
    }
    
    public void setAttributeIfAbsent(String name, Object value)
    {
        if (value != null)
            this.metadata.putIfAbsent(name, value);
    }
    
    public boolean containsAttribute(String name)
    {
        return this.metadata.containsKey(name.toLowerCase());
    }
    
    public Set<String> getAttributeNames()
    {
        return this.metadata.keySet();
    }
    
    public void removeAttribute(String name)
    {
        this.metadata.remove(name.toLowerCase());
    }

    @Override
    public Object get(String name, ExpressContext context, Object source) throws ExpressException
    {
        return this.getAttribute(name);
    }

    @Override
    public void set(String name, Object value, ExpressContext context, Object source) throws ExpressException
    {
        this.setAttribute(name, (value == null) ? null : String.valueOf(value));
    }
    
    /*
     * Common metadata attributes
     */
    
    public String getTitle()
    {
        return String.valueOf(this.getAttribute("title"));
    }
    
    public String getSubtitle()
    {
        return String.valueOf(this.getAttribute("subtitle"));
    }
    
    public String getDescription()
    {
        return String.valueOf(this.getAttribute("description"));
    }
    
    public String getDate()
    {
        return String.valueOf(this.getAttribute("date", "1970-01-01"));
    }
    
    public ZonedDateTime parseDate()
    {
        if (this.date == null)
        {
            try
            {
                this.date = ZonedDateTime.of(DATE_FORMAT.parse(this.getDate(), LocalDate::from), LocalTime.MIDNIGHT, ZoneId.systemDefault());
            }
            catch (DateTimeParseException e)
            {
                logger.warn("Failed to parse view data metadata", e);
            }
        }
        return this.date;
    }
    
    public String getAuthor()
    {
        return String.valueOf(this.getAttribute("author"));
    }
    
    public int getOrder()
    {
        return (int) this.getAttribute("order", 0);
    }
    
    public String getImage()
    {
        return String.valueOf(this.getAttribute("image"));
    }
    
    public String getVisibility()
    {
        return String.valueOf(this.getAttribute("visibility"));
    }
}
