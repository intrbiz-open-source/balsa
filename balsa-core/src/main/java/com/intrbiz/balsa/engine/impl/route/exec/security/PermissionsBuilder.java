package com.intrbiz.balsa.engine.impl.route.exec.security;

import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.List;

import com.intrbiz.balsa.engine.impl.route.exec.ExecutorClass;
import com.intrbiz.balsa.metadata.RequirePermission;
import com.intrbiz.balsa.metadata.RequirePermissions;
import com.intrbiz.util.Util;
import com.intrbiz.util.compiler.util.JavaUtil;

public class PermissionsBuilder extends SecurityBuilder
{
    private List<Permission> permissions = new LinkedList<Permission>();

    @Override
    public void fromAnnotation(Annotation a)
    {
        if (a instanceof RequirePermissions)
        {
            RequirePermissions pr = (RequirePermissions) a;
            for (RequirePermission p : pr.value())
            {
                this.permission(p);
            }
        }
        else if (a instanceof RequirePermission)
        {
            this.permission((RequirePermission) a);
        }
    }
    
    private void permission(RequirePermission p)
    {
        this.permissions.add(new Permission(p.value(), p.object()));
    }
    
    public PermissionsBuilder permission(String permission)
    {
        this.permissions.add(new Permission(permission));
        return this;
    }
    
    public PermissionsBuilder permission(String permission, String object)
    {
        this.permissions.add(new Permission(permission, object));
        return this;
    }
    
    public PermissionsBuilder permission(String... permissions)
    {
        for (String permission : permissions)
        {
            this.permissions.add(new Permission(permission));
        }
        return this;
    }

    @Override
    public void compile(ExecutorClass cls)
    {
        StringBuilder sb = cls.getExecutorLogic();
        // generate the permission expressions
        for (Permission permission : this.permissions)
        {
            sb.append("    context.require(context.permission(\"").append(JavaUtil.escapeString(permission.permission)).append("\"");
            if (! Util.isEmpty(permission.object))
            {
                sb.append(", \"").append(JavaUtil.escapeString(permission.object)).append("\"");
            }
            sb.append("));\r\n");   
        }
    }
    
    @Override
    public boolean isSingular()
    {
        return false;
    }
    
    private static final class Permission
    {
        public String permission;
        
        public String object;

        public Permission(String permission, String object)
        {
            super();
            this.permission = permission;
            this.object = Util.isEmpty(object) ? null : object;
        }
        
        public Permission(String permission)
        {
            this(permission, null);
        }
    }
}
