package com.intrbiz.balsa.engine.impl.route.exec.response;

import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.intrbiz.balsa.engine.impl.route.exec.ExecutorClass;
import com.intrbiz.balsa.error.http.BalsaNotFound;
import com.intrbiz.balsa.metadata.JSON;
import com.intrbiz.balsa.scgi.http.HTTP.HTTPStatus;

public class JSONResponse extends ResponseBuilder
{
    private Class<?> type;

    private HTTPStatus status = HTTPStatus.OK;

    private boolean notFoundIfNull = false;
    
    protected List<Class<?>> subTypes = new LinkedList<Class<?>>();
    
    protected Class<?> view = null;

    public JSONResponse()
    {
        super();
    }

    public JSONResponse type(Class<?> type)
    {
        this.type = type;
        return this;
    }
    
    public JSONResponse subTypes(Class<?>... subTypes)
    {
        for (Class<?> subType : subTypes)
        {
            this.subTypes.add(subType);
        }
        return this;
    }
    
    public JSONResponse view(Class<?> view)
    {
        this.view = view;
        return this;
    }

    @Override
    public void compile(ExecutorClass cls)
    {
        cls.addImport(ObjectMapper.class.getCanonicalName());
        cls.addImport(ObjectWriter.class.getCanonicalName());
        cls.addImport(ParameterNamesModule.class.getCanonicalName());
        cls.addImport(Jdk8Module.class.getCanonicalName());
        cls.addImport(JavaTimeModule.class.getCanonicalName());
        cls.addImport(JsonGenerator.class.getCanonicalName());
        cls.addImport(HTTPStatus.class.getCanonicalName());
        cls.addImport(this.type.getCanonicalName());
        cls.addImport(Iterable.class.getCanonicalName());
        if (this.notFoundIfNull) cls.addImport(BalsaNotFound.class.getCanonicalName());
        // setup the object mapper
        cls.addField(ObjectMapper.class.getSimpleName(), "jsonResMapper");
        cls.addField(ObjectWriter.class.getSimpleName(), "jsonResWriter");
        StringBuilder csb = cls.getConstructorLogic();
        csb.append("    this.jsonResMapper = this.application.createJSONMapper();\r\n");
        for (Class<?> subType : this.subTypes)
        {
            cls.addImport(subType.getCanonicalName());
            csb.append("    this.jsonResMapper.registerSubtypes(").append(subType.getSimpleName()).append(".class);\r\n");
        }
        if (this.view == null)
        {
            csb.append("    this.jsonResWriter = this.jsonResMapper.writer();\r\n");
        }
        else
        {
            csb.append("    this.jsonResWriter = this.jsonResMapper.writerWithView(").append(this.view.getCanonicalName()).append(".class);\r\n");
        }
        //
        StringBuilder sb = cls.getExecutorLogic();
        sb.append("    // encode the response to JSON\r\n");
        sb.append("    if (context.response() != null) {\r\n");
        if (this.notFoundIfNull)
        {
            sb.append("        if (res == null) throw new BalsaNotFound();");
        }
        sb.append("        JsonGenerator writer = context.response().status(HTTPStatus." + this.status.name() + ").json().getJsonWriter();\r\n");
        if (Iterable.class.isAssignableFrom(this.type))
        {
            sb.append("        writer.writeStartArray();\r\n");
            sb.append("        for (Object resElement : res)\r\n");
            sb.append("        {\r\n");
            sb.append("            this.jsonResWriter.writeValue(writer, resElement);\r\n");
            sb.append("        }\r\n");
            sb.append("        writer.writeEndArray();\r\n");
        }
        else
        {
            sb.append("        this.jsonResWriter.writeValue(writer, res);\r\n");
        }
        sb.append("    }\r\n");
    }

    @Override
    public void fromAnnotation(Annotation a, Annotation[] annotations, Class<?> returnType)
    {
        JSON j = (JSON) a;
        this.type(returnType);
        // Response status
        this.status = j.status();
        this.notFoundIfNull = j.notFoundIfNull();
        // Any sub types to add
        this.subTypes(j.value());
        // Optional JSON view
        if (j.view().length == 1) this.view(j.view()[0]);
    }

    public boolean isNotFoundIfNull()
    {
        return notFoundIfNull;
    }

    public void setNotFoundIfNull(boolean notFoundIfNull)
    {
        this.notFoundIfNull = notFoundIfNull;
    }

    public HTTPStatus getStatus()
    {
        return status;
    }

    public void setStatus(HTTPStatus status)
    {
        this.status = status;
    }

    public Class<?> getType()
    {
        return type;
    }

    public void setType(Class<?> type)
    {
        this.type = type;
    }
    
    public Class<?> getView()
    {
        return this.view;
    }

    @Override
    public void verify(Class<?> returnType)
    {
        if (void.class == returnType) throw new IllegalStateException("Cannot encode the response of a void method!");
    }
}
