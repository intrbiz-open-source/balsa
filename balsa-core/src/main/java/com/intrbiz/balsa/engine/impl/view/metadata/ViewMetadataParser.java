package com.intrbiz.balsa.engine.impl.view.metadata;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.engine.view.ViewMetadata;

/**
 * Extract metadata from plain text based views like Markdown and APT
 * 
 * The metadata must be at the start of the file, in the format:
 * 
 * ---
 * name: value
 * name: value
 * ---
 * 
 * 
 */
public class ViewMetadataParser
{
    private static final int MAX_METADATA_SIZE = 8192;
    
    private static Logger logger = LoggerFactory.getLogger(ViewMetadataParser.class);
    
    private static final TypeReference<Map<String, Object>> TYPE = new TypeReference<>() {};
    
    public static ViewMetadata extractMetadata(File file)
    {
        try (FileReader in = new FileReader(file))
        {
            char[] buffer = new char[MAX_METADATA_SIZE];
            int length = in.read(buffer);
            if (length > 0)
            {
                return extractMetadata(new String(buffer, 0, length));
            }
        }
        catch (Exception e)
        {
            logger.warn("Failed to parse view metadata", e);
        }
        return null;
    }
    
    public static ViewMetadata extractMetadata(String text)
    {
        ViewMetadata md = new ViewMetadata();
        extractMetadata(text, md);
        return md;
    }

    public static String extractMetadata(String text, ViewMetadata metadata)
    {
        int start = text.indexOf("---");
        if (start != -1)
        {
            int end = text.indexOf("---", start + 3);
            if (end != -1)
            {
                String metadataBlock = text.substring(start, end);
                logger.debug("Extracted metadata: '{}'", metadataBlock);
                // parse the metadata
                parseMetadata(metadataBlock, metadata);
                // return the view without the metadata
                return text.substring(end + 3);
            }
        }
        return text;
    }

    private static void parseMetadata(String meta, ViewMetadata metadata)
    {
        try
        {
            ObjectMapper yaml = BalsaContext.get().app().createYAMLMapper();
            // parse the metadata as YAML
            Map<String, Object> data = yaml.readValue(meta, TYPE);
            logger.debug("Parsed metadata: " + data);
            metadata.setAllAttributes(data);
        }
        catch (IOException e)
        {
            logger.warn("Failed to parse view metadata", e);
        }
    }
    
    
}
