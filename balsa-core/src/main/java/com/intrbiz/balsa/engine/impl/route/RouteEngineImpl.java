package com.intrbiz.balsa.engine.impl.route;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.RouteEngine;
import com.intrbiz.balsa.engine.impl.AbstractBalsaEngine;
import com.intrbiz.balsa.engine.impl.route.Route.Filter;
import com.intrbiz.balsa.engine.impl.route.exec.ExecBuilder;
import com.intrbiz.balsa.engine.route.Router;
import com.intrbiz.balsa.error.BalsaInternalError;
import com.intrbiz.balsa.error.http.BalsaNotFound;
import com.intrbiz.balsa.inject.Injector;
import com.intrbiz.balsa.listener.BalsaRequest;

/**
 * The actual routing engine
 */
public class RouteEngineImpl extends AbstractBalsaEngine implements RouteEngine
{
    private static Logger logger = LoggerFactory.getLogger(RouteEngineImpl.class);
    
    private final List<Router<?>> routers = new ArrayList<Router<?>>();

    private final List<PrefixContainer> prefixes = new ArrayList<PrefixContainer>();
    
    private int totalRoutes = 0;

    public RouteEngineImpl()
    {
        super();
    }

    public String getEngineName()
    {
        return "Balsa-Route-Engine";
    }

    public List<Router<?>> getRouters()
    {
        return Collections.unmodifiableList(this.routers);
    }
    
    public int getTotalRoutes()
    {
        return this.totalRoutes;
    }
    
    public List<PrefixContainer> getPrefixes()
    {
        return Collections.unmodifiableList(this.prefixes);
    }
    
    public List<RouteEntry> getRoutes()
    {
        List<RouteEntry> routes = new ArrayList<>(this.totalRoutes);
        for (PrefixContainer prefix : this.prefixes)
        {
            for (RouteEntry route : prefix.getRoutes())
            {
                routes.add(route);
            }
        }
        return routes;
    }

    public void router(Router<?> router) throws BalsaException
    {
        // extract all the meta information
        this.routers.add(router);
        // compile the routes
        this.compileRouter(router);
    }

    protected void compileRouter(Router<?> router) throws BalsaException
    {
        // get the prefix container for all these routes
        String prefix = router.getPrefix();
        PrefixContainer container = this.createContainer(prefix);
        // compile all the routes of the router
        for (Route route : Route.fromRouter(prefix, this.application, router))
        {
            // compile an executor
            try
            {
                ExecBuilder execBuilder = ExecBuilder.build(route);
                execBuilder.compile();
                container.registerRoute(new RouteEntry(container, execBuilder));
                this.totalRoutes++;
            }
            catch (Exception e)
            {
                throw new BalsaException("Failed to compile route executor for: " + route, e);
            }
        }
    }

    protected PrefixContainer createContainer(String prefix)
    {
        for (PrefixContainer container : this.prefixes)
        {
            if (prefix.equals(container.getPrefix())) return container;
        }
        PrefixContainer container = new PrefixContainer(prefix, this);
        this.prefixes.add(container);
        Collections.sort(this.prefixes);
        return container;
    }

    public String toString()
    {
        StringBuilder s = new StringBuilder("RoutingEngine\r\n");
        for (PrefixContainer prefix : this.prefixes)
        {
            s.append("\t" + prefix.toString() + "\r\n");
            for (RouteEntry route : prefix.getRoutes())
            {
                s.append("\t\t" + route + "\r\n");
            }
        }
        s.append("Total routes: " + this.totalRoutes);
        return s.toString();
    }

    public void route(BalsaContext context) throws Throwable
    {
        // ROUTE!
        BalsaRequest request = context.request();
        // try all prefixes
        for (PrefixContainer prefix : this.prefixes)
        {
            if (prefix.match(context, request))
            {
                RouteEntry handler = prefix.getHandler(context, request);
                if (handler != null)
                {
                    // before filter
                    for (PrefixContainer filterPrefix : this.prefixes)
                    {
                        if (filterPrefix.match(context, request))
                        {
                            List<RouteEntry> filters = filterPrefix.getFilters(context, request, Filter.BEFORE);
                            if (filters != null)
                            {
                                for (RouteEntry filter : filters)
                                {
                                    filter.execute(context);
                                }
                            }
                        }
                    }
                    // the route
                    handler.execute(context);
                    // before filter
                    for (PrefixContainer filterPrefix : this.prefixes)
                    {
                        if (filterPrefix.match(context, request))
                        {
                            List<RouteEntry> filters = filterPrefix.getFilters(context, request, Filter.AFTER);
                            if (filters != null)
                            {
                                for (RouteEntry filter : filters)
                                {
                                    filter.execute(context);
                                }
                            }
                        }
                    }
                    // all done
                    return;
                }
            }
        }
        throw new BalsaNotFound("Could not find: " + context.request().getRequestMethod() + " " + context.request().getPathInfo());
    }

    public void routeException(BalsaContext context, Throwable t) throws Throwable
    {
        // Abort the current response
        context.response().abortOnError(t);
        // Route the error
        BalsaRequest request = context.request();
        // try all prefixes
        for (PrefixContainer prefix : this.prefixes)
        {
            if (prefix.match(context, request))
            {
                RouteEntry handler = prefix.getExceptionHandler(context, request, t);
                if (handler != null)
                {
                    // before filter
                    for (PrefixContainer filterPrefix : this.prefixes)
                    {
                        if (filterPrefix.match(context, request))
                        {
                            List<RouteEntry> filters = filterPrefix.getExceptionFilters(context, request, Filter.BEFORE);
                            if (filters != null)
                            {
                                for (RouteEntry filter : filters)
                                {
                                    filter.executeException(context);
                                }
                            }
                        }
                    }
                    // the exception route
                    handler.executeException(context);
                    // after filter
                    for (PrefixContainer filterPrefix : this.prefixes)
                    {
                        if (filterPrefix.match(context, request))
                        {
                            List<RouteEntry> filters = filterPrefix.getExceptionFilters(context, request, Filter.AFTER);
                            if (filters != null)
                            {
                                for (RouteEntry filter : filters)
                                {
                                    filter.executeException(context);
                                }
                            }
                        }
                    }
                    // all done
                    return;
                }
            }
        }
        // We cannot handle the error captain
        throw new BalsaInternalError(t);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void start(BalsaApplication app) throws BalsaException
    {
        try
        {
            // inject any services into the routers
            for (Router<?> router : this.getRouters())
            {
                Injector.injectServices(this.application, router);
            }
            // start the prefix containers
            for (PrefixContainer prefix : this.prefixes)
            {
                prefix.start();
            }
            logger.info(this.toString());
            // start all the routers
            for (Router<?> router : this.getRouters())
            {
                ((Router) router).start(app);
            }
        }
        catch (Exception e)
        {
            throw new BalsaException("Failed to start router", e);
        }
    }
}
