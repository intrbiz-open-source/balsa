package com.intrbiz.balsa.engine.impl.view.express;

import java.io.IOException;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.view.BalsaView;
import com.intrbiz.balsa.engine.view.BalsaViewParser;
import com.intrbiz.balsa.engine.view.BalsaViewSource;
import com.intrbiz.balsa.engine.view.BalsaViewSource.Resource;
import com.intrbiz.express.template.ExpressTemplate;
import com.intrbiz.express.template.filter.ContentFilter;
import com.intrbiz.express.template.filter.HTMLContentFilter;
import com.intrbiz.express.template.filter.PlainTextContentFilter;
import com.intrbiz.express.value.ValueScript;

public class ExpressViewParserImpl implements BalsaViewParser
{
    @Override
    public BalsaView parse(Resource resource, BalsaContext context, String renderFormat) throws BalsaException
    {
        try
        {
            // parse the template and return it
            return new ExpressTemplateView(resource.getPath(), new ExpressTemplate(resource.getName(), this.contentFilter(resource), new ValueScript(context.getExpressContext(), resource.openReader())));
        }
        catch (IOException e)
        {
            throw new BalsaException("Failed to parse view", e);
        }
    }
    
    protected ContentFilter contentFilter(Resource resource)
    {
        if (BalsaViewSource.Formats.EXPRESS_HTML.equals(resource.getFormat()))
            return HTMLContentFilter.DEFAULT;
        return PlainTextContentFilter.DEFAULT;
    }
}
