package com.intrbiz.balsa.engine;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.component.BalsaEngine;
import com.intrbiz.balsa.engine.view.BalsaView;
import com.intrbiz.balsa.engine.view.BalsaViewParser;
import com.intrbiz.balsa.engine.view.BalsaViewRewriteRule;
import com.intrbiz.balsa.engine.view.BalsaViewSource;
import com.intrbiz.balsa.engine.view.BalsaViewStack;
import com.intrbiz.balsa.engine.view.ViewPath;
import com.intrbiz.balsa.error.view.BalsaViewNotFound;

/**
 * An engine to provide view processing to the Balsa Framework
 */
public interface ViewEngine extends BalsaEngine
{
    /**
     * Load a single view
     * @param view the view to load
     * @param context the current BalsaContext
     * @param renderFormat the desired output format (mime type), eg: text/html
     * @return the view
     * @throws BalsaException
     */
    BalsaView load(String view, BalsaContext context, String renderFormat) throws BalsaException;
    
    /**
     * Construct a view stack from the template views and views given
     * 
     * @param templates the template views
     * @param names the views
     * @return returns the view stack
     */
    default BalsaViewStack load(String[][] templates, String[] views, BalsaContext context, String renderFormat) throws BalsaException
    {
        // check we have at least one view name
        if (views == null || views.length == 0) throw new BalsaViewNotFound("Cannot load view, no names given");
        // the stack to populate
        BalsaViewStack viewStack = new BalsaViewStack();
        // process the templates
        if (templates != null && templates.length > 0)
        {
            for (String[] templateViews : templates)
            {
                if (templateViews != null && templateViews.length > 0)
                {
                    for (String name : templateViews)
                    {
                        // load the view
                        BalsaView view = this.load(name, context, renderFormat);
                        if (view == null) throw new BalsaViewNotFound("Cannot load view: " + name);
                        viewStack.addView(view);
                    }
                }
            }
        }
        // process the views
        for (String name : views)
        {
            // load the view
            BalsaView view = this.load(name, context, renderFormat);
            if (view == null) throw new BalsaViewNotFound("Cannot load view: " + name);
            viewStack.addView(view);
        }
        // return the view stack
        return viewStack;
    }

    /**
     * Does the engine cache views?
     * 
     * @return returns boolean
     */
    boolean isCache();

    /**
     * The engine should cache views (Default)
     */
    void cacheOn();

    /**
     * The engine should not cache views
     */
    void cacheOff();
    
    /**
     * Clear any cached views
     */
    void clearCache();

    
    void clearSources();

    void removeSource(BalsaViewSource source);

    void addSource(BalsaViewSource source);

    List<BalsaViewSource> getSources();

    
    void clearParsers();

    void removeParser(String format);

    void addParser(String format, BalsaViewParser parser);

    Map<String, BalsaViewParser> getParsers();
    
    
    void clearRewriteRules();
    
    void removeRewriteRule(BalsaViewRewriteRule rule);
    
    void addRewriteRule(BalsaViewRewriteRule rule);
    
    List<BalsaViewRewriteRule> getRewriteRules();
    
    /*
     * View searching
     */
    
    /**
     * List the possible views and containers at the given path
     * @param path the path to list
     * @return the list of child paths or empty set
     */
    Set<ViewPath> list(String path, BalsaContext context);
    
    /**
     * List the possible views and containers at the given path, with metadata parsed from the view
     * @param path the path to list
     * @return the list of child paths or empty set
     */
    Set<ViewPath> listWithViewMetadata(String path, BalsaContext context);
    
}
