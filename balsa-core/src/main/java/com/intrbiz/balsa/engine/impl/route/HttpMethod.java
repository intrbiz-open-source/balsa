package com.intrbiz.balsa.engine.impl.route;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.intrbiz.balsa.listener.BalsaRequest;

public final class HttpMethod
{
    /* Common raw method names, must be uppercase */
    private static final String METHOD_ANY    = "ANY";
    private static final String METHOD_GET    = "GET";
    private static final String METHOD_POST   = "POST";
    private static final String METHOD_PUT    = "PUT";
    private static final String METHOD_PATCH  = "PATCH";
    private static final String METHOD_DELETE = "DELETE";
    
    /**
     * Cache of custom HTTP Method
     */
    private static final ConcurrentMap<String, HttpMethod> INTERNED = new ConcurrentHashMap<>();
    
    /* Common Http Methods */
    public static final HttpMethod ANY    = new HttpMethod(METHOD_ANY);
    public static final HttpMethod GET    = new HttpMethod(METHOD_GET);
    public static final HttpMethod POST   = new HttpMethod(METHOD_POST);
    public static final HttpMethod PUT    = new HttpMethod(METHOD_PUT);
    public static final HttpMethod PATCH  = new HttpMethod(METHOD_PATCH);
    public static final HttpMethod DELETE = new HttpMethod(METHOD_DELETE);
    
    private final String methodUpper;
    
    private final String methodLower;
    
    private HttpMethod(String method)
    {
        super();
        this.methodUpper = Objects.requireNonNull(method).toUpperCase();
        this.methodLower = Objects.requireNonNull(method).toLowerCase();
    }
    
    private static final HttpMethod create(String method, boolean intern)
    {
        String methodUpper = Objects.requireNonNull(method).toUpperCase();
        // Use singletons for common methods
        switch (methodUpper)
        {
            case METHOD_ANY:    return ANY;
            case METHOD_GET:    return GET;
            case METHOD_POST:   return POST;
            case METHOD_PUT:    return PUT;
            case METHOD_PATCH:  return PATCH;
            case METHOD_DELETE: return DELETE;
        }
        HttpMethod httpMethod = INTERNED.get(methodUpper);
        if (httpMethod == null)
        {
            httpMethod = new HttpMethod(methodUpper);
            if (intern)
            {
                INTERNED.put(methodUpper, httpMethod);
            }
        }
        return httpMethod;
    }
    
    public static final HttpMethod create(String method)
    {
        return create(method, false);
    }
    
    public static final HttpMethod createInterned(String method)
    {
        return create(method, true);
    }
    
    public boolean match(String method)
    {
        /* Try to match as efficiently as possible */
        return method != null && (this.methodUpper.equals(method) || this.methodLower.equals(method) || this.methodLower.equals(method.toLowerCase()));
    }
    
    public boolean match(BalsaRequest request)
    {
        return this.match(request.getRequestMethod());
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((methodUpper == null) ? 0 : methodUpper.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        HttpMethod other = (HttpMethod) obj;
        if (methodUpper == null)
        {
            if (other.methodUpper != null) return false;
        }
        else if (!methodUpper.equals(other.methodUpper)) return false;
        return true;
    }
    
    public String toString()
    {
        return this.methodUpper;
    }
}
