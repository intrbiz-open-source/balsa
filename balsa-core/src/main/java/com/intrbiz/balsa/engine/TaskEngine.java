package com.intrbiz.balsa.engine;

import java.util.concurrent.Callable;

import com.intrbiz.balsa.component.BalsaEngine;
import com.intrbiz.balsa.engine.task.TaskFuture;

/**
 * Execute long running tasks in the background
 */
public interface TaskEngine extends BalsaEngine
{   

    /**
     * Lookup a {@link TaskFuture} state by it's id.
     * If the task is completed or cancelled, the future will be removed upon lookup.
     * 
     * Callers should not expect to be able to lookup {@link TaskFuture} more than once 
     * after the task is completed.  Nor should callers expect to be able to retrieve 
     * the {@link TaskFuture} at any time in the future after the task has completed, 
     * since the TaskEngine implementation may clean up {@link TaskFuture}s.
     * 
     * @param id the {@link TaskFuture} id ({@code TaskFuture.id()})
     * @return the {@link TaskFuture} or null if the id does not exist of if the task future got cleaned up
     */
    <T> TaskFuture<T> lookup(String id);
    
    /**
     * Execute a callable task returning a future to get the result
     */
    <T> TaskFuture<T> submit(Callable<T> task);
    
    /**
     * Execute a runnable task returning a future to await execution which will 
     * return the given result
     */
    default <T> TaskFuture<T> submit(Runnable task, T result)
    {
        return this.submit(() -> { task.run(); return result; });
    }
    
    
    /**
     * Execute a runnable task returning a future to await execution
     */
    default TaskFuture<?> submit(Runnable task)
    {
        return this.submit(task, null);
    }
    
    /**
     * Execute a random runnable task
     */
    default void execute(Runnable task)
    {
        this.submit(task);
    }
}
