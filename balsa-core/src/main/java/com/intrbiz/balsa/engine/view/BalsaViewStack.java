package com.intrbiz.balsa.engine.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.express.ExpressException;

/**
 * A stack of views which will encode the request
 */
public class BalsaViewStack
{   
    private List<BalsaView> views = new ArrayList<>();
    
    private int currentViewPointer = 0;
    
    public BalsaViewStack()
    {
        super();
    }
    
    public void addView(BalsaView view)
    {
        this.views.add(view);
    }
    
    public List<BalsaView> getViews()
    {
        return this.views;
    }
    
    public BalsaView getCurrentView()
    {
        return this.views.get(this.currentViewPointer);
    }
    
    public BalsaView getTargetView()
    {
        return this.views.get(this.views.size() - 1);
    }
    
    public BalsaView nextView()
    {
        if (this.hasNext())
        {
            this.currentViewPointer ++;
            return this.getCurrentView();
        }
        return null;
    }
    
    public boolean hasNext()
    {
        return this.currentViewPointer < this.views.size();
    }
    
    // decode / encode

    public void decode(BalsaContext context) throws BalsaException, ExpressException
    {
        this.getCurrentView().decode(context);
    }
    
    public void encode(BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException
    {
        this.getCurrentView().encode(context, to);
    }

    @Override
    public String toString()
    {
        return "BalsaViewStack[" + views + "]";
    }
}
