package com.intrbiz.balsa.engine.impl.view.balsa;

import java.io.IOException;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.view.BalsaView;
import com.intrbiz.balsa.engine.view.BalsaViewParser;
import com.intrbiz.balsa.engine.view.BalsaViewSource.Resource;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.balsa.view.component.View;
import com.intrbiz.balsa.view.parser.Parser;
import com.intrbiz.util.Util;

public class BalsaViewParserImpl implements BalsaViewParser
{
    @Override
    public BalsaView parse(Resource resource, BalsaContext context, String renderFormat) throws BalsaException
    {
        try
        {
            Component root = Parser.parse(context, resource.openReader(), Util.coalesceEmpty(renderFormat, "text/html"));
            if (root == null) throw new BalsaException("Could not parse the view: " + resource.getName());
            View view = new View(resource.getPath(), root);
            // pull out metadata
            view.extractMetadata();
            return view;
        }
        catch (IOException e)
        {
            throw new BalsaException("Failed to parse view", e);
        }
    }
}
