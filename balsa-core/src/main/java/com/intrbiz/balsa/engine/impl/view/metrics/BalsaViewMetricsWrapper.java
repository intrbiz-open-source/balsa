package com.intrbiz.balsa.engine.impl.view.metrics;

import java.io.IOException;

import com.codahale.metrics.Timer;
import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.ViewEngine;
import com.intrbiz.balsa.engine.view.BalsaView;
import com.intrbiz.balsa.engine.view.ViewMetadata;
import com.intrbiz.balsa.engine.view.ViewPath;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.value.ValueExpression;
import com.intrbiz.gerald.witchcraft.Witchcraft;
import com.intrbiz.gerald.witchcraft.Witchcraft.Scope;

public class BalsaViewMetricsWrapper extends BalsaView
{
    private final String id;
    
    private final Timer encodeTimer;
    
    private final BalsaView realView;
    
    public BalsaViewMetricsWrapper(String id, BalsaView realView)
    {
        super(realView.getPath());
        this.id = id;
        this.realView = realView;
        this.encodeTimer = Witchcraft.timer(ViewEngine.class, new Scope(this.id), "encode");
    }

    @Override
    public void decode(BalsaContext context) throws BalsaException, ExpressException
    {
        this.realView.decode(context);
    }

    @Override
    public void encode(BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException
    {
        Timer.Context tCtx = this.encodeTimer.time();
        try
        {
            this.realView.encode(context, to);
        }
        finally
        {
            tCtx.stop();
        }
    }

    @Override
    public String getTitle()
    {
        return realView.getTitle();
    }

    @Override
    public ValueExpression attribute(String name)
    {
        return this.realView.attribute(name);
    }

    @Override
    public ViewMetadata getMetadata()
    {
        return this.realView.getMetadata();
    }
    
    @Override
    public ViewPath getPath()
    {
        return this.realView.getPath();
    }

    @Override
    public ViewMetadata getMeta()
    {
        return this.realView.getMeta();
    }

    public String toString()
    {
        return "BalsaViewMetricsWrapper[" + this.id + "]";
    }
}
