package com.intrbiz.balsa.engine.impl;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.component.BalsaEngine;

/**
 * Basic engine implementation
 */
public abstract class AbstractBalsaEngine implements BalsaEngine
{
    protected BalsaApplication application;
    
    public AbstractBalsaEngine()
    {
        super();
    }
    
    @Override
    public final void setup(BalsaApplication application) throws BalsaException
    {
        this.application = application;
    }
    
    /**
     * Perform any actions which needs to happen when the engine is bound into the application
     * @throws BalsaException
     */
    protected void setupEngine() throws BalsaException
    {
    }
    
    @Override
    @Deprecated
    public void start() throws BalsaException
    {
    }

    @Override
    public void start(BalsaApplication application) throws BalsaException
    {
        this.start();
    }

    @Override
    public void stop()
    {
    }
}
