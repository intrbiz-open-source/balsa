package com.intrbiz.balsa.engine.impl.session;

import java.security.Principal;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.Timer;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.SessionEngine;
import com.intrbiz.balsa.engine.security.AuthenticationState;
import com.intrbiz.gerald.witchcraft.Witchcraft;

public class SimpleSessionEngine extends AbstractSessionEngine implements Runnable
{
    private ConcurrentMap<String, SimpleSession> sessions;

    private volatile boolean run = false;

    private Thread thread;
    
    private long idCounterMaskLastChanged = System.currentTimeMillis();

    private static Logger logger = LoggerFactory.getLogger(SimpleSessionEngine.class);
    
    /* Metrics */
    
    private final Counter activeSessions;
    
    private final Meter createdSessions;
    
    private final Meter destroyedSessions;
    
    private final Timer sessionLifeTimer;

    public SimpleSessionEngine()
    {
        super();
        // setup metrics
        this.activeSessions    = Witchcraft.counter(SessionEngine.class, "active-sessions");
        this.createdSessions   = Witchcraft.meter(SessionEngine.class, "created-sessions");
        this.destroyedSessions = Witchcraft.meter(SessionEngine.class, "destroyed-sessions");
        this.sessionLifeTimer  = Witchcraft.timer(SessionEngine.class, "session-lifetime");
    }
    
    public String getEngineName()
    {
        return "Balsa-Simple-Session-Engine";
    }
    
    protected SimpleSession newSession(String sessionId)
    {
        if (logger.isTraceEnabled()) logger.trace("Creating session: " + sessionId);
        //
        this.activeSessions.inc();
        this.createdSessions.mark();
        Timer.Context timer = this.sessionLifeTimer.time();
        //
        SimpleSession session = new SimpleSession(sessionId, timer);
        //
        this.sessions.put(sessionId, session);
        //
        return session;
    }
    
    @Override
    public SimpleSession getSession(String sessionId)
    {
        SimpleSession session = this.sessions.get(sessionId);
        if (session == null) session = this.newSession(sessionId);
        session.access();
        return session;
    }    
    
    @Override
    public <T extends Principal> void revokeAllSessionsOfPrincipal(T principal)
    {
        for (Entry<String, SimpleSession> key : this.sessions.entrySet())
        {
            AuthenticationState authState = key.getValue().authenticationState();
            if (authState != null && (principal.equals(authState.currentPrincipal()) || principal.equals(authState.authenticatingPrincipal())))
            {
                this.removeSession(key.getKey());
            }
        }
    }

    protected void removeSession(String id)
    {
        if (logger.isTraceEnabled()) logger.trace("Removing session: " + id);
        SimpleSession session = this.sessions.remove(id);
        if (session != null)
        {
            session.deactivate();
            // instrument
            this.activeSessions.dec();
            this.destroyedSessions.mark();
            session.getTimer().stop();
        }
    }

    public void run()
    {
        while (this.run)
        {
            try
            {
                this.sweep();
                synchronized (this)
                {
                    this.wait(10000);
                }
            }
            catch (InterruptedException e)
            {
            }
        }
    }

    protected void sweep()
    {
        long evictTime = System.currentTimeMillis() - (this.getSessionLifetime() * 60 * 1000);
        for (Entry<String, SimpleSession> key : this.sessions.entrySet())
        {
            if (key.getValue().lastAccess() < evictTime)
            {
                this.removeSession(key.getKey());
            }
        }
        // update the session id mask ?
        if ((System.currentTimeMillis() - this.idCounterMaskLastChanged) > (8 * 60 * 60 * 1000))
        {
            this.changeSessionMask();
            this.idCounterMaskLastChanged = System.currentTimeMillis();
        }
    }

    @Override
    public void start() throws BalsaException
    {
        this.sessions = new ConcurrentHashMap<String, SimpleSession>(Runtime.getRuntime().availableProcessors() * 100, 0.75F, Runtime.getRuntime().availableProcessors());
        logger.info("Configured with session lifetime of " + this.getSessionLifetime() + " minutes");
        this.run = true;
        this.thread = new Thread(new ThreadGroup("Balsa"), this, "BalsaSessionSweeper");
        this.thread.start();
    }

    @Override
    public void stop()
    {
        this.run = false;
        synchronized (this)
        {
            this.notifyAll();
        }
    }  
}