package com.intrbiz.balsa.engine.impl.view.express;

import java.io.IOException;
import java.io.Writer;
import java.util.Objects;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.view.BalsaView;
import com.intrbiz.balsa.engine.view.ViewPath;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.template.ExpressTemplate;
import com.intrbiz.express.value.ValueExpression;

public class ExpressTemplateView extends BalsaView
{
    private ExpressTemplate template;

    public ExpressTemplateView(ViewPath path, ExpressTemplate template)
    {
        super(path);
        this.template = Objects.requireNonNull(template);
    }
    
    public ExpressTemplate getTemplate()
    {
        return this.template;
    }

    @Override
    public void decode(BalsaContext context) throws BalsaException, ExpressException
    {
    }

    @Override
    public void encode(BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException
    {
        this.template.encode(context.getExpressContext(), null, this.getWriter(to));
    }
    
    protected Writer getWriter(final BalsaWriter to)
    {
        if (to instanceof Writer)
            return (Writer) to;
        return new Writer() {
            
            @Override
            public void write(char[] cbuf, int off, int len) throws IOException
            {
                to.write(cbuf, off, len);
            }

            @Override
            public void flush() throws IOException
            {
                to.flush();
            }

            @Override
            public void close() throws IOException
            {
                to.close();
            }
        };
    }

    @Override
    public String getTitle()
    {
        return null;
    }

    @Override
    public ValueExpression attribute(String name)
    {
        return null;
    }
}
