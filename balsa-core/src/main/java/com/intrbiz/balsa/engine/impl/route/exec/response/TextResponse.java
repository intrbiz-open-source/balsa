package com.intrbiz.balsa.engine.impl.route.exec.response;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLStreamWriter;

import com.intrbiz.balsa.engine.impl.route.exec.ExecutorClass;
import com.intrbiz.balsa.metadata.CSS;
import com.intrbiz.balsa.metadata.HTML;
import com.intrbiz.balsa.metadata.JavaScript;
import com.intrbiz.balsa.metadata.Text;
import com.intrbiz.balsa.scgi.http.HTTP.HTTPStatus;
import com.intrbiz.util.compiler.util.JavaUtil;
import com.intrbiz.util.Util;

public class TextResponse extends ResponseBuilder
{
    private Class<?> type;

    private HTTPStatus status = HTTPStatus.OK;

    private String contentType = "text/plain";
    
    private String[] encode = null;
    
    private String[] encodeOnly = null;
    
    private String set = null;

    public TextResponse()
    {
        super();
    }

    public TextResponse type(Class<?> type)
    {
        this.type = type;
        //
        return this;
    }
    
    public TextResponse encode(String[] encode)
    {
        this.encode = encode;
        return this;
    }

    public TextResponse encodeOnly(String[] encodeOnly)
    {
        this.encodeOnly = encodeOnly;
        return this;
    }
    
    public TextResponse status(HTTPStatus status)
    {
        this.status = status;
        return this;
    }
    
    public TextResponse contentType(String contentType)
    {
        this.contentType = contentType;
        return this;
    }
    
    public TextResponse set(String set)
    {
        this.set = set;
        return this;
    }

    @Override
    public void compile(ExecutorClass cls)
    {
        cls.addImport(JAXBContext.class.getCanonicalName());
        cls.addImport(XMLStreamWriter.class.getCanonicalName());
        cls.addImport(Marshaller.class.getCanonicalName());
        cls.addImport(HTTPStatus.class.getCanonicalName());
        cls.addImport(this.type.getCanonicalName());
        //
        StringBuilder sb = cls.getExecutorLogic();
        if (! Util.isEmpty(this.set))
        {
            sb.append("    // set the return value as a variable\r\n");
            sb.append("    context.var(\"").append(JavaUtil.escapeString(this.set)).append("\", res);\r\n");
        }
        sb.append("    // write out the text response\r\n");
        sb.append("    if (context.response() != null) {\r\n");
        // setup the response status and content type
        sb.append("        context.response().status(HTTPStatus." + this.status.name() + ").contentType(\"").append(JavaUtil.escapeString(this.contentType)).append("\");\r\n");
        // write the response directly
        if (! Util.isEmpty(this.encodeOnly))
        {
            // encode a view only
            sb.append("        context.encodeOnly(")
                .append(Arrays.stream(this.encodeOnly).map(s -> "\"" + JavaUtil.escapeString(s) + "\"").collect(Collectors.joining(",")))
                .append(");\r\n");
        }
        else if (! Util.isEmpty(this.encode))
        {
            // encode a view with the route and application templates
            sb.append("        context.encode(this.router.getAllTemplates(), ")
                .append(Arrays.stream(this.encodeOnly).map(s -> "\"" + JavaUtil.escapeString(s) + "\"").collect(Collectors.joining(",")))
                .append(");\r\n");
        }
        else
        {
            sb.append("        context.response().write(res);\r\n");
        }
        sb.append("    }\r\n");
    }

    @Override
    public void fromAnnotation(Annotation a, Annotation[] annotations, Class<?> returnType)
    {
        this.type(returnType);
        if (a instanceof Text)
        {
            this.status = ((Text) a).status();
            this.contentType = ((Text) a).contentType();
            this.encode = ((Text) a).encode();
            this.encodeOnly = ((Text) a).encodeOnly();
            this.set = ((Text) a).set();
        }
        else if (a instanceof HTML)
        {
            this.status = ((HTML) a).status();
            this.contentType = "text/html";
            this.encode = ((HTML) a).encode();
            this.encodeOnly = ((HTML) a).encodeOnly();
            this.set = ((HTML) a).set();
        }
        else if (a instanceof CSS)
        {
            this.status = ((CSS) a).status();
            this.contentType = "text/css";
            this.encode = ((CSS) a).encode();
            this.encodeOnly = ((CSS) a).encodeOnly();
            this.set = ((CSS) a).set();
        }
        else if (a instanceof JavaScript)
        {
            this.status = ((JavaScript) a).status();
            this.contentType = "text/javascript";
            this.encode = ((JavaScript) a).encode();
            this.encodeOnly = ((JavaScript) a).encodeOnly();
            this.set = ((JavaScript) a).set();
        }
    }

    public Class<?> getType()
    {
        return type;
    }

    public void setType(Class<?> type)
    {
        this.type = type;
    }

    public HTTPStatus getStatus()
    {
        return status;
    }

    public void setStatus(HTTPStatus status)
    {
        this.status = status;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }
    
    public String[] getEncode()
    {
        return this.encode;
    }

    public void setEncode(String[] encode)
    {
        this.encode = encode;
    }

    public String[] getEncodeOnly()
    {
        return this.encodeOnly;
    }

    public void setEncodeOnly(String[] encodeOnly)
    {
        this.encodeOnly = encodeOnly;
    }

    public String getSet()
    {
        return this.set;
    }

    public void setSet(String set)
    {
        this.set = set;
    }

    @Override
    public void verify(Class<?> returnType)
    {
        if (String.class != returnType) throw new IllegalStateException("The route does not return a String!");
    }
}
