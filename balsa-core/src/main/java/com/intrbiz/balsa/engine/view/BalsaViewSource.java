package com.intrbiz.balsa.engine.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Set;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;

public interface BalsaViewSource
{
    public static interface Formats
    {
        public static final String BALSA = "balsa";

        public static final String APT = "apt";

        public static final String MARKDOWN = "markdown";

        public static final String HTML = "html";
        
        public static final String EXPRESS_HTML = "express_html";
        
        public static final String EXPRESS_PLAIN = "express_plain";
    }
    
    public static interface Extensions
    {
        public static final String XML = "xml";
        
        public static final String ETH = "eth";
        
        public static final String ETP = "etp";
        
        public static final String MD = "md";
    }

    public static interface Charsets
    {
        public static final Charset UTF8 = Charset.forName("UTF8");
    }

    public static abstract class Resource
    {
        private final ViewPath path;

        private final String format;

        private final Charset charset;

        public Resource(ViewPath path, String format, Charset charset)
        {
            super();
            if (path == null) throw new IllegalArgumentException("Path cannot be null!");
            if (format == null) throw new IllegalArgumentException("Format cannot be null!");
            this.path = path;
            this.format = format;
            this.charset = charset;
        }
        
        public ViewPath getPath()
        {
            return this.path;
        }

        /**
         * The name of the resource
         * @return
         */
        public final String getName()
        {
            return this.path.getName();
        }

        /**
         * The format of the resource, eg: balsa, apt, markdown
         * @return
         */
        public final String getFormat()
        {
            return this.format;
        }

        /**
         * The charset of the resource
         * 
         * @return
         */
        public final Charset getCharset()
        {
            return this.charset;
        }

        /**
         * 
         * @return
         * @throws IOException
         */
        public abstract InputStream openStream() throws IOException;

        /**
         * Open the resource as a character stream
         * 
         * @return
         * @throws IOException
         */
        public Reader openReader() throws IOException
        {
            return new BufferedReader(new InputStreamReader(this.openStream(), this.charset != null ? this.charset : Charset.defaultCharset()));
        }
        
        /**
         * Slurp the reader into memory
         * @return
         */
        public String slurp() throws IOException
        {
            Reader reader = this.openReader();
            StringBuilder sb = new StringBuilder();
            try
            {
                char[] b = new char[4096];
                int r;
                while ((r = reader.read(b)) != -1)
                {
                    sb.append(b, 0, r);
                }
            }
            finally
            {
                reader.close();
            }
            return sb.toString();
        }
    }

    /**
     * Locate the view source data ready for parsing
     * 
     * @param name
     *            the view name
     * @param context
     *            the current context
     * @return the resource or null if it does not exist
     * @throws BalsaException
     */
    Resource open(String name, BalsaContext context) throws BalsaException;
    
    /**
     * List the possible views and containers at the given path
     * @param path the path to list
     * @return the list of child paths or empty set
     */
    Set<ViewPath> list(String path, BalsaContext context);
}
