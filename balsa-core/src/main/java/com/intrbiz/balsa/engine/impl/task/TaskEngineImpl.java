package com.intrbiz.balsa.engine.impl.task;

import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.intrbiz.balsa.engine.TaskEngine;
import com.intrbiz.balsa.engine.impl.AbstractBalsaEngine;
import com.intrbiz.balsa.engine.task.TaskFuture;

public class TaskEngineImpl extends AbstractBalsaEngine implements TaskEngine
{
    private final ExecutorService executor = Executors.newCachedThreadPool();
    
    private final ConcurrentMap<String, TaskFutureImpl<?>> futures = new ConcurrentHashMap<>();
    
    private long maxAgeNanos = TimeUnit.MINUTES.toNanos(30);
    
    private final Timer cleanup;
    
    public TaskEngineImpl()
    {
        super();
        // setup cleanup timer
        this.cleanup = new Timer();
        this.cleanup.schedule(new TimerTask() {
            public void run()
            {
                cleanupTaskFutures();
            }
        }, TimeUnit.MINUTES.toMillis(5), TimeUnit.MINUTES.toMillis(5));
    }
    
    public long getMaxFutureAge()
    {
        return TimeUnit.MILLISECONDS.convert(this.maxAgeNanos, TimeUnit.NANOSECONDS);
    }
    
    public TaskEngineImpl maxFutureAge(long time, TimeUnit unit)
    {
        this.maxAgeNanos = TimeUnit.NANOSECONDS.convert(time, unit);
        return this;
    }

    @Override
    public String getEngineName()
    {
      return "Balsa-Task-Engine";
    }
    
    protected void cleanupTaskFutures()
    {
        long nowNanos = System.nanoTime();
        // scan and cleanup the futures
        for (Iterator<TaskFutureImpl<?>> i = this.futures.values().iterator(); i.hasNext(); )
        {
            TaskFutureImpl<?> tf = i.next();
            if (tf.cleanUp(nowNanos, this.maxAgeNanos))
                i.remove();
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> TaskFuture<T> lookup(String id)
    {
        TaskFutureImpl<?> tf = this.futures.get(id);
        if (tf != null)
        {
            // mark access
            tf.mark();
            // should we clean up?
            if (tf.isDone() || tf.isCancelled())
                this.futures.remove(id);
        }
        return (TaskFuture<T>) tf;
    }

    protected <T> TaskFutureImpl<T> taskFuture(Future<T> future)
    {
        TaskFutureImpl<T> tf = new TaskFutureImpl<>(future);
        this.futures.put(tf.id(), tf);
        return tf;
    }

    @Override
    public void execute(Runnable task)
    {
        this.executor.execute(task);
    }

    @Override
    public <T> TaskFuture<T> submit(Callable<T> task)
    {
        return this.taskFuture(this.executor.submit(task));
    }

    @Override
    public TaskFuture<?> submit(Runnable task)
    {
        return this.taskFuture(this.executor.submit(task));
    }

    @Override
    public <T> TaskFuture<T> submit(Runnable task, T result)
    {
        return this.taskFuture(this.executor.submit(task, result));
    }
    
    private static final class TaskFutureImpl<T> implements TaskFuture<T>
    {
        private final String id;
        
        private final Future<T> future;
        
        private long lastAccessedNanos;

        public TaskFutureImpl(Future<T> future)
        {
            super();
            this.id = UUID.randomUUID().toString();
            this.lastAccessedNanos = System.nanoTime();
            this.future = future;
        }

        @Override
        public String id()
        {
            return this.id;
        }
        
        void mark()
        {
            this.lastAccessedNanos = System.nanoTime();
        }
        
        boolean cleanUp(long nowNanos, long timeoutNanos)
        {
            // can we remove this future?
            if ((this.isDone() || this.isCancelled()) && (nowNanos - this.lastAccessedNanos) >= timeoutNanos)
                return true;
            // update the last accessed time
            this.mark();
            return false;
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning)
        {
            return future.cancel(mayInterruptIfRunning);
        }

        @Override
        public boolean isCancelled()
        {
            return future.isCancelled();
        }

        @Override
        public boolean isDone()
        {
            return future.isDone();
        }

        @Override
        public T get() throws InterruptedException, ExecutionException
        {
            return future.get();
        }

        @Override
        public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException
        {
            return future.get(timeout, unit);
        }
    }
}
