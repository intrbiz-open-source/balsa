package com.intrbiz.balsa.engine.view;

import java.io.IOException;
import java.util.Objects;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.dynamic.DynamicEntity;
import com.intrbiz.express.value.ValueExpression;

public abstract class BalsaView implements DynamicEntity
{
    private final ViewPath path;
    
    public BalsaView(ViewPath path)
    {
        super();
        this.path = Objects.requireNonNull(path);
    }
    
    // meta data

    /**
     * Evaluate the view title attribute if defined
     */
    public abstract String getTitle();

    public ViewPath getPath()
    {
        return this.path;
    }
    
    public ViewMetadata getMetadata()
    {
        return this.path.getMetadata();
    }
    
    public ViewMetadata getMeta()
    {
        return this.path.getMetadata();
    }
    
    // view attributes
    
    /**
     * Lookup a view attribute
     */
    public abstract ValueExpression attribute(String name);

    @Override
    public Object get(String name, ExpressContext context, Object source) throws ExpressException
    {
        switch (name)
        {
            case "title": return this.getTitle();
            case "path": return this.path;
            case "meta": return this.path.getMetadata();
            case "metadata": return this.path.getMetadata();
            default: return this.attribute(name);
        }
    }
    
    // decode / encode

    public abstract void decode(BalsaContext context) throws BalsaException, ExpressException;
    
    public abstract void encode(BalsaContext context, BalsaWriter to) throws IOException, BalsaException, ExpressException;
}
