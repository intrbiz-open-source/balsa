package com.intrbiz.balsa.engine.impl.view;

import java.io.File;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Timer;
import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.ViewEngine;
import com.intrbiz.balsa.engine.impl.AbstractBalsaEngine;
import com.intrbiz.balsa.engine.impl.view.balsa.BalsaViewParserImpl;
import com.intrbiz.balsa.engine.impl.view.express.ExpressViewParserImpl;
import com.intrbiz.balsa.engine.impl.view.metrics.BalsaViewMetricsWrapper;
import com.intrbiz.balsa.engine.view.BalsaView;
import com.intrbiz.balsa.engine.view.BalsaViewParser;
import com.intrbiz.balsa.engine.view.BalsaViewRewriteRule;
import com.intrbiz.balsa.engine.view.BalsaViewSource;
import com.intrbiz.balsa.engine.view.ViewPath;
import com.intrbiz.balsa.engine.view.source.FileViewSource;
import com.intrbiz.balsa.error.view.BalsaViewNotFound;
import com.intrbiz.balsa.view.library.impl.CompositeComponentLibrary;
import com.intrbiz.balsa.view.library.registry.ComponentLibraryRegister;
import com.intrbiz.express.ExpressException;
import com.intrbiz.gerald.witchcraft.Witchcraft;
import com.intrbiz.util.cache.IBCache;
import com.intrbiz.util.cache.WeakCache;

public class BalsaViewEngineImpl extends AbstractBalsaEngine implements ViewEngine
{
    private IBCache<String, BalsaView> cache = new WeakCache<String, BalsaView>();

    private static Logger logger = LoggerFactory.getLogger(BalsaViewEngineImpl.class);

    private boolean cacheOn = true;

    private final Timer sourceDuration;

    private final Timer parseDuration;

    private final Timer loadDuration;

    protected List<BalsaViewRewriteRule> rewriteRules = new CopyOnWriteArrayList<BalsaViewRewriteRule>();

    protected List<BalsaViewSource> sources = new CopyOnWriteArrayList<BalsaViewSource>();

    protected ConcurrentMap<String, BalsaViewParser> parsers = new ConcurrentHashMap<String, BalsaViewParser>();
    
    protected boolean viewMetrics = true;

    public BalsaViewEngineImpl(boolean viewMetrics)
    {
        super();
        this.viewMetrics = viewMetrics;
        // setup metrics
        this.sourceDuration = Witchcraft.timer(ViewEngine.class, "source-duration");
        this.parseDuration  = Witchcraft.timer(ViewEngine.class, "parse-duration");
        this.loadDuration   = Witchcraft.timer(ViewEngine.class, "load-duration");
        // defaults
        this.addSource(new FileViewSource(BalsaViewSource.Formats.BALSA, BalsaViewSource.Charsets.UTF8, BalsaViewSource.Extensions.XML));
        this.addSource(new FileViewSource(BalsaViewSource.Formats.EXPRESS_HTML, BalsaViewSource.Charsets.UTF8, BalsaViewSource.Extensions.ETH));
        this.addSource(new FileViewSource(BalsaViewSource.Formats.EXPRESS_PLAIN, BalsaViewSource.Charsets.UTF8, BalsaViewSource.Extensions.ETP));
        this.addSource(new FileViewSource(BalsaViewSource.Formats.BALSA, BalsaViewSource.Charsets.UTF8, BalsaViewSource.Extensions.XML));
        this.addParser(BalsaViewSource.Formats.BALSA, new BalsaViewParserImpl());
        this.addParser(BalsaViewSource.Formats.EXPRESS_HTML, new ExpressViewParserImpl());
        this.addParser(BalsaViewSource.Formats.EXPRESS_PLAIN, new ExpressViewParserImpl());
    }
    
    public BalsaViewEngineImpl()
    {
        this(true);
    }
    
    @Override
    public void start() throws BalsaException
    {
        // scan for any component libraries
        for (File path : this.application.getComponentPath())
        {
            if (path.exists() && path.isDirectory())
            {
                for (File library : path.listFiles())
                {
                    if (library.exists() && library.isDirectory())
                    {
                        logger.info("Registering composite component library: " + library.getName());
                        ComponentLibraryRegister.getDefault().registerLibrary(new CompositeComponentLibrary(library));
                    }
                }
            }
        }
    }

    protected BalsaViewSource.Resource source(String name, BalsaContext context) throws BalsaException
    {
        Timer.Context tctx = this.sourceDuration.time();
        try
        {
            // simple linear search for the resource
            for (BalsaViewSource source : this.sources)
            {
                logger.trace("Trying source: " + source.getClass().getSimpleName());
                BalsaViewSource.Resource res = source.open(name, context);
                if (res != null) return res;
            }
            throw new BalsaViewNotFound(name);
        }
        finally
        {
            tctx.stop();
        }
    }

    protected BalsaView parse(BalsaViewSource.Resource resource, BalsaContext context, String renderFormat) throws BalsaException
    {
        Timer.Context tctx = this.parseDuration.time();
        try
        {
            BalsaViewParser parser = this.parsers.get(resource.getFormat());
            if (parser != null)
            {
                // parse it
                return parser.parse(resource, context, renderFormat);
            }
            throw new BalsaViewNotFound("Failed to find parser for " + resource.getFormat());
        }
        catch (ExpressException e)
        {
            throw new BalsaViewNotFound("Failed to parse view", e);
        }
        finally
        {
            tctx.stop();
        }
    }

    protected String rewriteView(String name)
    {
        for (BalsaViewRewriteRule rule : this.rewriteRules)
        {
            if (rule.accept(name))
            {
                name = rule.rewrite(name);
                // rules are chained by default
                if (rule.isLast()) return name;
            }
        }
        return name;
    }

    protected BalsaView loadActualView(String name, BalsaContext context, String renderFormat) throws BalsaException
    {
        // process any rewrite rules
        name = this.rewriteView(name);
        // source the view
        BalsaViewSource.Resource resource = this.source(name, context);
        logger.trace("Loaded resource: " + name + " => " + resource.getFormat() + " " + resource.getName());
        // parse the view
        BalsaView view = this.parse(resource, context, renderFormat);
        // add metrics
        return this.viewMetrics ? new BalsaViewMetricsWrapper(name, view) : view;
    }
    
    @Override
    public BalsaView load(String name, BalsaContext context, String renderFormat) throws BalsaException
    {
        try (Timer.Context tctx = this.loadDuration.time())
        {
            // check the cache
            BalsaView view = this.cacheOn ? this.cache.get(cacheKey(name, renderFormat)) : null;
            // actually load the view if we need to
            if (view == null)
                view = this.loadActualView(name, context, renderFormat);
            // not found check
            if (view == null)
                throw new BalsaViewNotFound("Failed to find view: " + name);
            // cache it?
            if (this.cacheOn) this.cache.cachePrivate(cacheKey(name, renderFormat), view);
            // done
            return view;
        }
    }
    
    protected static String cacheKey(String name, String renderFormat)
    {
        return name + "?format=" + renderFormat;
    }

    @Override
    public Set<ViewPath> list(String path, BalsaContext context)
    {
        Set<ViewPath> paths = new LinkedHashSet<>();
        for (BalsaViewSource source : this.sources)
        {
            paths.addAll(source.list(path, context));
        }
        return paths;
    }
    
    /**
     * List the possible views and containers at the given path, with metadata parsed from the view
     * @param path the path to list
     * @return the list of child paths or empty set
     */
    @Override
    public Set<ViewPath> listWithViewMetadata(String path, BalsaContext context)
    {
        Set<ViewPath> paths = this.list(path, context);
        for (ViewPath viewPath : paths)
        {
            String name = viewPath.isContainer() ? viewPath.getChildPath("index") : viewPath.getPath();
            try
            {
                // try to load the view and pull our the metadata
                BalsaView view = this.load(name, context, null);
                if (view != null)
                    viewPath.setMetadata(view.getMetadata());
            }
            catch (BalsaException e)
            {
                logger.debug("Failed to extract metadata from view: " + name, e);
            }
        }
        return paths;
    }

    @Override
    public boolean isCache()
    {
        return this.cacheOn;
    }

    @Override
    public void cacheOn()
    {
        this.cacheOn = true;
    }

    @Override
    public void cacheOff()
    {
        this.cacheOn = false;
        this.cache.clear();
        logger.warn("View caching has been disabled");
    }
    
    @Override
    public void clearCache()
    {
        this.cache.clear();
    }

    @Override
    public void clearSources()
    {
        this.sources.clear();
    }

    @Override
    public void removeSource(BalsaViewSource source)
    {
        this.sources.remove(source);
    }

    @Override
    public void addSource(BalsaViewSource source)
    {
        this.sources.add(source);
    }

    @Override
    public List<BalsaViewSource> getSources()
    {
        return Collections.unmodifiableList(this.sources);
    }

    @Override
    public void clearParsers()
    {
        this.parsers.clear();
    }

    @Override
    public void removeParser(String format)
    {
        this.parsers.remove(format);
    }

    @Override
    public void addParser(String format, BalsaViewParser parser)
    {
        this.parsers.put(format, parser);
    }

    @Override
    public Map<String, BalsaViewParser> getParsers()
    {
        return Collections.unmodifiableMap(this.parsers);
    }

    @Override
    public void clearRewriteRules()
    {
        this.rewriteRules.clear();
    }

    @Override
    public void removeRewriteRule(BalsaViewRewriteRule rule)
    {
        this.rewriteRules.remove(rule);
    }

    @Override
    public void addRewriteRule(BalsaViewRewriteRule rule)
    {
        this.rewriteRules.add(rule);
    }

    @Override
    public List<BalsaViewRewriteRule> getRewriteRules()
    {
        return Collections.unmodifiableList(this.rewriteRules);
    }

    @Override
    public String getEngineName()
    {
        return "Balsa-View-Engine";
    }
}
