package com.intrbiz.balsa.engine.view.source;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.view.BalsaViewSource;
import com.intrbiz.balsa.engine.view.ViewPath;
import com.intrbiz.util.Util;

public class FileViewSource implements BalsaViewSource
{
    private static Logger logger = LoggerFactory.getLogger(FileViewSource.class);
    
    public static final String PROD_VIEW_PATH = "views";
    
    public static final String TEST_VIEW_PATH = "views";
    
    public static final String DEV_VIEW_PATH = "src/main/views";
    
    public static final String PROD_CONTENT_PATH = "content";
    
    public static final String TEST_CONTENT_PATH = "content";
    
    public static final String DEV_CONTENT_PATH = "src/main/content";
    
    public static final String PROD_COMPONENT_PATH = "components";
    
    public static final String TEST_COMPONENT_PATH = "components";
    
    public static final String DEV_COMPONENT_PATH = "src/main/components";
    
    public static final String PROD_FUNCTIONS_PATH = "functions";
    
    public static final String TEST_FUNCTIONS_PATH = "functions";
    
    public static final String DEV_FUNCTIONS_PATH = "src/main/functions";
    
    public static final String DEFAULT_EXTENSION = "xml";
    
    private final String extension;
    
    private final String format;
    
    private final Charset charset;
    
    public FileViewSource(String format, Charset charset, String extension)
    {
        super();
        this.format = format;
        this.extension = extension;
        this.charset = charset;
    }
    
    private static String buildBasePath(File root, File file)
    {
        if (root == file || root.equals(file))
            return "";
        String base = buildBasePath(root, file.getParentFile());
        return base.length() > 0 ? base + "/" + file.getName() : file.getName();
    }
    
    @Override
    public Resource open(String name, BalsaContext context) throws BalsaException
    {
        List<File> path = context.app().getViewPath();
        for (File dir : path)
        {
            if (dir.exists() && dir.isDirectory())
            {
                final File viewFile = new File(dir, name + "." + this.extension);
                logger.trace("Trying file: " + viewFile.getAbsolutePath());
                if (viewFile.exists() && (! viewFile.isDirectory()) && viewFile.canRead())
                {
                    return new Resource(
                            new ViewPath(buildBasePath(dir, viewFile.getParentFile()), this.stripFileExtension(viewFile.getName()), false), 
                            this.format, this.charset) {
                        @Override
                        public InputStream openStream() throws IOException
                        {
                            return new FileInputStream(viewFile);
                        }
                    };
                }
            }
        }
        return null;
    }

    @Override
    public Set<ViewPath> list(String path, BalsaContext context)
    {
        Set<ViewPath> paths = new HashSet<>();
        for (File dir : context.app().getViewPath())
        {
            if (dir.exists() && dir.isDirectory())
            {
                final File baseDir = new File(dir, path);
                logger.trace("Trying file: " + baseDir.getAbsolutePath());
                if (baseDir.exists() && baseDir.isDirectory() && baseDir.canRead())
                {
                    for (File child : Util.coalesce(baseDir.listFiles(), new File[0]))
                    {
                        if (child.isDirectory())
                        {
                            paths.add(new ViewPath(path, child.getName(), true));
                        }
                        else if (child.isFile() && child.getName().endsWith("." + this.extension))
                        {
                            String name = this.stripFileExtension(child.getName());
                            if (name.length() > 0) paths.add(new ViewPath(path, name, false));
                        }
                    }
                }
            }
        }
        return paths;
    }
    
    public String stripFileExtension(String name)
    {
        name = name.substring(0, name.length() - (this.extension.length() + 1));
        return name;
    }

}
