package com.intrbiz.balsa.engine.task;

import java.util.concurrent.Future;

public interface TaskFuture<T> extends Future<T>
{
    String id();
}
