package com.intrbiz.balsa.engine.view;

import java.time.ZonedDateTime;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.dynamic.DynamicEntity;
import com.intrbiz.util.Util;

public class ViewPath implements DynamicEntity
{
    private final String basePath;
    
    private final String name;
    
    private final boolean container;
    
    private ViewMetadata metadata = null;
    
    public ViewPath(String basePath, String name, boolean container)
    {
        super();
        this.basePath = basePath;
        this.name = name;
        this.container = container;
    }
    
    public ViewPath(String basePath, String name, boolean container, ViewMetadata metadata)
    {
        this(basePath, name, container);
        this.metadata = metadata;
    }
    
    public String getBasePath()
    {
        return this.basePath;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public String getPath()
    {
        return Util.isEmpty(this.basePath) ? this.name : this.basePath + "/" + this.name;
    }
    
    public String getChildPath(String child)
    {
        String path = this.getPath();
        return Util.isEmpty(path) ? child : path + "/" + child;
    }
    
    public boolean isContainer()
    {
        return this.container;
    }

    public ViewMetadata getMetadata()
    {
        if (this.metadata == null)
            this.metadata = new ViewMetadata();
        return this.metadata;
    }

    public void setMetadata(ViewMetadata metadata)
    {
        this.metadata = metadata;
    }
    
    public Object getAttribute(String name, Object defaultValue)
    {
        return this.metadata == null ? null : this.metadata.getAttribute(name, defaultValue);
    }
    
    public <T> T getAttribute(String name, T defaultValue, Class<T> type)
    {
        return this.metadata == null ? null : this.metadata.getAttribute(name, defaultValue, type);
    }
    
    public Object getAttribute(String name)
    {
        return this.metadata == null ? null : this.metadata.getAttribute(name);
    }
    
    public <T> T getAttribute(String name, Class<T> type)
    {
        return this.metadata == null ? null : this.metadata.getAttribute(name, type);
    }
    
    /*
     * Common metadata attributes
     */
    
    public String getTitle()
    {
        return this.metadata == null ? null : this.getMetadata().getTitle();
    }
    
    public String getSubtitle()
    {
        return this.metadata == null ? null : this.getMetadata().getSubtitle();
    }
    
    public String getDescription()
    {
        return this.metadata == null ? null : this.getMetadata().getDescription();
    }
    
    public String getDate()
    {
        return this.metadata == null ? null : this.getMetadata().getDate();
    }
    
    public ZonedDateTime parseDate()
    {
        return this.metadata == null ? null : this.getMetadata().parseDate();
    }
    
    public String getAuthor()
    {
        return this.metadata == null ? null : this.getMetadata().getAuthor();
    }
    
    public int getOrder()
    {
        return this.metadata == null ? Integer.MAX_VALUE : this.getMetadata().getOrder();
    }
    
    public String getImage()
    {
        return this.metadata == null ? null : this.getMetadata().getImage();
    }

    @Override
    public Object get(String name, ExpressContext context, Object source) throws ExpressException
    {
        switch (name.toLowerCase())
        {
            case "basepath":  return this.getBasePath();
            case "name":      return this.getName();
            case "path":      return this.getPath();
            case "container": return this.isContainer();
            case "metadata":  return this.getMetadata();
            case "meta":      return this.getMetadata();
            case "date":      return this.getDate();
            case "order":     return this.getOrder();
        }
        return this.metadata.get(name, context, source);
    }

    @Override
    public String toString()
    {
        return "ViewPath(basePath=" + basePath + ", name=" + name + ", container=" + container + ", metadata=" + this.metadata + ")";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((basePath == null) ? 0 : basePath.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ViewPath other = (ViewPath) obj;
        if (basePath == null)
        {
            if (other.basePath != null) return false;
        }
        else if (!basePath.equals(other.basePath)) return false;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        return true;
    }
}
