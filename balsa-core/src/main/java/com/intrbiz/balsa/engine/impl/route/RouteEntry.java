package com.intrbiz.balsa.engine.impl.route;

import java.util.regex.Matcher;

import com.codahale.metrics.Timer;
import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.engine.impl.route.Route.Filter;
import com.intrbiz.balsa.engine.impl.route.Route.RoutePredicate;
import com.intrbiz.balsa.engine.impl.route.Route.RoutePredicate.PredicateAction;
import com.intrbiz.balsa.engine.impl.route.exec.ExecBuilder;
import com.intrbiz.balsa.error.BalsaInternalError;
import com.intrbiz.balsa.listener.BalsaRequest;
import com.intrbiz.balsa.scgi.parameter.StringParameter;
import com.intrbiz.gerald.witchcraft.Witchcraft;

/**
 * An entry in the routing engine
 */
public class RouteEntry implements Comparable<RouteEntry>
{
    private final PrefixContainer prefix;

    private final Route route;
    
    private final ExecBuilder builder;

    private RouteExecutor<?> executor;

    private final Timer requestDuration;

    public RouteEntry(PrefixContainer prefix, ExecBuilder builder) throws BalsaInternalError
    {
        super();
        this.prefix = prefix;
        this.builder = builder;
        this.route = builder.getRoute();
        this.requestDuration = Witchcraft.timer(route.getClass(), "request-duration");
    }

    public PrefixContainer getPrefix()
    {
        return this.prefix;
    }

    public Route getRoute()
    {
        return this.route;
    }

    public ExecBuilder getBuilder()
    {
        return this.builder;
    }

    public RouteExecutor<?> getExecutor()
    {
        return this.executor;
    }

    public boolean isExceptionHandler()
    {
        return this.route.isExceptionHandler();
    }

    public boolean isFilter()
    {
        return this.route.isFilter();
    }

    public Filter getFilter()
    {
        return this.route.getFilter();
    }
    
    public void start() throws Exception
    {
        this.executor = this.builder.executor();
    }
    
    public boolean match(BalsaContext context, BalsaRequest request)
    {
        return match(false, context, request);
    }

    public boolean match(boolean ignoreMethod, BalsaContext context, BalsaRequest request)
    {
        if (ignoreMethod || HttpMethod.ANY.equals(this.route.getMethod()) || this.route.getMethod().match(request))
        {
            Matcher m = this.route.getCompiledPattern().pattern.matcher(request.getPathInfo());
            if (m.matches())
            {
                // assert any predicates
                if (this.route.hasPredicates())
                {
                    for (RoutePredicate predicate : this.route.getPredicates())
                    {
                        PredicateAction action = predicate.apply(context, request);
                        if (action == PredicateAction.ACCEPT) break;
                        if (action == PredicateAction.REJECT) return false;
                    }
                }
                // extract parameters
                if (this.route.getCompiledPattern().as.length > 0)
                {
                    for (int i = 0; i < m.groupCount() && i < this.route.getCompiledPattern().as.length; i++)
                    {
                        request.addParameter(new StringParameter(this.route.getCompiledPattern().as[i], m.group(i + 1)));
                    }
                }
                return true;
            }
        }
        return false;
    }

    public boolean matchException(BalsaContext context, BalsaRequest request, Throwable t)
    {
        if (HttpMethod.ANY.equals(this.route.getMethod()) || this.route.getMethod().match(request))
        {
            Matcher m = this.route.getCompiledPattern().pattern.matcher(request.getPathInfo());
            if (m.matches())
            {
                // assert any predicates
                if (this.route.hasPredicates())
                {
                    for (RoutePredicate predicate : this.route.getPredicates())
                    {
                        PredicateAction action = predicate.apply(context, request);
                        if (action == PredicateAction.ACCEPT) break;
                        if (action == PredicateAction.REJECT) return false;
                    }
                }
                // do we have an exception match
                for (Class<? extends Throwable> exception : this.route.getExceptions())
                {
                    if (exception.isInstance(t)) return true;
                }
            }
        }
        return false;
    }

    public Object execute(BalsaContext context) throws Throwable
    {
        if (this.route.isExceptionHandler()) throw new IllegalAccessException("An exception handler can only be used to handle errors!");
        Object res = null;
        Timer.Context tctx = this.requestDuration.time();
        try
        {
            this.route.getRouter().before();
            if (this.executor != null) 
                res = this.executor.execute(context);
            this.route.getRouter().after();
        }
        finally
        {
            tctx.stop();
        }
        return res;
    }

    public Object executeException(BalsaContext context) throws Throwable
    {
        if (! this.route.isExceptionHandler()) throw new IllegalAccessException("An exception handler can only be used to handle errors!");
        Object res = null;
        Timer.Context tctx = this.requestDuration.time();
        try
        {
            if (this.executor != null) 
                res = this.executor.execute(context);
        }
        finally
        {
            tctx.stop();
        }
        return res;
    }

    public String toString()
    {
        try
        {
            return this.route.getMethod() + " " + this.route.getCompiledPattern() + " => " + this.builder.compile().getCanonicalName();
        }
        catch (Exception e)
        {
        }
        return this.route.getMethod() + " " + this.route.getCompiledPattern() + " => void";
    }

    protected String stripTrailingSlash(String s)
    {
        if ("/".equals(s)) return "";
        if (s.endsWith("/")) { return s.substring(0, s.length() - 1); }
        return s;
    }

    protected String ensureStartingSlash(String s)
    {
        if (!s.startsWith("/")) return "/" + s;
        return s;
    }

    @Override
    public int compareTo(RouteEntry o)
    {
        return this.route.compareTo(o.route);
    }
}
