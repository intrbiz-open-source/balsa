package com.intrbiz.balsa.engine;

import java.security.Principal;

import com.intrbiz.balsa.component.BalsaEngine;
import com.intrbiz.balsa.engine.session.BalsaSession;

public interface SessionEngine extends BalsaEngine
{
    public static final int DEFAULT_SESSION_LIFETIME = 30;
    
    int getSessionLifetime();

    void setSessionLifetime(int sessionLifetime);
    
    String makeId();
    
    /**
     * Get a session with the given id
     * @param sessionId
     * @return
     */
    BalsaSession getSession(String sessionId);
    
    /**
     * Revoke all session which have the given user logged in
     * @param <T>
     * @param principal the user which of the sessions to revoke
     */
    <T extends Principal> void revokeAllSessionsOfPrincipal(T principal);
}
