package com.intrbiz.balsa;

import static com.intrbiz.util.Util.*;

import java.io.IOException;
import java.io.StringWriter;
import java.security.Principal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.engine.SecurityEngine.ValidationLevel;
import com.intrbiz.balsa.engine.security.AuthenticationResponse;
import com.intrbiz.balsa.engine.security.AuthenticationState;
import com.intrbiz.balsa.engine.security.challenge.AuthenticationChallenge;
import com.intrbiz.balsa.engine.security.credentials.Credentials;
import com.intrbiz.balsa.engine.security.credentials.PasswordCredentials;
import com.intrbiz.balsa.engine.security.info.AuthenticationInfo;
import com.intrbiz.balsa.engine.security.info.SimpleAuthenticationInfo;
import com.intrbiz.balsa.engine.security.method.AuthenticationMethod;
import com.intrbiz.balsa.engine.session.BalsaSession;
import com.intrbiz.balsa.engine.task.DeferredActionTask;
import com.intrbiz.balsa.engine.task.TaskFuture;
import com.intrbiz.balsa.engine.view.BalsaView;
import com.intrbiz.balsa.engine.view.BalsaViewStack;
import com.intrbiz.balsa.engine.view.ViewPath;
import com.intrbiz.balsa.error.BalsaIOError;
import com.intrbiz.balsa.error.BalsaInternalError;
import com.intrbiz.balsa.error.BalsaSecurityException;
import com.intrbiz.balsa.listener.BalsaRequest;
import com.intrbiz.balsa.listener.BalsaResponse;
import com.intrbiz.balsa.scgi.parameter.Parameter;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.balsa.scgi.util.CookieBuilder;
import com.intrbiz.balsa.scgi.util.HTMLWriter;
import com.intrbiz.balsa.view.component.Component;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressEntityResolver;
import com.intrbiz.express.action.ActionHandler;
import com.intrbiz.util.URLUtil;
import com.intrbiz.util.Util;
import com.intrbiz.validation.converter.ConversionException;
import com.intrbiz.validation.validator.ValidationException;

/**
 * The balsa Context - represents the state of the Balsa application at any given moment in time.
 * 
 * The Context is bound to the current thread and therefore transient. The Context is only valid for the life time of the request.
 */
public class BalsaContext implements ExpressEntityResolver
{
    private static Logger logger = LoggerFactory.getLogger(BalsaContext.class);
    
    protected final static ThreadLocal<BalsaContext> currentInstance = new ThreadLocal<BalsaContext>();

    private final BalsaApplication application;

    private BalsaSession session;

    private final BalsaRequest request;

    private final BalsaResponse response;

    private final Map<String, Object> models = new TreeMap<String, Object>();
    
    private final Map<String, Object> sessionModelCache = new TreeMap<String, Object>();
    
    private final Map<String, Object> vars = new TreeMap<String, Object>();

    private Throwable exception = null;

    private long processingStart;

    private long processingEnd;

    private ExpressContext expressContext;

    private BalsaViewStack view;
    
    private BalsaViewStack include;
    
    private final List<ConversionException> conversionErrors = new LinkedList<ConversionException>();
    
    private final List<ValidationException> validationErrors = new LinkedList<ValidationException>();
    
    private final Map<String, ConversionException> namedConversionErrors = new HashMap<String, ConversionException>();
    
    private final Map<String, ValidationException> namedValidationErrors = new HashMap<String, ValidationException>();
    
    private Principal currentPrincipal;
    
    private String renderFormat;
    
    private final Stack<Component> compositeComponents = new Stack<>();

    public BalsaContext(BalsaApplication application, BalsaRequest request, BalsaResponse response)
    {
        super();
        this.application = application;
        this.request = request;
        this.response = response;
        this.expressContext = application.getExpressContextBuilder().build(this);
    }
    
    /**
     * Create a request-less BalsaContext
     */
    public BalsaContext(BalsaApplication application)
    {
        this(application, null, null);
    }
    
    /**
     * Create a request-less BalsaContext with the given session
     */
    public BalsaContext(BalsaApplication application, BalsaSession session)
    {
        this(application, null, null);
        this.session = session;
    }

    /**
     * Get the current instance of the balsa context.
     */
    public final static BalsaContext get()
    {
        return currentInstance.get();
    }

    /**
     * Set the current instance of the balsa context.
     */
    private final static void set(BalsaContext context)
    {
        if (context == null)
            currentInstance.remove();
        else
            currentInstance.set(context);
    }
    
    /**
     * Bind this BalsaContext instance to the current thread
     * @return
     */
    public BalsaContext bind()
    {
        BalsaContext.set(this);
        return this;
    }
    
    /**
     * Clear current instance of the balsa context.
     */
    public final static void clear()
    {
        currentInstance.remove();
    }
    
    /**
     * Unbind the BalsaContext for the current thread
     */
    public void unbind()
    {
        BalsaContext.clear();
    }

    /**
     * Get the Express context for this Balsa context
     * @return the Express context
     */
    public final ExpressContext getExpressContext()
    {
        return this.expressContext;
    }

    /**
     * Get the current application
     * 
     * @return returns balsaApplication
     */
    @SuppressWarnings("unchecked")
    public final <T extends BalsaApplication> T app()
    {
        return (T) this.application;
    }

    /**
     * Get the current session, creating one if needed.
     * 
     * @return the BalsaSession for this request
     */
    public final BalsaSession session()
    {
        if (this.session == null)
        {
            String sessionId = this.app().getSessionEngine().makeId();
            this.setSession(this.app().getSessionEngine().getSession(sessionId));
            // send the cookie
            if (this.response().isHeadersSent()) throw new BalsaInternalError("Cannot create session, headers have already been sent.");
            this.response().cookie().name(BalsaSession.COOKIE_NAME).value(sessionId).path(this.path("/")).httpOnly().secure(request().isSecure()).set();
        }
        return this.session;
    }
    
    /**
     * Try to get the current session
     * 
     * @return the BalsaSession for this request if one exists or null
     */
    public final BalsaSession trySession()
    {
        return this.session;
    }
    
    public final <R> R trySession(Function<BalsaSession, R> accessor)
    {
        return this.session == null ? null : accessor.apply(this.session);
    }

    /**
     * Set the current session
     * 
     * @param session
     *            returns void
     */
    public final void setSession(BalsaSession session)
    {
        this.session = session;
    }

    /**
     * Get the balsa request object
     */
    public final BalsaRequest request()
    {
        return this.request;
    }

    /**
     * Get the balsa response object
     */
    public final BalsaResponse response()
    {
        return this.response;
    }

    /**
     * Get the exception with cause the application error
     * 
     * @return returns Throwable
     */
    public final Throwable getException()
    {
        return exception;
    }

    /**
     * Set the exception which caused the application error
     * 
     * @param exception
     *            The exception which ocurred
     */
    public final void setException(Throwable exception)
    {
        this.exception = exception;
    }

    /**
     * Reset this current context
     */
    public void deactivate()
    {
        try
        {
            this.promoteSessionModelCache();
            this.sessionModelCache.clear();
            this.models.clear();
            this.vars.clear();
            this.conversionErrors.clear();
            this.namedConversionErrors.clear();
            this.validationErrors.clear();
            this.namedValidationErrors.clear();
            this.exception = null;
            this.view = null;
            this.include = null;
            this.session = null;
            this.currentPrincipal = null;
            this.compositeComponents.clear();
            this.expressContext = null;
        }
        catch (Exception e)
        {
            logger.error("Error whilst deactivating context", e);
        }
    }

    public void activate()
    {
        // rebuild the express context to avoid op limit issues
        this.expressContext = this.application.getExpressContextBuilder().build(this);
    }
    
    // conversion errors
    
    public boolean hasConversionErrors()
    {
        return ! (this.conversionErrors.isEmpty() && this.namedConversionErrors.isEmpty());
    }
    
    public void addConversionError(ConversionException cex)
    {
        this.conversionErrors.add(cex);
    }
    
    public void addConversionError(String name, ConversionException cex)
    {
        this.namedConversionErrors.put(name, cex);
    }
    
    public List<ConversionException> getConversionErrors()
    {
        return this.conversionErrors;
    }
    
    public ConversionException getConversionError(String name)
    {
        return this.namedConversionErrors.get(name);
    }
    
    public Map<String, ConversionException> getNamedConversionErrors()
    {
        return this.namedConversionErrors;
    }
    
    public void clearConversionErrors()
    {
        this.conversionErrors.clear();
        this.namedConversionErrors.clear();
    }
    
    // validation
    
    public boolean hasValidationErrors()
    {
        return ! (this.validationErrors.isEmpty() && this.namedValidationErrors.isEmpty());
    }
    
    public void addValidationError(ValidationException cex)
    {
        this.validationErrors.add(cex);
    }
    
    public void addValidationError(String name, ValidationException cex)
    {
        this.namedValidationErrors.put(name, cex);
    }
    
    public List<ValidationException> getValidationErrors()
    {
        return this.validationErrors;
    }
    
    public ValidationException getValidationErrors(String name)
    {
        return this.namedValidationErrors.get(name);
    }
    
    public Map<String, ValidationException> getNamedValidationErrors()
    {
        return this.namedValidationErrors;
    }
    
    public void clearValidationErrors()
    {
        this.validationErrors.clear();
        this.namedValidationErrors.clear();
    }

    // Timing

    public final long getProcessingStart()
    {
        return processingStart;
    }

    public final void setProcessingStart(long processingStart)
    {
        this.processingStart = processingStart;
    }

    public final long getProcessingEnd()
    {
        return processingEnd;
    }

    public final void setProcessingEnd(long processingEnd)
    {
        this.processingEnd = processingEnd;
    }

    // Helper functions

    /**
     * Get or create a registered model object
     * @param <T> the expected model type
     * @param name the model name
     * @return the model object
     */
    public <T> T model(String name)
    {
        return this.model(name, true, true);
    }
    
    /**
     * Try to get a model returning null if it has not been created
     * @param <T> the expected model type
     * @param name the model name
     * @return the model or null
     */
    public <T> T tryModel(String name)
    {
        return this.model(name, false, false);
    }
    
    @SuppressWarnings("unchecked")
    protected <T> T model(String name, boolean create, boolean errorIfNotRegistered)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        T model = (T) this.models.get(name);
        if (model == null)
        {
            model = (T) this.sessionModelCache.get(name);
        }
        if (model == null && this.session != null)
        {
            model = (T) this.session.getModel(name);
            if (model != null) {
                // promote into session model cache
                this.sessionModelCache.put(name, model);
            }
        }
        if (model == null && create)
        {
            if (this.application.isModel(name))
            {
                // need to create the model
                model = this.application.model(name);
                if (this.application.isSessionModel(name))
                {
                    this.sessionModelCache.put(name, model);
                }
                else
                {
                    this.models.put(name, model);
                }
            }
            else if (errorIfNotRegistered)
            {
                throw new BalsaException("The model '" + name + "' is not registered");
            }
        }
        return model;
    }
    
    /**
     * Clear a model bean if it is no longer needed
     * @param name the model to clear
     */
    public void clearModel(String name)
    {
        this.models.remove(name);
        this.sessionModelCache.remove(name);
        if (this.session != null)
            this.session.removeModel(name);
    }
    
    /**
     * Get the named variable
     * @param name the variable name
     * @return
     * returns Object
     */
    @SuppressWarnings("unchecked")
    public <T> T var(String name)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        return (T) this.vars.get(name);
    }
    
    /**
     * Store a variable
     * @param name the variable name
     * @param object the variable
     * returns void
     */
    public <T> T var(String name, T object)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        this.vars.put(name, object);
        return object;
    }
    
    /**
     * Remove a variable of the given name
     * @param name
     */
    public void removeVar(String name)
    {
        if (name == null) throw new IllegalArgumentException("Name cannot be null");
        this.vars.remove(name);
    }
    
    /**
     * Get the model or variable with the given name
     * @param name the model or variable name
     * @return
     */
    @Override
    public Object getEntity(String name, Object source)
    {
        // special entity names
        if ("balsa".equalsIgnoreCase(name)) return BalsaContext.this;
        if ("currentPrincipal".equalsIgnoreCase(name)) return BalsaContext.this.currentPrincipal();
        if ("authenticationInfo".equalsIgnoreCase(name)) return BalsaContext.this.authenticationInfo();
        if ("request".equalsIgnoreCase(name)) return BalsaContext.this.request;
        if ("response".equalsIgnoreCase(name)) return BalsaContext.this.response;
        if ("viewStack".equalsIgnoreCase(name)) return BalsaContext.this.getViewStack();
        if ("currentView".equalsIgnoreCase(name)) return BalsaContext.this.getViewStack().getCurrentView();
        if ("targetView".equalsIgnoreCase(name)) return BalsaContext.this.getViewStack().getTargetView();
        if ("include".equalsIgnoreCase(name)) return BalsaContext.this.getInclude();
        if ("application".equalsIgnoreCase(name)) return this.application;
        if ("session".equalsIgnoreCase(name)) return this.session;
        if ("config".equalsIgnoreCase(name)) return this.application.config();
        // try request var first
        Object value = this.var(name);
        if (value != null) return value;
        // try session vars
        if (this.session != null)
        {
            value = this.session.getVar(name);
            if (value != null) return value;
        }
        // try application vars
        value = this.application.var(name);
        if (value != null) return value;
        // try a model
        value = this.model(name, true, false);
        if (value != null) return value;
        // try a service
        value = this.service(name);
        return value;
    }
    
    @Override
    public ActionHandler getAction(String name, Object source)
    {
        return this.application.action(name);
    }

    /**
     * Get the value of the request parameter given
     * 
     * @param name
     *            the parameter name
     * @return returns String the parameter value
     */
    public String param(String name)
    {
        Parameter p = this.request.getParameter(name);
        if (p != null) return p.getStringValue();
        return null;
    }
    
    /**
     * Get the value of the request parameter given
     * @param name the parameter name
     * @return the List&lt;String&gt; parameter value or null
     */
    public List<String> listParam(String name)
    {
        Parameter p = this.request.getParameter(name);
        if (p != null) return p.getStringListValue();
        return new LinkedList<String>();
    }

    /**
     * Get the value of the request header given
     * 
     * @param name
     *            the header name
     * @return returns String the header value
     */
    public String header(String name)
    {
        return this.request().getHeader(name);
    }

    /**
     * Get the value of the request cookie given
     * 
     * @param name
     *            the cookie name
     * @return returns String the cookie value
     */
    public String cookie(String name)
    {
        return this.request().cookie(name);
    }
    
    /**
     * Set a cookie.  Cookies are set using a fluent interface, 
     * for example: <code>cookie().name("name").value("value").set()</code>
     * @return A CookieBuilder to create and set the cookie.
     */
    public CookieBuilder<BalsaResponse> cookie()
    {
        return this.response.cookie();
    }

    /**
     * Get the view which is currently being decoded or encoded
     * 
     * @return the view
     */
    public BalsaViewStack view()
    {
        return this.view;
    }

    /**
     * Get the view which is currently being decoded or encoded
     * 
     * @return the view
     */
    public BalsaViewStack getView()
    {
        return this.view;
    }
    
    
    /**
     * Get the view which is currently being included for decoded or encoded
     * 
     * @return the view
     */
    public BalsaViewStack getInclude()
    {
        return this.include;
    }
    
    /**
     * Get the view which is currently being included for decoded or encoded
     * 
     * @return the view
     */
    public BalsaViewStack include()
    {
        return this.include;
    }
    
    /**
     * Get the view which is currently being processed, be it the include or main view
     * @return the view
     */
    public BalsaViewStack getViewStack()
    {
        return this.include != null ? this.include : this.view;
    }
    
    /**
     * List the views under at the given path
     * @param path the path to list
     * @return a set of {@link ViewPath}s or empty set
     */
    public Set<ViewPath> listViews(String path)
    {
        return this.application.getViewEngine().list(path, this);
    }
    
    /**
     * List the views under at the given path with the metadata extract from each view
     * @param path the path to list
     * @return a set of {@link ViewPath}s or empty set
     */
    public Set<ViewPath> listWithViewMetadata(String path)
    {
        return this.application.getViewEngine().listWithViewMetadata(path, this);
    }
    
    /**
     * Load a single view, not registering it with this context in anyway
     * @param name the view name to load
     * @return the view or null if there was an error loading it
     */
    public BalsaView loadView(String name, String renderFormat)
    {
        try
        {
            return this.application.getViewEngine().load(name, this, renderFormat);
        }
        catch (BalsaException e)
        {
            logger.error("Error loading view " + name, e);
        }
        return null;
    }
    
    /**
     * Get the render format for view processing
     * @return the render format mime type
     */
    public String getRenderFormat()
    {
        return Util.coalesce(
            this.renderFormat,
            (this.response != null) ? this.response.getContentType() : null,
            "text/html"
        );
    }
    
    /**
     * Override the render format if it is different from that of the response content type
     * @param mimeType the render format mime type
     * @return this context
     */
    public BalsaContext overrideRenderFormat(String mimeType)
    {
        this.renderFormat = mimeType;
        return this;
    }

    /**
     * Decode the given views
     * 
     * @param useTemplate
     *            should the view engine use the configured application templates
     * @param views
     *            returns void
     */
    public void decode(String[][] templates, String... views) throws BalsaException
    {
        try
        {
            this.view = this.app().getViewEngine().load(templates, views, this, this.getRenderFormat());
            this.view.decode(this);
        }
        finally
        {
            this.view = null;
        }
    }

    public void decodeOnly(String... views) throws BalsaException
    {
        this.decode(null, views);
    }
    
    public void decodeInclude(String... views) throws BalsaException
    { 
        // set the include
        final BalsaViewStack previousInclude = this.include;
        this.include = this.app().getViewEngine().load(null, views, this, this.getRenderFormat());
        try
        {
            // decode
            this.include.decode(this);
        }
        finally
        {
            this.include = previousInclude;
        }
    }

    /**
     * Respond by encoding the given views
     * 
     * NB: This will not set the content type and status of the response.
     * 
     * @param views
     *            the views to encode
     * @throws BalsaException
     *             returns void
     */
    public void encode(BalsaWriter to, String[][] templates, String... views) throws BalsaException
    {
        try
        {
            try
            {
                this.view = this.app().getViewEngine().load(templates, views, this, this.getRenderFormat());
                // encode
                if (to == null) to = this.response().getViewWriter();
                this.view.encode(this, to);
                // flush the response
                to.flush();
            }
            finally
            {
                this.view = null;
            }
        }
        catch (IOException e)
        {
            throw new BalsaIOError("IO error while encoding view", e);
        }
    }

    public void encode(String[][] templates, String... views) throws BalsaException
    {
        this.encode(null, templates, views);
    }

    public void encodeOnly(BalsaWriter to, String... views) throws BalsaException
    {
        this.encode(to, null, views);
    }

    public void encodeOnly(String... views) throws BalsaException
    {
        this.encode(null, null, views);
    }

    public String encodeBuffered(String[][] templates, String... views) throws BalsaException
    {
        try
        {
            StringWriter sw = new StringWriter();
            HTMLWriter hw = new HTMLWriter(sw);
            this.encode(hw, templates, views);
            hw.close();
            return sw.toString();
        }
        catch (IOException e)
        {
            throw new BalsaIOError("IO error while encoding buffered view", e);
        }
    }

    public String encodeOnlyBuffered(String... views) throws BalsaException
    {
        return this.encodeBuffered(null, views);
    }
    
    public void encodeInclude(BalsaWriter to, String... views) throws BalsaException
    {
        try
        {
            final BalsaViewStack previousInclude = this.include;
            try
            {
                this.include = this.app().getViewEngine().load(null, views, this, this.getRenderFormat());
                // encode
                if (to == null) to = this.response().getViewWriter();
                this.include.encode(this, to);
            }
            finally
            {
                this.include = previousInclude;
            }
        }
        catch (IOException e)
        {
            throw new BalsaIOError("IO error while encoding view", e);
        }
    }
    
    public String encodeIncludeBuffered(String... views) throws BalsaException
    {
        try
        {
            StringWriter sw = new StringWriter();
            HTMLWriter hw = new HTMLWriter(sw);
            this.encodeInclude(hw, views);
            hw.close();
            return sw.toString();
        }
        catch (IOException e)
        {
            throw new BalsaIOError("IO error while encoding buffered view", e);
        }
    }

    /**
     * Translate the given URL into an absolute URL using information from the request.
     * 
     * The URL will not be translated, and returned unchanged, if: 1. the URL has a protocol (start with: '[A-Za-z]://') 2. the URL starts with '//'
     * 
     * @param url
     *            the relative url to translate
     * @return returns String
     */
    public String url(String url)
    {
        if (url == null) return null;
        if (url.startsWith("#")) return url;
        if (url.startsWith("//")) return url;
        if (url.indexOf("://") != -1) return url;
        // translate it
        StringBuilder sb = new StringBuilder();
        // the scheme
        int port = this.request.getServerPort();
        String scheme = this.request.getRequestScheme();
        sb.append(scheme != null ? scheme : (port == 443 ? "https" : "http"));
        sb.append("://");
        // server name
        sb.append(this.request.getServerName());
        // port?
        if (port != 80 && port != 443) sb.append(":").append(port);
        // script path
        String scriptPath = this.request.getScriptName();
        if (scriptPath.length() > 0)
        {
            if (!scriptPath.startsWith("/")) sb.append("/");
            sb.append(scriptPath);
        }
        // path
        if (!(url.startsWith("/") || scriptPath.endsWith("/"))) sb.append("/");
        sb.append(url);
        return sb.toString();
    }
    
    /**
     * Translate the given URL into an absolute URL using information from the request.
     * 
     * The URL will not be translated, and returned unchanged, if: 1. the URL has a protocol (start with: '[A-Za-z]://') 2. the URL starts with '//'
     * 
     * @param url the relative url to translate
     * @param query optional query string parameters
     * 
     * @return returns String
     */
    public String url(String url, Map<?, ?> query)
    {
        return url(URLUtil.buildURL(url, query));
    }
    
    /**
     * Translate the given URL into an absolute URL using information from the request.
     * 
     * The URL will not be translated, and returned unchanged, if: 1. the URL has a protocol (start with: '[A-Za-z]://') 2. the URL starts with '//'
     * 
     * @param url the relative url to translate
     * @param query optional query string parameters
     * @param fragment optional url fragment
     * 
     * @return returns String
     */
    public String url(String url, Map<?, ?> query, String fragment)
    {
        return url(URLUtil.buildURL(url, query, fragment));
    }

    /**
     * Translate the given path to a server absolute path using information from the request.
     * 
     * @param path
     *            the path to make absolute
     * @return returns String
     */
    public String path(String path)
    {
        StringBuilder sb = new StringBuilder();
        // script path
        String scriptPath = this.request.getScriptName();
        if (scriptPath.length() > 0)
        {
            if (!scriptPath.startsWith("/")) sb.append("/");
            sb.append(scriptPath);
        }
        // path
        if (!(path.startsWith("/") || scriptPath.endsWith("/"))) sb.append("/");
        sb.append(path);
        return sb.toString();
    }

    /**
     * Translate the given relative path into the URL for the public resource.
     * 
     * The resulting URL maybe an absolute server path or an absolute URL.
     * 
     * 
     * 
     * @param path
     *            the relative path to the public resource
     * @return the URL to the resource.
     */
    public String pub(String path)
    {
        return this.app().getPublicResourceEngine().pub(this, path);
    }

    /**
     * Redirect to another URL
     * 
     * @param url
     *            the URL to redirect to
     * @param permanent
     *            it the redirect permenant (301) returns void
     */
    public void redirect(String url, boolean permanent) throws BalsaException
    {
        try
        {
            this.response.redirect(this.url(url), permanent);
        }
        catch (IOException e)
        {
            throw new BalsaIOError("Failed to send redirect", e);
        }
    }
    
    /**
     * Redirect to another URL
     * 
     * @param url the URL to redirect to
     * @param query optional query string parameters
     * @param permanent if the redirect permenant (301) returns void
     */
    public void redirect(String url, Map<?, ?> query, boolean permanent) throws BalsaException
    {
        try
        {
            this.response.redirect(this.url(url, query), permanent);
        }
        catch (IOException e)
        {
            throw new BalsaIOError("Failed to send redirect", e);
        }
    }

    /**
     * Redirect (302) to another URL
     * 
     * @param url
     *            the URL to redirect to returns void
     */
    public void redirect(String url) throws BalsaException
    {
        redirect(url, false);
    }
    
    /**
     * Redirect (302) to another URL
     * 
     * @param url the URL to redirect to returns void
     * @param query optional query string parameters 
     */
    public void redirect(String url, Map<?, ?> query) throws BalsaException
    {
        redirect(url, query, false);
    }

    /**
     * Require a security constraint to be met, throwing a BalsaSecurityException if the constraint is not met
     * 
     * @param constraint the constraint which needs to be met
     */
    public void require(boolean constraint) throws BalsaException
    {
        if (!constraint) throw new BalsaSecurityException("Secuirty requirement not met");
    }

    /**
     * Require a security constraint to be met, throwing a BalsaSecurityException if the constraint is not met
     * 
     * @param constraint the constraint which needs to be met
     * @param message the message for the exception
     */
    public void require(boolean constraint, String message) throws BalsaException
    {
        if (!constraint) throw new BalsaSecurityException(message);
    }

    /**
     * Require a constraint to be met, throwing the given exception if the constraint is not met
     * 
     * @param constraint the constraint which needs to be met
     * @param securityException the exception to throw
     */
    public <E extends Exception> void require(boolean constraint, E securityException) throws E
    {
        if (!constraint) throw securityException;
    }
    
    /**
     * Require a constraint to be met, creating and throwing the given exception if the constraint is not met
     * 
     * @param constraint the constraint which needs to be met
     * @param securityException the supplier of the exception to throw
     */
    public <E extends Exception> void require(boolean constraint, Supplier<E> securityException) throws E
    {
        if (!constraint) throw securityException.get();
    }
    
    /**
     * No authentication is currently happening or has not happened
     */
    public boolean notAuthenticated()
    {
        return this.session == null ? false : this.session().authenticationState().isNotAuthenticated();
    }
    
    /**
     * Authentication is currently in progress
     */
    public boolean authenticating()
    {
        return this.session == null ? false : this.session().authenticationState().isAuthenticating();
    }
    
    /**
     * Do we have an authenticated principal
     */
    public boolean authenticated()
    {
        return this.session == null ? false : this.session().authenticationState().isAuthenticated();
    }
    
    /**
     * Get the authentication state for this current session
     */
    public AuthenticationState authenticationState()
    {
        return this.session == null ? null : this.session().authenticationState();
    }
    
    /**
     * Get the authentication info for this current session
     */
    public AuthenticationInfo authenticationInfo()
    {
        return this.session == null ? null : this.session().authenticationState().info();
    }

    /**
     * Check that the current principal is valid
     */
    public boolean validPrincipal(ValidationLevel validationLevel)
    {
        // delegate validation to the security manager
        return this.app().getSecurityEngine().isValidPrincipal(this.currentPrincipal(), this.authenticationInfo(), validationLevel);
    }
    
    /**
     * Check that the current principal is valid (strongly)
     */
    public boolean validPrincipal()
    {
        return this.validPrincipal(ValidationLevel.STRONG);
    }
    
    /**
     * Check that the current principal is valid (weakly)
     */
    public boolean principal()
    {
        return this.validPrincipal(ValidationLevel.WEAK);
    }

    /**
     * Get the current logged in principal from the request or the session
     */
    @SuppressWarnings("unchecked")
    public <T extends Principal> T currentPrincipal()
    {        
        if (this.currentPrincipal != null)
            return (T) this.currentPrincipal;
        return this.session == null ? null : this.session().authenticationState().currentPrincipal();
    }

    /**
     * Deauthenticate the current logged in principal
     */
    public void deauthenticate()
    {
        // reset to the default principal
        this.currentPrincipal = null;
        // reset the session auth state
        if (this.session != null) this.session.authenticationState().reset();
    }
    
    /**
     * Verify the given credentials are valid for the currently authenticated principal
     * @throws BalsaSecurityException should there be any issues verifying the credentials for the currently authenticated principal
     */
    public void verify(Credentials credentials) throws BalsaSecurityException
    {
        this.app().getSecurityEngine().verify(this.session().authenticationState(), credentials);
    }
    
    /**
     * Generate a set of authentication challenges for the currently authenticating principal. 
     * This is a map of the authentication method name to challenge.
     */
    public Map<String, AuthenticationChallenge> generateAuthenticationChallenges()
    {
        Principal principal = this.session().authenticationState().authenticatingPrincipal();
        if (principal == null) throw new BalsaException("There is no principal which is currently authenticating, cannot generate authentication challenges");
        return this.app().getSecurityEngine().generateAuthenticationChallenges(principal);
    }
    
    /**
     * Generate a set of challenges which are needed to authenticate the given 
     * principal via certain authentication methods.  This is a map of the 
     * authentication method name to challenge.
     */
    public Map<String, AuthenticationChallenge> generateAuthenticationChallenges(Principal principal)
    {
        return this.app().getSecurityEngine().generateAuthenticationChallenges(principal);
    }
    
    /**
     * Start an initially authenticated session for the given principal.  This can be used to initially login 
     * a principal upon registration journeys.
     * 
     * @param principal the principal which should be authenticated for this session
     */
    public void initiallyAuthenticated(Principal principal)
    {
        // ensure we force the creation of a session
        this.session()
            .authenticationState()
            .update(new AuthenticationResponse(principal, new SimpleAuthenticationInfo(AuthenticationMethod.INITIAL, null)));
    }
    
    /**
     * Start the authentication process.  The response will specify if
     * @throws BalsaSecurityException should there be any issues authenticating the user.
     */
    public AuthenticationResponse authenticate(Credentials credentials) throws BalsaSecurityException
    {
        try
        {
            // use the security engine to authenticate the user
            AuthenticationResponse response = this.app().getSecurityEngine().authenticate(this.session().authenticationState(), credentials, false);
            // update the authentication state
            return this.session().authenticationState().update(response);
        }
        catch (BalsaSecurityException e)
        {
            this.session().authenticationState().reset();
            throw e;
        }
    }

    /**
     * Authenticate for the life of this session, using a single factor authentication. 
     * This method will always return with a valid, authenticated user.
     * @throws BalsaSecurityException should there be any issues authenticating the user.
     */
    @SuppressWarnings("unchecked")
    public <T extends Principal> T authenticateSingleFactor(Credentials credentials, boolean force) throws BalsaSecurityException
    {
        try
        {
            // use the security engine to authenticate the user
            AuthenticationResponse response = this.app().getSecurityEngine().authenticate(this.session().authenticationState(), credentials, force);
            if (! (force || response.isComplete())) throw new BalsaSecurityException("Failed to authenticate user using single factor");
            // update the authentication state
            return (T) this.session().authenticationState().update(response).getPrincipal();
        }
        catch (BalsaSecurityException e)
        {
            this.session().authenticationState().reset();
            throw e;
        }
    }

    /**
     * Authenticate for the life of this session, using single factor authentication.  
     * This method will always return with a valid, authenticated user.
     * @throws BalsaSecurityException should there be any issues authenticating the user.
     */
    public <T extends Principal> T authenticate(String username, String password) throws BalsaSecurityException
    {
        return this.authenticateSingleFactor(new PasswordCredentials.Simple(username, password), false);
    }
    
    /**
     * Authenticate for the life of this request only, this avoids creating a session, 
     * this method will always return with a valid, authenticated user.
     * @throws BalsaSecurityException should there be any issues authenticating the user.
     */
    @SuppressWarnings("unchecked")
    public <T extends Principal> T authenticateRequest(Credentials credentials) throws BalsaSecurityException
    {
        // use the security engine to authenticate the user
        Principal principal = this.app().getSecurityEngine().authenticateRequest(credentials, false);
        if (principal == null) throw new BalsaSecurityException("Failed to authenticate user");
        // store the principal
        this.currentPrincipal = principal;
        return (T) principal;
    }
    
    /**
     * Authenticate for the life of this request only using a single authentication factor, 
     * this avoids creating a session, this method will always return with a valid, authenticated user.
     * @throws BalsaSecurityException should there be any issues authenticating the user.
     */
    @SuppressWarnings("unchecked")
    public <T extends Principal> T authenticateRequestSingleFactor(Credentials credentials) throws BalsaSecurityException
    {
        // use the security engine to authenticate the user
        Principal principal = this.app().getSecurityEngine().authenticateRequest(credentials, true);
        if (principal == null) throw new BalsaSecurityException("Failed to authenticate user");
        // store the principal
        this.currentPrincipal = principal;
        return (T) principal;
    }
    
    /**
     * Authenticate for the life of this request only, this avoids creating a session, 
     * this method will always return with a valid, authenticated user.
     * @throws BalsaSecurityException should there be any issues authenticating the user.
     */
    public <T extends Principal> T authenticateRequest(String username, String password) throws BalsaSecurityException
    {
        return this.authenticateRequest(new PasswordCredentials.Simple(username, password));
    }
    
    /**
     * Authenticate for the life of this request only using a single authentication factor, 
     * this avoids creating a session, this method will always return with a valid, authenticated user.
     * @throws BalsaSecurityException should there be any issues authenticating the user.
     */
    public <T extends Principal> T authenticateRequestSingleFactor(String username, String password) throws BalsaSecurityException
    {
        return this.authenticateRequestSingleFactor(new PasswordCredentials.Simple(username, password));
    }
    
    /**
     * Try to authenticate for the life of this session, should 
     * authentication not be possible then null is returned, exceptions are thrown.
     */
    public <T extends Principal> T tryAuthenticate(String username, String password)
    {
        try 
        {
            return this.authenticate(username, password);
        }
        catch (BalsaSecurityException e)
        {
            // ignore
        }
        return null;
    }

    /**
     * Try to authenticate for the life of this session, should 
     * authentication not be possible then null is returned, exceptions are thrown.
     */
    public <T extends Principal> T tryAuthenticateSingleFactor(Credentials credentials, boolean force)
    {
        try 
        {
            return this.authenticateSingleFactor(credentials, force);
        }
        catch (BalsaSecurityException e)
        {
            // ignore
        }
        return null;
    }
    
    /**
     * Try to authenticate for the life of this session, should 
     * authentication not be possible then null is returned, exceptions are thrown.
     */
    public AuthenticationResponse tryAuthenticate(Credentials credentials)
    {
        try 
        {
            return this.authenticate(credentials);
        }
        catch (BalsaSecurityException e)
        {
            // ignore
        }
        return null;
    }
    
    /**
     * Try to authenticate for the life of this request only, this avoids creating a session, 
     * should authentication not be possible then null is returned, exceptions are thrown.
     */
    public <T extends Principal> T tryAuthenticateRequest(String username, String password)
    {
        try 
        {
            return this.authenticateRequest(username, password);
        }
        catch (BalsaSecurityException e)
        {
            // ignore
        }
        return null;
    }
    
    /**
     * Try to authenticate for the life of this request only, this avoids creating a session, 
     * should authentication not be possible then null is returned, exceptions are thrown.
     */
    public <T extends Principal> T tryAuthenticateRequest(Credentials credentials)
    {
        try 
        {
            return this.authenticateRequest(credentials);
        }
        catch (BalsaSecurityException e)
        {
            // ignore
        }
        return null;
    }

    /**
     * Check that the current user has the given permission
     * 
     * @param permission
     *            the permission name
     * @return returns boolean
     */
    public boolean permission(String permission)
    {
        return this.app().getSecurityEngine().check(this.currentPrincipal(), permission);
    }

    /**
     * Check that the current user has the given permission over the given object
     * @param permission the permission name
     * @param object the object over which permission must be granted
     * @return true if and only if the current user has the given permission over th given object
     */
    public boolean permission(String permission, Object object)
    {
        return this.app().getSecurityEngine().check(this.currentPrincipal(), permission, object);
    }

    /**
     * Check that the given access token is valid
     * 
     * @param token
     * @return
     */
    public boolean validAccessToken(String token)
    {
        if (isEmpty(token)) return false;
        return this.application.getSecurityEngine().validAccess(token);
    }

    /**
     * Check that the given access token is valid for the current path
     * 
     * @param token
     * @return
     */
    public boolean validAccessTokenForURL(String token)
    {
        if (isEmpty(token)) return false;
        return this.application.getSecurityEngine().validAccessForURL(this.request.getPathInfo(), token);
    }

    /**
     * Generate an access token which is valid for the given URL
     * 
     * @param path
     * @return
     */
    public String generateAccessTokenForURL(String path)
    {
        return this.application.getSecurityEngine().generateAccessTokenForURL(path);
    }
    
    /**
     * Generate an access token
     * @return
     */
    public String generateAccessToken()
    {
        return this.application.getSecurityEngine().generateAccessToken();
    }

    /**
     * Get the named session variable
     * 
     * @param name
     *            the variable name
     * @return returns Object
     */
    @SuppressWarnings("unchecked")
    public <T> T sessionVar(String name)
    {
        return (T) this.session == null ? null : this.session().getVar(name);
    }

    /**
     * Store a variable in the session
     * 
     * @param name
     *            the variable name
     * @param object
     *            the variable returns void
     */
    public <T> T sessionVar(String name, T object)
    {
        return this.session().putVar(name, object);
    }
    
    /**
     * Promote all cached session models to the session
     */
    public void promoteSessionModelCache()
    {
        // promote all cached session model
        if (this.session != null)
        {
            for (Entry<String, Object> cachedModel : this.sessionModelCache.entrySet())
            {
                if (logger.isDebugEnabled()) logger.debug("Promoting session model to session: " + cachedModel.getKey());
                this.session.putModel(cachedModel.getKey(), cachedModel.getValue());
            }
        }
    }
    
    /**
     * Remove all cached session models
     */
    public void clearSessionModelCache()
    {
        this.sessionModelCache.clear();
    }
    
    /**
     * Forcefully remove the session model from our local cache
     * @param name the model name
     */
    public void uncacheSessionModel(String name)
    {
        this.sessionModelCache.remove(name);
    }
    
    // Services
    
    /**
     * Get the named instance of a service
     * @param <T> the service type
     * @param type the service type
     * @return the service or null if none is defined
     */
    public <T> T service(Class<T> type, String name)
    {
        return this.application.service(type, name);
    }

    /**
     * Get the default instance of a service
     * @param <T> the service type
     * @param type the service type
     * @return the service or null if none is defined
     */
    public <T> T service(Class<T> type)
    {
        return this.application.service(type);
    }

    /**
     * Get the named instance of a service with the given type simple name.
     * 
     * Note: This is intended to be used by Express expressions rather than from code.
     * 
     * @param <T>
     * @param type the class simple name
     * @param name the service registered name
     * @return the service or null if none can be found
     */
    public <T> T service(String type, String name)
    {
        return this.application.service(type, name);
    }

    /**
     * Get the default instance of a service with the given type simple name.
     * 
     * Note: This is intended to be used by Express expressions rather than from code.
     * 
     * @param <T>
     * @param type the class simple name
     * @return the service or null if none can be found
     */
    public <T> T service(String type)
    {
        return this.application.service(type);
    }

    // Actions

    /**
     * Execute the named action with the given arguments
     * @param action the action name
     * @param arguments the arguments to pass to the action
     * @return the result of the action
     * @throws Exception should the action fail for any reason
     */
    @SuppressWarnings("unchecked")
    public <T> T action(String action, Object... arguments) throws Exception
    {
        ActionHandler handler = this.app().action(action);
        if (handler == null) throw new BalsaException("The action " + action + " does not exist");
        return (T) handler.act(arguments);
    }

    /**
     * Execute the named action with the given arguments using the task engine, 
     * this causes the task to be executed out of band.  This returns a task id 
     * which can be used to poll the state of a task.
     * @param action the action name
     * @param arguments the arguments to pass to the action
     * @return the id of the started task
     */
    @SuppressWarnings("unchecked")
    public <T> TaskFuture<T> deferredAction(String action, Object... arguments)
    {
        // submit the task for execution
        return (TaskFuture<T>) this.app().getTaskEngine().submit(new DeferredActionTask(action, arguments));
    }
    
    /**
     * Poll the state of a deferred action, getting and removing it 
     * should it have completed
     */
    public <T> TaskFuture<T> pollDeferredAction(String id)
    {
        // lookup the task state
        return this.application.getTaskEngine().lookup(id);
    }
    
    /**
     * Peek at the current composite component
     * @return
     */
    public Component peekCompositeComponent()
    {
        return this.compositeComponents.peek();
    }
    
    /**
     * Pop the the current composite component from the stack
     * @return
     */
    public Component popCompositeComponent()
    {
        return this.compositeComponents.pop();
    }
    
    /**
     * Push a composite component onto the stack
     * @param component
     */
    public void pushCompositeComponent(Component component)
    {
        this.compositeComponents.push(component);
    }
    
    public Stack<Component> getCompositeComponents()
    {
        return this.compositeComponents;
    }

    // Static

    public final static BalsaContext Balsa()
    {
        return BalsaContext.get();
    }
    
    public final static <T> T withContext(BalsaContext context, Callable<T> task) throws Exception
    {
        try
        {
            // activate the context
            context.activate();
            // bind the context
            context.bind();
            // execute the task
            return task.call();
        }
        finally
        {
            // ensure the context is unbound
            context.unbind();
            // deactivate the context
            context.deactivate();
        }
    }
    
    public final static <T> T withContext(BalsaApplication application, BalsaSession session, Callable<T> task) throws Exception
    {
        return withContext(new BalsaContext(application, session), task);
    }
    
    public final static <T> T withContext(BalsaApplication application, Callable<T> task) throws Exception
    {
        return withContext(new BalsaContext(application), task);
    }
}
