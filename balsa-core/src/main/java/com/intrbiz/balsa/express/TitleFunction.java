package com.intrbiz.balsa.express;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;

public class TitleFunction extends Function
{
    public TitleFunction()
    {
        super("title");
    }

    /**
     * Evaluate the title chain
     */
    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        String title = BalsaContext.get().getViewStack().getTargetView().getTitle();
        return (title == null) ? "" : title;
    }
    
    @Override
    public boolean isIdempotent()
    {
        return false;
    }
}
