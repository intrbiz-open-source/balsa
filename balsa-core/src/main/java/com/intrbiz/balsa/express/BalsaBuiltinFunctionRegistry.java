package com.intrbiz.balsa.express;

import com.intrbiz.express.ExpressExtensionRegistry;

public class BalsaBuiltinFunctionRegistry extends ExpressExtensionRegistry
{
    public BalsaBuiltinFunctionRegistry()
    {
        super();
        /* Default Functions */
        // immutable
        this.addFunction(new BalsaFunction());
        this.addFunction(new PathInfoFunction());
        this.addFunction(new TitleFunction());
        this.addFunction(new RequestTokenFunction());
        this.addFunction("path", PathFunction.class);
        this.addFunction("public", PublicFunction.class);
        this.addFunction("access_token_for_url", RequestPathTokenFunction.class);
        this.addFunction("viewmeta", ViewMetaFunction.class);
        this.addFunction("includemeta", IncludeMetaFunction.class);
        this.addFunction("slug", SlugFunction.class);
    }
}
