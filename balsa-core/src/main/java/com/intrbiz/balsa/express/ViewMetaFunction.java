package com.intrbiz.balsa.express;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.engine.view.BalsaViewStack;
import com.intrbiz.balsa.engine.view.ViewMetadata;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.operator.Operator;

public class ViewMetaFunction extends Function
{
    public ViewMetaFunction()
    {
        super("viewmeta");
    }

    /**
     * Get a view metadata value
     */
    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        Operator name = this.getParameter(0);
        // get the metadata
        BalsaViewStack view = BalsaContext.get().getView();
        if (view != null)
        {
            ViewMetadata meta = view.getTargetView().getMetadata();
            if (meta != null)
            {
                // get the property or the whole lot
                return (name == null) ? meta : meta.getAttribute(String.valueOf(name.get(context, source)));
            }
        }
        return null;
    }
    
    @Override
    public boolean isIdempotent()
    {
        return false;
    }
}
