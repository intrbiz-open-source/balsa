package com.intrbiz.balsa.express;

import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressException;
import com.intrbiz.express.operator.Function;
import com.intrbiz.express.operator.Operator;
import com.intrbiz.util.Slug;

/**
 * Generate a URL slug from the given input
 */
public class SlugFunction extends Function
{
    public SlugFunction()
    {
        super("slug");
    }

    @Override
    public Object get(ExpressContext context, Object source) throws ExpressException
    {
        StringBuilder slug = new StringBuilder();
        // process all arguments
        for (Operator param : this.getParameters())
        {
            Object value = param.get(context, source);
            if (value != null)
                Slug.generateSlug(String.valueOf(value), slug);
        }
        return slug.toString();
    }
    
    /**
     * Generate a safe URL name from the given input
     * @param name the name to process
     * @param slug the slug to append to
     */
    @Deprecated
    public final static void generateSlug(String name, StringBuilder slug, boolean allowSlash)
    {
        Slug.generateSlug(name, slug, allowSlash);
    }
    
    @Deprecated
    public final static void generateSlug(String name, StringBuilder slug)
    {
        Slug.generateSlug(name, slug);
    }
    
    @Deprecated
    public final static String generateSlug(String name)
    {
        return Slug.generateSlug(name);
    }
    
    @Deprecated
    public final static String generateSlug(String name, boolean allowSlash)
    {
        return Slug.generateSlug(name, allowSlash);
    }

    @Override
    public boolean isIdempotent()
    {
        return false;
    }
}
