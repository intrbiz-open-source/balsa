package com.intrbiz.data.db.compiler.dialect;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import com.intrbiz.data.db.compiler.dialect.function.SQLFunctionGenerator;
import com.intrbiz.data.db.compiler.dialect.type.SQLEnumType;
import com.intrbiz.data.db.compiler.dialect.type.SQLType;
import com.intrbiz.data.db.compiler.model.Column;
import com.intrbiz.data.db.compiler.model.ForeignKey;
import com.intrbiz.data.db.compiler.model.Function;
import com.intrbiz.data.db.compiler.model.Index;
import com.intrbiz.data.db.compiler.model.Schema;
import com.intrbiz.data.db.compiler.model.Table;
import com.intrbiz.data.db.compiler.model.Type;
import com.intrbiz.data.db.compiler.util.SQLCommand;
import com.intrbiz.data.db.compiler.util.SQLScript;
import com.intrbiz.util.Util;

public abstract class SQLDialect
{
    public static String DATABASE_OWNER = "database.owner";
    
    protected final String dialect;
    
    protected String defaultOwner;
    
    protected String owner;
    
    protected Map<Class<? extends Annotation>, SQLFunctionGenerator> functionGenerators = new IdentityHashMap<Class<? extends Annotation>, SQLFunctionGenerator>();
    
    protected Map<Class<?>, List<SQLType>> classToType = new HashMap<>();
    
    protected Map<String, List<SQLType>> sqlTypeToType = new HashMap<>();
    
    public SQLDialect(String dialect, String defaultOwner)
    {
        this.dialect = dialect;
        this.defaultOwner = defaultOwner;
    }
    
    public final String getDialectName()
    {
        return this.dialect;
    }
    
    public void registerType(SQLType type) {
        for (Class<?> cls : type.getJavaTypes()) {
            this.classToType.merge(cls, Collections.singletonList(type), SQLDialect::merge);
        }
        for (String sqlType : type.getSQLTypes()) {
            this.sqlTypeToType.merge(sqlType.toUpperCase(), Collections.singletonList(type), SQLDialect::merge);
        }
    }
    
    private static List<SQLType> merge(List<SQLType> oldValue, List<SQLType> newValue) {
        List<SQLType> merged = new ArrayList<>();
        if (oldValue != null) merged.addAll(oldValue);
        if (newValue != null) merged.addAll(newValue);
        return merged;
    }
    
    public void registerFunctionGenerator(Class<? extends Annotation> type, SQLFunctionGenerator generator)
    {
        this.functionGenerators.put(type, generator);
    }

    protected SQLFunctionGenerator getFunctionGenerator(Class<? extends Annotation> type)
    {
        return this.functionGenerators.get(type);
    }
    
    // Schema owner
    
    public final String getOwner()
    {
        // use the explicit owner
        if (!Util.isEmpty(this.owner)) return this.owner;
        // look at a system property
        String dbOwner = System.getProperty(DATABASE_OWNER);
        if (! Util.isEmpty(dbOwner)) return dbOwner;
        // use the default
        return this.defaultOwner;
    }
    
    public final void setOwner(String owner)
    {
        this.owner = owner;
    }
    
    public final String getDefaultOwner()
    {
        return this.defaultOwner;
    }
    
    public final void setDefaultOwner(String owner)
    {
        this.defaultOwner = owner;
    }
    
    // SQL types
    
    @SuppressWarnings("unchecked")
    public SQLType getType(Class<?> javaClass)
    {
        // is it an enum
        if (Enum.class.isAssignableFrom(javaClass))
        {
            return new SQLEnumType((Class<? extends Enum<?>>) javaClass);
        }
        // do we have a type defined
        List<SQLType> types = this.classToType.get(javaClass);
        if (types != null && types.size() > 0) {
            return types.iterator().next();
        }
        // can't handle if
        throw new RuntimeException("Cannot get SQL type for java class: " + javaClass.getCanonicalName());
    }
    
    public SQLType getType(String sqlType) {
        List<SQLType> types =  this.sqlTypeToType.get(sqlType.toUpperCase());
        if (types != null && types.size() > 0) {
            return types.iterator().next();
        }
        throw new RuntimeException("The SQL type: " + sqlType + " is not supported");
    }
    
    // DDL
    
    public abstract SQLScript writeCreateSchema(Schema schema);
    
    public abstract SQLScript writeCreateTable(Table table);
    
    public abstract SQLScript writeAlterTableAddColumn(Table table, Column col);
    
    public abstract SQLScript writeAlterTableAddForeignKey(Table table, ForeignKey fkey);
    
    public abstract SQLScript writeCreateIndex(Table table, Index index);
    
    public abstract SQLScript writeCreatePartitionedTable(Table table);
    
    public abstract SQLScript writeCreatePartitionedTableHelpers(Table table);
    
    public abstract SQLScript writeCreateType(Type type);
    
    public abstract SQLScript writeAlterTypeAddColumn(Type type, Column col);
    
    public abstract SQLScript writeCreateFunction(Function function);
    
    // Module info
    
    public abstract SQLScript writeCreateSchemaNameFunction(Schema schema);
    
    public abstract SQLScript writeCreateSchemaVersionFunction(Schema schema);
    
    public abstract String getSchemaNameQuery(Schema schema);
    
    public abstract String getSchemaVersionQuery(Schema schema);
    
    // Function call SQL
    
    public abstract SQLCommand getFunctionCallQuery(Function function);
}
