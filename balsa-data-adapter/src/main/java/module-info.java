module com.intrbiz.balsa.data.db.compiler {
    
    requires org.slf4j;
    
    requires com.intrbiz.balsa.util;
    requires com.intrbiz.balsa.data.manager;
    requires com.intrbiz.balsa.data.annotations;
    requires com.intrbiz.balsa.util.compiler;
    requires com.intrbiz.balsa.express;
    
    exports com.intrbiz.data.db.compiler;
    
}