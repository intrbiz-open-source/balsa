module com.intrbiz.balsa.data.manager {
    
    requires transitive java.sql;
    
    requires org.slf4j;
    requires transitive com.zaxxer.hikari;
    
    requires com.intrbiz.balsa.util;
    requires transitive com.intrbiz.balsa.config;
    requires transitive com.intrbiz.balsa.metrics;
    
    exports com.intrbiz.data;
    exports com.intrbiz.data.metadata;
    exports com.intrbiz.data.db;
    exports com.intrbiz.data.db.pool;
    exports com.intrbiz.data.db.util;
    exports com.intrbiz.data.cache;
    exports com.intrbiz.data.cache.memory.local;
    exports com.intrbiz.data.cache.memory.shared;
    exports com.intrbiz.data.cache.tiered;
    exports com.intrbiz.data.cache.wrapper;
    
}