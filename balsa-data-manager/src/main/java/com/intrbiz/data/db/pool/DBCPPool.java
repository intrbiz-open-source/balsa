package com.intrbiz.data.db.pool;

import java.sql.Connection;
import java.sql.DriverManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Counter;
import com.intrbiz.gerald.witchcraft.Witchcraft;
import com.intrbiz.gerald.witchcraft.Witchcraft.Scope;
import com.intrbiz.util.Util;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


public class DBCPPool implements DatabasePool
{
    private DatabasePoolConfiguration cfg;

    private HikariDataSource datasource;
    
    protected Counter borrowedConnections;
    
    private static Logger logger = LoggerFactory.getLogger(DBCPPool.class);

    @Override
    public void close()
    {
        try
        {
            this.datasource.close();
        }
        catch (Exception e)
        {
        }
    }

    @Override
    public Connection connect() throws Exception
    {
        if (this.borrowedConnections != null) this.borrowedConnections.inc();
        return this.datasource.getConnection();
    }

    @Override
    public void configure(DatabasePoolConfiguration cfg) throws Exception
    {
        this.cfg = cfg;
        // register the driver
        if (! Util.isEmpty(this.cfg.getDriver()))
        {
            logger.debug("Loading JDBC Driver Class: " + this.cfg.getDriver());
            Class<?> driverClass = Class.forName(this.cfg.getDriver());
            DriverManager.registerDriver((java.sql.Driver) driverClass.newInstance());
            logger.debug("Successfully registered JDBC driver");
        }
        // get url, uname, password
        if (Util.isEmpty(this.cfg.getUrl())) throw new NullPointerException("A database URL must be given to configure the pool");
        if (Util.isEmpty(this.cfg.getUsername())) throw new NullPointerException("A database Username must be given to configure the pool");
        // create the connection factory
        this.datasource = new HikariDataSource(new HikariConfig() {{
            setJdbcUrl(cfg.getUrl());
            setUsername(cfg.getUsername());
            setPassword(cfg.getPassword());
            setConnectionTestQuery(cfg.getValidationSql());
            setMinimumIdle(cfg.getMaxIdle());
            setIdleTimeout(cfg.getIdleTimeout());
            setMaximumPoolSize(cfg.getMaxActive());
        }});
        // setup the metrics
        this.borrowedConnections = Witchcraft.counter(DatabasePool.class, new Scope(this.cfg.getUsername() + "@" + this.cfg.getUrl()), "borrowed-connections");
    }

    @Override
    public DatabasePoolConfiguration getConfiguration()
    {
        return this.cfg;
    }
}
