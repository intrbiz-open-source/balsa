package com.intrbiz.balsa.listener.ssh.vfs.entry;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.permissions.PermissionDeniedException;

public abstract class BalsaVFSFile extends BalsaVFSEntry
{
    
    public BalsaVFSFile(BalsaVFS vfs, BalsaVFSEntry parent, String name)
    {
        super(vfs, parent, name);
    }
    
    public BalsaVFSFile(BalsaVFS vfs, String name)
    {
        super(vfs, name);
    }
    
    @Override
    public boolean isDirectory()
    {
        return false;
    }

    @Override
    public boolean isFile()
    {
        return true;
    }
    
    @Override
    public List<AbstractFile> getChildren() throws IOException, PermissionDeniedException
    {
        return Collections.emptyList();
    }
}
