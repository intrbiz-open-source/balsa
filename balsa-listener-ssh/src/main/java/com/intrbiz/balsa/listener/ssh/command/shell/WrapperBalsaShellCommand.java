package com.intrbiz.balsa.listener.ssh.command.shell;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.sshtools.common.permissions.PermissionDeniedException;
import com.sshtools.server.vsession.ShellUtilities;
import com.sshtools.server.vsession.UsageException;
import com.sshtools.server.vsession.VirtualConsole;

public class WrapperBalsaShellCommand extends BalsaShellCommand
{
    private CommandNode commands = new CommandNode();

    public WrapperBalsaShellCommand(String[] command, String subsystem, String signature, String description)
    {
        super(new String[] { command[0] }, subsystem, signature, description);
    }
    
    public void addSubCommand(BalsaShellCommand subCommand)
    {
        CommandNode node = this.commands;
        String[] command = subCommand.getCommand();
        for (int i = 1; i < command.length; i++)
        {
            node = node.children.computeIfAbsent(command[i], k -> new CommandNode());
        }
        // store the node
        if (node.command != null)
            throw new IllegalArgumentException("A command with the name: " + subCommand.getName() + " already exists");
        node.command = subCommand;
    }
    
    public List<BalsaShellCommand> getSubCommands()
    {
        List<BalsaShellCommand> commands = new ArrayList<>();
        this.traverseSubCommands(this.commands, commands);
        return commands;
    }
    
    private void traverseSubCommands(CommandNode node, List<BalsaShellCommand> commands)
    {
        if (node.command != null)
            commands.add(node.command);
        for (CommandNode child : node.children.values())
        {
            this.traverseSubCommands(child, commands);
        }
    }

    @Override
    public void run(String[] commandArgs, VirtualConsole console) throws IOException, PermissionDeniedException, UsageException
    {
        if (commandArgs.length > 1)
        {
            // search through the tree trying to match a command
            CommandNode node = this.commands;
            int i = 1;
            while (i < commandArgs.length)
            {
                CommandNode next = node.children.get(commandArgs[i]);
                if (next == null)
                    break;
                node = next;
                i++;
            }
            // execute the command
            if (node.command != null)
            {
                // extract the sub command name
                String[] subCommandArgs = new String[(commandArgs.length - i) + 1];
                subCommandArgs[0] = BalsaShellCommand.buildCommandname(commandArgs, i);
                System.arraycopy(commandArgs, i, subCommandArgs, 1, commandArgs.length - i);
                // invoke the command
                node.command.run(subCommandArgs, console);
            }
            else
            {
                // we didn't find the command
                console.println("Error, no such command: " + Arrays.stream(commandArgs).collect(Collectors.joining(" ")));
                this.usage(console);
            }
        }
        else
        {
            this.usage(console);
        }
    }
    
    protected void usage(VirtualConsole console)
    {
        console.println(ShellUtilities.padString(this.getName(), 30) + this.getDescription());
        for (BalsaShellCommand sub : this.getSubCommands())
        {
            console.println("  - " + ShellUtilities.padString(sub.getUsage(), 80) + sub.getDescription());
        }
    }
    
    private static class CommandNode
    {              
        public final Map<String, CommandNode> children = new TreeMap<>();
        
        public BalsaShellCommand command;
    }
}
