package com.intrbiz.balsa.listener.ssh.security.engine.method;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.SecurityEngine;
import com.intrbiz.balsa.engine.impl.security.method.BaseAuthenticationMethod;
import com.intrbiz.balsa.engine.security.AuthenticationState;
import com.intrbiz.balsa.engine.security.challenge.AuthenticationChallenge;
import com.intrbiz.balsa.engine.security.method.AuthenticatedPrincipal;
import com.intrbiz.balsa.error.BalsaSecurityException;
import com.intrbiz.balsa.listener.ssh.security.credentials.SSHPublicKeyCredential;
import com.intrbiz.balsa.listener.ssh.security.engine.SSHPublicKeySecurityEngine;

public class SSHPublicKeyMethod extends BaseAuthenticationMethod<SSHPublicKeyCredential>
{
    private static Logger logger = LoggerFactory.getLogger(SSHPublicKeyMethod.class);
    
    protected SSHPublicKeySecurityEngine securityEngine;

    public SSHPublicKeyMethod()
    {
        super(SSHPublicKeyCredential.class, "SSH-Public-Key");
    }

    @Override
    public void setup(SecurityEngine engine) throws BalsaException
    {
        if (! (engine instanceof SSHPublicKeySecurityEngine))
            throw new BalsaException("The SSH Public Key authentication method can only be registered against a security engine which implements SSHPublicKeySecurityEngine");
        this.securityEngine = (SSHPublicKeySecurityEngine) engine;
    }

    @Override
    public AuthenticatedPrincipal authenticate(AuthenticationState state, SSHPublicKeyCredential key) throws BalsaSecurityException
    {
        logger.debug("Authenticating SSH Public Key: {}, {}", key.getPrincipalName(), key.getKey());
        // convert the public key to a principal
        Principal principal = this.securityEngine.princiaplForSSHPublicKey(key.getPrincipalName(), key.getKey());
        // did we get a principal
        if (principal == null)
        {
            logger.error("No such principal {} could be found.", key.getPrincipalName());
            throw new BalsaSecurityException("No such principal");
        }
        return new AuthenticatedPrincipal(principal, this.name);
    }

    @Override
    public AuthenticationChallenge generateAuthenticationChallenge(Principal principal) throws BalsaSecurityException
    {
        return null;
    }
}
