package com.intrbiz.balsa.listener.ssh.vfs.entry.overlay;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFSSession;
import com.intrbiz.balsa.listener.ssh.vfs.VFSUtil;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSEntry;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileFactory;
import com.sshtools.common.permissions.PermissionDeniedException;
import com.sshtools.common.sftp.SftpFileAttributes;

public abstract class BalsaVFSOverlayEntry<U extends BalsaVFSEntry> implements AbstractFile
{
    private static final Logger logger = LoggerFactory.getLogger(BalsaVFSOverlayEntry.class);
    
    protected final BalsaVFSSession vfs;
    
    protected final U underlay;
    
    protected final List<String> path;
    
    protected final String name;
    
    public BalsaVFSOverlayEntry(BalsaVFSSession vfs, U underlay, List<String> path)
    {
        super();
        this.vfs = Objects.requireNonNull(vfs);
        this.underlay = Objects.requireNonNull(underlay);
        this.path = Objects.requireNonNull(path);
        this.name = path.isEmpty() ? "" : path.get(path.size() - 1);
    }

    public U getUnderlay()
    {
        return this.underlay;
    }
    
    public BalsaVFSSession getVFS()
    {
        return this.vfs;
    }
    
    @Override
    public AbstractFileFactory<? extends AbstractFile> getFileFactory()
    {
        return this.vfs;
    }
    
    @Override
    public String getName()
    {
        return this.name;
    }
    
    @Override
    public String getAbsolutePath()
    {
        return VFSUtil.buildPath(this.path);
    }
    
    @Override
    public String getCanonicalPath()
    {
        return this.getAbsolutePath();
    }
    
    public boolean isRoot()
    {
        return this.underlay.isRoot();
    }
    
    public boolean isDynamic()
    {
        return this.underlay.isDynamic();
    }
    
    @Override
    public boolean isHidden()
    {
        return this.underlay.isHidden();
    }
    
    @Override
    public void refresh()
    {
    }
    
    @Override
    public long lastModified()
    {
        return this.underlay.lastModified();
    }
    
    @Override
    public boolean exists()
    {
        return this.underlay.exists();
    }
    
    @Override
    public boolean isReadable()
    {
        return this.underlay.isReadable();
    }
    
    @Override
    public boolean isWritable()
    {
        return this.underlay.isWritable();
    }
    
    @Override
    public final SftpFileAttributes getAttributes() throws IOException, PermissionDeniedException
    {
        return this.underlay.getAttributes();
    }
    
    @Override
    public final void setAttributes(SftpFileAttributes attrs) throws IOException
    {
    }
    
    @Override
    public AbstractFile resolveFile(String child) throws IOException, PermissionDeniedException
    {
        throw new FileNotFoundException("Only directories can have children");
    }
    
    @Override
    public void moveTo(AbstractFile target) throws IOException, PermissionDeniedException
    {
        // we don't really have a way to implement rename, so invoke copy from on the target
        logger.debug("Move to: " + target.getAbsolutePath() + " [" + target.getClass().getSimpleName() + "]");
        target.copyFrom(this);
    }
    
    public String toString()
    {
        return this.getClass().getSimpleName() + "(" + this.getAbsolutePath() + ") underlay " + this.underlay.toString();
    }
}
