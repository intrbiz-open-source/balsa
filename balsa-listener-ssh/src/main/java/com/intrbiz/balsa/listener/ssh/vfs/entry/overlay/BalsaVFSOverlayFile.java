package com.intrbiz.balsa.listener.ssh.vfs.entry.overlay;

import java.io.IOException;
import java.util.List;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFSSession;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSFile;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.permissions.PermissionDeniedException;

public abstract class BalsaVFSOverlayFile<U extends BalsaVFSFile> extends BalsaVFSOverlayEntry<U>
{
    
    public BalsaVFSOverlayFile(BalsaVFSSession vfs, U underlay, List<String> path)
    {
        super(vfs, underlay, path);
    }

    @Override
    public boolean isDirectory() throws IOException, PermissionDeniedException
    {
        return this.underlay.isDirectory();
    }

    @Override
    public boolean isFile() throws IOException, PermissionDeniedException
    {
        return this.underlay.isFile();
    }

    @Override
    public List<AbstractFile> getChildren() throws IOException, PermissionDeniedException
    {
        return this.underlay.getChildren();
    }

}
