package com.intrbiz.balsa.listener.ssh.vfs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.impl.route.RouteEngineImpl;
import com.intrbiz.balsa.engine.impl.route.RouteEntry;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSDirectory;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSDynamicDirectory;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSDynamicFile;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSEntry;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSInMemoryMutableFile;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSNoneEntry;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSRoot;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSStaticDirectory;
import com.intrbiz.balsa.metadata.SSHVFS;
import com.intrbiz.balsa.metadata.SSHVFS.Type;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.value.ValueExpression;
import com.intrbiz.util.Util;
import com.sshtools.common.events.Event;
import com.sshtools.common.files.AbstractFileFactory;
import com.sshtools.common.permissions.PermissionDeniedException;

public class BalsaVFS implements AbstractFileFactory<BalsaVFSEntry>
{
    private static final Logger logger = LoggerFactory.getLogger(BalsaVFS.class);

    private final BalsaApplication application;
    
    private final BalsaVFSRoot root;

    public BalsaVFS(BalsaApplication application)
    {
        super();
        this.application = Objects.requireNonNull(application);
        // setup the root
        this.root = new BalsaVFSRoot(this);
        // we need these dummy files to keep the shell happy
        this.root.addChild(new BalsaVFSInMemoryMutableFile(this, ".profile", new byte[0]).readOnly());
        this.root.addChild(new BalsaVFSInMemoryMutableFile(this, ".history", new byte[0]).readOnly());
        // scan the app and load the paths
        this.scan();
        // log the discovered VFS tree
        logger.info("Balsa SSH VFS Tree:");
        this.logVFSTree(this.root);
    }
    
    protected void logVFSTree(BalsaVFSDirectory dir)
    {
        for (BalsaVFSEntry child : dir.getChildEntries().values())
        {
            logger.info(" - {}", child);
            if (child instanceof BalsaVFSDirectory)
                logVFSTree((BalsaVFSDirectory) child);
        }
    }
    
    protected void scan()
    {
        logger.info("Scanning routes for SSH File handlers");
        for (RouteEntry route : ((RouteEngineImpl) this.application.getRoutingEngine()).getRoutes())
        {
            SSHVFS anno = route.getRoute().getHandler().getAnnotation(SSHVFS.class);
            if (anno != null)
            {
                Type type = this.getType(anno, route);
                ValueExpression expression = this.getExpression(anno);
                List<String> path = VFSUtil.canonicalisePath(VFSUtil.buildPath(route.getPrefix().getPrefix(), route.getRoute().getPattern()));
                String name = path.get(path.size() -1);
                logger.debug("Mounting {}::{}({}) to {}", path, type, name, route);
                BalsaVFSEntry entry = this.createVFSEntry(type, name, route, expression);
                this.addVFSRoute(path, entry);
            }
        }
    }
    
    protected Type getType(SSHVFS anno, RouteEntry route)
    {
        if (anno.type() == Type.AUTO)
            return route.getRoute().getPattern().endsWith("/") ? Type.DIRECTORY : Type.FILE;
        return anno.type();
    }
    
    protected ValueExpression getExpression(SSHVFS anno)
    {
        if (! Util.isEmpty(anno.expression()))
        {
            return new ValueExpression(ExpressContext.builder().withExtensions(this.application.expressExtensions()).build(), anno.expression());
        }
        return null;
    }
    
    protected BalsaVFSEntry createVFSEntry(Type type, String name, RouteEntry route, ValueExpression expression)
    {
        switch (type)
        {
            case DIRECTORY: return new BalsaVFSDynamicDirectory(this, name, route, expression);
            case FILE:      return new BalsaVFSDynamicFile(this, name, route);
            case AUTO:
            default:        throw new BalsaException("Could not create VFS Entry for " + type);
        }
    }
    
    protected void addVFSRoute(List<String> path, BalsaVFSEntry leaf)
    {
        // traverse through the directory tree
        BalsaVFSEntry parent = null;
        BalsaVFSEntry entry = this.root;
        for (int i = 0; i < (path.size() - 1); i++)
        {
            String part = path.get(i);
            logger.trace("Adding route {}:{} to {}", path, part, entry);
            if (entry instanceof BalsaVFSDirectory)
            {
                parent = entry;
                entry = this.findChild(((BalsaVFSDirectory) entry), part);
                // do we need to insert a entry
                if (entry == null)
                {
                    entry = this.addVFSChild((BalsaVFSDirectory) parent, new BalsaVFSStaticDirectory(this, part));
                    logger.trace("Creating VFS entry {} parent {}", entry, parent);
                }
            }
            else
            {
                throw new BalsaException("Cannot add VFS entry to " + entry + ", it is not a directory");
            }
        }
        // now add the leaf
        logger.trace("Adding route leaf entry {} to {}", leaf, entry);
        if (entry instanceof BalsaVFSDirectory)
        {
             this.addVFSChild((BalsaVFSDirectory) entry, leaf);
        }
        else
        {
            throw new BalsaException("Cannot add VFS entry to " + entry + ", it is not a directory");
        }
    }
    
    protected BalsaVFSEntry addVFSChild(BalsaVFSDirectory parent, BalsaVFSEntry newChild)
    {
        if (parent.hasChild(newChild.getName()))
        {
            BalsaVFSEntry existing = parent.getChildEntries().get(newChild.getName());
            // can only merge directories
            if (existing instanceof BalsaVFSDirectory && newChild instanceof BalsaVFSDirectory)
            {
                BalsaVFSDirectory winner = (BalsaVFSDirectory) ((existing instanceof BalsaVFSStaticDirectory) ? newChild : existing);
                BalsaVFSDirectory looser = (BalsaVFSDirectory) ((existing instanceof BalsaVFSStaticDirectory) ? existing : newChild);
                // copy over the lost children
                for (BalsaVFSEntry lostChild : looser.getChildEntries().values())
                {
                    this.addVFSChild(winner, lostChild);
                }
                // add the merged node
                return parent.replaceChild(winner);
            }
            else if (existing instanceof BalsaVFSDynamicFile && newChild instanceof BalsaVFSDynamicFile)
            {
                // merge the routes
                logger.debug("Merging routes from {} into {} ", newChild, existing);
                for (RouteEntry route : ((BalsaVFSDynamicFile) newChild).getRoutes().values())
                {
                    ((BalsaVFSDynamicFile) existing).addRoute(route);
                }
                return existing;
            }
            else
            {
                throw new BalsaException("Cannot merge conflicting VFS entries: " + existing + " with " + newChild);
            }
        }
        return parent.addChild(newChild);
    }

    public BalsaApplication getBalsaApplication()
    {
        return this.application;
    }
    
    public BalsaVFSRoot getRoot()
    {
        return this.root;
    }

    @Override
    public BalsaVFSEntry getFile(String path) throws PermissionDeniedException, IOException
    {
        logger.debug("Finding file {}", path);
        // get the canonical path vector
        List<String> parts = VFSUtil.canonicalisePath(path);
        // search for the file
        BalsaVFSEntry parent = null;
        BalsaVFSEntry entry = this.root;
        for (Iterator<String> i = parts.iterator(); i.hasNext();)
        {
            String part = i.next();
            // get child
            logger.debug("Get part: {} in {}", part, entry);
            if (entry instanceof BalsaVFSDirectory)
            {
                parent = entry;
                entry = this.findChild(((BalsaVFSDirectory) entry), part);
                if (entry == null) break;
                // remove the path we've successfully found
                i.remove();
            }
            else
            {
                throw new FileNotFoundException("Invalid path, " + entry.getAbsolutePath() + " is not a directory");
            }
        }
        // did we find the entry?
        if (entry != null)
        {
            logger.debug("Found file {} -> {}", path, entry);
            return entry;
        }
        // file does not exist, create an empty one
        String noneName = parts.stream().collect(Collectors.joining("/"));
        logger.debug("Creating none file {} with parent {}", noneName, parent);
        return new BalsaVFSNoneEntry(this, ((BalsaVFSDirectory) parent), noneName);
    }
    
    protected BalsaVFSEntry findChild(BalsaVFSDirectory parent, String name)
    {
        // look for direct match
        BalsaVFSEntry entry = parent.getChildEntries().get(name);
        if (entry != null) return entry;
        // pattern matches
        for (BalsaVFSEntry child : parent.getChildEntries().values())
        {
            if (child.isGlob() && child.matchesGlob(name))
            {
                logger.info("Matched {} with patterned entry {}", name, child);
                return child;
            }
        }
        return null;
    }

    @Override
    public Event populateEvent(Event evt)
    {
        return evt;
    }

    @Override
    public BalsaVFSEntry getDefaultPath() throws PermissionDeniedException, IOException
    {
        return this.getFile("/");
    }
}
