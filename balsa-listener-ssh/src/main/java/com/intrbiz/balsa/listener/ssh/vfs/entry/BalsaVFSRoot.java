package com.intrbiz.balsa.listener.ssh.vfs.entry;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;

public class BalsaVFSRoot extends BalsaVFSStaticDirectory
{
    public BalsaVFSRoot(BalsaVFS vfs)
    {
        super(vfs, "");
    }
    
    @Override
    public boolean isRoot()
    {
        return true;
    }
}
