package com.intrbiz.balsa.listener.ssh.vfs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.engine.session.BalsaSession;
import com.intrbiz.balsa.listener.ssh.util.SSHUtil;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSDynamicDirectory;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSDynamicFile;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSEntry;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSInMemoryMutableFile;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSNoneEntry;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSStaticDirectory;
import com.intrbiz.balsa.listener.ssh.vfs.entry.overlay.BalsaVFSOverlayDynamicDirectory;
import com.intrbiz.balsa.listener.ssh.vfs.entry.overlay.BalsaVFSOverlayDynamicFile;
import com.intrbiz.balsa.listener.ssh.vfs.entry.overlay.BalsaVFSOverlayEntry;
import com.intrbiz.balsa.listener.ssh.vfs.entry.overlay.BalsaVFSOverlayInMemoryMutableFile;
import com.intrbiz.balsa.listener.ssh.vfs.entry.overlay.BalsaVFSOverlayNoneEntry;
import com.intrbiz.balsa.listener.ssh.vfs.entry.overlay.BalsaVFSOverlayStaticDirectory;
import com.sshtools.common.events.Event;
import com.sshtools.common.files.AbstractFileFactory;
import com.sshtools.common.permissions.PermissionDeniedException;
import com.sshtools.common.ssh.SshConnection;

public class BalsaVFSSession implements AbstractFileFactory<BalsaVFSOverlayEntry<?>>
{
    public static final Logger logger = LoggerFactory.getLogger(BalsaVFSSession.class);
    
    // the underlay VFS
    private final BalsaVFS vfs;
    
    private final BalsaApplication application;
    
    private final SshConnection con;
    
    private final BalsaSession session;
    
    private final Map<String, BalsaVFSOverlayEntry<?>> overlays = new HashMap<>();

    public BalsaVFSSession(BalsaVFS vfs, BalsaApplication application, SshConnection con)
    {
        super();
        this.vfs = Objects.requireNonNull(vfs);
        this.application = Objects.requireNonNull(application);
        this.con = Objects.requireNonNull(con);
        this.session = Objects.requireNonNull(application.getSessionEngine().getSession(SSHUtil.sessionId(this.con)));
    }
    
    public BalsaApplication getBalsaApplication()
    {
        return this.application;
    }
    
    public BalsaSession getSession()
    {
        return this.session;
    }
    
    public SshConnection getConnection()
    {
        return this.con;
    }
    
    @Override
    public BalsaVFSOverlayEntry<?> getFile(String path) throws PermissionDeniedException, IOException
    {
        // canonicalise the path
        List<String> pathVector = VFSUtil.canonicalisePath(path);
        String absPath = VFSUtil.buildPath(pathVector);
        // check for an overlay override
        BalsaVFSOverlayEntry<?> overlay = this.overlays.get(absPath);
        if (overlay == null)
        {
            // get the underlay file
            BalsaVFSEntry underlay = this.vfs.getFile(path);
            // create the overlay
            overlay = this.createOverlay(underlay, pathVector);
            // cache?
            this.overlays.put(absPath, overlay);
            logger.info("Created overlay file {} ({}) to {}", path, absPath, overlay);
        }
        logger.info("Got overlay file {} ({}) to {}", path, absPath, overlay);
        return overlay;
    }
    
    protected BalsaVFSOverlayEntry<?> createOverlay(BalsaVFSEntry underlay, List<String> path) throws IOException
    {
        logger.debug("Creating overlay for {} at {}", underlay, path);
        // mutable files
        if (underlay instanceof BalsaVFSStaticDirectory)
            return new BalsaVFSOverlayStaticDirectory(this, (BalsaVFSStaticDirectory) underlay, path);
        if (underlay instanceof BalsaVFSDynamicDirectory)
            return new BalsaVFSOverlayDynamicDirectory(this, (BalsaVFSDynamicDirectory) underlay, path);
        if (underlay instanceof BalsaVFSDynamicFile)
            return new BalsaVFSOverlayDynamicFile(this, (BalsaVFSDynamicFile) underlay, path);
        if (underlay instanceof BalsaVFSInMemoryMutableFile)
            return new BalsaVFSOverlayInMemoryMutableFile(this, (BalsaVFSInMemoryMutableFile) underlay, path);
        if (underlay instanceof BalsaVFSNoneEntry)
            return new BalsaVFSOverlayNoneEntry(this, (BalsaVFSNoneEntry) underlay, path);
        // unhandled underlay type
        throw new FileNotFoundException("Cannot create overlay for " + underlay.getClass().getSimpleName());
    }

    @Override
    public Event populateEvent(Event evt)
    {
        return evt;
    }

    @Override
    public BalsaVFSOverlayEntry<?> getDefaultPath() throws PermissionDeniedException, IOException
    {
        return this.getFile("/");
    }
}
