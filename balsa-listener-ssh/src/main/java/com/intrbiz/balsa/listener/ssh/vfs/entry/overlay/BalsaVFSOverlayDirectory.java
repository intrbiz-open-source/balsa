package com.intrbiz.balsa.listener.ssh.vfs.entry.overlay;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFSSession;
import com.intrbiz.balsa.listener.ssh.vfs.VFSUtil;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSDirectory;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileRandomAccess;
import com.sshtools.common.permissions.PermissionDeniedException;

public abstract class BalsaVFSOverlayDirectory<U extends BalsaVFSDirectory> extends BalsaVFSOverlayEntry<U>
{
    
    public BalsaVFSOverlayDirectory(BalsaVFSSession vfs, U underlay, List<String> path)
    {
        super(vfs, underlay, path);
    }

    @Override
    public boolean isDirectory()
    {
        return this.underlay.isDirectory();
    }
    
    @Override
    public boolean isFile()
    {
        return this.underlay.isFile();
    }
    
    @Override
    public List<AbstractFile> getChildren() throws PermissionDeniedException, IOException
    {
        List<AbstractFile> children = new ArrayList<>();
        for (AbstractFile underlayChild : this.underlay.getChildren())
        {
            children.add(this.resolveFile(underlayChild.getName()));
        }
        return children;
    }

    @Override
    public boolean createFolder() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public boolean delete(boolean recursive) throws IOException, PermissionDeniedException
    {
        return false;
    }

    @Override
    public final AbstractFile resolveFile(String child) throws IOException, PermissionDeniedException
    {
        return this.getVFS().getFile(
                child.startsWith("/") ? 
                    child : 
                    VFSUtil.stripTrailingSlash(this.getAbsolutePath() + "/" + child)
        );
    }

    @Override
    public InputStream getInputStream() throws IOException, PermissionDeniedException
    {
        throw new IOException("Cannot write a directory");
    }

    @Override
    public OutputStream getOutputStream() throws IOException, PermissionDeniedException
    {
        return this.getOutputStream(false);
    }

    @Override
    public void copyFrom(AbstractFile src) throws IOException, PermissionDeniedException
    {
        throw new IOException("Cannot copy into a directory");
    }
    
    @Override
    public void moveTo(AbstractFile target) throws IOException, PermissionDeniedException
    {
        throw new IOException("Cannot move a directory");
    }

    @Override
    public long length() throws IOException, PermissionDeniedException
    {
        return 0;
    }

    @Override
    public boolean createNewFile() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public void truncate() throws PermissionDeniedException, IOException
    {
        throw new IOException("Cannot truncate a directory");
    }

    @Override
    public boolean supportsRandomAccess()
    {
        return false;
    }

    @Override
    public AbstractFileRandomAccess openFile(boolean writeAccess) throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public OutputStream getOutputStream(boolean append) throws IOException, PermissionDeniedException
    {
        throw new IOException("Cannot read a directory");
    }
}
