package com.intrbiz.balsa.listener.ssh.vfs.entry.overlay;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.engine.impl.route.RouteEntry;
import com.intrbiz.balsa.listener.ssh.context.BalsaSSHRequest;
import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFSSession;
import com.intrbiz.balsa.listener.ssh.vfs.VFSUtil;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSDynamicDirectory;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSEntry;
import com.intrbiz.express.DefaultContext;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressEntityResolver;
import com.intrbiz.express.value.ValueExpression;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.permissions.PermissionDeniedException;

public class BalsaVFSOverlayDynamicDirectory extends BalsaVFSOverlayDirectory<BalsaVFSDynamicDirectory>
{
    public static final Logger logger = LoggerFactory.getLogger(BalsaVFSOverlayDynamicDirectory.class);

    public BalsaVFSOverlayDynamicDirectory(BalsaVFSSession vfs, BalsaVFSDynamicDirectory underlay, List<String> path)
    {
        super(vfs, underlay, path);
    }

    @Override
    public boolean isDynamic()
    {
        return this.underlay.isDynamic();
    }

    @Override
    public List<AbstractFile> getChildren() throws PermissionDeniedException, IOException
    {
        List<AbstractFile> children = new ArrayList<>();
        // add any non-globs and compile the expressiosn to expand the directory list with
        List<BalsaVFSEntry> globs = new ArrayList<>();
        for (BalsaVFSEntry underlayChild : this.underlay.getChildEntries().values())
        {
            if (underlayChild.isGlob())
                globs.add(underlayChild);
            else
                children.add(this.resolveFile(underlayChild.getName()));
        }
        // execute and get the list of actual child nodes
        for (String childName : this.executeList())
        {
            if (! globs.isEmpty())
            {
                // expand the child names for each glob
                for (BalsaVFSEntry glob : globs)
                {
                    children.add(this.resolveFile(glob.buildGlobName(childName)));
                }
            }
            else
            {
                children.add(this.resolveFile(childName));
            }
        }
        return children;
    }
    
    protected void extractChildren(Iterable<?> values, List<String> children)
    {
        ValueExpression expression = this.underlay.getExpression();
        if (expression == null)
        {
            for (Object value : values)
            {
                children.add(String.valueOf(value));
            }
        }
        else
        {
            ExpressContext context = ExpressContext.builder().withResolver(ExpressEntityResolver.SOURCE).build();
            for (Object value : values)
            {
                children.add(String.valueOf(expression.get(context, value)));
            }
        }
    }
    
    protected void extractChildren(Stream<?> values, List<String> children)
    {
        ValueExpression expression = this.underlay.getExpression();
        if (expression == null)
        {
            for (Iterator<?> i = values.iterator(); i.hasNext();)
            {
                Object value = i.next();
                children.add(String.valueOf(value));
            }
        }
        else
        {
            ExpressContext context = ExpressContext.builder().withResolver(ExpressEntityResolver.SOURCE).build();
            for (Iterator<?> i = values.iterator(); i.hasNext();)
            {
                Object value = i.next();
                children.add(String.valueOf(expression.get(context, value)));
            }
        }
    }
    
    protected List<String> executeList() throws IOException
    {
        List<String> children = new ArrayList<>();
        logger.info("Executing list of dynamic directory {} overlayed at {}", this.underlay, this.path);
        // find the route
        final RouteEntry route = this.underlay.getRoute();
        // check we have a read handler
        if (route == null) throw new IOException("Cannot list virtual directory, no such route");
        // create the context
        final BalsaContext context = new BalsaContext(this.getVFS().getBalsaApplication(), new BalsaSSHRequest(route.getRoute().getMethod().toString(), VFSUtil.ensureTrailingSlash(this.getAbsolutePath())), null);
        context.setSession(this.getVFS().getSession());
        // bind the context and execute
        try
        {
            context.activate();
            context.bind();
            // invoke the route, calling match first to extract any path parameters
            if (! route.match(true, context, context.request()))
                throw new FileNotFoundException("Given path did not match the dynamic directory route");
            // execute the route
            Object result = route.execute(context);
            // process the result and extract the file name
            if (result instanceof Iterable)
            {
                this.extractChildren((Iterable<?>) result, children);
            }
            else if (result instanceof Stream)
            {
                this.extractChildren((Stream<?>) result, children);
            }
        }
        catch (Throwable e)
        {
            throw new IOException("Error reading dynamic directory", e);
        }
        finally
        {
            context.unbind();
            context.deactivate();
        }
        // return the listing
        return children;
    }
}
