package com.intrbiz.balsa.listener.ssh.command.shell;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.engine.impl.route.RouteEntry;
import com.intrbiz.balsa.listener.ssh.context.BalsaSSHRequest;
import com.intrbiz.balsa.listener.ssh.context.BalsaSSHShellCommandResponse;
import com.intrbiz.balsa.listener.ssh.util.SSHUtil;
import com.intrbiz.balsa.scgi.parameter.Parameter;
import com.intrbiz.balsa.scgi.parameter.StringParameter;
import com.sshtools.common.permissions.PermissionDeniedException;
import com.sshtools.server.vsession.ShellUtilities;
import com.sshtools.server.vsession.UsageException;
import com.sshtools.server.vsession.VirtualConsole;

public class RouteBalsaShellCommand extends BalsaShellCommand
{
    private static final Logger logger = LoggerFactory.getLogger(RouteBalsaShellCommand.class);
    
    private final BalsaApplication application;
    
    private final RouteEntry route;
    
    private final String[] argNames;

    public RouteBalsaShellCommand(String[] command, String subsystem, String signature, String description, BalsaApplication application, RouteEntry route, String[] argNames)
    {
        super(command, subsystem, signature, description);
        this.application = Objects.requireNonNull(application);
        this.route = Objects.requireNonNull(route);
        this.argNames = Objects.requireNonNull(argNames);
    }
    
    public RouteEntry getRoute()
    {
        return this.route;
    }

    @Override
    public void run(String[] args, VirtualConsole console) throws IOException, PermissionDeniedException, UsageException
    {
        if (args.length == this.argNames.length + 1)
        {
            // create the context
            final BalsaContext context = new BalsaContext(this.application, new BalsaSSHRequest(this.route.getRoute().getMethod().toString(), this.getCommandName(), this.buildParameters(args)), new BalsaSSHShellCommandResponse(console.getTerminal()));
            context.setSession(this.application.getSessionEngine().getSession(SSHUtil.sessionId(console.getConnection())));
            // bind the context and execute
            try
            {
                context.activate();
                context.bind();
                // invoke the route
                try
                {
                    this.route.execute(context);
                }
                finally
                {
                    // ensure we flush the response
                    context.response().flush();
                }
                // terminating new line
                console.println();
            }
            catch (Throwable e)
            {
                console.println("Error executing command: " + e.getMessage());
                logger.error("Failed to execute Balsa SSH Shell Command " + this.getCommandName(), e);
            }
            finally
            {
                context.unbind();
                context.deactivate();
            }
        }
        else
        {
            this.usage(console);
        }
    }
    
    private List<Parameter> buildParameters(String[] args)
    {
        List<Parameter> params = new ArrayList<>(args.length);
        for (int i = 0; i < this.argNames.length; i++)
        {
            params.add(new StringParameter(this.argNames[i], i, args[i + 1]));
        }
        return params;
    }
    
    private void usage(VirtualConsole console)
    {
        console.println(ShellUtilities.padString(this.getUsage(), 80) + this.getDescription());
    }
}
