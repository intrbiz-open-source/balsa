package com.intrbiz.balsa.listener.ssh;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.listener.BalsaListener;
import com.intrbiz.balsa.listener.ssh.command.shell.factory.BalsaShellCommandFactory;
import com.intrbiz.balsa.listener.ssh.log.SSHLog;
import com.intrbiz.balsa.listener.ssh.security.BalsaPasswordAuthenticator;
import com.intrbiz.balsa.listener.ssh.security.BalsaPubKeyAuthenticator;
import com.intrbiz.balsa.listener.ssh.shell.BSH;
import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;
import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFSSession;
import com.sshtools.common.command.ExecutableCommand;
import com.sshtools.common.logger.Log;
import com.sshtools.common.permissions.PermissionDeniedException;
import com.sshtools.common.ssh.SessionChannel;
import com.sshtools.common.ssh.SshConnection;
import com.sshtools.common.ssh.SshException;
import com.sshtools.common.ssh.UnsupportedChannelException;
import com.sshtools.server.DefaultServerChannelFactory;
import com.sshtools.server.SshServer;
import com.sshtools.server.SshServerContext;
import com.sshtools.server.vsession.CommandFactory;
import com.sshtools.server.vsession.ShellCommand;
import com.sshtools.server.vsession.ShellCommandFactory;
import com.sshtools.server.vsession.VirtualSessionPolicy;
import com.sshtools.server.vsession.VirtualShellNG;
import com.sshtools.synergy.ssh.ChannelNG;

public class BalsaSSHListener extends BalsaListener
{
    // virtual file system
    private BalsaVFS vfs;
    
    private boolean allowPasswordAuthentication = false;
    
    private boolean allowForwarding = false;
    
    private String shellWelcomeText = "Balsa SSH Shell ${version}";
    
    private List<CommandFactory<ShellCommand>> shellCommands = new ArrayList<>();
    
    // runtime state
    private SshServer sshServer;
    
    public BalsaSSHListener()
    {
        super(2022, 0);
    }
    
    public BalsaSSHListener(BalsaVFS vfs)
    {
        this();
        this.vfs = vfs;
    }

    @Override
    public String getEngineName()
    {
        return "Balsa-SSH-Listener";
    }

    @Override
    public String getListenerType()
    {
        return "ssh";
    }
        
    public boolean isAllowPasswordAuthentication()
    {
        return this.allowPasswordAuthentication;
    }

    public BalsaSSHListener setAllowPasswordAuthentication(boolean allowPasswordAuthentication)
    {
        this.allowPasswordAuthentication = allowPasswordAuthentication;
        return this;
    }

    public String getShellWelcomeText()
    {
        return this.shellWelcomeText;
    }

    public BalsaSSHListener setShellWelcomeText(String shellWelcomeText)
    {
        this.shellWelcomeText = shellWelcomeText;
        return this;
    }
    
    public boolean isAllowForwarding()
    {
        return this.allowForwarding;
    }

    public BalsaSSHListener setAllowForwarding(boolean allowForwarding)
    {
        this.allowForwarding = allowForwarding;
        return this;
    }

    public List<CommandFactory<ShellCommand>> getShellCommands()
    {
        return this.shellCommands;
    }
    
    public BalsaSSHListener clearShellCommands()
    {
        this.shellCommands.clear();
        return this;
    }
    
    public BalsaSSHListener addShellCommands(CommandFactory<ShellCommand> shellCommands)
    {
        this.shellCommands.add(shellCommands);
        return this;
    }

    public BalsaSSHListener setShellCommands(List<CommandFactory<ShellCommand>> shellCommands)
    {
        this.shellCommands = shellCommands;
        return this;
    }

    public BalsaVFS getVfs()
    {
        return this.vfs;
    }

    public BalsaSSHListener setVfs(BalsaVFS vfs)
    {
        this.vfs = vfs;
        return this;
    }

    protected void setupVFS() throws BalsaException
    {
        if (this.vfs == null)
        {
            this.vfs = new BalsaVFS(this.application);
        }
    }
    
    protected void setupCommands() throws BalsaException
    {
        // load all auto discovered commands
        this.shellCommands.add(new BalsaShellCommandFactory(this.application));
    }

    protected SshServer buildSSHServer() throws Exception
    {
        // bridge logging to SLF4J
        Log.setDefaultContext(new SSHLog());
        Log.setupCurrentContext(Log.getDefaultContext());
        // setup the server
        SshServer server = new SshServer(this.getPort()) {
            @Override
            protected void configure(SshServerContext sshContext, SocketChannel sc) throws IOException, SshException
            {
                super.configure(sshContext, sc);
                // inject some custom context configuration
                sshContext.setPolicy(VirtualSessionPolicy.class, new VirtualSessionPolicy(BalsaSSHListener.this.getShellWelcomeText()));
            }
        };
        // setup the SSH server
        server.enableSCP();
        // disable port forwarding
        if (! this.isAllowForwarding())
        {
            server.getForwardingPolicy().denyForwarding().denyGatewayForwarding();
        }
        // allow password auth?
        if (this.isAllowPasswordAuthentication())
        {
            server.addAuthenticator(new BalsaPasswordAuthenticator(this.application.getSecurityEngine(), this.application.getSessionEngine()));
        }
        // public key auth
        server.addAuthenticator(new BalsaPubKeyAuthenticator(this.application.getSecurityEngine(), this.application.getSessionEngine()));
        // Overlay VFS factory per connection
        server.setFileFactory((con) -> new BalsaVFSSession(this.getVfs(), this.application, con));
        // channel factory        
        server.setChannelFactory(new BalsaChannelFactory(this.getShellCommands()));
        //
        return server;
    }

    @Override
    public void start() throws BalsaException
    {
        // setup the VFS
        this.setupVFS();
        // setup commands
        this.setupCommands();
        // setup and start the listener
        try
        {
            // build the SSH server
            this.sshServer = this.buildSSHServer();
            // start the SSH listener
            this.sshServer.start();
        }
        catch (Exception e)
        {
            throw new BalsaException("Failed to start SSH listener", e);
        }
    }

    @Override
    public void shutdown()
    {
        this.sshServer.close();
    }

    @Override
    public void stop()
    {
    }
    
    private static class BalsaChannelFactory extends DefaultServerChannelFactory
    {
        private static final Logger logger = LoggerFactory.getLogger(BalsaChannelFactory.class);
        
        private ShellCommandFactory shellFactory;
        
        public BalsaChannelFactory(List<CommandFactory<ShellCommand>> additionalCommands)
        {
            // create the shell factory
            this.shellFactory = BSH.factory(additionalCommands);
            // log loaded commands
            logger.info("Balsa SSH inited with shell commands: " + this.shellFactory.getSupportedCommands());
            logger.info("Balsa SSH inited with commands: " + this.commands.names());
        }
         
        @Override
        protected ChannelNG<SshServerContext> createSessionChannel(SshConnection con) throws UnsupportedChannelException, PermissionDeniedException
        {
            logger.debug("Creating shell session channel");
            return new VirtualShellNG(con, this.shellFactory);
        }

        @Override
        public ExecutableCommand executeCommand(SessionChannel sessionChannel, String[] args, Map<String, String> environment) throws PermissionDeniedException, UnsupportedChannelException
        {
            logger.debug("Executing command: " + Arrays.asList(args));
            return super.executeCommand(sessionChannel, args, environment);
        }
    }
}
