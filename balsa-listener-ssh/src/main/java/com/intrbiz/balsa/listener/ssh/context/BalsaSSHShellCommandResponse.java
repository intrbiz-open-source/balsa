package com.intrbiz.balsa.listener.ssh.context;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.jline.terminal.Terminal;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.intrbiz.balsa.error.BalsaInternalError;
import com.intrbiz.balsa.listener.BalsaResponse;
import com.intrbiz.balsa.scgi.http.HTTP.HTTPStatus;
import com.intrbiz.balsa.scgi.util.BalsaWriter;
import com.intrbiz.balsa.scgi.util.CookieBuilder;
import com.intrbiz.balsa.scgi.util.HTMLWriter;

public class BalsaSSHShellCommandResponse implements BalsaResponse
{
    private Charset charset = StandardCharsets.UTF_8;

    private final Terminal terminal;

    private OutputStream out = null;
    
    private Writer writer = null;

    private HTMLWriter htmlWriter = null;

    private JsonGenerator jsonGenerator = null;

    private YAMLGenerator yamlGenerator = null;

    private XMLStreamWriter xmlWriter = null;

    public BalsaSSHShellCommandResponse(Terminal terminal)
    {
        super();
        this.terminal = terminal;
    }

    @Override
    public void abortOnError(Throwable t) throws BalsaInternalError
    {
    }

    @Override
    public HTTPStatus getStatus()
    {
        return HTTPStatus.OK;
    }

    @Override
    public BalsaResponse status(HTTPStatus status)
    {
        return this;
    }

    @Override
    public BalsaResponse ok()
    {
        return this;
    }

    @Override
    public BalsaResponse notFound()
    {
        return this;
    }

    @Override
    public BalsaResponse error()
    {
        return this;
    }

    @Override
    public BalsaResponse redirect(boolean permanent)
    {
        return this;
    }

    @Override
    public Charset getCharset()
    {
        return this.charset;
    }

    @Override
    public BalsaResponse charset(Charset charset)
    {
        this.charset = charset;
        return this;
    }

    @Override
    public String getContentType()
    {
        return null;
    }

    @Override
    public BalsaResponse contentType(String contentType)
    {
        return this;
    }

    @Override
    public BalsaResponse plain()
    {
        return this;
    }

    @Override
    public BalsaResponse html()
    {
        return this;
    }

    @Override
    public BalsaResponse javascript()
    {
        return this;
    }

    @Override
    public BalsaResponse json()
    {
        return this;
    }

    @Override
    public BalsaResponse yaml()
    {
        return this;
    }

    @Override
    public BalsaResponse xml()
    {
        return this;
    }

    @Override
    public BalsaResponse css()
    {
        return this;
    }

    @Override
    public String getCacheControl()
    {
        return null;
    }

    @Override
    public BalsaResponse cacheControl(String value)
    {
        return this;
    }

    @Override
    public String getExpires()
    {
        return null;
    }

    @Override
    public BalsaResponse expires(String value)
    {
        return this;
    }

    @Override
    public BalsaResponse expires(Date value)
    {
        return this;
    }

    @Override
    public BalsaResponse header(String name, String value)
    {
        return this;
    }

    @Override
    public BalsaResponse header(String name, Date value)
    {
        return this;
    }

    @Override
    public CookieBuilder<BalsaResponse> setCookie()
    {
        return new CookieBuilder<>() {
            public BalsaResponse set() {
                return BalsaSSHShellCommandResponse.this;
            }
        };
    }

    @Override
    public CookieBuilder<BalsaResponse> cookie()
    {
        return this.setCookie();
    }

    @Override
    public BalsaResponse redirect(String location, boolean permanent) throws IOException
    {
        return this;
    }

    @Override
    public List<String> getHeaders()
    {
        return Collections.emptyList();
    }

    @Override
    public BalsaResponse sendHeaders() throws IOException
    {
        return this;
    }

    @Override
    public BalsaResponse sendFile(String file) throws IOException
    {
        throw new IOException("Not supported");
    }

    @Override
    public OutputStream getOutput() throws IOException
    {
        if (this.out == null) this.out = this.terminal.output();
        return this.out;
    }

    @Override
    public Writer getWriter() throws IOException
    {
        if (this.writer == null) this.writer = new NewLineTransformer(this.terminal.writer());
        return writer;
    }

    @Override
    public BalsaResponse write(String content) throws IOException
    {
        this.terminal.writer().write(content);
        return this;
    }

    @Override
    public JsonGenerator getJsonWriter() throws IOException
    {
        if (this.jsonGenerator == null)
        {
            this.jsonGenerator = (new JsonFactory()).createGenerator(this.getWriter());
            this.jsonGenerator.setPrettyPrinter(new DefaultPrettyPrinter());
        }
        return this.jsonGenerator;
    }

    @Override
    public YAMLGenerator getYamlWriter() throws IOException
    {
        if (this.yamlGenerator == null)
        {
            this.yamlGenerator = (
                new YAMLFactory()
                    .disable(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID)
                    .enable(YAMLGenerator.Feature.MINIMIZE_QUOTES)
            ).createGenerator(this.getWriter());
            this.yamlGenerator.setPrettyPrinter(new DefaultPrettyPrinter());
        }
        return this.yamlGenerator;
    }

    @Override
    public XMLStreamWriter getXMLWriter() throws IOException, XMLStreamException
    {
        if (this.xmlWriter == null)
        {
            this.xmlWriter = XMLOutputFactory.newFactory().createXMLStreamWriter(this.getWriter());
        }
        return this.xmlWriter;
    }

    @Override
    public HTMLWriter getHtmlWriter() throws IOException
    {
        if (this.htmlWriter == null) this.htmlWriter = new HTMLWriter(this.getWriter());
        return this.htmlWriter;
    }

    @Override
    public BalsaWriter getViewWriter() throws IOException
    {
        return this.getHtmlWriter();
    }

    @Override
    public BalsaResponse flush() throws IOException
    {
        // flush any streams we have opened first
        if (this.yamlGenerator != null)
        {
            this.yamlGenerator.flush();
        }
        if (this.jsonGenerator != null)
        {
            this.jsonGenerator.flush();
        }
        if (this.xmlWriter != null)
        {
            try
            {
                this.xmlWriter.flush();
            }
            catch (XMLStreamException e)
            {
                throw new IOException("Failed to flush XML Stream", e);
            }
        }
        if (this.htmlWriter != null)
        {
            this.htmlWriter.flush();
        }
        else if (this.writer != null)
        {
            this.writer.flush();
        }
        return this;
    }

    @Override
    public boolean isHeadersSent()
    {
        return false;
    }
    
    /**
     * Replace `\n` with `\r\n` in the stream.
     */
    private static class NewLineTransformer extends Writer
    {
        //private static final char[] LN = { '\r', '\n'};
        
        private final Writer out;
        
        private char streamLast = 0;
        
        public NewLineTransformer(Writer out)
        {
            super();
            this.out = out;
        }

        @Override
        public synchronized void write(char[] cbuf, int off, int len) throws IOException
        {
            char last = this.streamLast;
            int end = off + len;
            int put = off;
            for (int i = off; i < end; i++)
            {
                char current = cbuf[i];
                if (current == '\n' && last != '\r')
                {
                    // write the last chunk
                    this.out.write(cbuf, put, i - put);
                    put = i;
                    // insert the \r
                    this.out.write('\r');
                }
                last = current;
            }
            if (put < end)
            {
                this.out.write(cbuf, put, end - put);
            }
            this.streamLast = last;
        }

        @Override
        public void flush() throws IOException
        {
            this.out.flush();
        }

        @Override
        public void close() throws IOException
        {
            this.out.close();
        }
    }
}
