package com.intrbiz.balsa.listener.ssh.command.shell.factory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaApplication;
import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.engine.impl.route.RouteEngineImpl;
import com.intrbiz.balsa.engine.impl.route.RouteEntry;
import com.intrbiz.balsa.engine.impl.route.exec.argument.ParameterArgument;
import com.intrbiz.balsa.listener.ssh.command.shell.BalsaShellCommand;
import com.intrbiz.balsa.listener.ssh.command.shell.RouteBalsaShellCommand;
import com.intrbiz.balsa.listener.ssh.command.shell.WrapperBalsaShellCommand;
import com.intrbiz.balsa.metadata.SSHCommand;
import com.intrbiz.util.Util;
import com.sshtools.common.permissions.PermissionDeniedException;
import com.sshtools.common.ssh.SshConnection;
import com.sshtools.server.vsession.CommandConfigurator;
import com.sshtools.server.vsession.CommandFactory;
import com.sshtools.server.vsession.ShellCommand;
import com.sshtools.server.vsession.UnsupportedCommandException;

public class BalsaShellCommandFactory extends CommandFactory<ShellCommand>
{
    private static final Logger logger = LoggerFactory.getLogger(BalsaShellCommandFactory.class);
    
    private final Map<String, BalsaShellCommand> commands = new HashMap<>();
    
    private final BalsaApplication application;
    
    public BalsaShellCommandFactory(BalsaApplication application)
    {
        super();
        this.application = Objects.requireNonNull(application);
        // scan the application and load any commands we find
        this.scan();
        // dump the found commands
    }
    
    protected void logCommands()
    {
        logger.info("Balsa SSH Commands:");
        for (BalsaShellCommand command : this.commands.values())
        {
            logger.info("  - {} -> {}", command.getCommandName(), command);
        }
    }
    
    protected void scan()
    {
        logger.info("Scanning routes for SSH Command handlers");
        for (RouteEntry route : ((RouteEngineImpl) this.application.getRoutingEngine()).getRoutes())
        {
            SSHCommand routerCmd = route.getRoute().getRouter().getClass().getAnnotation(SSHCommand.class);
            SSHCommand routeCmd  = route.getRoute().getHandler().getAnnotation(SSHCommand.class);
            if (routeCmd != null)
            {
                // get the command name
                String[] command =  buildCommand(routerCmd == null ? new String[0] : routerCmd.command(), routeCmd.command());
                // subsystem
                String subSystem = Util.coalesceEmpty(routeCmd.subSystem(), Optional.ofNullable(routerCmd).map(SSHCommand::subSystem).orElse(null));
                if (Util.isEmpty(subSystem)) throw new BalsaException("Command subSystem not specified for: " + route);
                // get the arguments
                String[] argNames = argumentNames(route);
                // help text
                String usage = usage(command, argNames);
                String commandDescription = Util.coalesceEmpty(routeCmd.commandDescription(), Optional.ofNullable(routerCmd).map(SSHCommand::commandDescription).orElse(null));
                String description = Util.coalesceEmpty(routeCmd.description(), Optional.ofNullable(routerCmd).map(SSHCommand::description).orElse(null), Optional.ofNullable(routerCmd).map(SSHCommand::commandDescription).orElse(null), commandDescription);
                // create the command
                logger.debug("Intalling command {} {} -> {}", Arrays.toString(command), route);
                RouteBalsaShellCommand theCommand = new RouteBalsaShellCommand(command, subSystem, usage, description, this.application, route, argNames);
                this.installCommand(theCommand);
                // wrapper?
                if (command.length > 1)
                {
                    BalsaShellCommand wrapperCommand = this.commands.get(command[0]);
                    if (wrapperCommand == null)
                    {
                        wrapperCommand = new WrapperBalsaShellCommand(command, subSystem, usage, commandDescription);
                        this.installCommand(wrapperCommand);
                    }
                    // add the sub command
                    ((WrapperBalsaShellCommand) wrapperCommand).addSubCommand(theCommand);
                }
            }
        }
    }
    
    private static String[] buildCommand(String[] a, String[] b)
    {
        String[] c = new String[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    
    private static String[] argumentNames(RouteEntry route)
    {
        List<String> argNames = new ArrayList<>();
        for (var arg : route.getBuilder().getArguments())
        {
            if (arg instanceof ParameterArgument)
            {
                argNames.add(((ParameterArgument) arg).getParameterName());
            }
        }
        return argNames.toArray(i -> new String[i]);
    }
    
    private String usage(String[] command, String[] argNames)
    {
        StringBuilder sb = new StringBuilder();
        boolean ns = false;
        for (String name : command)
        {
            if (ns) sb.append(" ");
            sb.append(name);
            ns = true;
        }
        for (String argName : argNames)
        {
            sb.append(" <").append(argName).append(">");
        }
        return sb.toString();
    }
    
    public BalsaApplication getApplication()
    {
        return this.application;
    }
    
    public CommandFactory<ShellCommand> installCommand(BalsaShellCommand command)
    {
        this.commands.put(command.getCommandName(), command);
        return this;
    }

    @Override
    public CommandFactory<ShellCommand> addConfigurator(CommandConfigurator<ShellCommand> configurator)
    {
        return this;
    }

    @Override
    public CommandFactory<ShellCommand> removeConfigurator(CommandConfigurator<ShellCommand> configurator)
    {
        return this;
    }

    @Override
    public CommandFactory<ShellCommand> installCommand(String cmd, Class<? extends ShellCommand> cls)
    {
        return this;
    }

    @Override
    public CommandFactory<ShellCommand> installCommand(Class<? extends ShellCommand> cls)
    {
        return this;
    }

    @Override
    public CommandFactory<ShellCommand> uninstallCommand(String cmd)
    {
        this.commands.remove(cmd);
        return this;
    }

    @Override
    public Set<String> getSupportedCommands()
    {
        return new HashSet<>(this.commands.keySet());
    }

    @Override
    public boolean supportsCommand(String command)
    {
        return this.commands.containsKey(command);
    }

    @Override
    public ShellCommand createCommand(String command, SshConnection con) throws UnsupportedCommandException, IllegalAccessException, InstantiationException, IOException, PermissionDeniedException
    {
        return this.newInstance(command, con);
    }

    // Nasty AF
    @Override
    protected ShellCommand newInstance(String command, SshConnection con) throws UnsupportedCommandException, IllegalAccessException, InstantiationException, IOException, PermissionDeniedException
    {
        return this.commands.get(command);
    }

    @Override
    protected void configureCommand(ShellCommand command, SshConnection con) throws IOException, PermissionDeniedException
    {
    }
}
