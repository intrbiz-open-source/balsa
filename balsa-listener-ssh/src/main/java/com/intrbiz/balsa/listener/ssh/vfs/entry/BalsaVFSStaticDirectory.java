package com.intrbiz.balsa.listener.ssh.vfs.entry;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;

public class BalsaVFSStaticDirectory extends BalsaVFSDirectory
{
    public BalsaVFSStaticDirectory(BalsaVFS vfs, BalsaVFSEntry parent, String name)
    {
        super(vfs, parent, name);
    }
    
    public BalsaVFSStaticDirectory(BalsaVFS vfs, String name)
    {
        super(vfs, name);
    }
}
