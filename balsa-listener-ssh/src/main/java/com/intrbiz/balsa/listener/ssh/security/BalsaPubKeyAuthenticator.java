package com.intrbiz.balsa.listener.ssh.security;

import java.io.IOException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.engine.SecurityEngine;
import com.intrbiz.balsa.engine.SessionEngine;
import com.intrbiz.balsa.engine.security.AuthenticationResponse;
import com.intrbiz.balsa.error.BalsaSecurityException;
import com.intrbiz.balsa.listener.ssh.security.credentials.SSHPublicKeyCredential;
import com.intrbiz.balsa.listener.ssh.util.SSHUtil;
import com.sshtools.common.auth.AbstractPublicKeyAuthenticationProvider;
import com.sshtools.common.ssh.SshConnection;
import com.sshtools.common.ssh.components.SshPublicKey;

public class BalsaPubKeyAuthenticator extends AbstractPublicKeyAuthenticationProvider
{
    private static final Logger logger = LoggerFactory.getLogger(BalsaPubKeyAuthenticator.class);
    
    private final SecurityEngine securityEngine;
    
    private final SessionEngine sessionEngine;

    public BalsaPubKeyAuthenticator(SecurityEngine securityEngine, SessionEngine sessionEngine)
    {
        super();
        this.securityEngine = Objects.requireNonNull(securityEngine);
        this.sessionEngine = Objects.requireNonNull(sessionEngine);
    }
    
    public String getName()
    {
        return "Balsa-Public-Key-Authenticator";
    }

    @Override
    public boolean isAuthorizedKey(SshPublicKey key, SshConnection con) throws IOException
    {
        try
        {
            logger.info("Authenticating session: {} for user: {}", con.getSessionId(), con.getUsername());
            AuthenticationResponse response = this.securityEngine.authenticate(null, SSHPublicKeyCredential.of(con.getUsername(), key), true);
            if (response != null && response.isComplete())
            {
                // create the corresponding balsa session with the principal
                this.sessionEngine.getSession(SSHUtil.sessionId(con)).authenticationState().update(response);
                // return auth ok
                return true;
            }
        }
        catch (BalsaSecurityException e)
        {
            logger.error("Failed to authenticate session: " + con.getSessionId(), e);
        }
        return false;
    }
}
