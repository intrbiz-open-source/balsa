package com.intrbiz.balsa.listener.ssh.shell;

import java.io.IOException;
import java.util.List;

import org.jline.reader.Candidate;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;

import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.permissions.PermissionDeniedException;
import com.sshtools.common.ssh.SshConnection;
import com.sshtools.server.vsession.CommandFactory;
import com.sshtools.server.vsession.RootShell;
import com.sshtools.server.vsession.ShellCommand;
import com.sshtools.server.vsession.ShellCommandFactory;
import com.sshtools.server.vsession.VirtualConsole;
import com.sshtools.server.vsession.commands.Catch;
import com.sshtools.server.vsession.commands.Clear;
import com.sshtools.server.vsession.commands.Date;
import com.sshtools.server.vsession.commands.Echo;
import com.sshtools.server.vsession.commands.Env;
import com.sshtools.server.vsession.commands.Exit;
import com.sshtools.server.vsession.commands.Help;
import com.sshtools.server.vsession.commands.Input;
import com.sshtools.server.vsession.commands.ShowLastError;
import com.sshtools.server.vsession.commands.Source;
import com.sshtools.server.vsession.commands.fs.Cat;
import com.sshtools.server.vsession.commands.fs.Cd;
import com.sshtools.server.vsession.commands.fs.Cp;
import com.sshtools.server.vsession.commands.fs.Ls;
import com.sshtools.server.vsession.commands.fs.Mkdir;
import com.sshtools.server.vsession.commands.fs.Mv;
import com.sshtools.server.vsession.commands.fs.Nano;
import com.sshtools.server.vsession.commands.fs.Pwd;
import com.sshtools.server.vsession.commands.fs.Refresh;
import com.sshtools.server.vsession.commands.fs.Rm;

public final class BSH extends RootShell
{   
    protected BSH(ShellCommandFactory factory, SshConnection con) throws PermissionDeniedException, IOException
    {
        super(factory, con);
    }
    
    public static final ShellCommandFactory factory(List<CommandFactory<ShellCommand>> additionalCommands)
    {
        return (new ShellCommandFactory() {

            @Override
            protected void installShellCommands()
            {
                // Shell commands
                commands.put("exit", Exit.class);
                commands.put("echo", Echo.class);
                commands.put("cat", Cat.class);
                commands.put("input", Input.class);
                commands.put("date", Date.class);
                commands.put("env", Env.class);
                commands.put("set", Env.class);
                commands.put("source", Source.class);
                commands.put("error", ShowLastError.class);
                commands.put("help", Help.class);
                commands.put("clear", Clear.class);
                commands.put("catch", Catch.class);
                // FS commands
                commands.put("pwd", Pwd.class);
                commands.put("cd", BCd.class);
                commands.put("rm", Rm.class);
                commands.put("mv", Mv.class);
                commands.put("cp", Cp.class);
                commands.put("refresh", Refresh.class);
                commands.put("mkdir", Mkdir.class);
                commands.put("ls", BLs.class);
                commands.put("cat", BCat.class);
                commands.put("nano", Nano.class);
            }

            @Override
            public RootShell createShell(SshConnection connection) throws PermissionDeniedException, IOException
            {
                BSH shell = new BSH(this, connection);
                configureCommand(shell, connection);
                return shell;
            }
            
            public ShellCommandFactory installFactories(List<CommandFactory<ShellCommand>> additionalCommands)
            {
                for (CommandFactory<ShellCommand> factory : additionalCommands)
                {
                    this.installFactory(factory);
                }
                return this;
            }
        }).installFactories(additionalCommands);
    }
    
    public static void fileComplete(LineReader reader, ParsedLine line, List<Candidate> candidates, boolean dirs, boolean files)
    {
        // very basic file completion
        try
        {
            AbstractFile cwd = VirtualConsole.getCurrentConsole().getCurrentDirectory();
            if (cwd != null && cwd.isDirectory())
            {
                String word = line.word();
                for (AbstractFile child : cwd.getChildren())
                {
                    if (((dirs && child.isDirectory()) || (files && child.isFile())) && child.getName().startsWith(word))
                    {
                        String can = child.getName() + "/";
                        candidates.add(new Candidate(can, can, null, null, null, null, true));
                    }
                }
            }
        }
        catch (IOException | PermissionDeniedException e)
        {
            e.printStackTrace();
        }
    }
    
    public static class BLs extends Ls
    {
        @Override
        public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates)
        {
            fileComplete(reader, line, candidates, true, true);
        }
    }
    
    public static class BCd extends Cd
    {
        @Override
        public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates)
        {
            fileComplete(reader, line, candidates, true, false);
        }
    }
    
    public static class BCat extends Cat
    {
        @Override
        public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates)
        {
            fileComplete(reader, line, candidates, false, true);
        }
    }
}
