package com.intrbiz.balsa.listener.ssh.vfs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class VFSUtil
{
    public static String buildPath(String prefix, String path)
    {
        return stripTrailingSlash(prefix) + ensureStartingSlash(path);
    }
    
    public static String buildPath(List<String> pathVector)
    {
        return ensureStartingSlash(pathVector.stream().collect(Collectors.joining("/")));
    }
    
    public static List<String> canonicalisePath(String path)
    {
        // files always relative to /
        if (! path.startsWith("/")) path = "/" + path;
        // split the path
        List<String> parts = new ArrayList<>(Arrays.asList(path.split("/")));
        // remove the first empty part
        if (parts.size() > 0) parts.remove(0);
        // process . and .. in the path
        for (ListIterator<String> i = parts.listIterator(); i.hasNext(); )
        {
            String part = i.next();
            if (".".equals(part))
            {
                i.remove();
            }
            else if ("..".equals(part))
            {
                // need to go back up the path
                i.remove();
                if (i.hasPrevious())
                {
                    i.previous();
                    i.remove();
                }
            }
            else if ("".equals(part))
            {
                i.remove();
            }
        }
        // return the absolute path vector
        return parts;
    }
    
    public static String stripTrailingSlash(String s)
    {
        if ("/".equals(s)) 
            return "";
        if (s.endsWith("/"))
            return s.substring(0, s.length() - 1);
        return s;
    }
    
    public static String stripStartingSlash(String s)
    {
        if ("/".equals(s)) 
            return "";
        if (s.startsWith("/")) 
            return s.substring(1);
        return s;
    }
    
    public static String stripStartingSpecials(String s)
    {
        if (s.startsWith(":") || s.startsWith("+"))
            return s.substring(1);
        if (s.startsWith("++") || s.startsWith("**"))
            return s.substring(2);
        return s;
    }

    public static String ensureStartingSlash(String s)
    {
        if (!s.startsWith("/")) 
            return "/" + s;
        return s;
    }
    
    public static String ensureTrailingSlash(String s)
    {
        if (!s.endsWith("/")) 
            return s + "/";
        return s;
    }
}
