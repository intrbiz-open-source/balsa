package com.intrbiz.balsa.listener.ssh.vfs.entry.overlay;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.List;

import org.apache.log4j.Logger;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFSSession;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSInMemoryMutableFile;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileRandomAccess;
import com.sshtools.common.permissions.PermissionDeniedException;

public class BalsaVFSOverlayInMemoryMutableFile extends BalsaVFSOverlayFile<BalsaVFSInMemoryMutableFile>
{
    private static final Logger logger = Logger.getLogger(BalsaVFSOverlayInMemoryMutableFile.class);
    
    private byte[] data = null;
    
    public BalsaVFSOverlayInMemoryMutableFile(BalsaVFSSession vfs, BalsaVFSInMemoryMutableFile underlay, List<String> path)
    {
        super(vfs, underlay, path);
    }

    @Override
    public boolean isWritable()
    {
        return true;
    }
    
    protected byte[] currentData()
    {
        return this.data == null ? this.underlay.getData() : this.data;
    }

    public long length()
    {
        return this.currentData().length;
    }
    
    public ByteBuffer asBuffer()
    {
        return ByteBuffer.wrap(this.currentData());
    }
    
    public void extend(int size) throws IOException, PermissionDeniedException
    {
        byte[] currentData = this.currentData();
        byte[] newData = new byte[currentData.length + size];
        System.arraycopy(currentData, 0, newData, 0, currentData.length);
        this.data = newData;
    }
    
    public void append(byte[] append) throws IOException, PermissionDeniedException
    {
        byte[] currentData = this.currentData();
        byte[] newData = new byte[currentData.length + append.length];
        System.arraycopy(currentData, 0, newData, 0, currentData.length);
        System.arraycopy(append, 0, newData, currentData.length, append.length);
        this.data = newData;
    }
    
    @Override
    public void truncate() throws IOException, PermissionDeniedException
    {
        this.data = new byte[0];
    }
    
    public void replace(byte[] data) throws IOException, PermissionDeniedException
    {
        this.data = (data == null) ? new byte[0] : data;
    }
    
    //

    @Override
    public InputStream getInputStream() throws IOException, PermissionDeniedException
    {
        return new ByteArrayInputStream(this.currentData());
    }

    @Override
    public OutputStream getOutputStream() throws IOException, PermissionDeniedException
    {
        return this.getOutputStream(false);
    }

    @Override
    public boolean createFolder() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public void copyFrom(AbstractFile src) throws IOException, PermissionDeniedException
    {
        logger.debug("Copy from: " + src.getAbsolutePath() + " [" + src.getClass().getSimpleName() + "]");
        // TODO: should we copy the given src into our buffer
    }

    @Override
    public boolean delete(boolean recursive) throws IOException, PermissionDeniedException
    {
        return false;
    }

    @Override
    public boolean createNewFile() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public boolean supportsRandomAccess()
    {
        return false;
    }

    @Override
    public AbstractFileRandomAccess openFile(boolean writeAccess) throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public OutputStream getOutputStream(boolean append) throws IOException, PermissionDeniedException
    {
        return new ByteArrayOutputStream() {
            @Override
            public void flush() throws IOException
            {
                super.flush();
                // set the content
                try {
                    if (append) append(this.toByteArray());
                    else replace(this.toByteArray());
                }
                catch (PermissionDeniedException e)
                {
                    throw new IOException("Could not write to file", e);
                }
            }
            
            @Override
            public void close() throws IOException
            {
                // this is mainly to work around a bug in jline nano, which does not close the stream before moving the file
                this.flush();
                super.close();
            }
        };
    }
}
