package com.intrbiz.balsa.listener.ssh.security.credentials;

import java.util.Objects;

import com.intrbiz.balsa.engine.security.credentials.LoginCredentials;
import com.sshtools.common.ssh.components.SshPublicKey;

public interface SSHPublicKeyCredential extends LoginCredentials
{
    /**
     * Get the SSH Public Key which is being used for the authentication
     * @return the SSH Public Key
     */
    SshPublicKey getKey();

    public static SSHPublicKeyCredential of(String username, SshPublicKey key)
    {
        return new Simple(username, key);
    }

    public static final class Simple implements SSHPublicKeyCredential
    {
        private final String username;
        
        private final SshPublicKey key;
        
        public Simple(String username, SshPublicKey key)
        {
            super();
            this.username = Objects.requireNonNull(username);
            this.key = Objects.requireNonNull(key);
        }

        @Override
        public String getPrincipalName()
        {
            return this.username;
        }

        @Override
        public void release()
        {
        }

        @Override
        public SshPublicKey getKey()
        {
            return this.key;
        }
    }
    
}
