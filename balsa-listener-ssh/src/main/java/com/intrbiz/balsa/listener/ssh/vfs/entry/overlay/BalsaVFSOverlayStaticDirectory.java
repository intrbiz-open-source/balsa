package com.intrbiz.balsa.listener.ssh.vfs.entry.overlay;

import java.util.List;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFSSession;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSStaticDirectory;

public class BalsaVFSOverlayStaticDirectory extends BalsaVFSOverlayDirectory<BalsaVFSStaticDirectory>
{
    public BalsaVFSOverlayStaticDirectory(BalsaVFSSession vfs, BalsaVFSStaticDirectory underlay, List<String> path)
    {
        super(vfs, underlay, path);
    }
}
