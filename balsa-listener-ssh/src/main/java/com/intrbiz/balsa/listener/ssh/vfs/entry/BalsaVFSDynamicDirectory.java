package com.intrbiz.balsa.listener.ssh.vfs.entry;

import com.intrbiz.balsa.engine.impl.route.RouteEntry;
import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;
import com.intrbiz.express.value.ValueExpression;

public class BalsaVFSDynamicDirectory extends BalsaVFSDirectory
{
    private final RouteEntry route;
    
    private final ValueExpression expression;
    
    public BalsaVFSDynamicDirectory(BalsaVFS vfs, BalsaVFSEntry parent, String name, RouteEntry route, ValueExpression expression)
    {
        super(vfs, parent, name);
        this.route = route;
        this.expression = expression;
    }
    
    public BalsaVFSDynamicDirectory(BalsaVFS vfs, String name, RouteEntry route, ValueExpression expression)
    {
        super(vfs, name);
        this.route = route;
        this.expression = expression;
    }
    
    @Override
    public boolean isDynamic()
    {
        return true;
    }
    
    /**
     * The route which can be used to dynamically invoke the handler
     */
    public RouteEntry getRoute()
    {
        return this.route;
    }
    
    public ValueExpression getExpression()
    {
        return this.expression;
    }
}
