package com.intrbiz.balsa.listener.ssh.vfs.entry.overlay;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFSSession;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSNoneEntry;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileRandomAccess;
import com.sshtools.common.permissions.PermissionDeniedException;

public class BalsaVFSOverlayNoneEntry extends BalsaVFSOverlayEntry<BalsaVFSNoneEntry>
{
    private static final Logger logger = Logger.getLogger(BalsaVFSOverlayNoneEntry.class);
    
    private boolean exists = false;
    
    private boolean directory = false;
    
    private boolean file = false;
    
    private byte[] data = new byte[0];
    
    public BalsaVFSOverlayNoneEntry(BalsaVFSSession vfs, BalsaVFSNoneEntry underlay, List<String> path)
    {
        super(vfs, underlay, path);
    }
    
    @Override
    public boolean isWritable()
    {
        return true;
    }

    @Override
    public boolean exists()
    {
        return this.exists;
    }
    
    public void append(byte[] append) throws IOException, PermissionDeniedException
    {
        if (! this.exists)
        {
            this.exists = true;
            this.file = true;
        }
        if (! this.file) throw new IOException("Can only write to a file");
        //
        byte[] currentData = this.data;
        byte[] newData = new byte[currentData.length + append.length];
        System.arraycopy(currentData, 0, newData, 0, currentData.length);
        System.arraycopy(append, 0, newData, currentData.length, append.length);
        this.data = newData;
    }
    
    public void replace(byte[] data) throws IOException, PermissionDeniedException
    {
        if (! this.exists)
        {
            this.exists = true;
            this.file = true;
        }
        if (! this.file) throw new IOException("Can only write to a file");
        //
        this.data = (data == null) ? new byte[0] : data;
    }

    @Override
    public InputStream getInputStream() throws IOException, PermissionDeniedException
    {
        if (! this.exists) throw new FileNotFoundException("File does not exist");
        if (! this.file) throw new IOException("Can only write to a file");
        return new ByteArrayInputStream(this.data);
    }

    @Override
    public List<AbstractFile> getChildren() throws IOException, PermissionDeniedException
    {
        return Collections.emptyList();
    }

    @Override
    public boolean isDirectory() throws IOException, PermissionDeniedException
    {
        return this.directory;
    }

    @Override
    public boolean isFile() throws IOException, PermissionDeniedException
    {
        return this.file;
    }

    @Override
    public OutputStream getOutputStream() throws IOException, PermissionDeniedException
    {
        return this.getOutputStream(false);
    }

    @Override
    public boolean createFolder() throws PermissionDeniedException, IOException
    {
        if (! this.exists)
        {
            this.exists = true;
            this.directory = true;
        }
        return this.exists && this.directory;
    }

    @Override
    public void copyFrom(AbstractFile src) throws IOException, PermissionDeniedException
    {
        logger.debug("Copy from: " + src.getAbsolutePath() + " [" + src.getClass().getSimpleName() + "]");
        // copy from the source file into our buffer
        try (InputStream in = src.getInputStream())
        {
            try (OutputStream out = this.getOutputStream())
            {
                int r;
                byte[] buffer = new byte[4096];
                while ((r = in.read(buffer)) != -1)
                {
                    out.write(buffer, 0, r);
                }
            }
        }
    }

    @Override
    public boolean delete(boolean recursive) throws IOException, PermissionDeniedException
    {
        if (this.exists)
        {
            this.exists = false;
            this.file = false;
            this.directory = false;
            this.data = new byte[0];
        }
        return ! this.exists;
    }

    @Override
    public long length() throws IOException, PermissionDeniedException
    {
        return this.data.length;
    }

    @Override
    public boolean createNewFile() throws PermissionDeniedException, IOException
    {
        if (! this.exists)
        {
            this.exists = true;
            this.file = true;
        }
        return this.exists && this.file;
    }

    @Override
    public void truncate() throws PermissionDeniedException, IOException
    {
        this.data = new byte[0];
    }

    @Override
    public boolean supportsRandomAccess()
    {
        return false;
    }

    @Override
    public AbstractFileRandomAccess openFile(boolean writeAccess) throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public OutputStream getOutputStream(boolean append) throws IOException, PermissionDeniedException
    {
        if (this.exists && ! this.file) throw new IOException("Can only write to a file");
        return new ByteArrayOutputStream() {
            @Override
            public void flush() throws IOException
            {
                super.flush();
                // set the content
                try {
                    if (append) append(this.toByteArray());
                    else replace(this.toByteArray());
                }
                catch (PermissionDeniedException e)
                {
                    throw new IOException("Could not write to file", e);
                }
            }
            
            @Override
            public void close() throws IOException
            {
                // this is mainly to work around a bug in jline nano, which does not close the stream before moving the file
                this.flush();
                super.close();
            }
        };
    }
}
