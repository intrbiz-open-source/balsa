package com.intrbiz.balsa.listener.ssh.vfs.entry.overlay;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.BalsaContext;
import com.intrbiz.balsa.engine.impl.route.RouteEntry;
import com.intrbiz.balsa.listener.ssh.context.BalsaSSHRequest;
import com.intrbiz.balsa.listener.ssh.context.BalsaSSHVFSResponse;
import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFSSession;
import com.intrbiz.balsa.listener.ssh.vfs.entry.BalsaVFSDynamicFile;
import com.intrbiz.balsa.metadata.SSHVFS;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileRandomAccess;
import com.sshtools.common.permissions.PermissionDeniedException;

public class BalsaVFSOverlayDynamicFile extends BalsaVFSOverlayFile<BalsaVFSDynamicFile>
{
    public static final Logger logger = LoggerFactory.getLogger(BalsaVFSOverlayDynamicFile.class);

    public BalsaVFSOverlayDynamicFile(BalsaVFSSession vfs, BalsaVFSDynamicFile underlay, List<String> path)
    {
        super(vfs, underlay, path);
    }

    @Override
    public boolean isDynamic()
    {
        return this.underlay.isDynamic();
    }
    
    
    
    @Override
    public boolean isReadable()
    {
        // check a read route exists
        return this.underlay.getRoute(SSHVFS.Method.READ, "GET", "ANY") != null;
    }

    @Override
    public boolean isWritable()
    {
        // check a write route exists
        return this.underlay.getRoute(SSHVFS.Method.WRITE, "PUT", "POST") != null;
    }
    
    protected void executeWrite(InputStream input) throws IOException
    {
        // find the route
        final RouteEntry route = this.underlay.getRoute(SSHVFS.Method.WRITE, "PUT", "POST");
        // check we have a read handler
        if (route == null) throw new IOException("Cannot write virtual file, no such route");
        // create the context
        final BalsaContext context = new BalsaContext(this.getVFS().getBalsaApplication(), new BalsaSSHRequest(route.getRoute().getMethod().toString(), this.getAbsolutePath(), input), null);
        context.setSession(this.getVFS().getSession());
        // bind the context and execute
        try
        {
            context.activate();
            context.bind();
            // invoke the route, calling match first to extract any path parameters
            if (! route.match(true, context, context.request()))
                throw new FileNotFoundException("Given path did not match the dynamic file route");
            route.execute(context);
        }
        catch (Throwable e)
        {
            throw new IOException("Error reading dynamic file", e);
        }
        finally
        {
            context.unbind();
            context.deactivate();
        }
    }
    
    protected InputStream executeRead() throws IOException
    {
        logger.info("Executing read of dynamic file {} overlayed at {}", this.underlay, this.path);
        // find the route
        final RouteEntry route = this.underlay.getRoute(SSHVFS.Method.READ, "GET", "ANY");
        // check we have a read handler
        if (route == null) throw new IOException("Cannot read virtual file, no such route");
        // create the context
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final BalsaContext context = new BalsaContext(this.getVFS().getBalsaApplication(), new BalsaSSHRequest(route.getRoute().getMethod().toString(), this.getAbsolutePath()), new BalsaSSHVFSResponse(out));
        context.setSession(this.getVFS().getSession());
        // bind the context and execute
        try
        {
            context.activate();
            context.bind();
            // invoke the route, calling match first to extract any path parameters
            if (! route.match(true, context, context.request()))
                throw new FileNotFoundException("Given path did not match the dynamic file route");
            try
            {
                route.execute(context);
            }
            finally
            {
                // ensure we flush the response
                context.response().flush();
            }
        }
        catch (Throwable e)
        {
            throw new IOException("Error reading dynamic file", e);
        }
        finally
        {
            context.unbind();
            context.deactivate();
        }
        // return the stream
        return new ByteArrayInputStream(out.toByteArray());
    }

    @Override
    public long length() throws IOException, PermissionDeniedException
    {
        return 0;
    }

    @Override
    public void truncate() throws IOException, PermissionDeniedException
    {
        throw new PermissionDeniedException("Cannot write to a virtual file");
    }

    @Override
    public InputStream getInputStream() throws IOException, PermissionDeniedException
    {
        return this.executeRead();
    }

    @Override
    public OutputStream getOutputStream() throws IOException, PermissionDeniedException
    {
        throw new PermissionDeniedException("Cannot write to a virtual file");
    }

    @Override
    public boolean createFolder() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public void copyFrom(AbstractFile src) throws IOException, PermissionDeniedException
    {
        // Nano will write a temp file and then move it, move defaults to calling copy
        logger.info("Executing copy from {} to dynamic file {} overlayed at {}", src, this.underlay, this.path);
        this.executeWrite(src.getInputStream());
    }

    @Override
    public boolean delete(boolean recursive) throws IOException, PermissionDeniedException
    {
        return false;
    }

    @Override
    public boolean createNewFile() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public boolean supportsRandomAccess()
    {
        return false;
    }

    @Override
    public AbstractFileRandomAccess openFile(boolean writeAccess) throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public OutputStream getOutputStream(boolean append) throws IOException, PermissionDeniedException
    {
        if (! this.isWritable()) throw new IOException("Cannot write to this dynamic file");
        if (append) throw new IOException("Append is not supported by dynamic files");
        // create a stream to buffer the request and then invoke the write on stream close
        return new ByteArrayOutputStream() {
            @Override
            public void close() throws IOException
            {
                super.close();
                // execute the route
                BalsaVFSOverlayDynamicFile.this.executeWrite(new ByteArrayInputStream(this.toByteArray()));
            }
        };
    }
}
