package com.intrbiz.balsa.listener.ssh.util;

import com.sshtools.common.ssh.SshConnection;

public class SSHUtil
{
    public static final String sessionId(SshConnection con)
    {
        return "ssh-" + con.getSessionId();
    }
}
