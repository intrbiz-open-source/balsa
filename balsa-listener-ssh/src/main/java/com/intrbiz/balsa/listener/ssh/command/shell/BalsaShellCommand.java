package com.intrbiz.balsa.listener.ssh.command.shell;

import java.util.Arrays;
import java.util.stream.Collectors;

import com.sshtools.server.vsession.ShellCommand;

public abstract class BalsaShellCommand extends ShellCommand
{
    protected String[] command;
    
    public BalsaShellCommand(String command[], String subsystem, String signature, String description)
    {
        super(buildCommandname(command), subsystem, signature, description);
        this.command = command;
    }
    
    public String[] getCommand()
    {
        return this.command;
    }
    
    public static final String buildCommandname(String[] command)
    {
        return Arrays.stream(command).collect(Collectors.joining("-"));
    }
    
    public static final String buildCommandname(String[] command, int limit)
    {
        return Arrays.stream(command).limit(limit).collect(Collectors.joining("-"));
    }
}
