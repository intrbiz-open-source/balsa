package com.intrbiz.balsa.listener.ssh.vfs.entry;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import com.intrbiz.balsa.engine.impl.route.Route;
import com.intrbiz.balsa.engine.impl.route.Route.CompiledPattern;
import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;
import com.intrbiz.balsa.listener.ssh.vfs.VFSUtil;
import com.intrbiz.express.ExpressContext;
import com.intrbiz.express.ExpressEntityResolver;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileFactory;
import com.sshtools.common.permissions.PermissionDeniedException;
import com.sshtools.common.sftp.SftpFileAttributes;
import com.sshtools.common.util.UnsignedInteger64;

public abstract class BalsaVFSEntry implements AbstractFile
{
    private final BalsaVFS vfs;
    
    private BalsaVFSEntry parent;
    
    private final String name;
    
    private final CompiledPattern glob;
    
    public BalsaVFSEntry(BalsaVFS vfs, BalsaVFSEntry parent, String name)
    {
        super();
        this.vfs = Objects.requireNonNull(vfs);
        this.parent = parent;
        this.name = Objects.requireNonNull(name);
        // is this a globbed entry
        this.glob = buildGlobPattern(name);
    }
    
    public BalsaVFSEntry(BalsaVFS vfs, String name)
    {
        this(vfs, null, name);
    }
    
    private static CompiledPattern buildGlobPattern(String name)
    {
        if (name.startsWith(":") || name.startsWith("+"))
        {
            return Route.compileBalsaPattern("", name);
        }
        return null;
    }
    
    public BalsaVFS getVFS()
    {
        return this.vfs;
    }
    
    @Override
    public AbstractFileFactory<? extends AbstractFile> getFileFactory()
    {
        return this.vfs;
    }
    
    @Override
    public String getName()
    {
        return this.name;
    }
    
    public boolean isGlob()
    {
        return this.glob != null;
    }
    
    public CompiledPattern getGlob()
    {
        return this.glob;
    }
    
    public boolean matchesGlob(String name)
    {
        return (this.glob == null) ? false : this.glob.getPattern().matcher(VFSUtil.ensureStartingSlash(VFSUtil.stripStartingSpecials(name))).matches();
    }
    
    public String buildGlobName(final String name)
    {
        if (this.glob != null)
            return VFSUtil.stripStartingSlash((String) this.glob.getBuilderExpression().get(ExpressContext.builder().withResolver(ExpressEntityResolver.SOURCE).build(), Arrays.asList(name)));
        return name;
    }
    
    @Override
    public String getAbsolutePath()
    {
        return this.parent == null ? this.name : this.parent.getAbsolutePath() + "/" + this.name;
    }
    
    @Override
    public String getCanonicalPath()
    {
        return this.getAbsolutePath();
    }
    
    public BalsaVFSEntry getParent()
    {
        return this.parent;
    }
    
    void setParent(BalsaVFSDirectory parent)
    {
        this.parent = parent;
    }
    
    public abstract boolean isDirectory();

    public abstract boolean isFile();
    
    public boolean isRoot()
    {
        return false;
    }
    
    public boolean isDynamic()
    {
        return false;
    }
    
    @Override
    public boolean isHidden()
    {
        return this.name.startsWith(".");
    }
    
    @Override
    public void refresh()
    {
    }
    
    @Override
    public long lastModified()
    {
        return 0;
    }
    
    @Override
    public boolean exists()
    {
        return true;
    }
    
    @Override
    public boolean isReadable()
    {
        return true;
    }
    
    @Override
    public boolean isWritable()
    {
        return false;
    }
    
    @Override
    public final SftpFileAttributes getAttributes() throws IOException, PermissionDeniedException
    {
        if(! exists()) throw new FileNotFoundException();
        SftpFileAttributes attr = new SftpFileAttributes(getSFTPFileType(), "UTF-8");
        if (! attr.isDirectory()) attr.setSize(new UnsignedInteger64(length()));
        attr.setTimes(new UnsignedInteger64(lastModified() / 1000), new UnsignedInteger64(lastModified() / 1000));
        attr.setPermissions(
            String.format("%s%s%s------", 
                isReadable() ? "r" : "-", 
                isWritable() ? "w" : "-", 
                (isDirectory() || isDynamic()) ? "x" : "-"
            )
        );
        attr.setUID("0");
        attr.setGID("0");
        return attr;
    }
    
    protected int getSFTPFileType() throws IOException, PermissionDeniedException
    {
        if (this.isDynamic()) return SftpFileAttributes.SSH_FILEXFER_TYPE_SPECIAL;
        if (this.isDirectory()) return SftpFileAttributes.SSH_FILEXFER_TYPE_DIRECTORY;
        if (this.isFile()) return SftpFileAttributes.SSH_FILEXFER_TYPE_REGULAR; 
        return SftpFileAttributes.SSH_FILEXFER_TYPE_UNKNOWN;
    }
    
    @Override
    public final void setAttributes(SftpFileAttributes attrs) throws IOException
    {
    }
    
    @Override
    public AbstractFile resolveFile(String child) throws IOException, PermissionDeniedException
    {
        throw new FileNotFoundException("Only directories can have children");
    }
    
    public String toString()
    {
        return  "[" 
                + (this.isDirectory() ? "D" : "-")
                + (this.isFile() ? "F" : "-") 
                + (this.isDynamic() ? "X" : "-") 
                + (this.isGlob() ? "G" : "-") 
                + "] " 
                + this.getAbsolutePath() 
                + " (" 
                + this.getClass().getSimpleName()
                + ")";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        BalsaVFSEntry other = (BalsaVFSEntry) obj;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (parent == null)
        {
            if (other.parent != null) return false;
        }
        else if (!parent.equals(other.parent)) return false;
        return true;
    }
}
