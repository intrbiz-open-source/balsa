package com.intrbiz.balsa.listener.ssh.log;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sshtools.common.logger.Log.Level;
import com.sshtools.common.logger.RootLoggerContext;

public class SSHLog implements RootLoggerContext
{
    private static final Logger logger = LoggerFactory.getLogger(SSHLog.class);

    public SSHLog()
    {
        super();
    }
    
    @Override
    public boolean isLogging(Level level)
    {
        switch (level)
        {
            case DEBUG:
                return logger.isDebugEnabled();
            case ERROR:
                return logger.isErrorEnabled();
            case INFO:
                return logger.isInfoEnabled();
            case NONE:
                return logger.isTraceEnabled();
            case TRACE:
                return logger.isTraceEnabled();
            case WARN:
                return logger.isWarnEnabled();
            default:
                break;
        }
        return false;
    }

    @Override
    public void log(Level level, String msg, Throwable e, Object... args)
    {
        switch (level)
        {
            case DEBUG:
                logger.debug(msg, args);
                if (e != null) logger.debug(msg, e);
                break;
            case ERROR:
                logger.error(msg, args);
                if (e != null) logger.error(msg, e);
                break;
            case INFO:
                logger.info(msg, args);
                if (e != null) logger.info(msg, e);
                break;
            case NONE:
                logger.trace(msg, args);
                if (e != null) logger.trace(msg, e);
                break;
            case TRACE:
                logger.trace(msg, args);
                if (e != null) logger.trace(msg, e);
                break;
            case WARN:
                logger.warn(msg, args);
                if (e != null) logger.warn(msg, e);
                break;
            default:
                logger.debug(msg, args);
                if (e != null) logger.debug(msg, e);
                break;
        }
    }

    @Override
    public void raw(Level level, String msg)
    {
        switch (level)
        {
            case DEBUG:
                //logger.debug(msg);
                break;
            case ERROR:
                logger.error(msg);
                break;
            case INFO:
                logger.info(msg);
                break;
            case NONE:
                logger.trace(msg);
                break;
            case TRACE:
                logger.trace(msg);
                break;
            case WARN:
                logger.warn(msg);
                break;
            default:
                logger.debug(msg);
                break;
        }
    }

    @Override
    public void close()
    {
    }

    @Override
    public void newline()
    {
    }

    @Override
    public void enableConsole(Level level)
    {
    }

    @Override
    public String getProperty(String key, String defaultValue)
    {
        logger.debug("getProperty({}, {})", key, defaultValue);
        // known properties
        switch (key)
        {
            case "maverick.log.connection.level": return "INFO";
            case "maverick.log.connection.server.level": return "INFO";
            case "maverick.log.file": return "false";
            case "maverick.log.console": return "false";
            case "maverick.log.connection.server": return "false";
        }
        // return default
        return defaultValue;
    }

    @Override
    public void shutdown()
    {
    }

    @Override
    public void enableFile(Level level, String logFile)
    {
    }

    @Override
    public void enableFile(Level level, File logFile)
    {
    }

    @Override
    public void enableFile(Level level, File logFile, int maxFiles, long maxSize)
    {
    }
    
}
