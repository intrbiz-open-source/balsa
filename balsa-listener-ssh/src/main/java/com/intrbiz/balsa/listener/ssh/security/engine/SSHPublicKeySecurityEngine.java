package com.intrbiz.balsa.listener.ssh.security.engine;

import java.security.Principal;

import com.sshtools.common.ssh.components.SshPublicKey;

/**
 * Authenticate using SSH Public Keys
 */
public interface SSHPublicKeySecurityEngine
{
    
    /**
     * Get the Principal if any which is represented by the given username and SSH Public Key.
     * @param username the username
     * @param key the SSH Public Key
     * @return the corresponding Principal or null
     */
    Principal princiaplForSSHPublicKey(String username, SshPublicKey key);
    
}
