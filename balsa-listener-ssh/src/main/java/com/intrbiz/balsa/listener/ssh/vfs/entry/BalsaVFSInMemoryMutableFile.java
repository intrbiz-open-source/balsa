package com.intrbiz.balsa.listener.ssh.vfs.entry;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Objects;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileRandomAccess;
import com.sshtools.common.permissions.PermissionDeniedException;

public class BalsaVFSInMemoryMutableFile extends BalsaVFSFile
{
    private boolean readOnly = false;
    
    private byte[] data;

    public BalsaVFSInMemoryMutableFile(BalsaVFS vfs, BalsaVFSEntry parent, String name, byte[] data)
    {
        super(vfs, parent, name);
        this.data = Objects.requireNonNull(data);
    }
    
    public BalsaVFSInMemoryMutableFile(BalsaVFS vfs, BalsaVFSEntry parent, String name)
    {
        this(vfs, parent, name, new byte[0]);
    }
    
    public BalsaVFSInMemoryMutableFile(BalsaVFS vfs, String name, byte[] data)
    {
        super(vfs, name);
        this.data = Objects.requireNonNull(data);
    }
    
    public BalsaVFSInMemoryMutableFile(BalsaVFS vfs, String name)
    {
        this(vfs, name, new byte[0]);
    }
    
    public BalsaVFSInMemoryMutableFile readOnly()
    {
        this.readOnly = true;
        return this;
    }
    
    public byte[] getData()
    {
        return this.data;
    }
    
    @Override
    public boolean isWritable()
    {
        return ! this.readOnly;
    }

    public long length()
    {
        return this.data.length;
    }
    
    public ByteBuffer asBuffer()
    {
        return ByteBuffer.wrap(data);
    }
    
    public void extend(int size) throws IOException, PermissionDeniedException
    {
        if (this.readOnly) throw new PermissionDeniedException("File is read-only");
        byte[] newData = new byte[this.data.length + size];
        System.arraycopy(this.data, 0, newData, 0, this.data.length);
        this.data = newData;
    }
    
    public void append(byte[] append) throws IOException, PermissionDeniedException
    {
        if (this.readOnly) throw new PermissionDeniedException("File is read-only");
        byte[] newData = new byte[this.data.length + append.length];
        System.arraycopy(this.data, 0, newData, 0, this.data.length);
        System.arraycopy(append, 0, newData, this.data.length, append.length);
        this.data = newData;
    }
    
    @Override
    public void truncate() throws IOException, PermissionDeniedException
    {
        if (this.readOnly) throw new PermissionDeniedException("File is read-only");
        this.data = new byte[0];
    }
    
    public void replace(byte[] data) throws IOException, PermissionDeniedException
    {
        if (this.readOnly) throw new PermissionDeniedException("File is read-only");
        this.data = (data == null) ? new byte[0] : data;
    }
    
    //

    @Override
    public InputStream getInputStream() throws IOException, PermissionDeniedException
    {
        return new ByteArrayInputStream(this.data);
    }

    @Override
    public OutputStream getOutputStream() throws IOException, PermissionDeniedException
    {
        return this.getOutputStream(false);
    }

    @Override
    public boolean createFolder() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public void copyFrom(AbstractFile src) throws IOException, PermissionDeniedException
    {
    }

    @Override
    public void moveTo(AbstractFile target) throws IOException, PermissionDeniedException
    {
    }

    @Override
    public boolean delete(boolean recursive) throws IOException, PermissionDeniedException
    {
        return false;
    }

    @Override
    public boolean createNewFile() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public boolean supportsRandomAccess()
    {
        return false;
    }

    @Override
    public AbstractFileRandomAccess openFile(boolean writeAccess) throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public OutputStream getOutputStream(boolean append) throws IOException, PermissionDeniedException
    {
        return new ByteArrayOutputStream() {
            @Override
            public void close() throws IOException
            {
                super.close();
                try {
                    // set the content
                    if (append) append(this.toByteArray());
                    else replace(this.toByteArray());
                }
                catch (PermissionDeniedException e)
                {
                    throw new IOException("Could not write to file", e);
                }
            }
        };
    }
}
