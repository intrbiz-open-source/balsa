package com.intrbiz.balsa.listener.ssh.vfs.entry;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;

import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileRandomAccess;
import com.sshtools.common.permissions.PermissionDeniedException;

public class BalsaVFSNoneEntry extends BalsaVFSEntry
{
    
    public BalsaVFSNoneEntry(BalsaVFS vfs, BalsaVFSEntry parent, String name)
    {
        super(vfs, parent, name);
    }

    public BalsaVFSNoneEntry(BalsaVFS vfs, String name)
    {
        super(vfs, name);
    }
    
    @Override
    public boolean exists()
    {
        return false;
    }

    @Override
    public InputStream getInputStream() throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public List<AbstractFile> getChildren() throws IOException, PermissionDeniedException
    {
        return Collections.emptyList();
    }

    @Override
    public boolean isDirectory()
    {
        return false;
    }

    @Override
    public boolean isFile()
    {
        return false;
    }

    @Override
    public OutputStream getOutputStream() throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public boolean createFolder() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public void copyFrom(AbstractFile src) throws IOException, PermissionDeniedException
    {
    }

    @Override
    public void moveTo(AbstractFile target) throws IOException, PermissionDeniedException
    {
    }

    @Override
    public boolean delete(boolean recursive) throws IOException, PermissionDeniedException
    {
        return false;
    }

    @Override
    public long length() throws IOException, PermissionDeniedException
    {
        return 0;
    }

    @Override
    public boolean createNewFile() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public void truncate() throws PermissionDeniedException, IOException
    {
    }

    @Override
    public boolean supportsRandomAccess()
    {
        return false;
    }

    @Override
    public AbstractFileRandomAccess openFile(boolean writeAccess) throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public OutputStream getOutputStream(boolean append) throws IOException, PermissionDeniedException
    {
        return null;
    }
}
