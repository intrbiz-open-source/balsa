package com.intrbiz.balsa.listener.ssh.security;

import java.io.IOException;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.engine.SecurityEngine;
import com.intrbiz.balsa.engine.SessionEngine;
import com.intrbiz.balsa.engine.security.AuthenticationResponse;
import com.intrbiz.balsa.engine.security.credentials.PasswordCredentials;
import com.intrbiz.balsa.error.BalsaSecurityException;
import com.intrbiz.balsa.listener.ssh.util.SSHUtil;
import com.sshtools.common.auth.PasswordAuthenticationProvider;
import com.sshtools.common.auth.PasswordChangeException;
import com.sshtools.common.ssh.SshConnection;

public class BalsaPasswordAuthenticator extends PasswordAuthenticationProvider
{
    private static final Logger logger = LoggerFactory.getLogger(BalsaPasswordAuthenticator.class);
    
    private final SecurityEngine securityEngine;
    
    private final SessionEngine sessionEngine;

    public BalsaPasswordAuthenticator(SecurityEngine securityEngine, SessionEngine sessionEngine)
    {
        super();
        this.securityEngine = Objects.requireNonNull(securityEngine);
        this.sessionEngine = Objects.requireNonNull(sessionEngine);
    }
    
    public String getName()
    {
        return "Balsa-Password-Authenticator";
    }

    @Override
    public boolean verifyPassword(SshConnection con, String username, String password) throws PasswordChangeException, IOException
    {
        try
        {
            logger.info("Authenticating session: {} for user: {}", con.getSessionId(), username);
            AuthenticationResponse response = this.securityEngine.authenticate(null, PasswordCredentials.of(username, password), true);
            if (response != null && response.isComplete())
            {
                // create the corresponding balsa session with the principal
                this.sessionEngine.getSession(SSHUtil.sessionId(con)).authenticationState().update(response);
                // return auth ok
                return true;
            }
        }
        catch (BalsaSecurityException e)
        {
            logger.error("Failed to authenticate session: " + con.getSessionId(), e);
        }
        return false;
    }

    @Override
    public boolean changePassword(SshConnection con, String username, String oldpassword, String newpassword) throws PasswordChangeException, IOException
    {
        throw new UnsupportedOperationException();
    }
}
