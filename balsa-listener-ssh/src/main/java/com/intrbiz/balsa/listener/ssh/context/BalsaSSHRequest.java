package com.intrbiz.balsa.listener.ssh.context;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.dataformat.yaml.YAMLParser;
import com.intrbiz.balsa.engine.session.BalsaSession;
import com.intrbiz.balsa.listener.BalsaRequest;
import com.intrbiz.balsa.scgi.parameter.Parameter;
import com.intrbiz.balsa.scgi.util.CookieSet;
import com.intrbiz.balsa.scgi.util.ParameterSet;

public class BalsaSSHRequest implements BalsaRequest, ParameterSet, CookieSet
{
    
    private String method;
    
    private String path;
    
    private Map<String, Parameter> parameters = new LinkedHashMap<>();
    
    private InputStream input;
    
    private Reader reader;
    
    private Object body;
    
    public BalsaSSHRequest(String method, String path)
    {
        super();
        this.method = method;
        this.path = path;
    }
    
    public BalsaSSHRequest(String method, String path, InputStream input)
    {
        super();
        this.method = method;
        this.path = path;
        this.input = input;
    }
    
    public BalsaSSHRequest(String method, String path, List<Parameter> params)
    {
        this(method, path);
        if (params != null)
        {
            for (Parameter param : params)
            {
                this.parameters.put(param.getName(), param);
            }
        }
    }

    @Override
    public void variable(String name, String value)
    {
    }

    @Override
    public int getContentLength()
    {
        return 0;
    }

    @Override
    public String getContentType()
    {
        return "application/octet-stream";
    }

    @Override
    public boolean isXml()
    {
        return false;
    }

    @Override
    public boolean isJson()
    {
        return false;
    }

    @Override
    public boolean isYaml()
    {
        return false;
    }

    @Override
    public String getVersion()
    {
        return null;
    }

    @Override
    public String getServerSoftware()
    {
        return "Balsa-SSH";
    }

    @Override
    public String getServerName()
    {
        return null;
    }

    @Override
    public String getServerAddress()
    {
        return null;
    }

    @Override
    public int getServerPort()
    {
        return 22;
    }

    @Override
    public String getServerProtocol()
    {
        return "SSH";
    }

    @Override
    public String getRemoteAddress()
    {
        return null;
    }

    @Override
    public int getRemotePort()
    {
        return 0;
    }

    @Override
    public String getRequestMethod()
    {
        return this.method;
    }

    @Override
    public String getRequestScheme()
    {
        return "SSH";
    }

    @Override
    public boolean isSecure()
    {
        return true;
    }

    @Override
    public String getRequestUri()
    {
        return this.path;
    }

    @Override
    public String getPathInfo()
    {
        return this.path;
    }

    @Override
    public String getQueryString()
    {
        return "";
    }

    @Override
    public String getScriptName()
    {
        return null;
    }

    @Override
    public String getScriptFileName()
    {
        return null;
    }

    @Override
    public String getDocumentRoot()
    {
        return null;
    }

    @Override
    public Map<String, String> getHeaders()
    {
        return Collections.emptyMap();
    }

    @Override
    public String getHeader(String name)
    {
        return null;
    }

    @Override
    public Set<String> getHeaderNames()
    {
        return Collections.emptySet();
    }

    @Override
    public Map<String, String> getVariables()
    {
        return Collections.emptyMap();
    }

    @Override
    public String getVariable(String name)
    {
        return null;
    }

    @Override
    public Set<String> getVariableNames()
    {
        return Collections.emptySet();
    }

    @Override
    public Map<String, Parameter> getParameters()
    {
        return this.parameters;
    }

    @Override
    public Parameter getParameter(String name)
    {
        return this.parameters.get(name);
    }

    @Override
    public void addParameter(Parameter parameter)
    {
        this.parameters.put(parameter.getName(), parameter);
    }

    @Override
    public void removeParameter(String name)
    {
        this.parameters.remove(name);
    }

    @Override
    public Set<String> getParameterNames()
    {
        return this.parameters.keySet();
    }

    @Override
    public Collection<Parameter> getParameterValues()
    {
        return this.parameters.values();
    }

    @Override
    public boolean containsParameter(String name)
    {
        return this.parameters.containsKey(name);
    }

    @Override
    public String cookie(String name)
    {
        return null;
    }

    @Override
    public void cookie(String name, String value)
    {
    }

    @Override
    public Map<String, String> cookies()
    {
        return Collections.emptyMap();
    }

    @Override
    public Set<String> cookieNames()
    {
        return Set.of(BalsaSession.COOKIE_NAME);
    }

    @Override
    public void removeCookie(String name)
    {
    }

    @Override
    public InputStream getInput()
    {
        return this.input;
    }

    @Override
    public Reader getReader()
    {
        if (this.reader == null && this.input != null)
        {
            this.reader = new InputStreamReader(this.input);
        }
        return this.reader;
    }

    @Override
    public JsonParser getJsonReader() throws IOException
    {
        return null;
    }

    @Override
    public YAMLParser getYamlReader() throws IOException
    {
        return null;
    }

    @Override
    public XMLStreamReader getXMLReader() throws IOException, XMLStreamException
    {
        return null;
    }

    @Override
    public Object getBody()
    {
        return this.body;
    }

    @Override
    public void setBody(Object body)
    {
        this.body = body;
    }

    @Override
    public String dump()
    {
        return "BalsaSSHVFSRequest(" + this.method + " " + this.path + ")";
    }
}
