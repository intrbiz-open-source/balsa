package com.intrbiz.balsa.listener.ssh.vfs.entry;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.intrbiz.balsa.engine.impl.route.RouteEntry;
import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;
import com.intrbiz.balsa.metadata.SSHVFS;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileRandomAccess;
import com.sshtools.common.permissions.PermissionDeniedException;

public class BalsaVFSDynamicFile extends BalsaVFSFile
{
    private final Map<String, RouteEntry> routes = new HashMap<>();
    
    public BalsaVFSDynamicFile(BalsaVFS vfs, BalsaVFSEntry parent, String name, RouteEntry... routes)
    {
        super(vfs, parent, name);
        this.addRoutes(routes);
    }
    
    public BalsaVFSDynamicFile(BalsaVFS vfs, String name, RouteEntry... routes)
    {
        super(vfs, name);
        this.addRoutes(routes);
    }
    
    @Override
    public boolean isDynamic()
    {
        return true;
    }
    
    @Override
    public boolean isReadable()
    {
        return this.routes.containsKey(SSHVFS.Method.READ) || this.routes.containsKey("GET") || this.routes.containsKey("ANY");
    }

    @Override
    public boolean isWritable()
    {
        return this.routes.containsKey(SSHVFS.Method.WRITE) || this.routes.containsKey("PUT") || this.routes.containsKey("POST");
    }

    public void addRoutes(RouteEntry... routes)
    {
        if (routes != null)
        {
            for (RouteEntry route : routes)
            {
                this.addRoute(route);
            }
        }
    }
    
    public void addRoute(RouteEntry route)
    {
        this.routes.put(route.getRoute().getMethod().toString(), route);
    }
    
    public Map<String, RouteEntry> getRoutes()
    {
        return Collections.unmodifiableMap(this.routes);
    }
    
    /**
     * Find the first route with the given method
     * @param methods the list of methods to search for
     * @return the first route found otherwise null
     */
    public RouteEntry getRoute(String... methods)
    {
        for (String method : methods)
        {
            RouteEntry entry = this.routes.get(method);
            if (entry != null)
                return entry;
        }
        return null;
    }
    
    @Override
    public long length() throws IOException, PermissionDeniedException
    {
        return 0;
    }

    @Override
    public void truncate() throws IOException, PermissionDeniedException
    {
        throw new PermissionDeniedException("Cannot write to a virtual file");
    }
    
    @Override
    public InputStream getInputStream() throws IOException, PermissionDeniedException
    {
        throw new PermissionDeniedException("Cannot read from a virtual file");
    }

    @Override
    public OutputStream getOutputStream() throws IOException, PermissionDeniedException
    {
        throw new PermissionDeniedException("Cannot write to a virtual file");
    }

    @Override
    public boolean createFolder() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public void copyFrom(AbstractFile src) throws IOException, PermissionDeniedException
    {
    }

    @Override
    public void moveTo(AbstractFile target) throws IOException, PermissionDeniedException
    {
    }

    @Override
    public boolean delete(boolean recursive) throws IOException, PermissionDeniedException
    {
        return false;
    }

    @Override
    public boolean createNewFile() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public boolean supportsRandomAccess()
    {
        return false;
    }

    @Override
    public AbstractFileRandomAccess openFile(boolean writeAccess) throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public OutputStream getOutputStream(boolean append) throws IOException, PermissionDeniedException
    {
        throw new PermissionDeniedException("Cannot write to a virtual file");
    }
}
