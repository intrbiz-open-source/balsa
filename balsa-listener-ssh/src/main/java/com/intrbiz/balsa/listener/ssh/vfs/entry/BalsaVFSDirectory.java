package com.intrbiz.balsa.listener.ssh.vfs.entry;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.intrbiz.balsa.BalsaException;
import com.intrbiz.balsa.listener.ssh.vfs.BalsaVFS;
import com.intrbiz.balsa.listener.ssh.vfs.VFSUtil;
import com.sshtools.common.files.AbstractFile;
import com.sshtools.common.files.AbstractFileRandomAccess;
import com.sshtools.common.permissions.PermissionDeniedException;

public abstract class BalsaVFSDirectory extends BalsaVFSEntry
{
    private final LinkedHashMap<String, BalsaVFSEntry> children = new LinkedHashMap<>();
    
    public BalsaVFSDirectory(BalsaVFS vfs, BalsaVFSEntry parent, String name)
    {
        super(vfs, parent, name);
    }
    
    public BalsaVFSDirectory(BalsaVFS vfs, String name)
    {
        super(vfs, name);
    }
    
    @Override
    public boolean isDirectory()
    {
        return true;
    }
    
    @Override
    public boolean isFile()
    {
        return false;
    }
    
    @Override
    public List<AbstractFile> getChildren()
    {
        return new ArrayList<>(this.children.values());
    }
    
    public Map<String, BalsaVFSEntry> getChildEntries()
    {
        return this.children;
    }
    
    public boolean hasChild(String name)
    {
        return this.children.containsKey(name);
    }
    
    public <T extends BalsaVFSEntry> T addChild(T child)
    {
        // check for duplicates
        if (this.children.containsKey(child.getName()))
            throw new BalsaException("A child with the name " + child.getName() + " already exists under " + this.getAbsolutePath());
        // add
        child.setParent(this);
        this.children.put(child.getName(), child);
        return child;
    }
    
    public <T extends BalsaVFSEntry> T replaceChild(T child)
    {
        child.setParent(this);
        this.children.put(child.getName(), child);
        return child;
    }
    
    public void removeChild(String name)
    {
        this.children.remove(name);
    }
    
    public void clearChildren()
    {
        this.children.clear();
    }

    @Override
    public boolean createFolder() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public boolean delete(boolean recursive) throws IOException, PermissionDeniedException
    {
        return false;
    }

    @Override
    public final AbstractFile resolveFile(String child) throws IOException, PermissionDeniedException
    {
        return this.getVFS().getFile(
                child.startsWith("/") ? 
                    child : 
                    VFSUtil.stripTrailingSlash(this.getAbsolutePath() + "/" + child)
        );
    }

    @Override
    public InputStream getInputStream() throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public OutputStream getOutputStream() throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public void copyFrom(AbstractFile src) throws IOException, PermissionDeniedException
    {
    }

    @Override
    public void moveTo(AbstractFile target) throws IOException, PermissionDeniedException
    {
    }

    @Override
    public long length() throws IOException, PermissionDeniedException
    {
        return 0;
    }

    @Override
    public boolean createNewFile() throws PermissionDeniedException, IOException
    {
        return false;
    }

    @Override
    public void truncate() throws PermissionDeniedException, IOException
    {
    }

    @Override
    public boolean supportsRandomAccess()
    {
        return false;
    }

    @Override
    public AbstractFileRandomAccess openFile(boolean writeAccess) throws IOException, PermissionDeniedException
    {
        return null;
    }

    @Override
    public OutputStream getOutputStream(boolean append) throws IOException, PermissionDeniedException
    {
        return null;
    }
}
