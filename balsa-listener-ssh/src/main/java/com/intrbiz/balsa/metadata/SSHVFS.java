package com.intrbiz.balsa.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Expose this route via the SSH VFS
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SSHVFS {
    
    /**
     * SSH Specific methods
     */
    public static interface Method
    {
        public static final String READ  = "READ";
        public static final String LIST  = "LIST";
        public static final String WRITE = "WRITE";
    }
    
    public enum Type { DIRECTORY, FILE, AUTO }
    
    /**
     * How to expose this route
     * @return
     */
    Type type() default Type.AUTO;
    
    /**
     * An optional value expression used to extract the file name of a directory child
     */
    String expression() default "";
}
