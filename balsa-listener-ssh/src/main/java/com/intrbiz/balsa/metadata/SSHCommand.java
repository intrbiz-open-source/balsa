package com.intrbiz.balsa.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface SSHCommand {
    
    /**
     * SSH Specific methods
     */
    public static interface Method
    {
        public static final String SHELL   = "SHELL";
        public static final String COMMAND = "COMMAND";
    }
    
    String[] command() default {};
    
    String subSystem() default "";
    
    String description() default "";

    String commandDescription() default "";
}
