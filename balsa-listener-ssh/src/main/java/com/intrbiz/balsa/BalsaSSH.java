package com.intrbiz.balsa;

import com.intrbiz.balsa.listener.ssh.BalsaSSHListener;

public class BalsaSSH implements BalsaPlugin
{
    public BalsaSSH()
    {
        super();
    }
    
    @Override
    public void setup(BalsaApplication app) throws Exception
    {
        // register the SSH Listener
        app.listener(new BalsaSSHListener());
    }
}
