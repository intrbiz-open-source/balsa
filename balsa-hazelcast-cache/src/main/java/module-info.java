module com.intrbiz.balsa.hazelcast.cache {
    
    requires com.intrbiz.balsa.util;
    requires transitive com.intrbiz.balsa.data.manager;
    
    requires transitive com.hazelcast.core;
    
    exports com.intrbiz.hazelcast.data.cache;
    
}
