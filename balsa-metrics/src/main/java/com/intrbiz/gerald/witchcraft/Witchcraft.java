package com.intrbiz.gerald.witchcraft;

import java.util.Objects;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.MetricRegistry.MetricSupplier;


/**
 * Witchcraft is a collection of intelligence sources which should be 
 * exposed by Gerald
 */
public final class Witchcraft
{    
    public static final WitchcraftRegistry get()
    {
        return WitchcraftRegistry.get();
    }
    
    private Witchcraft()
    {
    }
    
    public static class Scope
    {
        private final String scope;
        
        public Scope(String scope)
        {
            this.scope = Objects.requireNonNull(scope);
        }
        
        public String scope()
        {
            return this.scope;
        }
        
        public String toString()
        {
            return "[" + this.scope + "]";
        }
    }
    
    public static Scope scope(String scope)
    {
        return new Scope(scope);
    }
    
    public static String scoped(Scope scope, String name, String... names)
    {
        return name(name, names) + "." + scope.toString();
    }

    public static String scoped(Class<?> klass, String name, String scope)
    {
        return name(klass, name) + ".[" + scope + "]";
    }
    
    public static String name(Class<?> klass, Scope scope, String name)
    {
        return name(klass, name) + "." + scope.toString();
    }
    
    public static String name(Class<?> klass, Scope scope, String... names)
    {
        return name(klass, names) + "." + scope.toString();
    }
    
    public static String name(Class<?> klass, String... names)
    {
        return MetricRegistry.name(klass, names);
    }
    
    public static String name(String name, String... names)
    {
        return MetricRegistry.name(name, names);
    }

    public static Counter counter(String name)
    {
        return get().counter(name);
    }
    
    public static Counter counter(Class<?> klass, String... name)
    {
        return get().counter(klass, name);
    }
    
    public static Counter counter(Class<?> klass, Scope scope, String... name)
    {
        return get().counter(klass, scope, name);
    }

    public static Counter counter(String name, MetricSupplier<Counter> supplier)
    {
        return get().counter(name, supplier);
    }

    public static Histogram histogram(String name)
    {
        return get().histogram(name);
    }
    
    public static Histogram histogram(Class<?> klass, String... name)
    {
        return get().histogram(klass, name);
    }
    
    public static Histogram histogram(Class<?> klass, Scope scope, String... name)
    {
        return get().histogram(klass, scope, name);
    }

    public static Histogram histogram(String name, MetricSupplier<Histogram> supplier)
    {
        return get().histogram(name, supplier);
    }

    public static Meter meter(String name)
    {
        return get().meter(name);
    }
    
    public static Meter meter(Class<?> klass, String... name)
    {
        return get().meter(klass, name);
    }
    
    public static Meter meter(Class<?> klass, Scope scope, String... name)
    {
        return get().meter(klass, scope, name);
    }

    public static Meter meter(String name, MetricSupplier<Meter> supplier)
    {
        return get().meter(name, supplier);
    }

    @SuppressWarnings("rawtypes")
    public static <T extends Gauge> T gauge(String name)
    {
        return get().gauge(name);
    }
    
    @SuppressWarnings("rawtypes")
    public static <T extends Gauge> T gauge(Class<?> klass, String... name)
    {
        return get().gauge(klass, name);
    }
    
    @SuppressWarnings("rawtypes")
    public static <T extends Gauge> T gauge(Class<?> klass, Scope scope, String... name)
    {
        return get().gauge(klass, scope, name);
    }

    @SuppressWarnings("rawtypes")
    public static <T extends Gauge> T gauge(String name, MetricSupplier<T> supplier)
    {
        return get().gauge(name, supplier);
    }
    
    public static Timer timer(String name)
    {
        return get().timer(name);
    }
    
    public static Timer timer(Class<?> klass, String... name)
    {
        return get().timer(Witchcraft.name(klass, name));
    }
    
    public static Timer timer(Class<?> klass, Scope scope, String... name)
    {
        return get().timer(Witchcraft.name(klass, scope, name));
    }

    public static Timer timer(String name, MetricSupplier<Timer> supplier)
    {
        return get().timer(name, supplier);
    }
}
