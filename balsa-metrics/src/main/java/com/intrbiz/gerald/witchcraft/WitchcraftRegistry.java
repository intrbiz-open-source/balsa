package com.intrbiz.gerald.witchcraft;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.MetricRegistry.MetricSupplier;
import com.codahale.metrics.Timer;
import com.intrbiz.gerald.witchcraft.Witchcraft.Scope;

public final class WitchcraftRegistry
{
    private static final WitchcraftRegistry US = new WitchcraftRegistry();
    
    public static final WitchcraftRegistry get()
    {
        return US;
    }
    
    private final MetricRegistry registry = new MetricRegistry();
    
    private WitchcraftRegistry()
    {
        super();
    }
    
    public MetricRegistry getRegistry()
    {
        return this.registry;
    }
    
    public void clear()
    {
        // remove all metrics
        this.registry.removeMatching((e,v) -> true);
    }
    
    // delegates
    
    public Counter counter(String name)
    {
        return registry.counter(name);
    }
    
    public Counter counter(Class<?> klass, String... name)
    {
        return registry.counter(Witchcraft.name(klass, name));
    }
    
    public Counter counter(Class<?> klass, Scope scope, String... name)
    {
        return registry.counter(Witchcraft.name(klass, scope, name));
    }

    public Counter counter(String name, MetricSupplier<Counter> supplier)
    {
        return registry.counter(name, supplier);
    }

    public Histogram histogram(String name)
    {
        return registry.histogram(name);
    }
    
    public Histogram histogram(Class<?> klass, String... name)
    {
        return registry.histogram(Witchcraft.name(klass, name));
    }
    
    public Histogram histogram(Class<?> klass, Scope scope, String... name)
    {
        return registry.histogram(Witchcraft.name(klass, scope, name));
    }

    public Histogram histogram(String name, MetricSupplier<Histogram> supplier)
    {
        return registry.histogram(name, supplier);
    }

    public Meter meter(String name)
    {
        return registry.meter(name);
    }
    
    public Meter meter(Class<?> klass, String... name)
    {
        return registry.meter(Witchcraft.name(klass, name));
    }
    
    public Meter meter(Class<?> klass, Scope scope, String... name)
    {
        return registry.meter(Witchcraft.name(klass, scope, name));
    }

    public Meter meter(String name, MetricSupplier<Meter> supplier)
    {
        return registry.meter(name, supplier);
    }

    @SuppressWarnings("rawtypes")
    public <T extends Gauge> T gauge(String name)
    {
        return registry.gauge(name);
    }
    
    @SuppressWarnings("rawtypes")
    public <T extends Gauge> T gauge(Class<?> klass, String... name)
    {
        return registry.gauge(Witchcraft.name(klass, name));
    }
    
    @SuppressWarnings("rawtypes")
    public <T extends Gauge> T gauge(Class<?> klass, Scope scope, String... name)
    {
        return registry.gauge(Witchcraft.name(klass, scope, name));
    }

    @SuppressWarnings("rawtypes")
    public <T extends Gauge> T gauge(String name, MetricSupplier<T> supplier)
    {
        return registry.gauge(name, supplier);
    }

    public Timer timer(String name)
    {
        return registry.timer(name);
    }
    
    public Timer timer(Class<?> klass, String... name)
    {
        return registry.timer(Witchcraft.name(klass, name));
    }
    
    public Timer timer(Class<?> klass, Scope scope, String... name)
    {
        return registry.timer(Witchcraft.name(klass, scope, name));
    }

    public Timer timer(String name, MetricSupplier<Timer> supplier)
    {
        return registry.timer(name, supplier);
    }

}
