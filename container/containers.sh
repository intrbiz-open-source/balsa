#!/bin/sh

set -ex

## Base
docker build --pull --no-cache --target balsa-base -t registry.gitlab.com/intrbiz-open-source/balsa/base:latest .
docker push registry.gitlab.com/intrbiz-open-source/balsa/base:latest

## Base build
docker build --target balsa-base-build -t registry.gitlab.com/intrbiz-open-source/balsa/base-build:latest .
docker push registry.gitlab.com/intrbiz-open-source/balsa/base-build:latest

## Base web image
docker build --target balsa-base-web -t registry.gitlab.com/intrbiz-open-source/balsa/base-web:latest .
docker push registry.gitlab.com/intrbiz-open-source/balsa/base-web:latest
