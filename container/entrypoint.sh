#!/bin/sh

# Startup pause to work around slow shutdown
DELAY=${STARTUP_DELAY:-0}
sleep $DELAY

# Startup Balsa Application in the background
echo "Starting Balsa Application"
java -jar /balsa/*.app & disown

# configure nginx
echo "Setting HTTP Port to $HTTP_PORT"
sed -i "s/HTTP_PORT/$HTTP_PORT/" /etc/nginx/nginx.conf
echo "Setting SCGI Port to $BALSA_SCGI_PORT"
sed -i "s/BALSA_SCGI_PORT/$BALSA_SCGI_PORT/" /etc/nginx/nginx.conf
echo "Setting Max upload size to $UPLOAD_LIMIT"
sed -i "s/UPLOAD_LIMIT/$UPLOAD_LIMIT/" /etc/nginx/nginx.conf


# Exec nginx
echo "Starting nginx"
exec /usr/sbin/nginx -g "daemon off;" -c /etc/nginx/nginx.conf
