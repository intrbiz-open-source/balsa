module com.intrbiz.balsa.scgi {
    
    requires org.slf4j;
    requires com.codahale.metrics;
    requires com.intrbiz.balsa.metrics;
    
    exports com.intrbiz.balsa.scgi;
    exports com.intrbiz.balsa.scgi.http;
    exports com.intrbiz.balsa.scgi.parameter;
    exports com.intrbiz.balsa.scgi.middleware;
    exports com.intrbiz.balsa.scgi.util;
    
}