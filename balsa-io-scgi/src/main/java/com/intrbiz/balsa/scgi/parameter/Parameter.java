/*
 * Balsa SCGI
 * Copyright (c) 2012, Chris Ellis
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 */

package com.intrbiz.balsa.scgi.parameter;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

/**
 * A request parameter
 */
public interface Parameter extends Comparable<Parameter>
{   
    String getName();
    
    int getIndex();
    
    Object getValue();
    
    /* Helpers for strings */
    String getStringValue();
    
    /* Helpers for lists */
    List<Parameter> getListValue();
    
    default List<String> getStringListValue()
    {
        return Collections.singletonList(this.getStringValue());
    }
    
    default Parameter getListValue(int index)
    {
        return (this.getIndex() == index) ? this : null;
    }
    
    default String getStringListValue(int index)
    {
        return (this.getIndex() == index) ? this.getStringValue() : null;
    }
    
    int getLength();
    
    /* Helpers for files */
    default byte[] getBytesValue()
    {
        String value = this.getStringValue();
        return value == null ? null : value.getBytes(StandardCharsets.UTF_8);
    }
    
    default InputStream getInputStream()
    {
        byte[] data = this.getBytesValue();
        return data == null ? null : new ByteArrayInputStream(data);
    }
    
    boolean isEmpty();
    
    void close();
    
    /**
     * Merge another parameter into this, creating a list parameter wrapping both
     * @param other the parameter to merge in
     * @return the merged parameter
     */
    default Parameter merge(Parameter other)
    {
        return new ListParameter(this.getName(), this, other);
    }
}
