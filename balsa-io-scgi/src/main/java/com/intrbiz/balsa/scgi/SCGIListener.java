/*
 * Balsa SCGI
 * Copyright (c) 2012, Chris Ellis
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 */

package com.intrbiz.balsa.scgi;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.StandardSocketOptions;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Timer;
import com.intrbiz.gerald.witchcraft.Witchcraft;

/**
 * A simple 'pre-forked' SCGI server
 * 
 * Note: This is based on the Balsa SCGI implementation, modified to be more standalone.
 * 
 */
public class SCGIListener implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(SCGIListener.class);
    
    public static final int DEFAULT_PORT = 8090;

    public static final int DEFAULT_POOL_SIZE = 16;

    private int port = DEFAULT_PORT;

    private int poolSize = DEFAULT_POOL_SIZE;
    
    private boolean reusePort = false;

    protected SCGIProcessor processor;

    private volatile boolean run = false;

    private ServerSocket server;
    
    private Thread listenerThread;

    private ThreadFactory workerFactory;

    private SCGIWorker[] workers;

    private BlockingQueue<Socket> runQueue;
    
    private final Counter accepts;
    
    private final Timer requestDuration;
    
    private final Timer requestHeaderParseDuration;
    
    private final Timer requestProcessDuration;

    public SCGIListener()
    {
        super();
        // setup the metrics
        this.accepts                    = Witchcraft.counter(SCGIListener.class, "accepts");
        this.requestDuration            = Witchcraft.timer(SCGIListener.class, "request-duration");
        this.requestHeaderParseDuration = Witchcraft.timer(SCGIListener.class, "request-header-parse-duration");
        this.requestProcessDuration     = Witchcraft.timer(SCGIListener.class, "request-process-duration");
    }

    public SCGIListener(int port)
    {
        this();
        this.port = port;
    }

    public SCGIListener(int port, int poolSize)
    {
        this();
        this.port = port;
        this.poolSize = poolSize;
    }
    
    public SCGIListener(int port, int poolSize, SCGIProcessor processor)
    {
        this();
        this.port = port;
        this.poolSize = poolSize;
        this.processor = processor;
    }

    public int getPort()
    {
        return port;
    }

    /**
     * The port to listen on
     * @param port
     * returns void
     */
    public void setPort(int port)
    {
        this.port = port;
    }

    public int getPoolSize()
    {
        return poolSize;
    }

    /**
     * The number of worker threads
     * @param poolSize
     * returns void
     */
    public void setPoolSize(int poolSize)
    {
        this.poolSize = poolSize;
    }

    public boolean isReusePort()
    {
        return this.reusePort;
    }

    public void setReusePort(boolean reusePort)
    {
        this.reusePort = reusePort;
    }
    
    public SCGIListener withReusePort(boolean reusePort)
    {
        this.reusePort = reusePort;
        return this;
    }

    public SCGIProcessor getProcessor()
    {
        return processor;
    }

    /**
     * The processor to handler the requests
     * @param processor
     * returns void
     */
    public void setProcessor(SCGIProcessor processor)
    {
        this.processor = processor;
    }

    boolean isRun()
    {
        return this.run;
    }

    /**
     * Start the SCGI Listener
     * 
     * This will create a bind a socket, create a thread and start listening for requests.
     * 
     * @throws SCGIException
     * returns void
     */
    public void start() throws SCGIException
    {
        try
        {
            this.listenerThread = new Thread(new ThreadGroup("SCGI"), this, "SCGIListener");
            // factory
            this.workerFactory = new ThreadFactory() {
                
                private int count = 0;
                private ThreadGroup group = new ThreadGroup("SCGI");
                
                public Thread newThread(Runnable r)
                {
                    return new Thread(this.group, r, "SCGIWorker-" + (++count));
                }
            };
            // runqueue
            this.runQueue = new LinkedBlockingQueue<Socket>(this.getPoolSize() * 2);
            // prefork
            this.run = true;
            this.workers = new SCGIWorker[this.getPoolSize()];
            for (int i = 0; i < this.workers.length; i++)
            {
                this.workers[i] = new SCGIWorker(this, this.runQueue, this.workerFactory, this.requestDuration, this.requestHeaderParseDuration, this.requestProcessDuration);
                this.workers[i].start();
            }
            // listen
            this.server = new ServerSocket(this.getPort());
            this.server.setSoTimeout(20000);
            if (this.reusePort) this.server.setOption(StandardSocketOptions.SO_REUSEPORT, true);
            // start the listener
            this.listenerThread.start();
        }
        catch (BindException e)
        {
            throw new SCGIException("Failed to start SCGI Listener, could not bind to socket", e);
        }
        catch (IOException e)
        {
            throw new SCGIException("Failed to start SCGI Listener", e);
        }
    }

    public void run()
    {
        while (this.run)
        {
            try
            {
                // Place the socket onto the run queue
                Socket client = this.server.accept();
                this.accepts.inc();
                this.runQueue.offer(client);
            }
            catch (SocketTimeoutException e)
            {
                /* expected */
            }
            catch (IOException e)
            {
                logger.error("Error during listener run loop", e);
            }
        }
    }

    public void shutdown()
    {
        this.stop();
        logger.info("Shutting down workers");
        if (this.workers != null)
        {
            for (SCGIWorker worker : this.workers)
            {
                worker.await();
            }
        }
    }

    public void stop()
    {
        logger.info("Stopping listener");
        if (this.server != null)
        {
            // shutdown socket
            try
            {
                this.server.close();
            }
            catch (Exception e)
            {
            }
            // stop processing
            this.run = false;
            // notify workers
            try
            {
                this.runQueue.notifyAll();
            }
            catch (Exception e)
            {
            }
        }
    }
}
