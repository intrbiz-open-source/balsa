package com.intrbiz.balsa.scgi.util;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intrbiz.balsa.scgi.internal.util.Util;
import com.intrbiz.balsa.scgi.parameter.ListParameter;
import com.intrbiz.balsa.scgi.parameter.Parameter;
import com.intrbiz.balsa.scgi.parameter.StringParameter;

/**
 * Parse HTTP query strings
 */
public final class QueryStringParser
{
    private static Logger logger = LoggerFactory.getLogger(QueryStringParser.class);
    
    /**
     * Parse the given query string
     * @param query
     * @param request
     * returns void
     */
    public final static void parseQueryString(String query, ParameterSet request)
    {
        if (!Util.isEmpty(query))
        {
            int spos = 0, pos = 0;
            while ((pos = query.indexOf("&", spos)) != -1)
            {
                parseParameter(query.substring(spos, pos), request);
                spos = pos + 1;
            }
            parseParameter(query.substring(spos), request);
        }
    }

    /**
     * Parse the given parameter
     * @param parameter
     * @param request
     * returns void
     */
    public final static void parseParameter(String rawParameter, ParameterSet request)
    {
        // split the parameter
        int pos = rawParameter.indexOf("=");
        if (pos != -1)
        {
            // TODO: Charset
            String name  = URLDecoder.decode(rawParameter.substring(0, pos), StandardCharsets.UTF_8);
            String value = URLDecoder.decode(rawParameter.substring(pos + 1), StandardCharsets.UTF_8);
            // parse the parameter
            Parameter parameter = parseParameter(name, value, StringParameter::new);
            // merge the parameter into the set
            request.mergeParameter(parameter);
        }
    }
    
    /**
     * Construct a parameter of the given type from the name, value pair.
     * 
     * This will handling parsing parameter with indexed names ('name[index]'), 
     * and promoting to a list when needed.
     * 
     * @param <T>
     * @param name the parameter name
     * @param value the parameter value
     * @param supplier the factory to create the given parameter type
     * @return a parameter
     */
    public static <T> Parameter parseParameter(String name, T value, ParameterSupplier<T> supplier)
    {
        int index = -1;
        // handle array parameters in the form of name[index]
        // parameters ending in []
        if (name.endsWith("]"))
        {
            int sepIdx = name.lastIndexOf('[');
            if (sepIdx > 0)
            {
                try
                {
                    String strIndex = name.substring(sepIdx + 1, name.length() - 1);
                    if (! Util.isEmpty(strIndex))
                    {
                        index = Integer.parseInt(strIndex);
                    }
                    name = name.substring(0, sepIdx);
                }
                catch (NumberFormatException e)
                {
                    logger.debug("Failed to decode array parameter index, of '{}'", name);
                }
            }
        }
        // create the parameter
        Parameter parameter = supplier.create(name, index, value);
        // ensure any indexed parameters are wrapped in a list to support single item lists
        return (index == -1) ? parameter : new ListParameter(parameter);
    }
    
    @FunctionalInterface
    public static interface ParameterSupplier<T>
    {
        Parameter create(String name, int index, T value);
    }
}
