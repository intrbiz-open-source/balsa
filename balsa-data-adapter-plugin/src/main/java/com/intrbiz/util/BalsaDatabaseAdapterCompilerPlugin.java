package com.intrbiz.util;

/**
 * Compile database adapters at compile time rather than runtime
 * 
 * @goal balsa-data-adapter
 * @phase process-classes
 * @requiresProject
 * @requiresDependencyResolution runtime
 */
public class BalsaDatabaseAdapterCompilerPlugin extends DatabaseAdapterCompilerPlugin
{
    public BalsaDatabaseAdapterCompilerPlugin()
    {
        super();
    }
}
