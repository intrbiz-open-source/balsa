module com.intrbiz.balsa.data {
    
    requires com.intrbiz.balsa.core;
    requires transitive com.intrbiz.balsa.data.manager;
    
    exports com.intrbiz.balsa.data.metadata;
    
}
